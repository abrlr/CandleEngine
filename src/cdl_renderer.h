#ifndef __CDL_INCLUDE_CDL_RENDERER_H__
#define __CDL_INCLUDE_CDL_RENDERER_H__

//- Forward declarations

// Structs
struct cdl_renderer_texture;
struct cdl_renderer_light;
struct cdl_renderer_shader;
struct cdl_renderer_material;
struct cdl_renderer_environment;
struct cdl_renderer_camera;
struct cdl_renderer_api_framebuffer;
struct cdl_renderer_framebuffer;
struct cdl_renderer_model_transform;
struct cdl_renderer_model_instance_data;
struct cdl_renderer_model_instance_list;
struct cdl_renderer_batch_vertex;
struct cdl_renderer_batch_entry;
struct cdl_renderer_line_batch;
struct cdl_renderer_triangle_batch;
struct cdl_renderer_font;
struct cdl_render_state_clip_rect;
struct cdl_render_state;
struct cdl_render_state_vars;
struct cdl_render_command;
struct cdl_render_queue;
struct cdl_render_scene;
struct cdl_renderer_effect;
struct cdl_renderer_post_processing;
struct cdl_renderer_scene_view;
struct cdl_renderer_storage;
struct cdl_renderer;

// Typedefs
typedef u32 RendererShaderType;
typedef u32 CameraType;
typedef u32 BatchVertexFlags;
typedef u32 BatchUIVertexFlags;
typedef u32 RenderStateVarType;
typedef u32 RenderCommand;
typedef u32 RenderSceneType;

//-

//- [SECTION] Misc
internal v2f pixel_to_gl_size(v2i pixelSize);
internal v2f pixel_to_gl_coordinates(v2i pixelCoordinates);

internal v2f pixelf_to_gl_size(v2f pixelSize);
internal v2f pixelf_to_gl_coordinates(v2f pixelCoordinates);


//- [SECTION] Colors
internal color8 cdl_rgb(u8 r, u8 g, u8 b);
internal color8 cdl_rgba(u8 r, u8 g, u8 b, u8 a);
internal color8 cdl_rgba_from_v4f_new(v4f color);
internal color8 cdl_rgba_lerp(color8 target, color8 source, f32 factor);
internal v4f cdl_rgba_adjust_hue(color8 rgbColor, f32 hueAdjust);
internal color8 cdl_rgb_desaturate(color8 color, f32 desaturation);
internal color8 cdl_hex(u32 hexValue);

//- [SECTION] Textures
introspect("Renderer")
struct cdl_renderer_texture {
    cdl_assets_texture* asset;
    bool8 loaded;
    u32 id;
};

#define INVALID_TEXTURE U32Max
#define RENDERER_MAX_TEXTURE_UNIT_PER_FRAME 8

internal cdl_renderer_texture* cdl_renderer_texture_new(cdl_assets_texture* asset);
internal void cdl_renderer_texture_load(cdl_renderer_texture* texture);
internal cdl_renderer_texture* cdl_renderer_texture_get(cdl_assets_texture* asset);
internal i32 cdl_renderer_texture_push(cdl_render_scene* s, cdl_renderer_texture* texture);

//- [SECTION] Lights 
introspect("Renderer")
struct cdl_renderer_light {
    v3f position;
    f32 intensity;
    color8 color;
};


//- [SECTION] Shaders
enum RendererShaderType_ {
    RendererShaderType_Invalid,
    
    RendererShaderType_World,
    RendererShaderType_UI,
    RendererShaderType_Text,
    
    RendererShaderType_FullScreenEffect,
    RendererShaderType_FullScreenQuad,
    
    RendererShaderType_Count
};

introspect("Renderer")
struct cdl_renderer_shader {
    cdl_assets_shader* asset;
    bool8 isValid;
    RendererShaderType type;
    u32 ID;
    bool8 lit;
};

internal cdl_renderer_shader* cdl_renderer_shader_new(RendererShaderType type, cdl_assets_shader* asset);
internal void cdl_renderer_shader_load(cdl_renderer_shader* shader);
internal void cdl_renderer_shader_use(cdl_renderer_shader* shader, cdl_renderer_scene_view* view);

internal void cdl_renderer_shader_uniform_set_uint(cdl_renderer_shader* shader, char* uniform, u32 value);

internal void cdl_renderer_shader_uniform_set_int(cdl_renderer_shader* shader, char* uniform, i32 value);
internal void cdl_renderer_shader_uniform_set_int2(cdl_renderer_shader* shader, char* uniform, i32 x, i32 y);
internal void cdl_renderer_shader_uniform_set_int3(cdl_renderer_shader* shader, char* uniform, i32 x, i32 y, i32 z);
internal void cdl_renderer_shader_uniform_set_int4(cdl_renderer_shader* shader, char* uniform, i32 x, i32 y, i32 z, i32 w);

internal void cdl_renderer_shader_uniform_set_float(cdl_renderer_shader* shader, char* uniform, f32 value);
internal void cdl_renderer_shader_uniform_set_float2(cdl_renderer_shader* shader, char* uniform, f32 x, f32 y);
internal void cdl_renderer_shader_uniform_set_float3(cdl_renderer_shader* shader, char* uniform, f32 x, f32 y, f32 z);
internal void cdl_renderer_shader_uniform_set_float4(cdl_renderer_shader* shader, char* uniform, f32 x, f32 y, f32 z, f32 w);

internal void cdl_renderer_shader_uniform_set_mat4(cdl_renderer_shader* shader, char* uniform, const m4f& mat);


//- [SECTION] Materials
introspect("Renderer")
struct cdl_renderer_material {
    cdl_renderer_shader* shader;
    color8 color;
    
    bool8 hasTransparency;
    bool8 wireframe;
    
    u32 textureUniform;
    cdl_renderer_texture* diffuse;
    cdl_renderer_texture* normal;
};

internal cdl_renderer_material* cdl_renderer_material_new(cdl_renderer_shader* shader);
internal void cdl_renderer_material_use(cdl_render_scene* s, cdl_renderer_material* material);


//- [SECTION] Scene Environment
introspect("Renderer")
struct cdl_renderer_environment {
    v3f ambientDirection;
    color8 ambientColor;
    f32 ambientIntensity;
};


//- [SECTION] Camera
enum CameraType_ {
    CameraType_None
        ,
    CameraType_Perspective,
    CameraType_Orthographic,
};

introspect("Renderer")
struct cdl_renderer_camera {
    CameraType type;
    
    // Settings
    union {
        // Perspective
        f32 fov;
        
        // Orthographic
        f32 zoom;
    };
    
    f32 farPlane;
    f32 nearPlane;
    
    v3f position;
    v3f rotation;
    m4f projection;
};

internal cdl_renderer_camera* cdl_renderer_camera_new(CameraType type);
internal void cdl_renderer_camera_update_projection(cdl_renderer_camera* camera);
internal m4f cdl_renderer_camera_get_view(cdl_renderer_camera* camera);


//- [SECTION] Framebuffers
introspect("Renderer")
struct cdl_renderer_api_framebuffer {
    bool8 valid;
    
    v2f dim;
    u8 samples;
    
    u32 id;
    u32 color;
    u32 depthStencil;
    
    // Debug Purpose
    cdl_renderer_texture colorTexture;
    cdl_renderer_texture depthStencilTexture;
};

#define INVALID_FRAMEBUFFER U32Max
internal cdl_renderer_api_framebuffer* cdl_renderer_internal_api_framebuffer_get_empty();
internal cdl_renderer_api_framebuffer* cdl_renderer_api_framebuffer_new();
internal cdl_renderer_api_framebuffer* cdl_renderer_api_framebuffer_multisampled_new(u8 samples);
internal void cdl_renderer_api_framebuffer_clear(cdl_renderer_api_framebuffer* framebuffer);


introspect("Renderer")
struct cdl_renderer_framebuffer {
    bool8 valid;
    u8 samples;
    
    // Transient Framebuffer : the one used to render to the screen
    cdl_renderer_api_framebuffer* transient;
    
    // Mutlisampled Framebuffer
    cdl_renderer_api_framebuffer* multisampled;
};

internal cdl_renderer_framebuffer* cdl_renderer_internal_framebuffer_get_empty();
internal cdl_renderer_framebuffer* cdl_renderer_framebuffer_new(u8 samples);
internal void cdl_renderer_framebuffer_render(cdl_renderer_framebuffer* framebuffer);
internal void cdl_renderer_framebuffer_use(cdl_renderer_framebuffer* framebuffer);
internal void cdl_renderer_framebuffer_clear(cdl_renderer_framebuffer* framebuffer);


//- [SECTION] Instanced Rendering
introspect("Renderer")
struct cdl_renderer_model_transform {
    v3f position;
    v3f rotation;
    f32 scale;
};

internal cdl_renderer_model_transform cdl_renderer_model_transform_new();

introspect("Renderer")
struct cdl_renderer_model_instance_data {
    m4f modelMatrix;
    color8 modelColor;
    u32 modelTextures; // 4 x 8 bits : diffuse | normals | ... | ...
};

introspect("Renderer")
struct cdl_renderer_model_instance_list {
    u32 id;
    bool8 clearEveryFrame;
    bool8 dirty;
    
    cdl_assets_model_raw* asset;
    cdl_renderer_material* material;
    u32 vao;
    bool8 modelLoaded;
    
    u32 dataVBO;
    cdl_renderer_model_instance_data* instanceData;
    
    u32 count;
    u32 capacity;
};

internal cdl_renderer_model_instance_list* cdl_renderer_instance_list_get(cdl_render_scene* s, cdl_assets_model_raw* model, cdl_renderer_material* material);
internal void cdl_renderer_create_model_instance_list(cdl_render_scene* s, cdl_assets_model_raw* model, cdl_renderer_material* material, bool8 clearEveryFrame);
internal void cdl_renderer_internal_load_model_from_list(cdl_renderer_model_instance_list* list);


//- [SECTION] Batch Rendering
enum BatchVertexFlags_ {
    BatchVertexFlags_None              = BIT(0)
        ,
    BatchVertexFlags_UseTexture        = BIT(1),
    BatchVertexFlags_UseModelMat       = BIT(2)
};

enum BatchUIVertexFlags_ {
    BatchUIVertexFlags_None           = BIT(0),
    
    BatchUIVertexFlags_UseTexture     = BIT(1),
    BatchUIVertexFlags_UseSDF         = BIT(2),
    BatchUIVertexFlags_UseModelMat    = BIT(3),
    
    BatchUIVertexFlags_ColorPicker    = BIT(4),
    
    BatchUIVertexFlags_TextBold       = BIT(5),
    
    BatchUIVertexFlags_RoundCorners   = BIT(6),
    BatchUIVertexFlags_Circle         = BIT(7)
};

introspect("Renderer")
struct cdl_renderer_batch_vertex {
    v3f position;
    v2f uvs;
    v3f normal;
    color8 color;
    
    union {
        BatchVertexFlags flags;
        BatchUIVertexFlags uiFlags;
    };
    
    v4f misc;
};

#define RENDERER_BATCH_MAX_ENTRIES 4096
introspect("Renderer")
struct cdl_renderer_batch_entry {
    u32 id;
    
    cdl_renderer_batch_vertex* vertexStart;
    u32 vertexCount;
    
    u32* indexStart;
    u32 indexCount;
};

#define RENDERER_BATCH_MAX_LINE_COUNT ( 8192 * 4 )
#define RENDERER_BATCH_MAX_LINE_VERTEX_COUNT (RENDERER_BATCH_MAX_LINE_COUNT * 2)
#define RENDERER_BATCH_MAX_LINE_INDEX_COUNT (RENDERER_BATCH_MAX_LINE_COUNT * 2)
introspect("Renderer")
struct cdl_renderer_line_batch {
    bool8 dirty;
    
    u32 VAO;
    u32 VBO;
    u32 EBO;
    
    cdl_renderer_batch_entry* lastEntry;
    cdl_renderer_batch_entry entries[RENDERER_BATCH_MAX_ENTRIES];
    u32 nextEntryID;
    
    u32 vertexCount;
    cdl_renderer_batch_vertex* nextVertex;
    cdl_renderer_batch_vertex vertexBuffer[RENDERER_BATCH_MAX_LINE_VERTEX_COUNT];
    
    u32* nextIndex;
    u32 indexBuffer[RENDERER_BATCH_MAX_LINE_INDEX_COUNT];
};

#define RENDERER_BATCH_MAX_TRIANGLE_COUNT ( 4096 )
#define RENDERER_BATCH_MAX_TRIANGLE_VERTEX_COUNT (RENDERER_BATCH_MAX_TRIANGLE_COUNT * 4)
#define RENDERER_BATCH_MAX_TRIANGLE_INDEX_COUNT (RENDERER_BATCH_MAX_TRIANGLE_COUNT * 6)
introspect("Renderer")
struct cdl_renderer_triangle_batch {
    bool8 dirty;
    
    u32 VAO;
    u32 VBO;
    u32 EBO;
    
    cdl_renderer_batch_entry* lastEntry;
    cdl_renderer_batch_entry entries[RENDERER_BATCH_MAX_ENTRIES];
    u32 nextEntryID;
    
    u32 vertexCount;
    cdl_renderer_batch_vertex* nextVertex;
    cdl_renderer_batch_vertex vertexBuffer[RENDERER_BATCH_MAX_TRIANGLE_VERTEX_COUNT];
    
    u32* nextIndex;
    u32 indexBuffer[RENDERER_BATCH_MAX_TRIANGLE_INDEX_COUNT];
};


//- [SECTION] Text Rendering
introspect("Renderer")
struct cdl_renderer_font {
    cdl_assets_font* asset;
    cdl_renderer_texture* texture;
};

internal cdl_renderer_font* cdl_renderer_font_new(cdl_assets_font* asset);

//- [SECTION] Render States
enum RenderStateVarType_ {
    RenderStateVarType_None,
    
    RenderStateVarType_ClipRect,
    RenderStateVarType_Wireframe,
    RenderStateVarType_LineWidth,
    
    RenderStateVarType_Count,
    // Depth test, etc...
};

introspect("Renderer")
struct cdl_render_state_clip_rect {
    u32 depth;
    rect2 rect;
};

introspect("Renderer")
struct cdl_render_state {
    RenderStateVarType varType;
    
    // Triangles data
    struct {
        cdl_renderer_batch_vertex* vertexStart;
        u32 vertexCount;
        
        u32* indexStart;
        u32 indexCount;
    };
    
    // Line data
    struct {
        cdl_renderer_batch_vertex* lineVertexStart;
        u32 lineVertexCount;
        
        u32* lineIndexStart;
        u32 lineIndexCount;
    };
    
    bool8 wireframe;
    bool8 hasClipRect;
    cdl_render_state_clip_rect* clipRect;
    u32* lineWidth;
};

introspect("Renderer")
struct cdl_render_state_vars {
    u32 currentDepth;
    cdl_render_state_clip_rect clipRects[128];
    cdl_render_state_clip_rect* currentClipRect;
    
    u32 lineWidth[128];
    u32* currentLineWidth;
};

internal void cdl_renderer_state_process(cdl_render_state* state);
internal void cdl_renderer_state_reset();


//- [SECTION] Render Queue
enum RenderCommand_ {
    RenderCommand_Invalid,
    
    // 2D
    RenderCommand_2DPoint,
    RenderCommand_2DLine,
    RenderCommand_2DRect,
    RenderCommand_2DRectFilled,
    RenderCommand_2DRectTextured,
    RenderCommand_2DGlyph,
    
    // 3D
    RenderCommand_WorldPoint,
    RenderCommand_WorldLine,
    RenderCommand_WorldModel,
    RenderCommand_WorldModelWireframe,
    RenderCommand_WorldModelTextured,
    RenderCommand_WorldModelInstance,
    
    RenderCommand_Count
};

struct cdl_render_command {
    bool8 processed;
    RenderCommand type;
    
    cdl_renderer_texture* texture;
    u32 thickness;
    color8 color;
    
    union {
        // 2D
        v2f point2D;
        line2 line2D;
        rect2 rect;
        
        // 3D
        v3f point3D;
        line3 line3D;
        
        // Model
        struct {
            u32 triangleCount;
            v3f* positions;
            v2f* uvs;
            v3f* normals;
            color8* colors;
            
            u32 indexCount;
            u32* indices;
        };
        
        // Model Instance
        struct {
            cdl_assets_model_raw* model;
            cdl_renderer_model_transform transform;
            cdl_renderer_material* material;
        };
    };
};

struct cdl_render_queue {
    cdl_render_command buffer[2048]; // Ring Buffer
    cdl_render_command* nextCommand;
    cdl_render_command* current;
};

//- [SECTION] Render Scenes
enum RenderSceneType_ {
    RenderSceneType_None,
    
    RenderSceneType_2D,
    RenderSceneType_3D,
    
    RenderSceneType_Count
};

introspect("Renderer")
struct cdl_render_scene {
    RenderSceneType type;
    bool8 dirty;
    
    cdl_assets_model_raw* defaultModel;
    
    // 3D : Environment
    cdl_renderer_environment environment;
    
    // 3D : Lights
    bool8 useLights;
    cdl_renderer_light lights[8];
    cdl_renderer_light* nextLight;
    
    // States
    cdl_render_state states[128];
    cdl_render_state* currentState;
    u32 statesCount;
    cdl_render_state_vars statesVars;
    
    // Instances
    cdl_renderer_model_instance_list instanceLists[64];
    u32 instanceListsCount;
    
    // Batches
    cdl_renderer_line_batch lines;
    cdl_renderer_triangle_batch triangles;
    cdl_renderer_triangle_batch staticTriangles;
    
    // Textures
    cdl_renderer_texture* textures[RENDERER_MAX_TEXTURE_UNIT_PER_FRAME];
};

internal cdl_render_scene* cdl_render_scene_new(RenderSceneType type);
internal void cdl_render_scene_begin(cdl_render_scene* s);
internal void cdl_render_scene_render(cdl_render_scene* s);

//- [SECTION] Render Scene Variables
internal void cdl_render_scene_push_clip_rect(cdl_render_scene* s, rect2 clipRect);
internal void cdl_render_scene_pop_clip_rect(cdl_render_scene* s);
internal void cdl_render_scene_push_line_width(cdl_render_scene* s, u32 lineWidth);
internal void cdl_render_scene_pop_line_width(cdl_render_scene* s);
internal void cdl_render_scene_push_wireframe(cdl_render_scene* s);
internal void cdl_render_scene_pop_wireframe(cdl_render_scene* s);


//- [SECTION] World Render Scene Points
internal void cdl_render_scene_push_point(cdl_render_scene* s, v3f p, f32 size, color8 color);


//- [SECTION] World Render Scene Lines 
internal void cdl_render_scene_push_line(cdl_render_scene* s, v3f p1, v3f p2, color8 color);


//- [SECTION] World Render Scene Triangles & Quads
internal void cdl_render_scene_push_triangles(cdl_render_scene* s, u32 triangleCount, v3f* positions, color8* colors);
internal void cdl_render_scene_push_quads(cdl_render_scene* s, u32 quadCount, v3f* positions, v2f* scales, color8* colors);
internal void cdl_render_scene_push_quad_gradient(cdl_render_scene* s, v3f position, v2f scale, color8 topRightColor, color8 bottomRightColor, color8 bottomLeftColor, color8 topLeftColor);
internal void cdl_render_scene_push_quad_textured(cdl_render_scene* s, v3f position, v2f scale, v4f uvs, u32 texture);


//- [SECTION] World Render Scene Models 
internal void cdl_render_scene_push_model(cdl_render_scene* s, u32 vertexCount, v3f* positions, v2f* uvs, v3f* normals, color8* colors, u32 texture, u32 indexCount, u32* indices);
internal void cdl_render_scene_push_model_textured(cdl_render_scene* s, u32 vertexCount, v3f* positions, v2f* uvs, v3f* normals, u32 texture, u32 indexCount, u32* indices);

internal void cdl_render_scene_push_model_instance(cdl_render_scene* s, cdl_assets_model_raw* model, cdl_renderer_material* material, cdl_renderer_model_transform transform);


//- [SECTION] World Render Scene Static Models
internal u32 cdl_render_scene_push_static_model(cdl_render_scene* s, u32 vertexCount, v3f* positions, v2f* uvs, v3f* normals, color8* colors, u32 texture, u32 indexCount, u32* indices);
internal void cdl_render_scene_remove_static_model(cdl_render_scene* s, u32 modelEntryID);

//- [SECTION] World Environment
internal cdl_renderer_light* cdl_render_scene_push_light(cdl_render_scene* s, v3f pos, color8 color);

//- [SECTION] UI Scenes Points
internal void cdl_render_scene_ui_push_point(cdl_render_scene* s, v2f p, f32 size, color8 color);


//- [SECTION] UI Scenes Lines
internal void cdl_render_scene_ui_push_line(cdl_render_scene* s, v2f p1, v2f p2, f32 thickness, color8 color);


//- [SECTION] UI Scenes Curves
internal void cdl_render_scene_ui_bezier_cubic(cdl_render_scene* s, v2f p1, v2f p2, v2f p3, color8 color, u32 steps);
internal void cdl_render_scene_ui_bezier_quadratic(cdl_render_scene* s, v2f p1, v2f p2, v2f p3, v2f p4, color8 color, u32 steps);


//- [SECTION] UI Scenes Triangles & Quads
internal void cdl_render_scene_ui_push_triangles(cdl_render_scene* s, u32 triangleCount, v2f* positions, color8* colors);
internal void cdl_render_scene_ui_push_quads(cdl_render_scene* s, u32 quadCount, v2f* positions, v2f* sizes, color8* colors);
internal void cdl_render_scene_ui_push_quad(cdl_render_scene* s, v2f position, v2f size, color8 color);
internal void cdl_render_scene_ui_push_quad_textured(cdl_render_scene* s, v2f position, v2f size, v4f uvs, u32 texture);
internal void cdl_render_scene_ui_push_quad_textured_colored(cdl_render_scene* s, v2f position, v2f size, v4f uvs, color8 color, u32 texture);
internal void cdl_render_scene_ui_push_quad_sdf(cdl_render_scene* s, v2f position, v2f size, v4f uvs, u32 texture);
internal void cdl_render_scene_ui_push_quad_gradient(cdl_render_scene* s, v2f position, v2f size, v4f topRightColor, v4f bottomRightColor, v4f bottomLeftColor, v4f topLeftColor);
internal void cdl_render_scene_ui_push_quad_color_picker(cdl_render_scene* s, v2f position, v2f size, f32 hueAdjust);
internal void cdl_render_scene_ui_push_quad_rounded(cdl_render_scene* s, v2f position, v2f size, color8 color, i32 cornerRadius);
internal void cdl_render_scene_ui_push_circle(cdl_render_scene* s, v2f position, f32 radius, f32 width, color8 color);


//- [SECTION] UI Scenes Rects
internal void cdl_render_scene_ui_push_rect(cdl_render_scene* s, rect2 rect, color8 color);
internal void cdl_render_scene_ui_push_rect_outline(cdl_render_scene* s, rect2 rect, color8 color, f32 thickness);


//- [SECTION] UI Scenes Text
internal void cdl_render_scene_ui_push_glyph(cdl_render_scene* s, u32 texture, v3f position, v2f scale, v4f uvs, cdl_renderer_font* font, cdl_text_atb attributes);
internal v2f cdl_render_scene_ui_text(cdl_render_scene* s, char* text, v2f position, cdl_renderer_font* font, cdl_text_atb attributes);


//- [SECTION] Post Processing
introspect("Renderer")
struct cdl_renderer_effect {
    cdl_renderer_shader* shader;
    cdl_renderer_framebuffer* framebuffer;
    
    bool8 hasParents;
    cdl_renderer_effect* parents[2];
};

introspect("Renderer")
struct cdl_renderer_post_processing {
    u32 effectCount;
    cdl_renderer_effect effects[8];
    bool8 processed[8];
};

internal cdl_renderer_effect* cdl_renderer_effect_new(cdl_assets_shader* shaderAsset, cdl_renderer_effect* parents[4]);
internal void cdl_renderer_effect_process(cdl_renderer_scene_view* view, cdl_renderer_effect* effect, bool8 isFinal);


//- [SECTION] Scene View
introspect("Renderer")
struct cdl_renderer_scene_view {
    i32 renderOrder; // if set to -1, then the scene will only be rendered to its attached framebuffer
    
    cdl_renderer_camera* camera;
    cdl_render_scene* scene;
    cdl_renderer_framebuffer* framebuffer;
    
    cdl_renderer_shader* defaultShader;
    cdl_renderer_post_processing post;
    
#ifdef CDL_DEBUG
    char debugName[32];
#endif
};

internal cdl_renderer_scene_view* cdl_renderer_scene_view_new(i32 spot, cdl_render_scene* scene, cdl_renderer_camera* camera, cdl_renderer_shader* defaultShader, u32 msaaSamples);
internal void cdl_renderer_scene_view_begin(cdl_renderer_scene_view* view);
internal void cdl_renderer_scene_view_render(cdl_renderer_scene_view* view);

internal void cdl_renderer_internal_render_scene_instances_and_static(cdl_renderer_scene_view* view);
internal void cdl_renderer_internal_render_scene(cdl_render_scene* s);


//- [SECTION] Renderer
introspect("Renderer")
struct cdl_renderer_storage {
    cdl_renderer_camera cameras[64];
    cdl_renderer_camera* nextCamera;
    
    cdl_renderer_shader shaders[64];
    cdl_renderer_shader* nextShader;
    
    cdl_renderer_material materials[64];
    cdl_renderer_material* nextMaterial;
    
    cdl_renderer_texture textures[128];
    cdl_renderer_texture* nextTexture;
    
    cdl_renderer_font fonts[64];
    cdl_renderer_font* nextFont;
    
    cdl_renderer_framebuffer framebuffers[64];
    cdl_renderer_framebuffer* nextFramebuffer;
    
    cdl_renderer_api_framebuffer apiFramebuffers[64];
    cdl_renderer_api_framebuffer* nextAPIFramebuffer;
    
    cdl_render_scene scenes[8];
    cdl_render_scene* nextScene;
    
    cdl_renderer_scene_view views[64];
    cdl_renderer_scene_view* nextView;
};

introspect("Renderer")
struct cdl_renderer {
    bool8 isValid;
    
    // Full Screen Quad
    u32 fsq;
    cdl_renderer_post_processing post;
    
    // Application World
    cdl_render_scene* world;
    cdl_renderer_effect* finalWorldEffect;
    cdl_renderer_scene_view* worldView;
    
    // Application UI
    cdl_render_scene* ui;
    cdl_renderer_scene_view* uiView;
    
    // Current State
    struct {
        cdl_renderer_shader* currentShader;
    };
    
    // Shaders
    struct {
        cdl_renderer_shader* defaultUIShader;
        cdl_renderer_shader* defaultWorldShader;
        cdl_renderer_shader* fsQuadShader;
    };
    
    // Materials
    struct {
        cdl_renderer_material* defaultMaterial;
    };
    
    // Fonts
    struct {
        cdl_renderer_font* segoe_ui;
        cdl_renderer_font* arial;
        cdl_renderer_font* currentFont;
    };
    
    // Storage
    cdl_renderer_storage storage;
};
extern cdl_renderer* renderer;

internal f32 cdl_renderer_aspect_ratio();
internal rect2 cdl_renderer_frame_rect();

internal bool8 cdl_renderer_api_init();
inline internal bool8 cdl_renderer_api_flush_errors(const char* funcCall, u32 line);
internal void cdl_renderer_create(cdl_renderer* renderer);
internal void cdl_renderer_set(cdl_renderer* renderer);
internal void cdl_renderer_frame_begin();
internal void cdl_renderer_frame_end();

internal void cdl_renderer_start_render_queue_work();

//- [SECTION] UI Helpers
// Points
#define ui_point(p, size, color) (cdl_render_scene_ui_push_point(renderer->ui, p, size, color))

// Lines
#define ui_line(p1, p2, thickness, color) (cdl_render_scene_ui_push_line(renderer->ui, p1, p2, thickness, color))

// Curves
#define ui_bezier_quadratic(p1, p2, p3, thickness, color, steps) (cdl_render_scene_ui_push_bezier_quadratic(renderer->ui, p1, p2, p3, color, thickness, steps))
#define ui_bezier_cubic(p1, p2, p3, p4, thickness, color, steps) (cdl_render_scene_ui_push_bezier_cubic(renderer->ui, p1, p2, p3, p4, color, thickness, steps))

// Triangles & Quads
#define ui_triangles(count, positions, colors) (cdl_render_scene_ui_push_triangles(renderer->ui, count, positions, colors))
#define ui_quads(count, positions, sizes, colors) (cdl_render_scene_ui_push_quads(renderer->ui, count, positions, sizes, colors))
#define ui_quad(position, size, color) (cdl_render_scene_ui_push_quad(renderer->ui, position, size, color))
#define ui_quad_textured(position, size, uvs, texture) (cdl_render_scene_ui_push_quad_textured(renderer->ui, position, size, uvs, texture))
#define ui_quad_textured_tinted(position, size, uvs, color, texture) (cdl_render_scene_ui_push_quad_textured_tinted(renderer->ui, position, size, uvs, color, texture))
#define ui_quad_sdf(position, size, uvs, texture) (cdl_render_scene_ui_push_quad_sdf(renderer->ui, position, size, uvs, texture))
#define ui_quad_gradient(position, size, topRight, bottomRight, bottomLeft, topLeft) (cdl_render_scene_ui_push_quad_gradient(renderer->ui, position, size, topRight, bottomRight, bottomLeft, topLeft))
#define ui_color_picker(position, size, hueAdjust) (cdl_render_scene_ui_push_quad_color_picker(renderer->ui, position, size, hueAdjust))
#define ui_quad_rounded(position, size, color, radius) (cdl_render_scene_ui_push_quad_rounded(renderer->ui, position, size, color, radius))
#define ui_circle(position, radius, width, color) (cdl_render_scene_ui_push_circle(renderer->ui, position, radius, width, color))

// Rect
#define ui_rect(rect, color) (cdl_render_scene_ui_push_rect(renderer->ui, rect, color))
#define ui_rect_outline(rect, color, width) (cdl_render_scene_ui_push_rect_outline(renderer->ui, rect, color, width))

// Text
#define ui_glyph(texture, fontsize, position, scale, uvs, font, color) (cdl_render_scene_ui_push_glyph(renderer->ui, texture, fontSize, position, scale, uvs, font, color))
#define ui_text(text, position, renderFont, attributes) (cdl_render_scene_ui_text(renderer->ui, text, position, renderFont, attributes))


//- [SECTION] World Helpers
// Points
#define world_point(pos, size, color) (cdl_render_scene_push_point(renderer->world, pos, size, color))

// Lines
#define world_line(p1, p2, color)  (cdl_render_scene_push_line(renderer->world, p1, p2, color))

// Curves
#define world_bezier_quadratic(p1, p2, p3, color, steps) (cdl_render_scene_push_bezier_quadratic(renderer->world, p1, p2, p3, color, steps))
#define world_bezier_cubic(p1, p2, p3, p4, color, steps) (cdl_render_scene_push_bezier_cubic(renderer->world, p1, p2, p3, p4, color, steps))

// Triangles & Quads
#define world_triangles(triangleCount, pos, colors) (cdl_render_scene_push_triangles(renderer->world, triangleCount, pos, color))
#define world_quad(pos, scale, color) (cdl_render_scene_push_quads(renderer->world, 1, &pos, &scale, &color))
#define world_quads(quadCount, pos, scales, colors) (cdl_render_scene_push_quads(renderer->world, quadCount, pos, scales, colors))
#define world_quad_gradient(pos, topRight, bottomRight, bottomLeft, topLeft) (cdl_render_scene_push_quad_gradient(renderer->world, count, pos, topRight, bottomRight, bottomLeft, topLeft))
#define world_quad_textured(pos, scale, uvs, texture) (cdl_render_scene_push_quad_textured(renderer->world, pos, scale, uvs, texture))

// Models
#define world_model(vtxCount, positions, normals, colors, idxCount, indices) (cdl_render_scene_push_model(renderer->world, vtxCount, positions, 0, normals, colors, INVALID_TEXTURE, idxCount, indices))
#define world_model_textured(vtxCount, positions, uvs, normals, texture, indexCount, indices) (cdl_render_scene_push_model(renderer->world, vtxCount, positions, uvs, normals, 0, texture, indexCount, indices))
#define world_model_instance(rawModel, transform) (cdl_render_scene_push_model_instance(renderer->world, rawModel, 0, transform))
#define world_model_instance_with_mat(rawModel, material, transform) (cdl_render_scene_push_model_instance(renderer->world, rawModel, material, transform))

// Static Models
#define world_static_model(vtxCount, positions, uvs, normals, colors, texture, idxCount, indices) (cdl_render_scene_push_static_model(renderer->world, vtxCount, positions, uvs, normals, colors, texture, idxCount, indices))
#define world_rem_static_model(modelEntryID) (cdl_render_scene_remove_static_model(modelEntryID))

// Environment
#define world_light(pos, color) (cdl_render_scene_push_light(renderer->world, pos, color))


#endif // __CDL_INCLUDE_CDL_RENDERER_H__