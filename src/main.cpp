#define introspect(args)

//- Thrid Party
#include <GL/glew.h>
#include <GLFW/glfw3.h>

//- Base Headers
#include "cdl_std.c"

#ifdef ABE_WIN
#define GLFW_EXPOSE_NATIVE_WIN32
#include <GLFW/glfw3native.h>
#include <dsound.h>
#include <xinput.h>

#elif defined(ABE_APPLE)
#error "Missing Headers"

#elif defined(ABE_LINUX)
#include <X11/Xlib.h>
#define GLFW_EXPOSE_NATIVE_X11
#include <GLFW/glfw3native.h>
#include <dlfcn.h> // so 
#include <sys/stat.h> // filestat
#include <fcntl.h> // open
#include <unistd.h> // close
#include <stdlib.h> // malloc, free
#include <sys/mman.h> // mmap

#endif

//- Engine Headers
#include "cdl_engine_events.h"
#include "cdl_assets.h"
#include "cdl_audio.h"
#include "cdl_engine.h"
#include "cdl_platform.h"

//- Platform Sources
#include "platform/glfw_callbacks.cpp"

#ifdef ABE_WIN
#include "platform/win32/win32_window_utility.cpp"
#include "platform/win32/win32_file_system.cpp"
#include "platform/win32/win32_file_io.cpp"
#include "platform/win32/win32_input_recording.cpp"
#include "platform/win32/win32_dsound.cpp"
#include "platform/win32/win32_xinput.cpp"
#include "platform/win32/win32_dll.cpp"
#include "platform/win32/win32_platform.cpp"
#include "platform/win32/win32_memory.cpp"

#elif defined(ABE_APPLE)
#error "Missing Includes in main.cpp"

#elif defined(ABE_LINUX)
#include "platform/linux/linux_window_utility.cpp"
#include "platform/linux/linux_file_system.cpp"
#include "platform/linux/linux_file_io.cpp"
#include "platform/linux/linux_input_recording.cpp"
#include "platform/linux/linux_sound.cpp"
#include "platform/linux/linux_input.cpp"
#include "platform/linux/linux_so.cpp"
#include "platform/linux/linux_platform.cpp"
#include "platform/linux/linux_memory.cpp"

#else
#error "Unsupported Platform"
#endif

#include "platform/cdl_platform.cpp"

cdl_platform* platform;
cdl_engine_runtime* core;

int main()
{
    cdl_platform Platform = ZeroStruct;
    cdl_engine_runtime Core = ZeroStruct;
    
    platform = &Platform;
    core = &Core;
    
    if ( cdl_platform_initialize() ) {
        while (Core.run) {
            cdl_platform_update();
        }
        cdl_platform_clean();
    }
    
    return 0;
}