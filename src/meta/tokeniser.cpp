#include <stdio.h>

enum TokenType_ {
    TokenType_None,
    
    TokenType_Identifier,
    TokenType_Numeric,
    TokenType_String,
    TokenType_Comment,
    
    TokenType_Pound,
    TokenType_ParenOpen,
    TokenType_ParenClose,
    TokenType_BracketOpen,
    TokenType_BracketClose,
    TokenType_CurlyBracketOpen,
    TokenType_CurlyBracketClose,
    
    TokenType_Comma,
    TokenType_Colon,
    TokenType_SemiColon,
    TokenType_Dot,
    
    TokenType_At,
    TokenType_Percent,
    
    TokenType_Ampersand,
    TokenType_Superior,
    TokenType_Inferior,
    TokenType_ExclamationMark,
    TokenType_QuestionMark,
    TokenType_Pipe,
    TokenType_Tilde,
    
    TokenType_Asterisk,
    TokenType_Slash,
    TokenType_Plus,
    TokenType_Minus,
    TokenType_Equal,
    
    TokenType_EOF,
    
    TokenType_Count
};

struct Token {
    TokenType_ type;
    str8 token;
    Token* previous;
    Token* next;
};

struct Tokeniser {
    char* cursor;
    Token* tokens;
    Token* nextToken;
};

struct MetaStruct {
    char name[128];
    MetaStruct* next;
};
global MetaStruct* firstMetaStruct;

internal str8 GetFileStringAndNullTerminate(char* path)
{
    str8 result = ZeroStruct;
    FILE* f = fopen(path, "r");
    assert(f);
    
    fseek(f, 0, SEEK_END);
    u32 fileSize = ftell(f);
    fseek(f, 0, SEEK_SET);
    
    result.string = (char*)calloc(fileSize + 1, sizeof(char));
    fread(result.string, fileSize, 1, f);
    result.string[fileSize] = '\0';
    result.size = fileSize;
    
    fclose(f);
    
    return result;
}

internal void EatLineComment(char** cursor)
{
    while ( *(*cursor) != '\n' ) { 
        (*cursor)++;
        if ( *(*cursor) == '\0' ) {
            break;
        }
    }
}

internal void EatMultiLineComment(char** cursor)
{
    while ( *(*cursor) != '*' && *(*(cursor) + 1) != '/' ) {
        (*cursor)++; 
        if ( *(*cursor) == '\0' ) {
            break;
        }
    }
}

internal void EatWhitespaces(char** cursor)
{
    while ( IsWhitespace(*(*cursor)) ) {
        (*cursor)++;
        
        if ( *(*cursor) == '\0' ) {
            break;
        }
        
        if ( *(*cursor) == '/' && *(*cursor + 1) == '/' ) {
            EatLineComment(cursor);
        } else if ( *(*cursor) == '/' && *(*cursor + 1) == '*' ) {
            EatMultiLineComment(cursor);
        }
    }
}

internal Token GetToken(Tokeniser* tokeniser)
{
    char* tempCursor = tokeniser->cursor;
    EatWhitespaces(&tempCursor);
    
    Token result = ZeroStruct;
    result.token.string = tokeniser->cursor;
    result.token.size = 1;
    
    char c = *tempCursor;
    
    if ( c == '#' ) {
        result.type = TokenType_Pound;
        tempCursor++;
    } 
    
    else if ( c == '(' ) {
        result.type = TokenType_ParenOpen;
        tempCursor++;
    } 
    
    else if ( c == ')' ) {
        result.type = TokenType_ParenClose;
        tempCursor++;
    } 
    
    else if ( c == '[' ) {
        result.type = TokenType_BracketOpen;
        tempCursor++;
    } 
    
    else if ( c == ']' ) {
        result.type = TokenType_BracketClose;
        tempCursor++;
    } 
    
    else if ( c == '{' ) {
        result.type = TokenType_CurlyBracketOpen;
        tempCursor++;
    } 
    
    else if ( c == '}' ) {
        result.type = TokenType_CurlyBracketClose;
        tempCursor++;
    } 
    
    else if ( c == ',' ) {
        result.type = TokenType_Comma;
        tempCursor++;
    } 
    
    else if ( c == ':' ) {
        result.type = TokenType_Colon;
        tempCursor++;
    } 
    
    else if ( c == ';' ) {
        result.type = TokenType_SemiColon;
        tempCursor++;
    } 
    
    else if ( c == '.' ) {
        result.type = TokenType_Dot;
        tempCursor++;
    }
    
    else if ( c == '@' ) {
        result.type = TokenType_At;
        tempCursor++;
    }
    
    else if ( c == '%' ) {
        result.type = TokenType_Percent;
        tempCursor++;
    }
    
    else if ( c == '&' ) {
        result.type = TokenType_Ampersand;
        tempCursor++;
    }
    
    else if ( c == '>' ) {
        result.type = TokenType_Superior;
        tempCursor++;
    }
    
    else if ( c == '<' ) {
        result.type = TokenType_Inferior;
        tempCursor++;
    }
    
    else if ( c == '!' ) {
        result.type = TokenType_ExclamationMark;
        tempCursor++;
    }
    
    else if ( c == '?' ) {
        result.type = TokenType_QuestionMark;
        tempCursor++;
    }
    
    else if ( c == '|' ) {
        result.type = TokenType_Pipe;
        tempCursor++;
    }
    
    else if ( c == '~' ) {
        result.type = TokenType_Tilde;
        tempCursor++;
    }
    
    else if ( c == '*' ) {
        result.type = TokenType_Asterisk;
        tempCursor++;
    } 
    
    else if ( c == '/' ) {
        result.type = TokenType_Slash;
        tempCursor++;
    } 
    
    else if ( c == '+' ) {
        result.type = TokenType_Plus;
        tempCursor++;
    } 
    
    else if ( c == '-' ) {
        result.type = TokenType_Minus;
        tempCursor++;
    }
    
    else if ( c == '=' ) {
        result.type = TokenType_Equal;
        tempCursor++;
    }
    
    else if ( c == '\"' || c == '\'' ) {
        result.type = TokenType_String;
        tempCursor++;
        
        while ( *tempCursor != '\"' && *tempCursor != '\'' ) {
            tempCursor++;
            if ( *tempCursor == '\0' ) {
                break;
            }
        }
        
        tempCursor++;
        result.token.size = tempCursor - result.token.string;
    }
    
    else if ( IsAlpha(c) ) {
        result.type = TokenType_Identifier;
        
        while ( IsAlpha(*tempCursor)
               || IsNumber(*tempCursor)
               || *tempCursor == '_' ) {
            tempCursor++;
            if ( *tempCursor == '\0' ) {
                break;
            }
        }
        
        result.token.size = tempCursor - result.token.string;
    }
    
    else if ( IsNumber(c) ) {
        result.type = TokenType_Numeric;
        
        while ( IsNumber(*tempCursor) || *tempCursor == '.' ) {
            tempCursor++;
            if ( *tempCursor == '\0' ) {
                break;
            }
        }
        
        result.token.size = tempCursor - result.token.string;
    }
    
    else if ( c == '\0' ) {
        result.type = TokenType_EOF;
        tempCursor++;
    } 
    
    else {
        result.type = TokenType_None;
        tempCursor++;
    }
    
    return result;
}

internal Token* NewToken(Tokeniser* tokeniser)
{
    Token* result = tokeniser->nextToken;
    
    if ( result != tokeniser->tokens ) {
        Token* previous = result - 1;
        result->previous = previous;
        previous->next = result;
    }
    
    result->token.string = tokeniser->cursor;
    result->token.size = 1;
    tokeniser->nextToken++;
    
    return result;
}

internal Token* GetTokenAndAdvance(Tokeniser* tokeniser)
{
    EatWhitespaces(&tokeniser->cursor);
    
    Token* result = NewToken(tokeniser);
    
    char c = *tokeniser->cursor;
    if ( c == '#' ) {
        result->type = TokenType_Pound;
        tokeniser->cursor++;
    } 
    
    else if ( c == '(' ) {
        result->type = TokenType_ParenOpen;
        tokeniser->cursor++;
    } 
    
    else if ( c == ')' ) {
        result->type = TokenType_ParenClose;
        tokeniser->cursor++;
    } 
    
    else if ( c == '[' ) {
        result->type = TokenType_BracketOpen;
        tokeniser->cursor++;
    } 
    
    else if ( c == ']' ) {
        result->type = TokenType_BracketClose;
        tokeniser->cursor++;
    } 
    
    else if ( c == '{' ) {
        result->type = TokenType_CurlyBracketOpen;
        tokeniser->cursor++;
    } 
    
    else if ( c == '}' ) {
        result->type = TokenType_CurlyBracketClose;
        tokeniser->cursor++;
    } 
    
    else if ( c == ',' ) {
        result->type = TokenType_Comma;
        tokeniser->cursor++;
    } 
    
    else if ( c == ':' ) {
        result->type = TokenType_Colon;
        tokeniser->cursor++;
    } 
    
    else if ( c == ';' ) {
        result->type = TokenType_SemiColon;
        tokeniser->cursor++;
    } 
    
    else if ( c == '.' ) {
        result->type = TokenType_Dot;
        tokeniser->cursor++;
    }
    
    else if ( c == '@' ) {
        result->type = TokenType_At;
        tokeniser->cursor++;
    }
    
    else if ( c == '%' ) {
        result->type = TokenType_Percent;
        tokeniser->cursor++;
    }
    
    else if ( c == '&' ) {
        result->type = TokenType_Ampersand;
        tokeniser->cursor++;
    }
    
    else if ( c == '>' ) {
        result->type = TokenType_Superior;
        tokeniser->cursor++;
    }
    
    else if ( c == '<' ) {
        result->type = TokenType_Inferior;
        tokeniser->cursor++;
    }
    
    else if ( c == '!' ) {
        result->type = TokenType_ExclamationMark;
        tokeniser->cursor++;
    }
    
    else if ( c == '?' ) {
        result->type = TokenType_QuestionMark;
        tokeniser->cursor++;
    }
    
    else if ( c == '|' ) {
        result->type = TokenType_Pipe;
        tokeniser->cursor++;
    }
    
    else if ( c == '~' ) {
        result->type = TokenType_Tilde;
        tokeniser->cursor++;
    }
    
    else if ( c == '*' ) {
        result->type = TokenType_Asterisk;
        tokeniser->cursor++;
    } 
    
    else if ( c == '/' ) {
        result->type = TokenType_Slash;
        tokeniser->cursor++;
    } 
    
    else if ( c == '+' ) {
        result->type = TokenType_Plus;
        tokeniser->cursor++;
    } 
    
    else if ( c == '-' ) {
        result->type = TokenType_Minus;
        tokeniser->cursor++;
    }
    
    else if ( c == '=' ) {
        result->type = TokenType_Equal;
        tokeniser->cursor++;
    }
    
    else if ( c == '\"' || c == '\'' ) {
        result->type = TokenType_String;
        tokeniser->cursor++;
        
        while ( *tokeniser->cursor != '\"' && *tokeniser->cursor != '\'' ) {
            tokeniser->cursor++;
            if ( *tokeniser->cursor == '\0' ) {
                break;
            }
        }
        
        tokeniser->cursor++;
        result->token.size = tokeniser->cursor - result->token.string;
    }
    
    else if ( IsAlpha(c) || c == '_' ) {
        result->type = TokenType_Identifier;
        
        while ( IsAlpha(*tokeniser->cursor)
               || IsNumber(*tokeniser->cursor)
               || *tokeniser->cursor == '_' ) {
            tokeniser->cursor++;
            if ( *tokeniser->cursor == '\0' ) {
                break;
            }
        }
        
        result->token.size = tokeniser->cursor - result->token.string;
    }
    
    else if ( IsNumber(c) ) {
        result->type = TokenType_Numeric;
        
        while ( IsNumber(*tokeniser->cursor) || *tokeniser->cursor == '.' ) {
            tokeniser->cursor++;
            if ( *tokeniser->cursor == '\0' ) {
                break;
            }
        }
        
        result->token.size = tokeniser->cursor - result->token.string;
    }
    
    else if ( c == '\0' ) {
        result->type = TokenType_EOF;
        tokeniser->cursor++;
    } 
    
    else {
        result->type = TokenType_None;
        tokeniser->cursor++;
    }
    
    return result;
}

internal bool8 TokenStringEquals(Token* token, char* str)
{
    return MemoryCompare(token->token.string, str, StringLength(str));
}

internal bool8 RequireTokenAndAdvance(Token** token, TokenType_ type)
{
    (*token)++;
    if ( (*token)->type == type ) return 1;
    return 0;
}