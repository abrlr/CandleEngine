#include "cdl_std.c"
#include "tokeniser.cpp"

internal void ParseMember(FILE* f, Token* structNameToken, Token* token)
{
    bool8 isPtr = 0;
    Token* memberTypeToken = token;
    Token* nextToken = token->next;
    Token* memberNameToken = nextToken;
    
    if ( nextToken->type == TokenType_Asterisk ) {
        memberNameToken = nextToken->next;
        fprintf(f, "\t{ \"%.*s\", \"%.*s\", MetaType_%.*sPtr, (u32)struct_offset(%.*s, %.*s), 1 },\n", 
                structNameToken->token.size, structNameToken->token.string,
                memberNameToken->token.size, memberNameToken->token.string, 
                memberTypeToken->token.size, memberTypeToken->token.string,
                structNameToken->token.size, structNameToken->token.string,
                memberNameToken->token.size, memberNameToken->token.string
                ); 
    } 
    
    else if ( nextToken->type == TokenType_Identifier ) {
        fprintf(f, "\t{ \"%.*s\", \"%.*s\", MetaType_%.*s, (u32)struct_offset(%.*s, %.*s), 1 },\n", 
                structNameToken->token.size, structNameToken->token.string,
                memberNameToken->token.size, memberNameToken->token.string, 
                memberTypeToken->token.size, memberTypeToken->token.string,
                structNameToken->token.size, structNameToken->token.string,
                memberNameToken->token.size, memberNameToken->token.string
                ); 
    }
    
    
    token = memberNameToken;
}

internal void ParseStruct(FILE* f, Token* token)
{
    // Get struct tokens
    Token* structNameToken = token->next;
    token = structNameToken->next;
    
    fprintf(f, "global cdl_meta_members %.*s_members[] = {\n",
            structNameToken->token.size, structNameToken->token.string);
    
    // Parse the struct
    u32 bracketDelta = 0;
    
    for (;;) {
        
        // Account for anonymous struct
        // TODO(abe): nested structs
        if ( token->type == TokenType_CurlyBracketOpen ) { bracketDelta++; }
        if ( token->type == TokenType_CurlyBracketClose ) { bracketDelta--; }
        
        // ifdef, ifndef, endif in structs
        if ( token->type == TokenType_Pound && token->next->type == TokenType_Identifier ) {
            if ( MemoryCompare(token->next->token.string, "ifdef", StringLength("ifdef")) ) {
                while ( !MemoryCompare(token->token.string, "endif", StringLength("endif")) ) {
                    token = token->next;
                }
            } else if ( MemoryCompare(token->next->token.string, "ifndef", StringLength("ifndef")) ) {
                while ( !MemoryCompare(token->token.string, "endif", StringLength("endif")) ) {
                    token = token->next;
                }
            } else if ( MemoryCompare(token->next->token.string, "endif", StringLength("endif")) ) {
                token = token->next;
            }
        }
        
        // Struct Members
        if ( token->type == TokenType_Identifier ) {
            
            // TODO(abe): maybe only pick one member of the union
            if ( MemoryCompare(token->token.string, "union", StringLength("union")) ) {
                token = token->next->next;
                bracketDelta++;
            }
            
            ParseMember(f, structNameToken, token);
        }
        
        token = token->next;
        
        if ( !(token && bracketDelta) ) break;
    }
    
    fprintf(f, "};\n\n");
    
    MetaStruct* meta = (MetaStruct*)calloc(1, sizeof(MetaStruct));
    MemoryCopy(meta->name, structNameToken->token.string, structNameToken->token.size);
    meta->next = firstMetaStruct;
    firstMetaStruct = meta;
    
}

internal void ParseEnum(FILE* f, Token* token)
{
    Token* enumNameToken = token->next;
    token = enumNameToken->next;
    
    fprintf(f, "global char* %.*sto_string[] = {\n", enumNameToken->token.size, enumNameToken->token.string);
    bool8 parse = 1;
    
    while ( parse ) {
        if ( token->type == TokenType_Identifier ) {
            // Stringify Enum Value
            fprintf(f, "\t\"%.*s\",\n", token->token.size, token->token.string);
            
            // Skip to the next ',' or '}'
            while ( token && token->type != TokenType_Comma ) {
                if (token->type == TokenType_CurlyBracketClose) {
                    parse = 0;
                    break;
                }
                
                token = token->next;
            }
        }
        
        if ( parse ) {
            token = token->next;
            if ( token->type == TokenType_CurlyBracketClose ) {
                parse = 0;
            }
        }
    }
    
    fprintf(f, "};\n\n");
}

internal void ParseIntrospectableParams(Token** token)
{
    for (;;) {
        *token = (*token)->next;
        if ( (*token)->type == TokenType_ParenClose ) { break; }
    }
}

internal void ParseIntrospectable(FILE* f, Token* token)
{
    if ( RequireTokenAndAdvance(&token, TokenType_ParenOpen) ) {
        
        ParseIntrospectableParams(&token);
        
        Token* typeToken = token->next;
        if ( TokenStringEquals(typeToken, "struct") ) {
            ParseStruct(f, typeToken);
        } 
        
        else {
            assert(0); // Not supported yet
        }
        
    }
}

internal void ParseFile(FILE* f, Tokeniser* tokeniser)
{
    //- Get all tokens in the file
    Token* t = GetTokenAndAdvance(tokeniser);
    while ( t->type != TokenType_EOF ) {
        t = GetTokenAndAdvance(tokeniser);
        
        // NOTE(abe): Activate sometimes
#if 0
        if ( t->type == TokenType_None ) {
            fprintf(f, "%.*s\n", t->token.size, t->token.string);
        }
#endif
    }
    
    //- Process the tokens
    Token* token = tokeniser->tokens;
    while ( token ) {
        
        // If token is an identifier
        if ( token->type == TokenType_Identifier ) {
            
            if ( MemoryCompare(token->token.string, "introspect", StringLength("introspect")) ) {
                bool8 isIntrospectDefine = token->previous->type == TokenType_Identifier && MemoryCompare(token->previous->token.string, "define", StringLength("define"));
                if ( isIntrospectDefine ) {
                    // Skip if this is a "#define introspect(args)"
                    token = token->next;
                    continue;
                }
                
                ParseIntrospectable(f, token);
            } else if ( MemoryCompare(token->token.string, "enum", StringLength("enum")) ) {
                ParseEnum(f, token);
            }
            
        }
        
        token = token->next;
    }
    
}

internal void ParseEngineHeaders()
{
    FILE* f = fopen("cdl_generated.h", "w");
    
    if ( f ) {
        fprintf(f, "#ifndef __CDL_INCLUDE_CDL_GENERATED_H__\n#define __CDL_INCLUDE_CDL_GENERATED_H__\n\n");
        fprintf(f, "// Generated by preprocessor.cpp\n\n");
        
        char* files[] = {
            "std/abe_algorithms.h",
            "std/abe_base.h",
            "std/abe_formats.h",
            //"std/abe_hashtable.h",
            "std/abe_log.h",
            "std/abe_maths.h",
            "std/abe_profiler.h",
            "std/abe_random.h",
            "std/abe_threads.h",
            
            "cdl_assets.h",
            "cdl_audio.h",
            "cdl_engine.h",
            "cdl_engine_debug.h",
            "cdl_engine_events.h",
            "cdl_platform.h",
            "cdl_renderer.h",
            "cdl_ui.h",
            "cdl_imgui.h"
        };
        
        for (u32 i = 0; i < ARRAY_COUNT(files); i++) {
            fprintf(f, "//- Parsed File: %s\n", files[i]);
            str8 file = GetFileStringAndNullTerminate(files[i]);
            
            Tokeniser tokeniser = ZeroStruct;
            tokeniser.cursor = file.string;
            tokeniser.tokens = (Token*)calloc(1024 * 64, sizeof(Token));
            tokeniser.nextToken = tokeniser.tokens;
            
            ParseFile(f, &tokeniser);
            free(file.string);
            free(tokeniser.tokens);
            
            fprintf(f, "//-\n\n");
        }
        
        fprintf(f, "#define META_HANDLE_TYPE_DUMP(memberPtr) \\\n");
        for (MetaStruct* meta = firstMetaStruct; meta; meta = meta->next) {
            fprintf(f, "\tcase MetaType_%s: \\\n\t{ \\\n\t\tprint_meta_struct(%s_members, ARRAY_COUNT(%s_members), memberPtr, indent + 1); \\\n\t} break; %c\n", meta->name, meta->name, meta->name, meta->next ? '\\' : ' ');
            
        }
        fprintf(f, "\n\n");
        
        fprintf(f, "#endif // __CDL_INCLUDE_CDL_GENERATED_H__\n");
        
        fclose(f);
    }
}

i32 main (i32 argc, char** argv)
{
    ParseEngineHeaders();
    
    return 0;
}