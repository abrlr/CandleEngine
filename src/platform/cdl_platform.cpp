//- cdl_platform.cpp
// Platform Agnostic Layer used by main.cpp

#ifdef ABE_WIN
const char* appName = "application.dll";
const char* appNameLocked = "application.locked.dll";
#elif defined(ABE_APPLE)
const char* appName = "application.dylib";
const char* appNameLocked = "application.locked.dylib";
#elif defined(ABE_LINUX)
const char* appName = "application.so";
const char* appNameLocked = "application.locked.so";
#endif

internal bool8 cdl_platform_initialize()
{
    Platform_GetLocations();
    
    platform->targetFramesPerSecond = 60;
    
    core->run = 1;
    core->hasFixedAspectRatio = 0;
    
    core->windowWidth = CDL_ENGINE_APP_WIDTH;
    core->windowHeight = CDL_ENGINE_APP_HEIGHT;
    core->frameWidth = CDL_ENGINE_APP_WIDTH;
    core->frameHeight = CDL_ENGINE_APP_HEIGHT;
    core->aspectRatio = core->windowWidth / core->windowHeight;
    
    //- Create GLFW Window
    GLFWwindow* glfwWindow = Platform_CreateGLFWWindow();
    if ( !glfwWindow ) {
        core->run = 0;
        return 0;
    }
    Platform_SetWindow(glfwWindow);
    
    //- Engine State
    bool8 appLoaded = Platform_LoadApp((char*)appName, (char*)appNameLocked);
    if ( !appLoaded ) {
        core->run = 0;
        return 0;
    }
    platform->AppSetPlatform(platform, core);
    
    //- Sound
    bool8 audioInitialized = Platform_InitAudioSystem();
    
    //- Engine Memory
    if ( !Platform_InitializeEngineMemory() ) {
        core->run = 0;
        return 0;
    }
    
    //- Function Binding
    platform->setControllerVibration = Platform_SetControllerVibration;
    
    // Window
    platform->setWindowTitle = Platform_WindowSetTitle;
    platform->setFullScreen = Platform_WindowSetFullScreen;
    platform->isActive = Platform_WindowIsActive;
    platform->requestAttention = Platform_WindowRequestAttention;
    
    // File System
    platform->fsIterator = Platform_FSIteratorNew;
    platform->fsIterate = Platform_FSIterate;
    
    // File IO
    platform->freefile = Platform_FreeFileMemory;
    platform->readfile = Platform_ReadEntireFile;
    platform->writefile = Platform_WriteEntireFile;
    platform->openfileexplorer = Platform_OpenFileExplorer;
    platform->savefileexplorer = Platform_SaveFileExplorer;
    
    //- Threads
    core->highQueue = thread_work_queue_create(THREAD_COUNT);
    thread_spawn(THREAD_COUNT, platform->threads, &core->highQueue);
    //thread_work_queue lowQueue = thread_work_queue_create(THREAD_COUNT);
    
    Platform_StartTimers();
    platform->AppInit(&core->memory);
    
    return 1;
}

internal void cdl_platform_update()
{
    // NOTE(abe): use glfwPollEvents if the app needs real time update
    //               use glfwWaitEvents if the application doesn't need to be updated at
    //               a constant time interval (less CPU intensive)
    glfwPollEvents();
    Platform_ProcessInputs();
    
    //- Reload App Code if necessary
    Platform_TryReloadApp((char*)appName, (char*)appNameLocked);
    Platform_StartFrame();
    
    //- Application Update
    glClearColor( 1.f / 255.f, 1.f / 255.f, 1.f / 255.f, 1.0f );
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    if ( platform->appCodeIsValid ) {
        platform->AppUpdate(&core->memory);
    }
    
    //- End Frame
    glfwSwapBuffers(platform->glfwWindow);
    Platform_ResetEventQueue();
    Platform_EndFrame();
    
    //- Timers
    core->totalTime += platform->frameTime;
    core->deltaTime = platform->frameTime;
    platform->frameCount++;
}

internal void cdl_platform_clean()
{
    platform->AppClean(&core->memory);
}