internal bool8 Platform_LoadApp(char* name, char* nameLocked)
{
    bool8 result = 0;
    local i32 uniqueID = 1;
    
    char appPath[CDL_MAX_PATH];
    Platform_BuildFilepathInAppFolder(name, appPath, CDL_MAX_PATH);
    
    char appTempPath[CDL_MAX_PATH];
    Platform_BuildFilepathInAppFolder(nameLocked, appTempPath, CDL_MAX_PATH);
    
    log_msg("Application library path is \"%s\".\n", appPath);
    log_msg("Application library locked is \"%s\".\n", appTempPath);
    
    bool8 copyDone = 0;
    
    cdl_file soFile = Platform_ReadEntireFile(appPath);
    if ( !soFile.content ) {
        log_err("Couldn't open file \"%s\".\n", appPath);
        goto out;
    }
    
    copyDone = Platform_WriteEntireFile(appTempPath, &soFile);
    if ( !copyDone ) {
        log_err("Failed to copy \"%s\" to \"%s\".\n", appPath, appTempPath);
        goto out;
    }
    
    platform->appHandle = dlopen(appTempPath, RTLD_LAZY | RTLD_GLOBAL);
    if ( !platform->appHandle ) {
        log_err("Failed to open application library.\n");
        goto out;
    }
    
    platform->AppInit = (EngineInitFunc*)dlsym(platform->appHandle, "EngineInit");
    platform->AppReload = (EngineHotReloadFunc*)dlsym(platform->appHandle, "EngineHotReload");
    platform->AppUpdate = (EngineUpdateFunc*)dlsym(platform->appHandle, "EngineUpdateAndRender");
    platform->AppSetPlatform = (EngineSetPlatformFunc*)dlsym(platform->appHandle, "EngineSetPlatform");
    platform->AppClean = (EngineCleanFunc*)dlsym(platform->appHandle, "EngineClean");
    platform->appCodeIsValid = ( platform->AppInit && platform->AppReload && platform->AppUpdate && platform->AppSetPlatform && platform->AppClean );
    
    if ( !platform->appCodeIsValid ) {
        platform->AppInit = 0;
        platform->AppReload = 0;
        platform->AppUpdate = 0;
        platform->AppSetPlatform = 0;
        platform->AppClean = 0;
        log_err("Application library is not valid.\n");
        goto out;
    }
    
    result = 1;
    platform->appLastUpdateTime = Platform_GetLastUpdateTime(appPath);
    
    {
        char buffer[256];
        tm* timeinfo = localtime(&platform->appLastUpdateTime);
        strftime(buffer, sizeof(buffer), "%b %d %H:%M", timeinfo);
        log_msg("\"%s\" last update was %s.\n", appPath, buffer);
    }
    
    log_info("Application library loaded.\n");
    
    out:
    if ( soFile.content ) {
        Platform_FreeFileMemory(soFile.content);
    }
    
    return result;
}

internal void Platform_UnloadApp()
{
    if ( platform->appCodeIsValid ) {
        dlclose(platform->appHandle);
    }
    
    platform->appCodeIsValid = 0;
    platform->AppReload = 0;
    platform->AppUpdate = 0;
    platform->AppSetPlatform = 0;
    platform->AppClean = 0;
}

internal void Platform_TryReloadApp(char* name, char* nameLocked)
{
    char appPath[CDL_MAX_PATH];
    Platform_BuildFilepathInAppFolder(name, appPath, CDL_MAX_PATH);
    
    u32 appUpdateTime = Platform_GetLastUpdateTime(appPath);
    if ( appUpdateTime > platform->appLastUpdateTime ) {
        Platform_UnloadApp();
        Platform_LoadApp(name, nameLocked);
        
        if ( platform->appCodeIsValid ) {
            platform->AppSetPlatform(platform, core);
            platform->AppReload(&core->memory);
        }
    }
}