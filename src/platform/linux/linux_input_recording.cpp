internal void Platform_RecordInputs()
{
}

internal void Platform_BeginInputRecording(int inputSlot)
{
}

internal void Platform_EndInputRecording()
{
}

internal void Platform_BeginInputPlayback(int inputSlot)
{
}

internal void Platform_EndInputPlayback()
{
}

internal void Platform_PlaybackInputs()
{
}

internal void Platform_CheckForRecordOrPlayback()
{
}