internal void Platform_GetLocations()
{
    // Get Working Directory
    getcwd(platform->appFilepath, CDL_MAX_PATH);
    platform->appFilepath[StringLength(platform->appFilepath)] = '/';
    log_msg("Working Directory is \"%s\".\n", platform->appFilepath);
}

internal void Platform_BuildFilepathInAppFolder(char* filename, char* targetFilepath, i32 targetSize, i32 uniqueID = 0)
{
    if ( uniqueID ) {
        char uniqueFileName[CDL_MAX_PATH] = { };
        sprintf(uniqueFileName, "%d_%s", uniqueID, filename);
        ConcatString(StringLength(platform->workingDirectory), platform->workingDirectory,
                     StringLength(uniqueFileName), uniqueFileName,
                     targetSize, targetFilepath);
    } else {
        ConcatString(StringLength(platform->workingDirectory), platform->workingDirectory,
                     StringLength(filename), filename,
                     targetSize, targetFilepath);
    }
}

internal time_t Platform_GetLastUpdateTime(char* filename)
{
    time_t lastUpdateTime = 0;
    
	struct stat filestat;
	if ( stat(filename, &filestat) == 0 ) {
		lastUpdateTime = filestat.st_mtime;
	}
    
    return lastUpdateTime;
}

PLATFORM_FREEFILEMEMORY(Platform_FreeFileMemory)
{
    if ( fileMemory ) {
        free(fileMemory);
    } else {
        log_warn("Nothing to free.\n");
    }
}

PLATFORM_READENTIREFILE(Platform_ReadEntireFile)
{
    cdl_file result = { };
    
    struct stat fileStat;
    i32 allocSuccess = -1;
    i32 fd = open(filepath, O_RDONLY);
    
	if ( fd == -1 ) {
        log_err("Couldn't open %s\n", filepath);
        goto out;
    }
    
    if ( fstat(fd, &fileStat) != 0 ) {
        log_err("Failed stat reading for %s\n", filepath);
        goto out;
    }
    
    result.size = fileStat.st_size;
    result.content = (char*)malloc(result.size);
    
    if ( result.content ) {
        ssize_t bytesRead;
        bytesRead = read(fd, result.content, result.size);
        
        if (bytesRead != result.size) {
            Platform_FreeFileMemory(result.content);
            result.content = 0;
        }
    } else {
        log_err("Couldn't allocate %d bytes while reading %s\n", result.size, filepath);
    }
    
    
    out:
    if ( fd != -1 ) {
        close(fd);
    }
    
    return result;
}

PLATFORM_WRITEENTIREFILE(Platform_WriteEntireFile)
{
    bool8 result = false;
    
    i32 fd = open(filepath, O_WRONLY | O_CREAT, 0644);
	if (fd != -1) {
		ssize_t bytesWritten = write(fd, file->content, file->size);
		result = ( bytesWritten == file->size );
        
		if ( !result ) {
			log_err("File was not correctly written (%d bytes written, %d expected)\n", bytesWritten, file->size);
		}
        
		close(fd);
	} else {
        log_err("Couldn't open %s\n", filepath);
	}
    
    return result;
}

PLATFORM_OPENFILEEXPLORER(Platform_OpenFileExplorer)
{
    return false;
}

PLATFORM_SAVEFILEEXPLORER(Platform_SaveFileExplorer)
{
    return false;
}