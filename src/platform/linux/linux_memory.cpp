internal bool8 Platform_InitializeEngineMemory()
{
    u64 permanentContainerSize = PERMANENT_MEMORY_SIZE;
    u64 frameContainerSize = FRAME_MEMORY_SIZE;
    u64 transientContainerSize = TRANSIENT_MEMORY_SIZE;
    u64 totalMemorySize = permanentContainerSize + frameContainerSize + transientContainerSize;
    platform->engineMemoryBlockSize = permanentContainerSize;
    platform->engineMemoryBlock = mmap(0, totalMemorySize, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANON, -1, 0);
    
    if ( !platform->engineMemoryBlock ) {
        log_err("MMAP Allocation Failed.\n");
        return 0;
    }
    
    u8* memoryBlockAddress = (u8*)platform->engineMemoryBlock;
    memory_block_new(&core->memory.permanent, memoryBlockAddress, permanentContainerSize);
    
    memoryBlockAddress += permanentContainerSize;
    memory_block_new(&core->memory.frame, memoryBlockAddress, frameContainerSize);
    
    memoryBlockAddress += frameContainerSize;
    memory_block_new(&core->memory.transient, memoryBlockAddress, transientContainerSize);
    
    return 1;
}