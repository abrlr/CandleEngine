internal void Platform_SetWindow(GLFWwindow* glfwWindow)
{
    platform->glfwWindow = glfwWindow;
    platform->nativeWindow = glfwGetX11Window(glfwWindow);
}

internal void Platform_StartTimers()
{
}

internal void Platform_StartFrame()
{
    
}

internal void Platform_EndFrame()
{
    
}