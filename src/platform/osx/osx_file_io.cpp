internal void OSXGetExecutableFilename(osx_state* state)
{
    pid_t PID = getpid();
	int pidPathRes = proc_pidpath(PID, state->appFilepath, sizeof(state->appFilepath));
    
	if ( pidPathRes <= 0 ) {
		log_err("OSXGetExecutableFilename: Error while getting process path for PID %d\n", PID);
	}
    
    state->lastSlashPlusOne = state->appFilepath;
    for(char *scanIndex = state->lastSlashPlusOne; *scanIndex; ++scanIndex) {
        if(*scanIndex == '/') {
            state->lastSlashPlusOne = scanIndex + 1;
        }
    }
}

internal void OSXBuildFilepathInAppFolder(osx_state* state, char* filename, char* targetFilepath, int targetSize)
{
    ConcatString(state->lastSlashPlusOne - state->appFilepath, state->appFilepath,
                 StringLength(filename), filename,
                 targetSize, targetFilepath);
}

internal size_t OSXGetLastUpdateTime(char* filepath)
{
	time_t lastUpdateTime = 0;
    
	struct stat filestat;
	if ( stat(filepath, &filestat) == 0 )
	{
		lastUpdateTime = filestat.st_mtimespec.tv_sec;
	}
    
	return lastUpdateTime;
}

PLATFORM_FREEFILEMEMORY(OSXFreeFileMemory)
{
	if (fileMemory) {
		free(fileMemory);
	}
}

PLATFORM_READENTIREFILE(OSXReadEntireFile)
{
	cdl_file result = ZeroStruct;
    
	int fd = open(filepath, O_RDONLY);
	if (fd != -1) {
		struct stat fileStat;
        
		if (fstat(fd, &fileStat) == 0) {
			int allocSuccess = -1;
			result.size = fileStat.st_size;
			result.content = (char*)malloc(result.size);
            
			if (result.content) {
				allocSuccess = 0;
			}
            
			if (allocSuccess == 0) {
				ssize_t bytesRead;
				bytesRead = read(fd, result.content, result.size);
                
				if (bytesRead != result.size) {
					OSXFreeFileMemory(result.content);
					result.content = 0;
				}
			} else {
				log_err("OSXReadEntireFile: Couldn't allocate %d bytes while reading %s\n", result.size, filepath);
			}
		} else {
			log_err("OSXReadEntireFile: Stat reading for %s\n", filepath);
		}
        
		close(fd);
	} else {
        log_err("OSXReadEntireFile: Couldn't open %s\n", filepath);
	}
    
	return result;
}

PLATFORM_WRITEENTIREFILE(OSXWriteEntireFile)
{
	bool32 result = false;
    
	int fd = open(filepath, O_WRONLY | O_CREAT, 0644);
	if (fd != -1) {
		ssize_t bytesWritten = write(fd, file->content, file->size);
		result = ( bytesWritten == file->size );
        
		if ( !result ) {
			log_err("OSXWriteEntireFile: File was not correctly written (%d bytes written, %d expected)\n", bytesWritten, file->size);
		}
        
		close(fd);
	} else {
        log_err("OSXWriteEntireFile: Couldn't open %s\n", filepath);
	}
    
	return result;
}

PLATFORM_OPENFILEEXPLORER(OSXOpenFileExplorer)
{
    return false;
}

PLATFORM_SAVEFILEEXPLORER(OSXSaveFileExplorer)
{
    return false;
}