#ifndef __CDL_INCLUDE_OSX_H__
#define __CDL_INCLUDE_OSX_H__

/*
struct win32_sound_output {
    u32 runningSampleIndex;
    int samplePerSecond;
    int latencySampleCount;
    int bytesPerSample;
    int audioBufferSize;
};
*/

struct platform_application_code {
    bool32 isValid;
	size_t dylibLastUpadteTime;
    void* dylibHandle;
    
    EngineHotReloadFunc* HotReload;
    EngineUpdateFunc* Update;
    EngineSetPlatformFunc* SetPlatform;
};

#define OSX_FILEPATH_COUNT (PROC_PIDPATHINFO_MAXSIZE)

struct platform_state {
    GLFWwindow* glfwWindow;
    void* nativeWindow;
    
    int recordingFile;
    int playbackFile;
    
    void* engineMemoryBlock;
    u64 engineMemoryBlockSize;
    
    char appFilepath[OSX_FILEPATH_COUNT];
    char* lastSlashPlusOne;
};

#endif // __CDL_INCLUDE_OSX_H__