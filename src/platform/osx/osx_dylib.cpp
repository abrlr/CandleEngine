internal osx_application_code OSXLoadApplicationDylib(char* dylibName, char* dylibTempName)
{
    osx_state state = ZeroStruct;
    OSXGetExecutableFilename(&state);
    
    char dylibFullPath[OSX_FILEPATH_COUNT];
    OSXBuildFilepathInAppFolder(&state, dylibName, dylibFullPath, OSX_FILEPATH_COUNT);
    
    char dylibTempFullPath[OSX_FILEPATH_COUNT];
    OSXBuildFilepathInAppFolder(&state, dylibTempName, dylibTempFullPath, OSX_FILEPATH_COUNT);
    
    osx_application_code engineCode = ZeroStruct;
    engineCode.dylibLastUpadteTime = OSXGetLastUpdateTime(dylibFullPath);
    engineCode.isValid = false;
    
    cdl_file dylib = OSXReadEntireFile(dylibFullPath);
    bool32 copyDone = false;
    if ( dylib.content ) {
        copyDone = OSXWriteEntireFile(dylibTempFullPath, &dylib);
        OSXFreeFileMemory(dylib.content);
    } else {
        log_err("Couldn't copy dylib %s.\n", dylibFullPath);
        return engineCode;
    }
    
    if ( copyDone ) {
        engineCode.dylibHandle = dlopen(dylibTempFullPath, RTLD_LAZY | RTLD_GLOBAL);
        if ( engineCode.dylibHandle ) {
            engineCode.HotReload = (EngineHotReloadFunc*)dlsym(engineCode.dylibHandle, "EngineHotReload");
            engineCode.Update = (EngineUpdateFunc*)dlsym(engineCode.dylibHandle, "EngineUpdateAndRender");
            engineCode.SetPlatform = (EngineSetPlatformFunc*)dlsym(engineCode.dylibHandle, "EngineSetPlatform");
            
            engineCode.isValid = (bool32)( engineCode.Update && engineCode.SetPlatform && engineCode.HotReload);
        } else {
            log_err("Couldn't open engine dylib\n");
        }
    }
    
	if ( !engineCode.isValid ) {
		engineCode.Update = 0;
        engineCode.SetPlatform = 0;
        engineCode.HotReload = 0;
	} else {
        engineCode.SetPlatform(&gEngine);
    }
    
    
	return engineCode;
}

internal void OSXUnloadApplicationDylib(osx_application_code* engine)
{
    if ( core->dylibHandle ) {
        int success = dlclose(core->dylibHandle);
        if ( success != 0 ) {
            log_err("Couldn't close dylib handle %p (error %d)\n", core->dylibHandle, success);
        }
        core->dylibHandle = 0;
    }
    
    core->isValid = false;
    core->Update = 0;
    core->SetPlatform = 0;
    core->HotReload = 0;
}