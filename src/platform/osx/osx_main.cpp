#ifdef __APPLE__
#define introspect(args)

//- Third party Headers
#include <GL/glew.h>
#include <GLFW/glfw3.h>

//- Base Headers
#include "cdl_std.h"

//- Engine Headers 
#include "cdl_platform.h"
#include "cdl_engine_events.h"
#include "cdl_assets.h"
#include "cdl_audio.h"
#include "cdl_engine.h"

//- OSX Includes
#undef U64Max
#undef global
#undef local

#define GLFW_EXPOSE_NATIVE_COCOA
#include <GLFW/glfw3native.h>

#define global static
#define local static
#define U64Max UINT64_MAX

#include <dlfcn.h> // dylib 
#include <sys/stat.h> // filestat
#include <fcntl.h> // open
#include <unistd.h> // close
#include <stdlib.h> // malloc, free
#include <sys/mman.h> // mmap
#include <libproc.h> // pid

#include "osx.h"
cdl_engine_runtime* engine;
cdl_engine_runtime gEngine;

#include "glfw_callbacks.cpp"
#include "osx_file_io.cpp"
#include "osx_input_recording.cpp"
#include "osx_dylib.cpp"

//- Platform Entry Point
int main()
{
    //- Main Init 
    engine = &gEngine;
    gEngine.hasFixedAspectRatio = 0;
    gEngine.maxEventsPerFrame = 1024;
    gEngine.platform.targetFramesPerSecond = 60;
    
    //- Create GLFW Window
    GLFWwindow* glfwWindow = CreateGLFWWindow();
    if ( !glfwWindow ) {
        return -1;
    }
    id nativeWindow = glfwGetCocoaWindow(glfwWindow);
    
    //- Engine State
    platform_state state = ZeroStruct;
    OSXGetExecutableFilename(&state);
    platform_application_code engineDLL = OSXLoadApplicationDylib("engine.dylib", "engine_locked.dylib");
    if ( !engineDLL.isValid ) {
        log_err("Engine dll was not initialised correctly\n");
    }
    state.glfwWindow = glfwWindow;
    state.nativeWindow = nativeWindow;
    
    //- Engine Memory Setup
    cdl_engine_memory engineMemory = ZeroStruct;
    u64 permanentContainerSize = (u64)MegaBytes(64);
    u64 transientContainerSize = (u64)GigaBytes(1);
    
    u64 totalMemorySize = permanentContainerSize + transientContainerSize;
    state.engineMemoryBlockSize = permanentContainerSize;
    state.engineMemoryBlock = mmap(0, totalMemorySize, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANON, -1, 0);
    
    memory_block_new(&engineMemory.permanent, state.engineMemoryBlock, permanentContainerSize);
    
    u32 offset = permanentContainerSize;
    memory_block_new(&engineMemory.frame, ((u8*)(state.engineMemoryBlock) + offset), frameContainerSize);
    
    offset += frameContainerSize;
    memory_block_new(&engineMemory.transient, ((u8*)(state.engineMemoryBlock) + offset), transientContainerSize);
    
    if  ( !engineMemory.permanent.location || !engineMemory.transient.location || !engineMemory.frame.location ) {
        log_err("Engine memory allocation failed\n");
        return -1;
    }
    
    gEngine.mem = &engineMemory;
    gEngine.platform.state = &state;
    
    //- Platform Functions Binding 
    
    // File IO
    gEngine.platform.freefile = OSXFreeFileMemory;
    gEngine.platform.readfile = OSXReadEntireFile;
    gEngine.platform.writefile = OSXWriteEntireFile;
    gEngine.platform.openfileexplorer = OSXOpenFileExplorer;
    gEngine.platform.savefileexplorer = OSXSaveFileExplorer;
    
    gEngine.windowWidth = CDL_ENGINE_APP_WIDTH;
    gEngine.windowHeight = CDL_ENGINE_APP_HEIGHT;
    gEngine.frameWidth = CDL_ENGINE_APP_WIDTH;
    gEngine.frameHeight = CDL_ENGINE_APP_HEIGHT;
    gEngine.aspectRatio = gEngine.windowWidth / gEngine.windowHeight;
    glViewport(0, 0, gEngine.windowWidth, gEngine.windowHeight);
    
    // --- Threads ---
    
    // TODO(abe): Query CPU to know how much threads we should create
#define THREAD_COUNT (4)
    
    thread_work_queue highQueue = thread_work_queue_create(THREAD_COUNT);
    thread_work_queue lowQueue = thread_work_queue_create(THREAD_COUNT);
    
    thread threads[THREAD_COUNT] = { };
    thread_spawn(THREAD_COUNT, (thread*)threads, &highQueue);
    
    engineMemory.highQueue = &highQueue;
    engineMemory.lowQueue = &lowQueue;
    
    PlatformRegisterEvent(cdl_engine_platform_event{PlatformEventType_application_start});
    
    //- Loading Application Dylib
    char dylibFullPath[OSX_FILEPATH_COUNT];
    OSXGetExecutableFilename(&state);
    OSXBuildFilepathInAppFolder(&state, "engine.dylib", dylibFullPath, OSX_FILEPATH_COUNT);
    
    //- Time
    f32 applicationStartupTime = glfwGetTime();
    local u32 totalFrameCount = 0;
    
    f32 averageUpdateTime = 0;
    
    //-
    //- Update Loop
    //-
    
    while ( gEngine.run ) {
        
        // NOTE(abe): use glfwPollEvents if the app needs real time update
        //               use glfwWaitEvents if the application doesn't need to be updated at
        //               a constant time interval (less CPU intensive)
        glfwPollEvents();
        PlatformProcessInputs(glfwWindow);
        
        size_t engineCodeUpdate = OSXGetLastUpdateTime(dylibFullPath);
        if ( engineCodeUpdate > engineDLL.dylibLastUpadteTime ) {
            OSXUnloadApplicationDylib(&engineDLL);
            sleep(1);
            engineDLL = OSXLoadApplicationDylib("engine.dylib", "engine_locked.dylib");
            log_info("Hot reloading !\n");
            if ( engineDLL.isValid ) {
                engineDLL.HotReload(&engineMemory);
            }
        }
        
        OSXCheckForRecordOrPlayback(glfwWindow, &state);
        
        if ( gEngine.recordInputs ) {
            OSXInputRecording(&state);
        }
        
        if ( gEngine.playbackInputs ) {
            OSXPlaybackInputs(&state);
        }
        
        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT);
        
        f32 startUpdate = glfwGetTime();
        // TODO(abe): give the time to advance per frame to the engine
        if ( engineDLL.isValid ) {
            engineDLL.Update(&engineMemory);
        }
        
        f32 elapsedUpdateTime = (f32)glfwGetTime() - startUpdate;
        averageUpdateTime += elapsedUpdateTime;
        gEngine.processingTime = elapsedUpdateTime;
        
		glfwSwapBuffers(glfwWindow);
        PlatformResetEventQueue();
        
		f32 totalFrameTime = (f32)glfwGetTime() - startUpdate;
        gEngine.totalTime += totalFrameTime;
        gEngine.deltaTime = totalFrameTime;
        
        totalFrameCount++;
    }
    
    f32 totalTimeElapsed = glfwGetTime() - applicationStartupTime;
    log_msg("Average FPS: %.3f\n", totalFrameCount / totalTimeElapsed);
    log_msg("Engine Update average time: %.3fms\n", 1000.f * averageUpdateTime / totalFrameCount);
    
    OSXUnloadApplicationDylib(&engineDLL);
    
    glfwTerminate();
    return 0;
}

#endif