internal void OSXBeginInputRecording(osx_state* state, int inputSlot)
{
    char fullFilepath[OSX_FILEPATH_COUNT];
    OSXBuildFilepathInAppFolder(state, "inputrecording.cdli", fullFilepath, OSX_FILEPATH_COUNT);
    
    state->recordingFile = open(fullFilepath, O_WRONLY | O_CREAT, 0644);
    if ( state->recordingFile != -1 ) {
        gEngine.recordInputs = true;
        ssize_t bytesWritten = write(state->recordingFile, state->engineMemoryBlock, state->engineMemoryBlockSize);
        
        if ( bytesWritten != state->engineMemoryBlockSize ) {
            log_err("OSXBeginInputRecording: wrote %d bytes in input recording file, but memory block is %d bytes\n", bytesWritten, state->engineMemoryBlockSize);
            gEngine.recordInputs = false;
        }
        
    } else {
        gEngine.recordInputs = false;
        log_err("OSXBeginInputRecording: couldn't open input record file %s\n", fullFilepath);
    }
}

internal void OSXEndInputRecording(osx_state* state)
{
    close(state->recordingFile);
    state->recordingFile = 0;
}

internal void OSXBeginInputPlayback(osx_state* state, int inputSlot)
{
    char fullFilepath[OSX_FILEPATH_COUNT];
    OSXBuildFilepathInAppFolder(state, "inputrecording.cdli", fullFilepath, OSX_FILEPATH_COUNT);
    
    state->playbackFile = open(fullFilepath, O_RDONLY);
    if ( state->playbackFile != -1 ) {
        gEngine.playbackInputs = true;
        ssize_t bytesRead = read(state->playbackFile, state->engineMemoryBlock, state->engineMemoryBlockSize);
        if ( bytesRead == state->engineMemoryBlockSize ) {
            
        } else {
            log_warn("OSXBeginInputPlayback: engine memory was not restored properly\n");
            gEngine.playbackInputs = false;
            close(state->playbackFile);
            state->playbackFile = 0;
        }
    } else {
        gEngine.playbackInputs = false;
        log_err("Win32BeginInputPlayback: couldn't open input record file %s\n", fullFilepath);
    }
    
}

internal void OSXEndInputPlayback(osx_state* state)
{
    close(state->playbackFile);
    state->playbackFile = 0;
}

internal void OSXInputRecording(osx_state* state)
{
    ssize_t bytesWritten;
    bytesWritten = write(state->recordingFile, gEngine.currentKeyboard.keys, sizeof(gEngine.currentKeyboard));
    bytesWritten = write(state->recordingFile, gEngine.currentControllers, sizeof(gEngine.currentControllers));
    bytesWritten = write(state->recordingFile, &gEngine.currentMouse, sizeof(gEngine.currentMouse));
}

internal void OSXPlaybackInputs(osx_state* state)
{
    ssize_t bytesRead;
    bytesRead = read(state->playbackFile, gEngine.currentKeyboard.keys, sizeof(gEngine.currentKeyboard));
    if ( bytesRead == sizeof(gEngine.currentKeyboard) ) {
        bytesRead = read(state->playbackFile, gEngine.currentControllers, sizeof(gEngine.currentControllers));
        if ( bytesRead == sizeof(gEngine.currentControllers) ) {
            bytesRead = read(state->playbackFile, &gEngine.currentMouse, sizeof(gEngine.currentMouse));
            if ( bytesRead == sizeof(gEngine.currentMouse) ) {
                
            } else {
                OSXEndInputPlayback(state);
                OSXBeginInputPlayback(state, gEngine.playbackSlot);
                log_warn("OSXPlaybackInputs: end of input .cdli file\n");
            }
        } else {
            OSXEndInputPlayback(state);
            OSXBeginInputPlayback(state, gEngine.playbackSlot);
            log_warn("OSXPlaybackInputs: end of input .cdli file\n");
        }
    } else {
        OSXEndInputPlayback(state);
        OSXBeginInputPlayback(state, gEngine.playbackSlot);
        log_warn("OSXPlaybackInputs: end of input .cdli file\n");
    }
}

internal void OSXCheckForRecordOrPlayback(GLFWwindow* appWindow, osx_state* state)
{
    // TODO(abe): only for debug and tweaking purpose (debug builds)
    if ( gEngine.currentKeyboard.keys[KEY_L] && !gEngine.previousKeyboard.keys[KEY_L] && gEngine.currentKeyboard.keys[KEY_LEFT_SHIFT] ) {
        if ( gEngine.recordInputs ) {
            gEngine.recordInputs = false;
            log_msg("Leaving input recording mode (slot %d)\n", gEngine.recordingSlot);
            OSXEndInputRecording(state);
        } else {
            if ( gEngine.playbackInputs ) {
                log_err("Begin input was recorded\n");
                return;
            }
            gEngine.recordingSlot = 1;
            log_msg("Entering input recording mode (slot %d)\n", gEngine.recordingSlot);
            OSXBeginInputRecording(state, gEngine.recordingSlot);
            ARRAY_SET(gEngine.currentKeyboard.keys, 0);
            ARRAY_SET(gEngine.currentControllers[0].buttons, 0);
            ARRAY_SET(gEngine.currentControllers[0].axes, 0);
            ARRAY_SET(gEngine.currentMouse.buttons, 0);
        }
    }
    
    if ( gEngine.currentKeyboard.keys[KEY_L] && !gEngine.previousKeyboard.keys[KEY_L] && gEngine.currentKeyboard.keys[KEY_LEFT_ALT] && !gEngine.playbackInputs ) {
        if ( gEngine.recordInputs ) {
            log_warn("Stop input recording before entering playback mode (slot %d)\n", gEngine.recordingSlot);
        } else {
            gEngine.playbackSlot = gEngine.recordingSlot;
            gEngine.recordingSlot = 0;
            log_msg("Entering playback mode (slot %d)\n", gEngine.playbackSlot);
            OSXBeginInputPlayback(state, gEngine.playbackSlot);
        }
    }
    
    if ( gEngine.currentKeyboard.keys[KEY_F2] && gEngine.playbackInputs ) {
        gEngine.playbackInputs = false;
        log_msg("Leaving playback mode (slot %d)\n", gEngine.playbackSlot);
        OSXEndInputPlayback(state);
    }
    
    local bool32 isTranslucent = false;
    if ( gEngine.currentKeyboard.keys[KEY_F1] && !gEngine.previousKeyboard.keys[KEY_F1] ) {
        if ( !isTranslucent ) {
            glfwSetWindowOpacity(appWindow, 0.5f);
            glfwSetWindowAttrib(appWindow, GLFW_FLOATING, 1);
            isTranslucent = true;
        } else {
            glfwSetWindowOpacity(appWindow, 1.f);
            glfwSetWindowAttrib(appWindow, GLFW_FLOATING, 0);
            isTranslucent = false;
        }
    }
    
    // TODO(abe): choose to stop now or at the end of the recording
}