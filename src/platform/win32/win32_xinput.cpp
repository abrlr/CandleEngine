PLATFORM_INPUT_SET_CONTROLLER_VIBRATION(Platform_SetControllerVibration)
{
    XINPUT_VIBRATION state;
    state.wLeftMotorSpeed = lowFreqValue * 65535;
    state.wRightMotorSpeed = highFreqValue * 65535;
    
    XInputSetState(controller, &state);
}