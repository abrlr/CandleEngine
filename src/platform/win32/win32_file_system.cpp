struct win32_fs_iterator_data {
    HANDLE handle;
    WIN32_FIND_DATA ffd;
};

PLATFORM_FS_ITERATOR_NEW(Platform_FSIteratorNew)
{
    cdl_filesystem_iterator result = ZeroStruct;
    result.valid = 1;
    result.isRecursive = recursive;
    MemoryCopy(result.path, directory, CDL_MAX_PATH);
    
    win32_fs_iterator_data* nativeData = (win32_fs_iterator_data*)&result.nativeData;
    nativeData->handle = INVALID_HANDLE_VALUE;
    
    return result;
}

internal void Platform_FSHandleFile(cdl_filesystem_iterator* it)
{
    win32_fs_iterator_data* nativeData = (win32_fs_iterator_data*)&it->nativeData;
    LARGE_INTEGER fileSize;
    
    if ( nativeData->ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY ) {
        it->currentIsDir = 1;
        
        bool8 isUp = MemoryCompare(nativeData->ffd.cFileName, "..", StringLength(nativeData->ffd.cFileName));
        bool8 isCurrent = MemoryCompare(nativeData->ffd.cFileName, ".", StringLength(nativeData->ffd.cFileName));
        bool8 isUpOrCurrent = isUp || isCurrent;
        
        if ( !isUpOrCurrent ) {
            MemoryCopy(it->currentPath, nativeData->ffd.cFileName, CDL_MAX_PATH);
            log_info("Dir: %s\n", it->currentPath);
        }
    }
    
    else {
        it->currentIsDir = 0;
        
        fileSize.LowPart = nativeData->ffd.nFileSizeLow;
        fileSize.HighPart = nativeData->ffd.nFileSizeHigh;
        it->currentSize = fileSize.QuadPart;
        MemoryCopy(it->currentPath, nativeData->ffd.cFileName, CDL_MAX_PATH);
        log_info("File: %s (%d bytes)\n", it->currentPath, it->currentSize);
    }
}

PLATFORM_FS_ITERATE(Platform_FSIterate)
{
    win32_fs_iterator_data* nativeData = (win32_fs_iterator_data*)&it->nativeData;
    
    TCHAR szDir[CDL_MAX_PATH];
    DWORD dwError = 0;
    
    ConcatString(StringLength(it->path), it->path, 2, "/*", CDL_MAX_PATH, szDir);
    
    if ( nativeData->handle == INVALID_HANDLE_VALUE ) {
        nativeData->handle = FindFirstFile(szDir, &nativeData->ffd);
        Platform_FSHandleFile(it);
    } 
    
    else {
        bool8 found = FindNextFile(nativeData->handle, &nativeData->ffd);
        it->valid = found;
        
        if ( found ) {
            Platform_FSHandleFile(it);
        }
    }
}