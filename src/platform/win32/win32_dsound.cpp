#define D_SOUND_CREATE(name) HRESULT WINAPI name(LPCGUID lpGUID, LPDIRECTSOUND *ppDS, LPUNKNOWN pUnkOuter)
typedef D_SOUND_CREATE(d_sound_create);

internal bool8 Platform_InitDSound(i32 bufferSize, i32 samplesPerSeconds)
{
    HWND window = platform->nativeWindow;
    IDirectSoundBuffer** soundBuffer = (IDirectSoundBuffer**)&platform->audio.nativeSoundBuffer;
    
    HMODULE dSoundlib = LoadLibraryA("dsound.dll");
    if ( dSoundlib ) {
        
        d_sound_create* directSoundCreate = (d_sound_create*)GetProcAddress(dSoundlib, "DirectSoundCreate");
        IDirectSound* directSound;
        
        if ( directSoundCreate && SUCCEEDED(directSoundCreate(0, &directSound, 0)) ) {
            
            WAVEFORMATEX waveformat = ZeroStruct;
            waveformat.wFormatTag = WAVE_FORMAT_PCM;
            waveformat.nChannels = 2;
            waveformat.nSamplesPerSec = samplesPerSeconds;
            waveformat.wBitsPerSample = 16;
            waveformat.nBlockAlign = (waveformat.nChannels * waveformat.wBitsPerSample) / 8;
            waveformat.nAvgBytesPerSec = waveformat.nBlockAlign * waveformat.nSamplesPerSec;
            waveformat.cbSize = 0;
            
            if ( SUCCEEDED(directSound->SetCooperativeLevel(window, DSSCL_PRIORITY)) ) {
                
                DSBUFFERDESC bufferDescription = ZeroStruct;
                bufferDescription.dwSize = sizeof(bufferDescription);
                bufferDescription.dwFlags = DSBCAPS_PRIMARYBUFFER | DSBCAPS_GETCURRENTPOSITION2;
                IDirectSoundBuffer* primaryBuffer;
                
                if ( SUCCEEDED(directSound->CreateSoundBuffer(&bufferDescription, &primaryBuffer, 0)) ) {
                    if ( SUCCEEDED(primaryBuffer->SetFormat(&waveformat)) ) {
                        // TODO(abe): buffer successfully created
                    } else {
                        // TODO(abe): logging
                    }
                } else {
                    // TODO(abe): logging
                }
            } else {
                // TODO(abe): logging
            }
            
            DSBUFFERDESC bufferDescription = ZeroStruct;
            bufferDescription.dwSize = sizeof(bufferDescription);
            bufferDescription.dwFlags = DSBCAPS_GETCURRENTPOSITION2;
            bufferDescription.dwBufferBytes = bufferSize;
            bufferDescription.lpwfxFormat = &waveformat;
            
            if ( SUCCEEDED(directSound->CreateSoundBuffer(&bufferDescription, &*soundBuffer, 0)) ) {
                // TODO(abe): logging
            } else {
                // TODO(abe): logging
            }
            
        } else {
            // TODO(abe): logging
        }
    } else {
        // TODO(abe): logging
    }
    
    return 1;
}

internal void Platform_ClearAudioBuffer()
{
    IDirectSoundBuffer* soundBuffer = (IDirectSoundBuffer*)platform->audio.nativeSoundBuffer;
    void* region1; void* region2;
    DWORD size1, size2;
    
    if ( SUCCEEDED(soundBuffer->Lock(0, platform->audio.audioBufferSize, &region1, &size1, &region2, &size2, 0)) ) {
        
        u8* destSamplePtr = (u8*)region1;
        for (DWORD byteIndex = 0; byteIndex < size1; byteIndex++) {
            *destSamplePtr++ = 0;
        }
        
        destSamplePtr = (u8*)region2;
        for (DWORD byteIndex = 0; byteIndex < size2; byteIndex++) {
            *destSamplePtr++ = 0;
        }
        
        soundBuffer->Unlock(region1, size1, region2, size2);
    }
}

internal void Platform_FillAudioBuffer(DWORD byteToLock, DWORD bytesToWrite,
                                       cdl_audio_native_buffer* sourceAudioBuffer)
{
    IDirectSoundBuffer* soundBuffer = (IDirectSoundBuffer*)platform->audio.nativeSoundBuffer;
    void* region1; 
    void* region2;
    DWORD size1, size2;
    
    if ( SUCCEEDED(soundBuffer->Lock(byteToLock, bytesToWrite, &region1, &size1, &region2, &size2, 0)) ) {
        DWORD region1SampleCount = size1 / platform->audio.bytesPerSample;
        u16* destSamplePtr = (u16*)region1;
        i16* srcSamplePtr = sourceAudioBuffer->samples;
        for (DWORD sampleIndex = 0; sampleIndex < region1SampleCount; sampleIndex++) {
            *destSamplePtr++ = *srcSamplePtr++;
            *destSamplePtr++ = *srcSamplePtr++;
            platform->audio.runningSampleIndex++;
        }
        
        DWORD region2SampleCount = size2 / platform->audio.bytesPerSample;
        destSamplePtr = (u16*)region2;
        for (DWORD sampleIndex = 0; sampleIndex < region2SampleCount; sampleIndex++) {
            *destSamplePtr++ = *srcSamplePtr++;
            *destSamplePtr++ = *srcSamplePtr++;
            platform->audio.runningSampleIndex++;
        }
        
        soundBuffer->Unlock(region1, size1, region2, size2);
    }
}

internal void Platform_UpdatePlayCursor()
{
    DWORD writeCursor = 0;
    DWORD playCursor = 0;
    IDirectSoundBuffer* soundBuffer = (IDirectSoundBuffer*)platform->audio.nativeSoundBuffer;
    cdl_platform_audio* a = &platform->audio;
    
    if ( SUCCEEDED(soundBuffer->GetCurrentPosition(&playCursor, &writeCursor)) ) {
        if ( !a->valid ) {
            a->runningSampleIndex = writeCursor / a->bytesPerSample;
        }
        
        a->valid = true;
        a->lastPlayCursor = playCursor;
    } else {
        a->valid = false;
    }
}

internal bool8 Platform_InitAudioSystem()
{
    bool8 result = 0;
    core->mixer.volume = 1;
    
    cdl_platform_audio* a = &platform->audio;
    a->samplesPerSecond = 48000;
    a->bytesPerSample = sizeof(i16) * 2;
    a->audioBufferSize = a->samplesPerSecond * a->bytesPerSample;
    a->latencySampleCount = 3 * ( a->samplesPerSecond / 30 );
    a->soundSamples = (i16*)VirtualAlloc(0, a->audioBufferSize, MEM_RESERVE | MEM_COMMIT, PAGE_READWRITE);
    
    if ( platform->audio.soundSamples ) {
        if ( Platform_InitDSound(a->audioBufferSize, a->samplesPerSecond) ) {
            Platform_ClearAudioBuffer();
            
            IDirectSoundBuffer* soundBuffer = (IDirectSoundBuffer*)platform->audio.nativeSoundBuffer;
            soundBuffer->Play(0, 0, DSBPLAY_LOOPING);
            
            cdl_audio_native_buffer appAudioBuffer = ZeroStruct;
            appAudioBuffer.samples = a->soundSamples;
            appAudioBuffer.samplesPerSecond = a->samplesPerSecond;
            core->mixer.native = appAudioBuffer;
            
            result = 1;
        }
    } else {
        log_err("Error while allocating sound buffer\n");
    }
    
    return result;
}