internal void Platform_RecordInputs()
{
    DWORD bytesWritten;
    cdl_engine_events* evs = &core->events;
    WriteFile(platform->recordingFile, evs->currentKeyboard.keys, sizeof(evs->currentKeyboard.keys), &bytesWritten, 0);
    WriteFile(platform->recordingFile, evs->currentControllers, sizeof(evs->currentControllers), &bytesWritten, 0);
    WriteFile(platform->recordingFile, &evs->currentMouse, sizeof(evs->currentMouse), &bytesWritten, 0);
}

internal void Platform_BeginInputRecording(int inputSlot)
{
    char fullFilepath[CDL_MAX_PATH];
    Platform_BuildFilepathInAppFolder("inputrecording.cdli", fullFilepath, CDL_MAX_PATH);
    
    platform->recordingFile = CreateFileA(fullFilepath, GENERIC_WRITE, 0, 0, CREATE_ALWAYS, 0, 0);
    if ( platform->recordingFile != INVALID_HANDLE_VALUE ) {
        core->recordInputs = true;
        DWORD bytesWritten;
        WriteFile(platform->recordingFile, platform->engineMemoryBlock, (DWORD)platform->engineMemoryBlockSize, &bytesWritten, 0);
    } else {
        core->recordInputs = false;
        log_err("Couldn't open input record file %s\n", fullFilepath);
    }
}

internal void Platform_EndInputRecording()
{
    CloseHandle(platform->recordingFile);
    platform->recordingFile = 0;
}

//- Input Playback
internal void Platform_BeginInputPlayback(int inputSlot)
{
    char fullFilepath[CDL_MAX_PATH];
    Platform_BuildFilepathInAppFolder("inputrecording.cdli", fullFilepath, CDL_MAX_PATH);
    
    platform->playbackFile = CreateFileA(fullFilepath, GENERIC_READ, 0, 0, OPEN_EXISTING, 0, 0);
    if ( platform->playbackFile != INVALID_HANDLE_VALUE ) {
        core->playbackInputs = true;
        DWORD bytesRead;
        if ( ReadFile(platform->playbackFile, platform->engineMemoryBlock, (DWORD)platform->engineMemoryBlockSize, &bytesRead, 0) ) {
            
        } else {
            log_warn("Engine memory was not restored properly\n");
            core->playbackInputs = false;
            CloseHandle(platform->playbackFile);
            platform->playbackFile = 0;
        }
    } else {
        core->playbackInputs = false;
        log_err("Couldn't open input record file %s\n", fullFilepath);
    }
}

internal void Platform_EndInputPlayback()
{
    CloseHandle(platform->playbackFile);
    platform->playbackFile = 0;
}

internal void Platform_PlaybackInputs()
{
    DWORD bytesRead;
    cdl_engine_events* evs = &core->events;
    if ( ReadFile(platform->playbackFile, evs->currentKeyboard.keys, sizeof(evs->currentKeyboard.keys), &bytesRead, 0) && bytesRead ) {
        if ( ReadFile(platform->playbackFile, evs->currentControllers, sizeof(evs->currentControllers), &bytesRead, 0) ) {
            if ( ReadFile(platform->playbackFile, &evs->currentMouse, sizeof(evs->currentMouse), &bytesRead, 0)) {
                
            } else {
                Platform_EndInputPlayback();
                Platform_BeginInputPlayback(core->playbackSlot);
                log_warn("End of input .cdli file\n");
            }
        } else {
            Platform_EndInputPlayback();
            Platform_BeginInputPlayback(core->playbackSlot);
            log_warn("End of input .cdli file\n");
        }
    } else {
        Platform_EndInputPlayback();
        Platform_BeginInputPlayback(core->playbackSlot);
        log_warn("End of input .cdli file\n");
    }
}

internal void Platform_CheckForRecordOrPlayback()
{
    cdl_engine_events* evs = &core->events;
    // TODO(abe): only for debug and tweaking purpose (debug builds)
    if ( evs->currentKeyboard.keys[KEY_L] && !evs->previousKeyboard.keys[KEY_L] && evs->currentKeyboard.keys[KEY_LEFT_SHIFT] ) {
        if ( core->recordInputs ) {
            core->recordInputs = false;
            log_msg("Leaving input recording mode (slot %d)\n", core->recordingSlot);
            Platform_EndInputRecording();
        } else {
            if ( core->playbackInputs ) {
                log_err("Begin input was recorded\n");
                return;
            }
            core->recordingSlot = 1;
            log_msg("Entering input recording mode (slot %d)\n", core->recordingSlot);
            Platform_BeginInputRecording(core->recordingSlot);
            ARRAY_SET(evs->currentKeyboard.keys, 0);
            ARRAY_SET(evs->currentControllers[0].buttons, 0);
            ARRAY_SET(evs->currentControllers[0].axes, 0);
            ARRAY_SET(evs->currentMouse.buttons, 0);
        }
    }
    
    if ( evs->currentKeyboard.keys[KEY_L] && !evs->previousKeyboard.keys[KEY_L] && evs->currentKeyboard.keys[KEY_LEFT_ALT] && !core->playbackInputs ) {
        if ( core->recordInputs ) {
            log_warn("Stop input recording before entering playback mode (slot %d)\n", core->recordingSlot);
        } else {
            core->playbackSlot = core->recordingSlot;
            core->recordingSlot = 0;
            log_msg("Entering playback mode (slot %d)\n", core->playbackSlot);
            Platform_BeginInputPlayback(core->playbackSlot);
        }
    }
    
    if ( evs->currentKeyboard.keys[KEY_INSERT] && core->playbackInputs ) {
        core->playbackInputs = false;
        log_msg("Leaving playback mode (slot %d)\n", core->playbackSlot);
        Platform_EndInputPlayback();
    }
    
    local bool8 isTranslucent = false;
    if ( evs->currentKeyboard.keys[KEY_HOME] && !evs->previousKeyboard.keys[KEY_HOME] ) {
        if ( !isTranslucent ) {
            glfwSetWindowOpacity(platform->glfwWindow, 0.5f);
            glfwSetWindowAttrib(platform->glfwWindow, GLFW_FLOATING, 1);
            isTranslucent = true;
        } else {
            glfwSetWindowOpacity(platform->glfwWindow, 1.f);
            glfwSetWindowAttrib(platform->glfwWindow, GLFW_FLOATING, 0);
            isTranslucent = false;
        }
    }
    
    // TODO(abe): choose to stop now or at the end of the recording
}