internal bool8 Platform_InitializeEngineMemory()
{
    bool8 initialized = 1;
    
    u64 permanentContainerSize = PERMANENT_MEMORY_SIZE;
    u64 frameContainerSize = FRAME_MEMORY_SIZE;
    u64 transientContainerSize = TRANSIENT_MEMORY_SIZE;
    u64 totalMemorySize = permanentContainerSize + frameContainerSize + transientContainerSize;
    platform->engineMemoryBlockSize = permanentContainerSize;
    
    LPVOID baseAddress = (LPVOID)MegaBytes(2 * (u64)1024);
    platform->engineMemoryBlock = VirtualAlloc(baseAddress, (SIZE_T)totalMemorySize, MEM_RESERVE | MEM_COMMIT, PAGE_READWRITE);
    
    if ( !platform->engineMemoryBlock ) {
        log_err("Engine Memory Allocation Failed");
        initialized = 0;
    }
    
    u8* memoryBlockAddress = (u8*)platform->engineMemoryBlock;
    memory_block_new(&core->memory.permanent, memoryBlockAddress, permanentContainerSize);
    
    memoryBlockAddress += permanentContainerSize;
    memory_block_new(&core->memory.frame, memoryBlockAddress, frameContainerSize);
    
    memoryBlockAddress += frameContainerSize;
    memory_block_new(&core->memory.transient, memoryBlockAddress, transientContainerSize);
    
    return initialized;
}