internal void Platform_SetWindow(GLFWwindow* glfwWindow)
{
    platform->glfwWindow = glfwWindow;
    platform->nativeWindow = glfwGetWin32Window(glfwWindow);
}

internal void Platform_StartTimers()
{
    platform->sleepIsGranular = ( timeBeginPeriod(1) == TIMERR_NOERROR );
    QueryPerformanceFrequency(&platform->pcFrequency);
    QueryPerformanceCounter(&platform->appStart); 
}

internal void Platform_StartFrame()
{
    QueryPerformanceCounter(&platform->frameStart);
    core->rng.value = platform->frameStart.QuadPart;
    
    //- Input Record & Playback
    Platform_CheckForRecordOrPlayback();
    if ( core->recordInputs ) {
        Platform_RecordInputs();
    } else if ( core->playbackInputs ) {
        Platform_PlaybackInputs();
    }
    
    //- Sound Pre Processing
    cdl_platform_audio* a = &platform->audio;
    if ( a->valid ) {
        a->byteToLock = (( a->runningSampleIndex * a->bytesPerSample ) % a->audioBufferSize);
        
        a->targetCursor = (( a->lastPlayCursor + (a->latencySampleCount * a->bytesPerSample) ) % a->audioBufferSize);
        
        a->bytesToWrite = 0;
        
        if ( a->byteToLock > a->targetCursor ) {
            a->bytesToWrite = (a->audioBufferSize - a->byteToLock);
            a->bytesToWrite += a->targetCursor;
        }
        
        else {
            a->bytesToWrite = (a->targetCursor - a->byteToLock);
        }
    }
    
    core->mixer.native.sampleCount = a->bytesToWrite / a->bytesPerSample;
    
    //- Timer
    QueryPerformanceCounter(&platform->appUpdateStart);
}

internal void Platform_EndFrame()
{
    QueryPerformanceCounter(&platform->appUpdateEnd);
    
    i64 elapsedCounters = platform->appUpdateEnd.QuadPart - platform->appUpdateStart.QuadPart;
    f32 elapsedMS = ( (f64)elapsedCounters / platform->pcFrequency.QuadPart );
    
    core->processingTime = elapsedMS;
    
    //- Process Audio
    cdl_platform_audio* a = &platform->audio;
    if ( a->valid ) {
        Platform_FillAudioBuffer(a->byteToLock, a->bytesToWrite, &core->mixer.native);
    }
    Platform_UpdatePlayCursor();
    
    //- Wait for Target FPS
    LARGE_INTEGER frameEnd;
    QueryPerformanceCounter(&frameEnd);
    
    f64 targetSecondsPerFrame = (f64)(1.f / platform->targetFramesPerSecond);
    i64 elapsedCount = frameEnd.QuadPart - platform->frameStart.QuadPart;
    i64 desiredCount = (i64)(targetSecondsPerFrame * platform->pcFrequency.QuadPart);
    i64 waitCount = desiredCount - elapsedCount;
    
    LARGE_INTEGER waitStart;
    LARGE_INTEGER waitEnd;
    
    QueryPerformanceCounter(&waitStart);
    
    while (waitCount > 0) {
        
        if (platform->sleepIsGranular) {
            DWORD msToSleep = (DWORD)(1000.0 * ((f64)(waitCount) / platform->pcFrequency.QuadPart));
            if ( msToSleep > 0 ) {
                Sleep(msToSleep);
            }
        }
        
        QueryPerformanceCounter(&waitEnd);
        waitCount -= waitEnd.QuadPart - waitStart.QuadPart;
        waitStart = waitEnd;
    }
    
    //- Final Frame Time
    QueryPerformanceCounter(&frameEnd);
    elapsedCounters = frameEnd.QuadPart - platform->frameStart.QuadPart;
    platform->frameTime = ( (f64)elapsedCounters / platform->pcFrequency.QuadPart );
}