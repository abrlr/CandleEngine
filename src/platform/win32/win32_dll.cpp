internal bool8 Platform_LoadApp(char* name, char* nameLocked)
{
    bool8 result = 1;
    local i32 uniqueID = 1;
    
    char appFullPath[CDL_MAX_PATH];
    Platform_BuildFilepathInAppFolder(name, appFullPath, CDL_MAX_PATH);
    
    char appLockedFullPath[CDL_MAX_PATH];
    Platform_BuildFilepathInAppFolder(nameLocked, appLockedFullPath, CDL_MAX_PATH, uniqueID++);
    
    DWORD appFullAttrib = GetFileAttributesA(appFullPath);
    DWORD appLockedFullAttrib = GetFileAttributesA(appLockedFullPath);
    
    bool8 copyDone = CopyFileA(appFullPath, appLockedFullPath, FALSE);
    if ( copyDone ) {
        platform->appHandle = LoadLibraryA(appLockedFullPath);
        if ( platform->appHandle ) {
            platform->AppInit = (EngineInitFunc*)GetProcAddress(platform->appHandle, "EngineInit");
            platform->AppReload = (EngineHotReloadFunc*)GetProcAddress(platform->appHandle, "EngineHotReload");
            platform->AppUpdate = (EngineUpdateFunc*)GetProcAddress(platform->appHandle, "EngineUpdateAndRender");
            platform->AppSetPlatform = (EngineSetPlatformFunc*)GetProcAddress(platform->appHandle, "EngineSetPlatform");
            platform->AppClean = (EngineCleanFunc*)GetProcAddress(platform->appHandle, "EngineClean");
            platform->appCodeIsValid = ( platform->AppReload && platform->AppUpdate && platform->AppSetPlatform );
        } else {
            result = 0;
            log_err("Couldn't open application dll\n");
        }
    } else {
        result = 0;
        DWORD lastError = GetLastError();
        log_err("Error trying to copy %s to %s: %d\n", appFullPath, appLockedFullPath, lastError);
    }
    
    if ( !platform->appCodeIsValid ) {
        platform->AppInit = 0;
        platform->AppReload = 0;
        platform->AppUpdate = 0;
        platform->AppSetPlatform = 0;
        platform->AppClean = 0;
    } else {
        platform->appLastUpdateTime = Platform_GetLastUpdateTime(appFullPath);
    }
    
    return result;
}

internal void Platform_UnloadApp()
{
    if ( platform->appCodeIsValid ) {
        FreeLibrary(platform->appHandle);
    }
    
    platform->appCodeIsValid = 0;
    platform->AppReload = 0;
    platform->AppUpdate = 0;
    platform->AppSetPlatform = 0;
    platform->AppClean = 0;
}

internal void Platform_TryReloadApp(char* name, char* nameLocked)
{
    char appFullPath[CDL_MAX_PATH];
    Platform_BuildFilepathInAppFolder(name, appFullPath, CDL_MAX_PATH);
    
    FILETIME appCodeUpdateTime = Platform_GetLastUpdateTime(appFullPath);
    if ( CompareFileTime(&appCodeUpdateTime, &platform->appLastUpdateTime) ) {
        Platform_UnloadApp();
        Platform_LoadApp(name, nameLocked);
        
        if ( platform->appCodeIsValid ) {
            platform->AppSetPlatform(platform, core);
            platform->AppReload(&core->memory);
        }
    }
}