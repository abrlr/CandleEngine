PLATFORM_WINDOW_SETTITLE(Platform_WindowSetTitle)
{
    if ( !SetWindowTextA(platform->nativeWindow, windowTitle) ) {
        log_err("Could not set the window title\n");
    }
}

// TODO(abe): add the possibility to change the full screen monitor
PLATFORM_WINDOW_SETFULLSCREEN(Platform_WindowSetFullScreen)
{
    //- Full Screen 
    if ( toggle ) {
        i32 monitorCount = 0;
        GLFWmonitor* monitor = glfwGetMonitors(&monitorCount)[0];
        GLFWvidmode* mode = (GLFWvidmode*)glfwGetVideoMode(monitor);
        
        glfwSetWindowMonitor(platform->glfwWindow, monitor, 0, 0, mode->width, mode->height, mode->refreshRate);
        PlatformWindowResizeCallback(platform->glfwWindow, mode->width, mode->height);
    }
    
    //- Windowed 
    else {
        glfwSetWindowMonitor(platform->glfwWindow, 0, 200, 200, CDL_ENGINE_APP_WIDTH, CDL_ENGINE_APP_HEIGHT, 60);
        PlatformWindowResizeCallback(platform->glfwWindow, CDL_ENGINE_APP_WIDTH, CDL_ENGINE_APP_HEIGHT);
    }
}

PLATFORM_WINDOW_ISACTIVE(Platform_WindowIsActive)
{
    return (platform->nativeWindow == GetActiveWindow());
}

PLATFORM_WINDOW_REQUESTATTENTION(Platform_WindowRequestAttention)
{
    if ( !Platform_WindowIsActive() ) {
        FlashWindow(platform->nativeWindow, TRUE);
    }
}
