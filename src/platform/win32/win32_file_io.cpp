internal void Platform_GetLocations()
{
    // Get Working Directory
    DWORD sizeofFilename = GetCurrentDirectoryA(sizeof(platform->workingDirectory), platform->workingDirectory);
    platform->workingDirectory[sizeofFilename] = '\\';
    log_msg("Working Directory is \"%s\".\n", platform->workingDirectory);
}

internal void Platform_BuildFilepathInAppFolder(char* filename, char* targetFilepath, i32 targetSize, i32 uniqueID = 0)
{
    if ( uniqueID ) {
        char uniqueFileName[CDL_MAX_PATH] = { };
        sprintf(uniqueFileName, "%d_%s", uniqueID, filename);
        ConcatString(StringLength(platform->workingDirectory), platform->workingDirectory,
                     StringLength(uniqueFileName), uniqueFileName,
                     targetSize, targetFilepath);
    } else {
        ConcatString(StringLength(platform->workingDirectory), platform->workingDirectory,
                     StringLength(filename), filename,
                     targetSize, targetFilepath);
    }
}

internal FILETIME Platform_GetLastUpdateTime(char* filename)
{
    FILETIME lastUpdate = ZeroStruct;
    WIN32_FILE_ATTRIBUTE_DATA filedata;
    if ( GetFileAttributesEx(filename, GetFileExInfoStandard, &filedata) ) {
        lastUpdate = filedata.ftLastWriteTime;
    }
    
    return lastUpdate;
}

PLATFORM_FREEFILEMEMORY(Platform_FreeFileMemory)
{
    if ( fileMemory ) {
        VirtualFree(fileMemory, 0, MEM_RELEASE);
    }  
}

PLATFORM_READENTIREFILE(Platform_ReadEntireFile)
{
    cdl_file result = ZeroStruct;
    HANDLE fileHandle = CreateFileA(filepath, GENERIC_READ, FILE_SHARE_READ, 0, OPEN_EXISTING, 0, 0);
    if ( fileHandle != INVALID_HANDLE_VALUE ) {
        LARGE_INTEGER filesize;
        if ( GetFileSizeEx(fileHandle, &filesize) ) {
            u32 filesize32 = SafeTruncateU64toU32(filesize.QuadPart);
            result.content = VirtualAlloc(0, filesize32, MEM_COMMIT | MEM_RESERVE, PAGE_READWRITE);
            if ( result.content ) {
                DWORD bytesRead;
                if ( ReadFile(fileHandle, result.content, (DWORD)filesize.QuadPart, &bytesRead, 0) && (bytesRead == filesize32) ) {
                    result.size = bytesRead;
                } else {
                    Platform_FreeFileMemory(result.content);
                    result.content = 0;
                    result.size = 0;
                    log_err("Win32ReadEntireFile: Error while reading file %d: bytesRead != filesize\n", filepath);
                }
            } else {
                log_err("Win32ReadEntireFile: Error while allocating memory\n");
            }
        } else {
            log_err("Win32ReadEntireFile: Couldn't get filesize for %s\n", filepath);
        }
        CloseHandle(fileHandle);
    } else {
        log_err("Win32ReadEntireFile: couldn't open file %s\n", filepath);
    }
    return result;
}

PLATFORM_WRITEENTIREFILE(Platform_WriteEntireFile)
{
    bool8 result = false;
    HANDLE fileHandle = CreateFileA(filepath, GENERIC_WRITE, 0, 0, CREATE_ALWAYS, 0, 0);
    if ( fileHandle != INVALID_HANDLE_VALUE ) {
        DWORD bytesWritten;
        if ( WriteFile(fileHandle, file->content, file->size, &bytesWritten, 0) ) {
            result = ( bytesWritten == file->size );
        } else {
            log_err("Win32WriteEntireFile: Error while writing to file %s\n", filepath);
        }
        CloseHandle(fileHandle);
    } else {
        log_err("Win32WriteEntireFile: Error while creating file %s\n", filepath);
    }
    return result;
}

PLATFORM_OPENFILEEXPLORER(Platform_OpenFileExplorer)
{
    const wchar_t* explorer_filter = L"All Files\0*.*\0"; 
    if ( filter ) {
        explorer_filter = (const wchar_t*)filter;
    }
    
    OPENFILENAME ofn;
    wchar_t filename[CDL_MAX_PATH] = L"";
    ZeroMemory(&ofn, sizeof(ofn));
    
    ofn.lStructSize = sizeof(OPENFILENAME);
    ofn.hwndOwner = NULL;
    ofn.lpstrFilter = filter;
    ofn.lpstrFile = (LPSTR)filename;
    ofn.nMaxFile = CDL_MAX_PATH;
    ofn.Flags = OFN_EXPLORER | OFN_FILEMUSTEXIST | OFN_HIDEREADONLY;
    ofn.lpstrDefExt = fileExtension;
    
    if ( GetOpenFileNameA(&ofn) ) {
        strcpy(openFilepath, (char*)filename);
        return true;
    }
    return false;
}

PLATFORM_SAVEFILEEXPLORER(Platform_SaveFileExplorer)
{
    OPENFILENAME ofn;
    wchar_t filename[CDL_MAX_PATH] = L"";
    ZeroMemory(&ofn, sizeof(ofn));
    
    ofn.lStructSize = sizeof(OPENFILENAME);
    ofn.hwndOwner = NULL;
    ofn.lpstrFilter = (LPSTR)L"All Files\0*.*\0";
    ofn.lpstrFile = (LPSTR)filename;
    ofn.nMaxFile = CDL_MAX_PATH;
    ofn.Flags = OFN_EXPLORER | OFN_FILEMUSTEXIST | OFN_HIDEREADONLY;
    ofn.lpstrDefExt = fileExtension;
    
    if ( GetSaveFileNameA(&ofn) ) {
        strcpy(saveFilepath, (char*)filename);
        return true;
    }
    return false;
}