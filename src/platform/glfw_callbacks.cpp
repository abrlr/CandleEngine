// NOTE(abe): see https://www.glfw.org/docs/latest/group__keys.html for more details on key odes
internal KeyboardKey_ KeyFromGLFWKeycode(int keycode)
{
    KeyboardKey_ key = KEY_UNKNOWN;
    if ( keycode == -1 ) {
        key = KEY_UNKNOWN;
    } else if ( keycode >= 44 && keycode <= 57 ) { // 0-9 keys and "," and "-"  and "." and "/" keys
        key = (KeyboardKey_)(keycode - 41);
    } else if ( keycode >= 65 && keycode <= 90 ) { // A-Z keys
        key = (KeyboardKey_)(keycode - 46);
    } else if ( keycode == 32 ) { // "   " key
        key = KEY_SPACE;
    } else if ( keycode == 39 ) { // " ' " key
        key = KEY_APOSTROPHE;
    } else if ( keycode == 59 ) { // ";" key
        key = KEY_SEMICOLON;
    }  else if ( keycode == 61 ) { // "=" key
        key = KEY_EQUAL;
    } else if ( keycode >= 91 && keycode <= 93 ) { // "[" and "\" and "]" keys
        key = (KeyboardKey_)(keycode - 45);
    } else if ( keycode == 96 ) { // " ` "
        key = KEY_GRAVE_ACCENT;
    } else if ( keycode >= 256 && keycode <= 269 ) { // special keys
        key = (KeyboardKey_)(keycode - 205);
    } else if ( keycode >= 280 && keycode <= 284 ) { // caps lock, scroll lock, num lock, print screen, pause
        key = (KeyboardKey_)(keycode - 215);
    }  else if ( keycode >= 290 && keycode <= 314 ) { // F1-F25
        key = (KeyboardKey_)(keycode - 220);
    } else if ( keycode >= 320 && keycode <= 336 ) {  // numeric keypad keys
        key = (KeyboardKey_)(keycode - 225);
    } else if ( keycode >= 340 && keycode <= 348) { // left & right shift, ctrl, super, alt and menu
        key = (KeyboardKey_)(keycode - 228);
    }
    
    // TODO(abe): finish keycode mapping
    
    return key;
}

internal void PlatformRegisterEvent(cdl_engine_platform_event event)
{
    cdl_engine_events* evs = &core->events;
    if ( evs->count < MAX_EVENT_PER_FRAME ) {
        evs->queue[evs->count++] = event;
        
        switch (event.type)
        {
            case PlatformEventType_application_start:
            {
                core->run = 1;
            } break;
            
            case PlatformEventType_application_close:
            {
                core->run = 0;
            } break;
            
            case PlatformEventType_application_resized:
            {
                core->windowWidth = event.window.size.width;
                core->windowHeight = event.window.size.height;
                core->frameWidth = event.window.frame.width;
                core->frameHeight = event.window.frame.height;
                core->frameBorderLeft = event.window.border.x;
                core->frameBorderTop = event.window.border.y;
				if ( !core->hasFixedAspectRatio )
					core->aspectRatio = core->windowWidth / core->windowHeight;
            } break;
            
            case PlatformEventType_key_pressed:
            {
                evs->currentKeyboard.keys[event.key] = true;
                //log_msg("Key pressed: %s (%d)\n", KeyboardKey__get_name(eKey).string, eKey);
            } break;
            
            case PlatformEventType_key_released:
            {
                evs->currentKeyboard.keys[event.key] = false;
            } break;
            
            case PlatformEventType_text:
            {
                evs->currentKeyboard.text[evs->currentKeyboard.numberOfChar++] = event.text;
            } break;
            
            case PlatformEventType_controller_connect:
            {
                log_warn("Joystick %d connected\n", event.controller.ID);
                evs->connectedControllers++;
            } break;
            
            case PlatformEventType_controller_disconnect:
            {
                log_warn("Joystick %d disconnected\n", event.controller.ID);
                evs->connectedControllers--;
            } break;
            
            case PlatformEventType_controller_button_pressed:
            {
                ControllerButton eButton = event.controller.button;
                evs->currentControllers[event.controller.ID].buttons[eButton] = true;
            } break;
            
            case PlatformEventType_controller_button_released:
            {
                ControllerButton eButton = event.controller.button;
                evs->currentControllers[event.controller.ID].buttons[eButton] = false;
            } break;
            
            case PlatformEventType_controller_axes_moved:
            {
                ControllerAxes eAxes = event.controller.axes;
                evs->currentControllers[event.controller.ID].axes[eAxes] = event.controller.axesAmount;
            } break;
            
            case PlatformEventType_mouse_enter:
            {
                evs->mouseInWindow = true;
            } break;
            
            case PlatformEventType_mouse_leave:
            {
                evs->mouseInWindow = false;
            } break;
            
            case PlatformEventType_mouse_pressed:
            {
                evs->currentMouse.buttons[event.mouse.button] = true;
            } break;
            
            case PlatformEventType_mouse_released:
            {
                evs->currentMouse.buttons[event.mouse.button] = false;
            } break;
            
            case PlatformEventType_mouse_scrolled:
            {
                evs->currentMouse.scroll = event.mouse.scroll;
            } break;
            
            case PlatformEventType_mouse_moved:
            {
                evs->currentMouse.position = event.mouse.position;
				evs->currentMouse.delta = {
					(f32)(event.mouse.position.x - evs->previousMouse.position.x),
					(f32)(event.mouse.position.y - evs->previousMouse.position.y),
				};
            } break;
            
            default:
            {
                
            } break;
        }
    }
}

internal void Platform_ResetEventQueue()
{
    cdl_engine_events* evs = &core->events;
    evs->count = 0;
    ARRAY_COPY(evs->currentKeyboard.keys, evs->previousKeyboard.keys);
    ARRAY_COPY(evs->currentKeyboard.text, evs->previousKeyboard.text);
    evs->previousKeyboard.numberOfChar = evs->currentKeyboard.numberOfChar;
    evs->currentKeyboard.numberOfChar = 0;
    ARRAY_COPY(evs->currentControllers, evs->previousControllers);
    evs->previousMouse = evs->currentMouse;
    evs->currentMouse.delta.x = 0;
    evs->currentMouse.delta.y = 0;
    evs->currentMouse.scroll.x = 0;
    evs->currentMouse.scroll.y = 0;
}

internal void PlatformWindowCloseCallback(GLFWwindow* window)
{
    PlatformRegisterEvent(cdl_engine_platform_event{PlatformEventType_application_close});
}

internal void PlatformWindowMovedCallback(GLFWwindow* window, int x, int y)
{
    cdl_engine_platform_event pEvent;
    pEvent.type = PlatformEventType_application_moved;
    pEvent.window.position.x = x;
    pEvent.window.position.y = y;
    
    glViewport(core->frameBorderLeft, core->frameBorderTop, core->frameWidth, core->frameHeight);
    PlatformRegisterEvent(pEvent);
}

internal void PlatformWindowResizeCallback(GLFWwindow* window, int width, int height)
{
    cdl_engine_platform_event pEvent;
    pEvent.type = PlatformEventType_application_resized;
    pEvent.window.size.width = width;
    pEvent.window.size.height = height;
    
	if ( core->hasFixedAspectRatio ) {
		if ( height * core->aspectRatio > width ) {
			// determinant dimension = width
			pEvent.window.frame.width = f32_floor(width);
			pEvent.window.frame.height = f32_floor(width / core->aspectRatio);
		} else {
			// determinant dimension = height
			pEvent.window.frame.height = f32_floor(height);
			pEvent.window.frame.width = f32_floor(core->aspectRatio * height);
		}
        
		pEvent.window.border.x = f32_floor( ( width - pEvent.window.frame.width ) / 2.f );
    	pEvent.window.border.y = f32_floor( ( height - pEvent.window.frame.height ) / 2.f );
	} else {
        
		pEvent.window.frame.width = width;
		pEvent.window.frame.height = height;
		pEvent.window.border.x = 0;
		pEvent.window.border.y = 0;
        
	}
    
    glViewport(pEvent.window.border.x, pEvent.window.border.y, pEvent.window.frame.width, pEvent.window.frame.height);
    
    PlatformRegisterEvent(pEvent);
}

internal void PlatformDragDropCallback(GLFWwindow* window, int count, const char** paths)
{
    for (int i = 0; i < count; i++) {
        //handle_dropped_file(paths[i]);
        log_msg("File dropped: %s\n", paths[i]);
    }
}

internal void PlatformKeyCallback(GLFWwindow* window, int glfwKey, int scancode, int action, int mods)
{
    cdl_engine_platform_event pEvent;
    pEvent.key = KeyFromGLFWKeycode(glfwKey);
    switch (action) {
        case GLFW_PRESS:
        {   // TODO(abe): think about a way to implement scancodes instead of named keys (to support multiple keyboard layout by default)
            pEvent.type = PlatformEventType_key_pressed;
            //log_msg("Key scancode: %d\n", scancode);
            //log_msg("Key name: %s\n", glfwGetKeyName(key, glfwGetKeyScancode(key)));
        } break;
        
        case GLFW_RELEASE:
        {
            pEvent.type = PlatformEventType_key_released;
        } break;
        
        case GLFW_REPEAT:
        {
            pEvent.type = PlatformEventType_key_repeat;
        } break;
        
        default:
        {
            assert(0); // unknown key event
        } break;
    }
    PlatformRegisterEvent(pEvent);
}

internal void PlatformCharCallback(GLFWwindow* window, unsigned int codepoint)
{
    cdl_engine_platform_event pEvent;
    pEvent.type = PlatformEventType_text;
    pEvent.text = (char)codepoint;
    PlatformRegisterEvent(pEvent);
}

internal void PlatformJoystickCallback(int jid, int event)
{
    cdl_engine_platform_event pEvent;
    pEvent.controller.ID = (i8)jid;
    if (event == GLFW_CONNECTED) {
        pEvent.type = PlatformEventType_controller_connect;
    } else if (event == GLFW_DISCONNECTED) {
        pEvent.type = PlatformEventType_controller_disconnect;
    }
    PlatformRegisterEvent(pEvent);
}

internal void PlatformCursorEnterCallback(GLFWwindow* window, int enter)
{
    if ( enter) {
        PlatformRegisterEvent(cdl_engine_platform_event{PlatformEventType_mouse_enter});
    } else {
        PlatformRegisterEvent(cdl_engine_platform_event{PlatformEventType_mouse_leave});
    }
}

internal void PlatformMouseButtonCallback(GLFWwindow* window, int button, int action, int mods)
{
    cdl_engine_platform_event pEvent;
    pEvent.mouse.button = (MouseButton_)button;
    if ( action == GLFW_PRESS ) {
        pEvent.type = PlatformEventType_mouse_pressed;
    } else {
        pEvent.type = PlatformEventType_mouse_released;
    }
    PlatformRegisterEvent(pEvent);
}

internal void PlatformMouseScrolledCallback(GLFWwindow* window, double xoffset, double yoffset)
{
    cdl_engine_platform_event pEvent;
    pEvent.type = PlatformEventType_mouse_scrolled;
    pEvent.mouse.scroll = v2f{ (f32)xoffset, (f32)yoffset };
    PlatformRegisterEvent(pEvent);
}

internal void PlatformMouseMovedCallback(GLFWwindow* window, double xpos, double ypos)
{
    cdl_engine_platform_event pEvent;
    pEvent.type = PlatformEventType_mouse_moved;
    pEvent.mouse.position = v2i{ (i32)xpos, (i32)ypos };
    PlatformRegisterEvent(pEvent);
    
    // TODO(abe): compute delta between current and previous mouse position
}

internal void Platform_ProcessInputs()
{
    GLFWwindow* window = platform->glfwWindow;
    GLFWgamepadstate state;
    cdl_engine_platform_event pEvent;
    for (int controllerID = 0; controllerID < 4; controllerID++) {
        if (glfwGetGamepadState(controllerID, &state)) {
            
            pEvent.controller.ID = (i8)controllerID;
            for (int buttonID = 0; buttonID < ControllerButton_Count; buttonID++) {
                pEvent.controller.button = (ControllerButton_)buttonID;
                if (state.buttons[buttonID]) {
                    pEvent.type = PlatformEventType_controller_button_pressed;
                } else {
                    pEvent.type = PlatformEventType_controller_button_released;
                }
                PlatformRegisterEvent(pEvent);
            }
            
            for (int axesID = 0; axesID < ControllerAxes_Count; axesID++) {
                pEvent.type = PlatformEventType_controller_axes_moved;
                pEvent.controller.axes = (ControllerAxes_)axesID;
                pEvent.controller.axesAmount = state.axes[axesID];
                PlatformRegisterEvent(pEvent);             
            }
            
        }
    }
}

internal GLFWwindow* Platform_CreateGLFWWindow()
{
    //- GLFW Init
    if ( glfwInit() == GLFW_FALSE ) {
        log_err("Failed at GLFW init\n");
        return 0;
    }
    
    //- Window hints 
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
#if defined(ABE_APPLE)
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
#endif
    glfwWindowHint(GLFW_SAMPLES, 4);
    glfwWindowHint(GLFW_AUTO_ICONIFY, GL_FALSE);
    
    //- Create Window 
    GLFWwindow* w = glfwCreateWindow(CDL_ENGINE_APP_WIDTH, CDL_ENGINE_APP_HEIGHT, CDL_ENGINE_APP_NAME, NULL, NULL);
    if (w == NULL) {
        log_err("Failed to create GLFW window\n");
        glfwTerminate();
        return 0;
    }
    glfwMakeContextCurrent(w);
    
    //- Callbacks 
    glfwSetWindowCloseCallback(w, PlatformWindowCloseCallback);
    glfwSetFramebufferSizeCallback(w, PlatformWindowResizeCallback);
    glfwSetDropCallback(w, PlatformDragDropCallback);
    
    glfwSetKeyCallback(w, PlatformKeyCallback);
    glfwSetCharCallback(w, PlatformCharCallback);
    glfwSetJoystickCallback(PlatformJoystickCallback);
    glfwSetCursorEnterCallback(w, PlatformCursorEnterCallback);
    glfwSetMouseButtonCallback(w, PlatformMouseButtonCallback);
    glfwSetScrollCallback(w, PlatformMouseScrolledCallback);
    glfwSetCursorPosCallback(w, PlatformMouseMovedCallback);
    
    return w;
}