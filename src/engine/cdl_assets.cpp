internal u32 cdl_assets_internal_next_id()
{
    return core->assets->nextID++;
}


//- [SECTION] Shaders 
internal cdl_assets_shader* cdl_assets_internal_shader_new(char* path)
{
    PROFILED_FUNC;
    
    u32 offset = InterlockedIncrement32(&core->assets->nextShader);
    cdl_assets_shader* result = &core->assets->shaders[offset];
    result->head.type = AssetType_Shader;
    result->head.state = AssetState_Created;
    MemoryCopy(result->head.path, path, StringLength(path));
    result->head.path[StringLength(path)] = '\0';
    result->head.id = cdl_assets_internal_next_id();
    result->head.uuid = guid_new(&core->rng);
    
    return result;
}

internal cdl_assets_shader* cdl_assets_shader_create(ShaderType type, char* path)
{
    PROFILED_FUNC;
    
    cdl_assets_shader* result = cdl_assets_internal_shader_new(path);
    result->type = type;
    
    char* afterLastSlashInPath = path + GetLastSlashLoc(path) + 1;
    log_msg("Created shader asset: %s\n", afterLastSlashInPath, StringLength(afterLastSlashInPath));
    
    return result;
}

internal cdl_assets_shader* cdl_assets_shader_create_and_load(ShaderType type, char* path)
{
    PROFILED_FUNC;
    
    cdl_assets_shader* result = cdl_assets_internal_shader_new(path);
    result->type = type;
    cdl_assets_shader_load_code(result);
    
    char* afterLastSlashInPath = path + GetLastSlashLoc(path) + 1;
    log_msg("Created & loaded code of shader asset: %s\n", afterLastSlashInPath, StringLength(afterLastSlashInPath));
    
    return result;
}

internal void cdl_assets_shader_load_code(cdl_assets_shader* asset)
{
    PROFILED_FUNC;
    
    asset->head.state = AssetState_Requested;
    
    cdl_file shaderFile = platform->readfile(asset->head.path);
    str8 shaderCode = ZeroStruct;
    shaderCode.string = (char*)shaderFile.content;
    shaderCode.size = StringLength(shaderCode.string);
    
    u32 vertexCodeStart = GetFirstTokenLocation("@vertex", shaderCode) + StringLength("@vertex");
    u32 fragmentCodeStart = GetFirstTokenLocation("@fragment", shaderCode) + StringLength("@fragment");
    
    u32 vertexCodeEnd = GetFirstTokenLocation("@fragment", shaderCode);
    u32 fragmentCodeEnd = shaderCode.size;
    
    char* vertexCodeStartChar = shaderCode.string + vertexCodeStart;
    char* fragmentCodeStartChar = shaderCode.string + fragmentCodeStart;
    
    asset->vertexCode.size = vertexCodeEnd - vertexCodeStart;
    TranAllocArray(&asset->vertexCode.string, "Shader Vertex Code", char, asset->vertexCode.size + 1);
    MemoryCopy(asset->vertexCode.string, vertexCodeStartChar, asset->vertexCode.size);
    asset->vertexCode.string[asset->vertexCode.size] = 0;
    
    asset->fragmentCode.size = fragmentCodeEnd - fragmentCodeStart;
    TranAllocArray(&asset->fragmentCode.string, "Shader Fragment Code", char, asset->fragmentCode.size + 1);
    MemoryCopy(asset->fragmentCode.string, fragmentCodeStartChar, asset->fragmentCode.size);
    asset->fragmentCode.string[asset->fragmentCode.size] = 0;
    
    asset->head.state = AssetState_Loaded;
}

internal cdl_assets_shader* cdl_assets_shader_get(ShaderType type, char* path)
{
    PROFILED_FUNC;
    
    cdl_assets_shader* result = 0;
    for (u32 i = 0; i < ARRAY_COUNT(core->assets->shaders); i++) {
        cdl_assets_shader* current = core->assets->shaders + i;
        if ( current && current->type == type 
            && MemoryCompare(path, current->head.path, StringLength(path)) ) {
            result = current;
            break;
        }
    }
    
    return result;
}


//- [SECTION] Textures 
internal cdl_assets_texture* cdl_assets_internal_texture_new(char* path)
{
    PROFILED_FUNC;
    
    u32 offset = InterlockedIncrement32(&core->assets->nextTexture);
    cdl_assets_texture* result = &core->assets->textures[offset];
    result->head.type = AssetType_Texture;
    result->head.state = AssetState_Created;
    MemoryCopy(result->head.path, path, StringLength(path));
    result->head.path[StringLength(path)] = '\0';
    result->head.id = cdl_assets_internal_next_id();
    result->head.uuid = guid_new(&core->rng);
    
    return result;
}

internal cdl_assets_texture* cdl_assets_texture_create(TextureType type, char* path)
{
    PROFILED_FUNC;
    
    cdl_assets_texture* result = cdl_assets_internal_texture_new(path);
    result->type = type;
    
    char* afterLastSlashInPath = path + GetLastSlashLoc(path) + 1;
    log_msg("Created texture asset: %s\n", afterLastSlashInPath, StringLength(afterLastSlashInPath));
    
    return result;
}

internal cdl_assets_texture* cdl_assets_texture_create_and_load(TextureType type, char* path)
{
    PROFILED_FUNC;
    
    cdl_assets_texture* result = cdl_assets_internal_texture_new(path);
    result->type = type;
    cdl_assets_texture_load_pixels(result);
    
    char* afterLastSlashInPath = path + GetLastSlashLoc(path) + 1;
    log_msg("Created & loaded pixels of texture asset: %s\n", afterLastSlashInPath, StringLength(afterLastSlashInPath));
    
    return result;
}

internal void cdl_assets_texture_load_pixels(cdl_assets_texture* asset)
{
    PROFILED_FUNC;
    
    asset->head.state = AssetState_Requested;
    
    stbi_set_flip_vertically_on_load(true);
    unsigned char* data = stbi_load(asset->head.path, &asset->width, &asset->height, &asset->channels, 0);
    
    u32 dataSize = asset->width * asset->height * asset->channels;
    TranAllocArray(&asset->pixels, "TexturePixels", unsigned char, dataSize);
    MemoryCopy(asset->pixels, data, dataSize);
    
    asset->head.state = AssetState_Loaded;
    
    stbi_image_free(data);
}

internal cdl_assets_texture* cdl_assets_texture_get(char* path)
{
    PROFILED_FUNC;
    
    cdl_assets_texture* result = 0;
    for (u32 i = 0; i < ARRAY_COUNT(core->assets->textures); i++) {
        cdl_assets_texture* current = core->assets->textures + i;
        if ( current && MemoryCompare(path, current->head.path, StringLength(path)) ) {
            result = current;
            break;
        }
    }
    
    return result;
}


//- [SECTION] Fonts 
internal cdl_assets_font* cdl_assets_internal_font_new(char* fontFile)
{
    PROFILED_FUNC;
    
    u32 offset = InterlockedIncrement32(&core->assets->nextFont);
    cdl_assets_font* result = &core->assets->fonts[offset];
    result->head.type = AssetType_Font;
    result->head.state = AssetState_Created;
    MemoryCopy(result->head.path, fontFile, StringLength(fontFile));
    result->head.path[StringLength(fontFile)] = '\0';
    result->head.id = cdl_assets_internal_next_id();
    result->head.uuid = guid_new(&core->rng);
    
    return result;
}

internal cdl_assets_font* cdl_assets_font_create(char* fontFile, char* fontTexture)
{
    PROFILED_FUNC;
    
    cdl_assets_font* result = cdl_assets_internal_font_new(fontFile);
    result->texture = cdl_assets_texture_create(TextureType_Diffuse, fontTexture);
    
    char* afterLastSlashInPath = fontFile + GetLastSlashLoc(fontFile) + 1;
    log_msg("Created font asset: %s\n", afterLastSlashInPath, StringLength(afterLastSlashInPath));
    
    return result;
}

internal cdl_assets_font* cdl_assets_font_create_and_load(char* fontFile, char* fontTexture)
{
    PROFILED_FUNC;
    
    cdl_assets_font* result = cdl_assets_internal_font_new(fontFile);
    result->texture = cdl_assets_texture_create_and_load(TextureType_Diffuse, fontTexture);
    cdl_assets_font_load_data(result);
    
    char* afterLastSlashInPath = fontFile + GetLastSlashLoc(fontFile) + 1;
    log_msg("Created & loaded font asset: %s\n", afterLastSlashInPath, StringLength(afterLastSlashInPath));
    
    return result;
}

internal void cdl_assets_font_load_data(cdl_assets_font* asset)
{
    PROFILED_FUNC;
    
    asset->head.state = AssetState_Requested;
    
    cdl_file fontfile = platform->readfile(asset->head.path);
    
    i32 maxGlyphSize = 0;
    char* line = (char*)fontfile.content;
    u32 lineIndex = 1;
    
    //- Scan file looking for character data
    for (u32 i = 0; i < fontfile.size; i++) {
        // Build the line str8
        str8 currentLine = ZeroStruct;
        currentLine.string = line;
        currentLine.size = GetLineLength(line);
        if ( currentLine.size == 0 ) continue;
        i += currentLine.size;
        
        // First token is char id=
        char* token = "char id=";
        i32 charID = ReadI32AfterTokenAndAdvance(token, &currentLine);
        if ( charID != I32Max ) {
            cdl_assets_glyph* thisChar = &asset->glyphs[charID];
            thisChar->id = charID;
            thisChar->xpos = ReadI32AfterTokenAndAdvance("x=", &currentLine);
            thisChar->ypos = ReadI32AfterTokenAndAdvance("y=", &currentLine);
            thisChar->width = ReadI32AfterTokenAndAdvance("width=", &currentLine);
            thisChar->height = ReadI32AfterTokenAndAdvance("height=", &currentLine);
            thisChar->xoffset = ReadI32AfterTokenAndAdvance("xoffset=", &currentLine);
            thisChar->yoffset = ReadI32AfterTokenAndAdvance("yoffset=", &currentLine);
            thisChar->xadvance = ReadI32AfterTokenAndAdvance("xadvance=", &currentLine);
            
            if ( thisChar->height > maxGlyphSize ) maxGlyphSize = thisChar->height;
        }
        
        lineIndex++;
        line = GetNextLine(line);
    }
    asset->glyphSize = maxGlyphSize;
    
    platform->freefile(fontfile.content);
    
    asset->head.state = AssetState_Loaded;
}

internal cdl_assets_font* cdl_assets_font_get(char* fontFile)
{
    PROFILED_FUNC;
    
    cdl_assets_font* result = 0;
    for (u32 i = 0; i < ARRAY_COUNT(core->assets->fonts); i++) {
        cdl_assets_font* current = core->assets->fonts + i;
        if ( current && MemoryCompare(fontFile, current->head.path, StringLength(fontFile)) ) {
            result = current;
            break;
        }
    }
    
    return result;
}


//- [SECTION] Models 
internal cdl_assets_model_raw* cdl_assets_internal_model_raw_new(char* path)
{
    PROFILED_FUNC;
    
    u32 offset = InterlockedIncrement32(&core->assets->nextRawModel);
    cdl_assets_model_raw* result = &core->assets->rawModels[offset];
    result->head.type = AssetType_ModelRaw;
    result->head.state = AssetState_Created;
    MemoryCopy(result->head.path, path, StringLength(path));
    result->head.path[StringLength(path)] = '\0';
    result->head.id = cdl_assets_internal_next_id();
    result->head.uuid = guid_new(&core->rng);
    
    return result;
}

internal cdl_assets_model_raw* cdl_assets_model_raw_create(char* name, u32 vertexCount, v3f* positions, v2f* uvs, v3f* normals, u32 indexCount, u32* indices)
{
    PROFILED_FUNC;
    
    cdl_assets_model_raw* result = cdl_assets_internal_model_raw_new(name);
    
    result->head.state = AssetState_Requested;
    
    result->vertexCount = vertexCount;
    result->indexCount = indexCount;
    
    TranAllocArray(&result->vertices, "ModelVertices", cdl_assets_model_vertex, result->vertexCount);
    TranAllocArray(&result->indices, "ModelIndices", u32, result->indexCount);
    
    for (u32 i = 0; i < vertexCount; i++) {
        cdl_assets_model_vertex* v = result->vertices + i;
        v->localPosition = positions[i];
        v->uv = uvs[i];
        v->normal = normals[i];
        v->color = color_white;
    }
    
    MemoryCopy(result->indices, indices, indexCount * sizeof(u32));
    
    result->head.state = AssetState_Loaded;
    
    return result;
}

internal cdl_assets_model_raw* cdl_assets_model_raw_create_from_vertex_defs(char* name, u32 vertexDefCount, u32* vertexDefs, v3f* positions, v2f* uvs, v3f* normals)
{
    PROFILED_FUNC;
    
    // VertexDefs are arrays of indices, similar to those found at the face definitions section of obj files
    // for example: 1, 2, 4, 5, 1, 2
    // means that the first vertex uses the 1st position, the 2nd uv and the 4th normal
    // and the second vertex uses the 5th position, the 1st uv and the 2nd normal
    
    cdl_assets_model_raw* result = cdl_assets_internal_model_raw_new(name);
    
    result->head.state = AssetState_Requested;
    
    // One vertex definition contain a position index, a uv index and a normal index
    u32 indexCount = vertexDefCount / 3;
    
    // Worst case scenario: every single vertex is different -> there is a need for indexCount vertices
    result->vertexCount = indexCount;
    result->indexCount = indexCount;
    
    TranAllocArray(&result->vertices, "ModelVertices", cdl_assets_model_vertex, result->vertexCount);
    TranAllocArray(&result->indices, "ModelIndices", u32, result->indexCount);
    
    u32 currentVertex = 0;
    for (u32 i = 0; i < indexCount; i++) {
        u32 positionIndex = vertexDefs[i * 3];
        u32 uvIndex = vertexDefs[i * 3 + 1];
        u32 normalIndex = vertexDefs[i * 3 + 2];
        
        // Check if the vertex was already saved
        bool8 alreadySaved = 0;
        
        for (u32 svIndex = 0; svIndex < currentVertex; svIndex++) {
            cdl_assets_model_vertex* v = result->vertices + svIndex;
            
            bool8 localPosEquals = v3f_equals(v->localPosition, positions[positionIndex]);
            bool8 uvEquals = uvs ? v2f_equals(v->uv, uvs[uvIndex]) : 1;
            bool8 normalEquals = normals ? v3f_equals(v->normal, normals[normalIndex]) : 1;
            
            if ( localPosEquals && uvEquals && normalEquals ) {
                alreadySaved = 1;
                result->indices[i] = svIndex;
                break;
            }
            
        }
        
        // If not saved yet, register the new data
        if ( !alreadySaved ) {
            cdl_assets_model_vertex* v = result->vertices + currentVertex;
            v->localPosition = positions[positionIndex];
            
            if ( uvs ) {
                v->uv = uvs[uvIndex];
            }
            
            if ( normals ) {
                v->normal = normals[normalIndex];
            }
            
            v->color = color_white;
            result->indices[i] = currentVertex;
            currentVertex++;
        }
    }
    
    result->head.state = AssetState_Loaded;
    
    return result;
}

internal cdl_assets_model_raw* cdl_assets_model_raw_create_from_obj(char* path)
{
    PROFILED_FUNC;
    
    cdl_assets_model_raw* result = cdl_assets_internal_model_raw_new(path);
    
    result->head.state = AssetState_Requested;
    
    cdl_file modelFile = platform->readfile(path);
    if ( !modelFile.content ) {
        log_err("Error while loading asset model %s\n", path);
        return 0;
    }
    
    char* currentLine = (char*)modelFile.content;
    u32 lineCount = GetLineCount((char*)modelFile.content);
    
    //- Count positions, uvs, normals & faces in model
    u32 readPositionsCount = 0;
    u32 readUVsCount = 0;
    u32 readNormalsCount = 0;
    u32 faceCount = 0;
    
    for (u32 i = 0; i < lineCount; i++) {
        if ( MemoryCompare(currentLine, "v ", 2) ) {
            readPositionsCount++;
        }
        
        else if ( MemoryCompare(currentLine, "vt", 2) ) {
            readUVsCount++;
        }
        
        else if ( MemoryCompare(currentLine, "vn", 2) ) {
            readNormalsCount++;
        }
        
        else if (MemoryCompare(currentLine, "f ", 2) ) {
            faceCount++;
        }
        
        currentLine = GetNextLine(currentLine);
    }
    
    // NOTE(abe): this reader duplicate vertices for each face they are a part of
    // e.g: for a quad model, made of two triangles, the reader will register 2 * 3 = 6 vertices
    // instead of 4, which is the minimal amount
    u32 vertexCount = faceCount * 3;
    result->vertexCount = vertexCount; 
    result->indexCount = vertexCount;
    
    //- Alloc arrays
    
    // Arrays to store sorted cdl_assets_model_vertex data
    TranAllocArray(&result->vertices, "ModelVertices", cdl_assets_model_vertex, result->vertexCount);
    TranAllocArray(&result->indices, "ModelIndices", u32, result->indexCount);
    
    u32 currentVertex = 0;
    u32 currentIndex = 0;
    
    // Arrays to store read data in order
    v3f* readPositions = 0;
    FrameAllocArray(&readPositions, "TempV3f", v3f, readPositionsCount);
    v2f* readUVs = 0;
    FrameAllocArray(&readUVs, "TempV2f", v2f, readUVsCount);
    v3f* readNormals = 0;
    FrameAllocArray(&readNormals, "TempV3f", v3f, readNormalsCount);
    
    u32 currentPosition = 0;
    u32 currentUV = 0;
    u32 currentNormal = 0;
    
    //- Scan file for vertex data
    currentLine = (char*)modelFile.content;
    for (u32 i = 0; i < lineCount; i++) {
        
        // Construct line str8
        str8 toSplit = {};
        toSplit.string = currentLine;
        toSplit.size = GetLineLength(currentLine);
        
        // Split line by space
        u32 splitCount = 0;
        str8 splits[8] = { };
        SplitOnWhitespace(toSplit, splits, &splitCount);
        
        // Vertex position : "v x y z"
        if ( MemoryCompare(currentLine, "v ", 2) ) {
            readPositions[currentPosition++] = v3f_new(ReadF32(splits[1]), ReadF32(splits[2]), ReadF32(splits[3]));
        }
        
        // Vertex uv coordinates : "vt u v"
        else if ( MemoryCompare(currentLine, "vt", 2) ) {
            readUVs[currentUV++] = v2f_new(ReadF32(splits[1]), ReadF32(splits[2]));
        }
        
        // Vertex normal : "vn x y z"
        else if ( MemoryCompare(currentLine, "vn", 2) ) {
            readNormals[currentNormal++] = v3f_new(ReadF32(splits[1]), ReadF32(splits[2]), ReadF32(splits[3]));
        }
        
        // Face descrption : "f p1/uv1/n1 p2/uv2/n2 p3/uv3/n3"
        // p1, p2, p3: position index in the readPositions array
        // uv1, uv2, uv3: uv index in the readUVs array
        // n1, n2, n3: normal index in the readNormals array
        else if ( MemoryCompare(currentLine, "f ", 2) ) {
            
            for (u32 faceVertex = 1; faceVertex < splitCount; faceVertex++) {
                u32 fSplitCount = 0;
                str8 fSplits[3] = { };
                SplitOnChar(splits[faceVertex], '/', fSplits, &fSplitCount);
                
                // Indices have - 1 because obj count from 1 up to vertexCount
                u32 positionIndex = ReadU32(fSplits[0]) - 1;
                u32 uvIndex = ReadU32(fSplits[1]) - 1;
                u32 normalIndex = ReadU32(fSplits[2]) - 1;
                
                // Check if the vertex was already saved
                bool8 alreadySaved = 0;
                
                for (u32 svIndex = 0; svIndex < currentVertex; svIndex++) {
                    cdl_assets_model_vertex* v = result->vertices + svIndex;
                    
                    if ( v3f_equals(v->localPosition, readPositions[positionIndex])
                        && v2f_equals(v->uv, readUVs[uvIndex])
                        && v3f_equals(v->normal, readNormals[normalIndex]) ) {
                        alreadySaved = 1;
                        result->indices[currentIndex] = svIndex;
                        break;
                    }
                    
                }
                
                // If not saved yet, register the new data
                if ( !alreadySaved ) {
                    cdl_assets_model_vertex* v = result->vertices + currentVertex;
                    v->localPosition = readPositions[positionIndex];
                    v->uv = readUVs[uvIndex];
                    v->normal = readNormals[normalIndex];
                    v->color = color_white;
                    result->indices[currentIndex] = currentVertex;
                    currentVertex++;
                }
                currentIndex++;
            }
            
        }
        
        currentLine = GetNextLine(currentLine);
    }
    
    // Update the vertex count the account for duplicate vertices
    result->vertexCount = currentVertex;
    
    // Free used memory
    platform->freefile(modelFile.content);
    
    result->head.state = AssetState_Loaded;
    
    return result;
}


//- [SECTION] Audio
internal cdl_assets_audio* cdl_assets_internal_audio_new(char* path)
{
    PROFILED_FUNC;
    
    u32 offset = InterlockedIncrement32(&core->assets->nextAudio);
    cdl_assets_audio* result = &core->assets->audios[offset];
    result->head.type = AssetType_Audio;
    result->head.state = AssetState_Created;
    MemoryCopy(result->head.path, path, StringLength(path));
    result->head.path[StringLength(path)] = '\0';
    result->head.id = cdl_assets_internal_next_id();
    result->head.uuid = guid_new(&core->rng);
    
    return result;
}

internal cdl_assets_audio* cdl_assets_audio_create(AudioType type, char* path)
{
    PROFILED_FUNC;
    
    cdl_assets_audio* result = cdl_assets_internal_audio_new(path);
    result->type = type;
    
    char* afterLastSlashInPath = path + GetLastSlashLoc(path) + 1;
    log_msg("Created audio asset: %s\n", afterLastSlashInPath, StringLength(afterLastSlashInPath));
    
    return result;
}

internal cdl_assets_audio* cdl_assets_audio_create_and_load(AudioType type, char* path)
{
    PROFILED_FUNC;
    
    cdl_assets_audio* result = cdl_assets_internal_audio_new(path);
    result->type = type;
    cdl_assets_audio_load(result);
    
    char* afterLastSlashInPath = path + GetLastSlashLoc(path) + 1;
    log_msg("Created audio asset: %s\n", afterLastSlashInPath, StringLength(afterLastSlashInPath));
    
    return result;
}

internal void cdl_assets_audio_load(cdl_assets_audio* asset)
{
    PROFILED_FUNC;
    
    cdl_file file = platform->readfile(asset->head.path);
    assert(file.size);
    
    wave_header* wave = (wave_header*)file.content;
    assert(wave->riffID == riff_chunk_id_riff);
    assert(wave->waveID == riff_chunk_id_wave);
    
    for (riff_file_iterator it = riff_file_iterator_new(wave + 1, wave->fileSize - 4);
         riff_file_iterator_valid(&it); 
         riff_file_iterate(&it)) {
        u32 chunkID = riff_file_iterator_get_chunk_type(it);
        switch (chunkID) {
            case riff_chunk_id_fmt:
            {
                wave_chunk_fmt* fmt = (wave_chunk_fmt*)it.at;
                assert(fmt->formatTag == 1);
                assert(fmt->sampleRate == 48000);
                assert(fmt->bitsPerSample == 16);
                assert(fmt->blockAlign == 2 * fmt->channels);
                
                // Copy format description to the asset
                asset->channels = fmt->channels;
                asset->sampleRate = fmt->sampleRate;
                asset->byteRate = fmt->byteRate;
                asset->blockAlign = fmt->blockAlign;
                asset->bitsPerSample = fmt->bitsPerSample;
            } break;
            
            case riff_chunk_id_data:
            {
                riff_chunk_header* chunk = (riff_chunk_header*)it.at;
                asset->dataSize = chunk->size;
                asset->sampleCount = asset->dataSize / ( asset->bitsPerSample / 4 );
                assert(asset->sampleCount);
                
                i16* samples = (i16*)((u8*)it.at + sizeof(riff_chunk_header));
                switch (asset->channels) {
                    case 1:
                    {
                        TranAllocArray(&asset->leftSamples, asset->head.path, i16, asset->dataSize);
                        MemoryCopy(asset->leftSamples, samples, asset->dataSize);
                        assert(asset->leftSamples);
                    } break;
                    
                    case 2:
                    {
                        TranAllocArray(&asset->leftSamples, asset->head.path, i16, asset->sampleCount);
                        TranAllocArray(&asset->rightSamples, asset->head.path, i16, asset->sampleCount);
                        
                        for (u32 i = 0; i < asset->sampleCount; i++) {
                            asset->leftSamples[i] = samples[2 * i];
                            asset->rightSamples[i] = samples[2 * i + 1];
                        }
                        
                        assert(asset->leftSamples);
                        assert(asset->rightSamples);
                    } break;
                    
                    default:
                    {
                        assert(!"Unsupported channel count in WAV file\n");
                    } break;
                }
                
                u32 duration = 0;
                duration = asset->sampleCount / asset->sampleRate;
                log_info("Duration: %ds\n", duration);
            } break;
            
            default:
            {
            } break;
        }
        
    }
    
    platform->freefile(file.content);
}


//- [SECTION] Assets 
internal void cdl_engine_assets_init()
{
    PROFILED_FUNC;
    
    //- Shaders
    cdl_assets_shader* worldShaderAsset = cdl_assets_shader_create_and_load(ShaderType_World, ASSET_SHADER(world));
    cdl_assets_shader* uiShaderAsset = cdl_assets_shader_create_and_load(ShaderType_UI, ASSET_SHADER(ui));
    cdl_assets_shader* fqQuadShaderAsset = cdl_assets_shader_create_and_load(ShaderType_FullScreenQuad, ASSET_SHADER(fsquad));
    
    //- Textures
    cdl_assets_texture* checkSDF = cdl_assets_texture_create_and_load(TextureType_Diffuse, ASSET_ICON(check));
    cdl_assets_texture* squareSDF = cdl_assets_texture_create_and_load(TextureType_Diffuse, ASSET_ICON(square));
    
    //- Fonts 
    cdl_assets_font* arial = cdl_assets_font_create(ASSET_FONT_FILE(arial), ASSET_FONT_TEXTURE(arial));
    cdl_assets_font* segoe = cdl_assets_font_create(ASSET_FONT_FILE(segoe_ui), ASSET_FONT_TEXTURE(segoe_ui));
}