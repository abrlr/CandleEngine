//- Defines
#define STB_IMAGE_IMPLEMENTATION
#define STBI_ONLY_PNG
#define IM_OWN_CONTEXT

#ifdef CDL_DEBUG
#define ABE_CONSOLE_ENABLE
#endif

//- External Headers
#include <GL/glew.h>
#include <math.h>
#include "ext/stb_image.h"

//- Engine Headers
#include "cdl_std.c"
#include "cdl_meta.h"

#include "cdl_engine_events.h"
#include "cdl_assets.h"
#include "cdl_audio.h"
#include "cdl_engine.h"
#include "cdl_text.h"
#include "cdl_renderer.h"
#include "cdl_ui.h"
#include "cdl_imgui.h"
#include "cdl_platform.h"
#include "cdl_engine_debug.h"
#include "cdl_generated.h"

//- Engine Sources
#include "cdl_meta.cpp"
#include "cdl_engine_events.cpp"
#include "cdl_assets.cpp"
#include "cdl_audio.cpp"
#include "cdl_text.cpp"
#include "cdl_renderer.cpp"
#include "cdl_ui.cpp"
#include "cdl_imgui.cpp"

//- Applications Sources
#include STRINGIFY(APPLICATION_INCLUDE)

//- Engine Struct
cdl_engine_runtime* core;
cdl_platform* platform;
im_context* g_imctx;

struct engine_state {
    cdl_renderer renderer;
    application_memory app;
    
#ifdef CDL_DEBUG
    debug_memory debug;
    profiler profile;
#endif
    
    im_context imctx;
};
s_assert(sizeof(engine_state) < PERMANENT_MEMORY_SIZE, "engine_state struct is too big");

//- Engine Initialisation
extern "C" ENGINE_INIT(EngineInit)
{
    // Allocate Memory
    engine_state* state = 0;
    PermAllocStruct(&state, "Engine State", engine_state);
    PermAllocStruct(&core->assets, "Engine Assets", cdl_assets);
    
    // Debug Initialisation
#ifdef CDL_DEBUG
#ifdef ABE_CONSOLE_ENABLE
    log_set_console(&state->debug.devConsole);
#endif
    profiler_set(&state->profile);
#endif
    
    // IM Init
    g_imctx = &state->imctx;
    im_initialise();
    
    // Assets Init 
    cdl_engine_assets_init();
    
    // Renderer Init
    cdl_renderer_set(&state->renderer);
    cdl_renderer_create(&state->renderer);
    if ( !state->renderer.isValid ) {
        core->run = false;
        return;
    }
    
    engineMemory->isInitialized = true;
    log_msg("Engine memory initialized\n");
}

//- Engine Hot Reload
extern "C" ENGINE_HOTRELOAD(EngineHotReload)
{
    engine_state* state = (engine_state*)engineMemory->permanent.location;
    
#ifdef CDL_DEBUG
#ifdef ABE_CONSOLE_ENABLE
    log_set_console(&state->debug.devConsole);
#endif
    profiler_set(&state->profile);
#endif
    
    // Renderer
    cdl_renderer_set(&state->renderer);
    if ( !cdl_renderer_api_init() ) {
        core->run = false;
        return;
    }
    
    log_warn("Hot Reloading");
}

//- Engine Update
extern "C" ENGINE_UPDATE(EngineUpdateAndRender)
{
    MemoryClear(&engineMemory->frame);
    engine_state* state = (engine_state*)engineMemory->permanent.location;
    g_imctx = &state->imctx;
    
#if CDL_DEBUG
    profiler_begin_frame(&state->profile);
    {
#endif
        
        // Application Update
        cdl_renderer_frame_begin();
        application_update(&state->app, &core->highQueue);
        cdl_renderer_frame_end();
        cdl_audio_play();
        
#ifdef CDL_DEBUG
    }
    profiler_end_frame(&state->profile);
    
    // Engine Debug
    if ( keyboard_key_on_down(KEY_F2) ) state->debug.display = !state->debug.display;
    
    if ( state->debug.display ) {
        cdl_renderer_frame_begin();
        debug_update(&state->debug);
        cdl_renderer_frame_end();
    }
#endif
    
    local i32 memoryLayoutUpdatePeriod = 30;
    if ( memoryLayoutUpdatePeriod-- < 0 ) {
        memory_block_layout_collapse(&core->memory.transient);
        memoryLayoutUpdatePeriod = 30;
    }
}

//- Engine Set Platform
extern "C" ENGINE_SETPLATFORM(EngineSetPlatform)
{
    core = enginePtr;
    platform = platformPtr;
}

//- Engine Clean
extern "C" ENGINE_CLEAN(EngineClean)
{
    im_clean();
}

#ifdef CDL_DEBUG
#include "cdl_engine_debug.cpp"
#endif