struct debug_tooltip {
    bool8 display;
    i32 ID;
    void* data;
};

cdl_ui_widget_callback(debug_tab_assets_show_texture)
{
    debug_memory* debug = (debug_memory*)w->userData1;
    cdl_ui_widget* target = &debug->emWPTextureTarget;
    
    cdl_assets_texture* t = (cdl_assets_texture*)w->userData2;
    if ( t ) {
        target->texture = cdl_renderer_texture_get(t);
        target->attributes.constraints.aspect = t->height / (f32)t->width;
    }
    
    cdl_renderer_texture* rt = (cdl_renderer_texture*)w->userData3;
    if ( rt ) {
        target->texture = rt;
        target->attributes.constraints.aspect = core->frameHeight / (f32)core->frameWidth;
    }
    
    target->dirty = 1;
    __cdl_ui_internal_update_rect(target);
}

cdl_ui_widget_callback(debug_tab_assets_reload_shader)
{
    debug_memory* debug = (debug_memory*)w->userData1;
    cdl_assets_shader* s = (cdl_assets_shader*)w->userData2;
    
    for (cdl_renderer_shader* rs = renderer->storage.shaders; rs < renderer->storage.nextShader; rs++) {
        if ( rs->asset == s ) {
            cdl_renderer_shader_load(rs);
        }
    }
}

internal debug_tooltip debug_draw_memory_block(debug_memory* debug, memory_block* block, v2i* position);
cdl_ui_widget_callback(debug_tab_memory_on_draw)
{
    debug_memory* debug = (debug_memory*)w->userData1;
    v2f mousePosition = v2itof(mouse_get_position());
    rect2 r = w->rect;
    
    memory_block* block = 0;
    debug_tooltip tooltip = ZeroStruct;
    v2i position = v2ftoi(r.min);
    
    position.x += 10;
    position.y += 25;
    
    block = &core->memory.permanent;
    debug_tooltip permTT = debug_draw_memory_block(debug, block, &position);
    if ( permTT.display ) tooltip = permTT;
    
    position.y += 25;
    
    block = &core->memory.transient;
    debug_tooltip tranTT = debug_draw_memory_block(debug, block, &position);
    if ( tranTT.display ) tooltip = tranTT;
    
    position.y += 25;
    
    block = &core->memory.frame;
    debug_tooltip frameTT = debug_draw_memory_block(debug, block, &position);
    if ( frameTT.display ) tooltip = frameTT;
    
    if ( tooltip.display ) {
        block = (memory_block*)tooltip.data;
        memory_block_entry* entry = &block->entries[tooltip.ID];
        
        // Convert the size in a relevant unit 
        f32 sizeToPrint = 0;
        char* sizeUnit = "";
        {
            f32 sizeInKBytes = entry->size / (f32)KiloBytes(1);
            f32 sizeInMBytes = entry->size / (f32)MegaBytes(1);
            f32 sizeInGBytes = entry->size / (f32)GigaBytes(1);
            
            if ( sizeInGBytes < 1000.f ) {
                sizeToPrint = sizeInGBytes;
                sizeUnit = "Gb";
            }
            
            if ( sizeInMBytes < 1000.f ) {
                sizeToPrint = sizeInMBytes;
                sizeUnit = "Mb";
            }
            
            if ( sizeInKBytes < 1000.f ) {
                sizeToPrint = sizeInKBytes;
                sizeUnit = "Kb";
            }
            
            if ( entry->size < 1000.f ) {
                sizeToPrint = entry->size;
                sizeUnit = "b";
            }
        }
        
        char buffer[300] = { };
        sprintf(buffer, "Block Entry %d: %s\n"
                " - File.Line: %s.%d\n"
                " - Size: %.2f %s\n"
                " - Location: %d\n",
                entry->id, entry->name, entry->file, entry->line, sizeToPrint, sizeUnit, (u32)(entry->location - block->entries->location));
        
        
        cdl_text_atb attributes = cdl_text_atb_new();
        attributes.anchor = TextAnchor_TopLeft;
        v2f textDim = cdl_text_get_dimensions(buffer, renderer->currentFont->asset, attributes);
        v2f quadDim = v2f_times(textDim, 1.05f);
        v2f quadPos = v2f_sub(mousePosition, v2f_times(quadDim, -0.5f));
        
        v2f offset = v2f_new(10, 0); // add offset to avoid the mouse cursor
        quadPos = v2f_add(quadPos, offset);
        offset.x += 5; // padding for the text in the quad
        v2f textPos = v2f_add(mousePosition, offset);
        
        ui_quad(quadPos, quadDim, cdl_rgba(25, 25, 25, 190));
        ui_text(buffer, textPos, renderer->currentFont, attributes);
    }
}

//- Frame Profiler
cdl_ui_widget_callback(debug_tab_profiler_on_draw)
{
    debug_memory* debug = (debug_memory*)w->userData1;
    v2f mousePosition = v2itof(mouse_get_position());
    rect2 r = w->rect;
    
    u32 profilerHistoryCount = 200; 
    u32 colorCount = 256;
    
    // Allocate Ressources
    if ( !debug->pAllocated ) {
        TranAllocArray(&debug->pastProfilers, "debug::profilers", profiler, profilerHistoryCount);
        TranAllocArray(&debug->profilerColors, "debug:profiler_colors", color8, colorCount);
        debug->pAllocated = 1;
        
        random_series* s = &core->rng;
        for (u32 i = 0; i < colorCount; i++) {
            debug->profilerColors[i] = cdl_rgb(random_u32(s), random_u32(s), random_u32(s));
        }
    }
    
    // Register Frame
    debug->nextProfilerIndex++;
    if ( debug->nextProfilerIndex == profilerHistoryCount ) {
        debug->nextProfilerIndex = 0;
    }
    MemoryCopy(&debug->pastProfilers[debug->nextProfilerIndex], profiler_ptr, sizeof(profiler));
    
    // Up/Down a level
    if ( keyboard_key_on_down(KEY_KP_ADD) ) {
        debug->inspectedDepth = i32_clamp(debug->inspectedDepth + 1, 0, 10);
    }
    
    if ( keyboard_key_on_down(KEY_KP_SUBTRACT) ) {
        debug->inspectedDepth = i32_clamp(debug->inspectedDepth - 1, 0, 10);
    }
    
    //- Display Frame Profiler
    {
        u32 xoffset = r.min.x + 20;
        u32 areaWidth = rect2_dim(r).width;
        
        for (u32 pIndex = 0; pIndex < profilerHistoryCount; pIndex++) {
            u32 yoffset = r.max.y - 20;
            
            profiler* currentProfiler = debug->pastProfilers + pIndex;
            u32 count = currentProfiler->count;
            u32 current = 0;
            
            for (u32 i = 0; i < count; i++) {
                profiler_block* b = currentProfiler->blocks + i;
                if ( b->depth == debug->inspectedDepth ) {
                    v2f p1 = v2f_new(xoffset, yoffset);
                    v2f p2 = v2f_new(xoffset, yoffset - 20 * b->time / 1000000);
                    color8 color = debug->profilerColors[i % colorCount];
                    ui_line(p1, p2, 1, color);
                    yoffset -= 20 * b->time / 1000000;
                }
            }
            
            xoffset += 2;
        }
        
        char* buffer = 0;
        FrameAllocArray(&buffer, "profiler::buffer", char, 128);
        cdl_text_atb attributes = cdl_text_atb_new();
        attributes.anchor = TextAnchor_TopLeft;
        
        f32 yoffset = r.min.y + 20;
        i32 current = 0;
        i32* blockIndices = 0;
        FrameAllocArray(&blockIndices, "profiler::block indices", i32, colorCount);
        f32* blockTimes = 0;
        FrameAllocArray(&blockTimes, "profiler::block times", f32, colorCount);
        
        profiler* p = debug->pastProfilers + debug->nextProfilerIndex;
        for (u32 i = 0; i < p->count; i++) {
            profiler_block* b = p->blocks + i;
            if ( b->depth == debug->inspectedDepth ) {
                blockTimes[current] = (f32)b->time;
                blockIndices[current++] = i;
            }
        }
        
        algorithms_quicksort(blockTimes, blockIndices, sizeof(i32), 0, current - 1);
        
        for (i32 i = current - 1; i >= 0 && i >= current - 10; i--) {
            profiler_block* b = p->blocks + blockIndices[i];
            sprintf(buffer, "%s: %.3fms\n", b->name, b->time / 1000000.f);
            
            attributes.color = debug->profilerColors[blockIndices[i] % colorCount];
            ui_text(buffer, v2f_new(r.min.x + 20, yoffset), renderer->currentFont, attributes);
            yoffset += attributes.size;
        }
    }
    
    //- Display frozen frame
    if ( debug->hasFrozenFrame ) {
        profiler* p = debug->frozenProfiler;
        
#ifdef ABE_WIN
        u32 start = p->blocks[0].start.QuadPart;
#elif (defined(ABE_APPLE) || defined(ABE_LINUX))
        u32 start = p->blocks[0].start.tv_nsec;
#endif
        
        u32 end = 0;
        
        for (u32 i = 0; i < p->count; i++) {
            profiler_block* b = p->blocks + i;
            
#ifdef ABE_WIN
            u32 bstart = b->start.QuadPart;
            u32 bend = b->end.QuadPart;
#elif (defined(ABE_APPLE) || defined(ABE_LINUX))
            u32 bstart = b->start.tv_nsec;
            u32 bend = b->end.tv_nsec;
#endif
            
            if ( bend > end ) {
                end = bend;
            }
        }
        
        f32 frameTime = 0;
        f32 pixelPerCounter = ( end - start ) / (f32)( rect2_dim(r).width - 20 );
        f32 elementHeight = 10;
        
        for (i32 depth = -1; depth < 50; depth++) {
            i32 yoffset = r.min.y + rect2_dim(r).height / 3 + ( depth + 1 ) * (elementHeight + 2);
            
            for (u32 i = 0; i < p->count; i++) {
                profiler_block* b = p->blocks + i;
                if ( b->depth == depth ) {
#ifdef ABE_WIN
                    u32 bstart = b->start.QuadPart;
                    u32 bend = b->end.QuadPart;
#elif (defined(ABE_APPLE) || defined(ABE_LINUX))
                    u32 bstart = b->start.tv_nsec;
                    u32 bend = b->end.tv_nsec;
#endif
                    
                    if ( depth == 0 ) {
                        frameTime += b->time / 1000000.f;
                    }
                    
                    color8 color = debug->profilerColors[i % 256];
                    v2f dim = v2f_new( (bend - bstart) / pixelPerCounter, elementHeight);
                    v2f pos = v2f_new( r.min.x + 5 + (bstart - start) / pixelPerCounter + dim.x / 2, yoffset);
                    
                    if ( b->name[0] == 'g' && b->name[1] == 'l' ) {
                        color = color_red;
                    }
                    
                    ui_quad(pos, dim, color);
                }
            }
        }
        
        cdl_text_atb attributes = cdl_text_atb_new();
        attributes.anchor = TextAnchor_TopLeft;
        
        char buffer[256] = { };
        sprintf(buffer, "Frame Time: %.3fms\n", frameTime);
        ui_text(buffer, v2f_new(r.min.x + 5, r.min.y + rect2_dim(r).height / 3 - 20), renderer->currentFont, attributes);
    }
}

internal void debug_profiler_save_frame(debug_memory* debug)
{
    u32 fileSize = 0;
    char* fileContent = 0;
    FrameAllocArray(&fileContent, "profiler:export", char, 4192 * 8);
    char* ptr = fileContent;
    MemoryZero(fileContent, 4192 * 4);
    
    {
        sprintf(ptr, "var frame = { \"blocks\": [\n");
        u32 length = StringLength(ptr);
        ptr += length;
        fileSize += length;
        
        u32 count = profiler_ptr->count;
        for (u32 i = 0; i < count; i++) {
            profiler_block* b = profiler_ptr->blocks + i;
            
            char* file = b->file + GetLastSlashLoc(b->file) + 1;
            
            i32 parentIndex = b->parent ? (u32)(b->parent - profiler_ptr->blocks) : -1;
            i32 nextIndex = b->next ? (u32)(b->next - profiler_ptr->blocks) : -1;
            i32 childIndex = b->child ? (u32)(b->child - profiler_ptr->blocks) : -1;
            
            sprintf(ptr, "\t{ \"id\": %d, \"name\": \"%s\", \"file\": \"%s\", \"line\": %d, \"depth\": %d, \"start\": %lld, \"end\": %lld, \"time\": %d, \"parent\": %d, \"next\": %d, \"child\": %d },\n", 
                    i, b->name, file, b->line, b->depth,
#ifdef ABE_WIN
                    b->start.QuadPart, b->end.QuadPart, b->time,
#elif (defined(ABE_APPLE) || defined(ABE_LINUX))
                    b->start.tv_nsec, b->end.tv_nsec, b->time,
#endif
                    parentIndex, nextIndex, childIndex);
            length = StringLength(ptr);
            ptr += length;
            fileSize += length;
        }
        
        sprintf(ptr, "]\n}\n");
        
        length = StringLength(ptr);
        ptr += length;
        fileSize += length;
    }
    
    cdl_file profilerOut;
    profilerOut.content = fileContent;
    profilerOut.size = fileSize;
    
    platform->writefile("frame.js", &profilerOut);
    
    //- Save Frame Profiler for In-Engine inspection
    if ( !debug->hasFrozenFrame ) {
        TranAllocStruct(&debug->frozenProfiler, "Frozen Profiler", profiler);
        debug->hasFrozenFrame = 1;
    }
    MemoryCopy(debug->frozenProfiler, profiler_ptr, sizeof(profiler));
}

cdl_ui_widget_callback(debug_tab_profiler_save)
{
    debug_memory* debug = (debug_memory*)w->userData1;
    debug_profiler_save_frame(debug);
}

cdl_ui_widget_callback(debug_tab_console_on_draw)
{
#ifdef ABE_CONSOLE_ENABLE
    debug_memory* debug = (debug_memory*)w->userData1;
    rect2 r = w->rect;
    v2f mid = rect2_mid(r);
    v2f dim = rect2_dim(r);
    
    i32 fontSize = 12;
    i32 lineWidth = dim.width - 5;
    local u32 scrollOffset = 0;
    
    v2f consolePos = v2f_new(r.min.x, r.max.y);
    rect2 consoleRect = r;
    
    if ( debug->UI.hotInteraction.w == w ) {
        if ( mouse_button_get(mouse_LEFT) ) {
            scrollOffset += mouse_get_delta().y * 0.25f;
            scrollOffset = i32_clamp(scrollOffset, 0, debug->devConsole.lineCount - 1);
        }
    }
    
    cdl_text_atb attributes = cdl_text_atb_new();
    attributes.size = fontSize;
    attributes.lineWidth = lineWidth;
    attributes.anchor = TextAnchor_TopLeft;
    attributes.style = TextStyle_AlignLeft;
    
    v2f textPos = consolePos;
    ui_push_clip_rect(consoleRect);
    for (i32 i = scrollOffset; i < debug->devConsole.lineCount; i++) {
        log_console_line* line = &debug->devConsole.lines[i];
        f32 lineSize = cdl_text_get_dimensions(line->lineStr, renderer->currentFont->asset, attributes).y;
        textPos.y -= lineSize;
        if ( textPos.y < consolePos.y - dim.y ) { break; }
        
        attributes.color = line->color;
        ui_text(line->lineStr, textPos, renderer->currentFont, attributes);
    }
    ui_pop_clip_rect();
#endif
}

internal void debug_init(debug_memory* debug)
{
    debug->initialised = 1;
    
    //- UI Widgets
    debug->UI.rootWidget = &debug->windowCanvas;
    
    //- Window Canvas
    cdl_ui_widget_attributes windowCanvasAttrib = cdl_ui_widget_attributes_new();
    cdl_ui_widget_new(&debug->windowCanvas, 0, 0, UIWidgetType_Canvas, windowCanvasAttrib);
    
    //- Bottom Bar
    cdl_ui_widget_attributes bottomBarAttrib = cdl_ui_widget_attributes_new();
    bottomBarAttrib.layout = UIWidgetLayout_Horizontal;
    bottomBarAttrib.constraints.type = UIWidgetConstraint_SizeRelativeX 
        | UIWidgetConstraint_SizeFixedY
        | UIWidgetConstraint_PosFixedBottom;
    bottomBarAttrib.constraints.size = v2f_new(1, 18);
    bottomBarAttrib.constraints.positions.bottom = 0;
    
    
    cdl_ui_widget_attributes bottomBarButtonAttrib = cdl_ui_widget_attributes_new();
    bottomBarButtonAttrib.constraints.type = UIWidgetConstraint_SizeFixedX
        | UIWidgetConstraint_MarginFixedLeft
        | UIWidgetConstraint_PosFixedTop;
    bottomBarButtonAttrib.constraints.size = v2f_new(150, 1);
    bottomBarButtonAttrib.constraints.positions.top = 0;
    bottomBarButtonAttrib.constraints.margins.left = 10;
    
    // Bar Panel
    cdl_ui_widget_new(&debug->bottomBar, &debug->windowCanvas, 0, UIWidgetType_Panel, bottomBarAttrib);
    
    // deltatime text
    cdl_ui_widget_new(&debug->bottomBarDeltaTimeDisplay, &debug->bottomBar, "", UIWidgetType_Label, bottomBarButtonAttrib);
    
    // elapsed time text
    cdl_ui_widget_new(&debug->bottomBarElapsedTimeDisplay, &debug->bottomBar, "", UIWidgetType_Label, bottomBarButtonAttrib);
    
    // asset inspector toggle
    cdl_ui_widget_new(&debug->bottomBarToggleEngineManagement, &debug->bottomBar, "Engine Management", UIWidgetType_Boolean, bottomBarButtonAttrib);
    
    //- Engine Management Window
    //
    cdl_ui_widget_attributes emWindowAttr = cdl_ui_widget_attributes_new();
    emWindowAttr.flags = UIWidgetFlags_EatInputs | UIWidgetFlags_Resizable;
    emWindowAttr.layout = UIWidgetLayout_Vertical;
    emWindowAttr.constraints.type = UIWidgetConstraint_SizeRelativeX
        | UIWidgetConstraint_SizeRelativeY
        | UIWidgetConstraint_MarginRelativeLeft
        | UIWidgetConstraint_MarginRelativeTop;
    emWindowAttr.constraints.size = v2f_new(0.5, 0.5);
    emWindowAttr.constraints.margins.left = 0.25;
    emWindowAttr.constraints.margins.top = 0.25;
    cdl_ui_widget_new(&debug->emWindow, &debug->windowCanvas, "", UIWidgetType_Panel, emWindowAttr);
    debug->emWindow.hide = 1;
    
    // Window Menu
    cdl_ui_widget_attributes emWMenuBarAttr = cdl_ui_widget_attributes_new();
    emWMenuBarAttr.layout = UIWidgetLayout_Horizontal;
    emWMenuBarAttr.constraints.type = UIWidgetConstraint_SizeRelativeX | UIWidgetConstraint_SizeFixedY;
    emWMenuBarAttr.constraints.size = v2f_new(1, 20);
    cdl_ui_widget_new(&debug->emWMenuBar, &debug->emWindow, "", UIWidgetType_MenuPanel, emWMenuBarAttr);
    
    // Menu Buttons
    cdl_ui_widget_attributes emWMenuButtonAttr = cdl_ui_widget_attributes_new();
    emWMenuButtonAttr.constraints.type = UIWidgetConstraint_SizeFixedX
        | UIWidgetConstraint_SizeFixedY
        | UIWidgetConstraint_MarginFixedLeft;
    emWMenuButtonAttr.constraints.size = v2f_new(100, 20);
    emWMenuButtonAttr.constraints.margins.left = 10;
    
    cdl_ui_widget_new(&debug->emWMBAssetInspectorRadio, &debug->emWMenuBar, "Asset Inspector", UIWidgetType_RadioButton, emWMenuButtonAttr);
    cdl_ui_widget_new(&debug->emWMBProfilerRadio, &debug->emWMenuBar, "Profiler", UIWidgetType_RadioButton, emWMenuButtonAttr);
    cdl_ui_widget_new(&debug->emWMBMemoryRadio, &debug->emWMenuBar, "Memory", UIWidgetType_RadioButton, emWMenuButtonAttr);
    cdl_ui_widget_new(&debug->emWMBConsoleRadio, &debug->emWMenuBar, "Console", UIWidgetType_RadioButton, emWMenuButtonAttr);
    
    debug->emWMBAssetInspectorRadio.toggled = 1;
    cdl_ui_radiobutton_group(&debug->emWMBAssetInspectorRadio, &debug->emWMBConsoleRadio);
    
    // Window Panel
    cdl_ui_widget_attributes emWPanelAttrib = cdl_ui_widget_attributes_new();
    emWPanelAttrib.layout = UIWidgetLayout_Vertical;
    emWPanelAttrib.constraints.type = UIWidgetConstraint_MarginFixedTop;
    cdl_ui_widget_new(&debug->emWPanel, &debug->emWindow, "", UIWidgetType_Panel, emWPanelAttrib);
}

internal debug_tooltip debug_draw_memory_block(debug_memory* debug, memory_block* block, v2i* position)
{
    PROFILED_BLOCK("debug_draw_memory_block");
    
    local color8 colors[2 * MEMORY_POOL_MAX_ENTRIES] = { };
    local bool8 colors_set = 0;
    local i32 slotPerRow = 100;
    
    debug_tooltip result = ZeroStruct;
    
    if ( !colors_set ) {
        random_series* s = &core->rng;
        for (i32 i = 0; i < ARRAY_COUNT(colors); i++) {
            colors[i] = cdl_rgb(random_u32(s), random_u32(s), random_u32(s));
        }
        
        colors_set = 1;
    }
    
    i32 xOffset = position->x;
    i32 yOffset = position->y;
    
    for (i32 i = 0; i < MEMORY_POOL_MAX_ENTRIES; i++) {
        
        memory_block_entry* entry = &block->entries[i];
        if ( entry->id == 0 && entry->location == 0 ) continue;
        
        v2f scale = v2f_new(0, 15);
        f32 sizeInKBytes = entry->size / (f32)KiloBytes(1);
        f32 sizeInMBytes = entry->size / (f32)MegaBytes(1);
        f32 sizeInGBytes = entry->size / (f32)GigaBytes(1);
        
        if ( sizeInGBytes < 1000.f ) {
            scale.x = 128;
        }
        
        if ( sizeInMBytes < 1000.f ) {
            scale.x = 64;
        }
        
        if ( sizeInKBytes < 1000.f ) {
            scale.x = 32;
        }
        
        if ( entry->size < 1000.f ) {
            scale.x = 8;
        }
        
        v2f pos = v2f_new(xOffset + scale.x / 2 + 2,
                          yOffset + ( i / slotPerRow ) * ( scale.y + 2 ));
        xOffset += scale.x + 2;
        position->y = pos.y;
        
        v2f topLeft = v2f_new(pos.x - scale.width / 2, pos.y - scale.height / 2);
        v2f bottomRight = v2f_new(pos.x + scale.width / 2, pos.y + scale.height / 2);
        v2f mousePosition = v2itof(mouse_get_position());
        
        if ( collisions_v2f_in_bounds(mousePosition, topLeft, bottomRight) ) {
            result.display = 1;
            result.ID = i;
            result.data = (void*)block;
        }
        
        color8 color = *(colors + entry->id);
        if ( !entry->used ) { color = cdl_rgb(120, 120, 120); }
        ui_quad(pos, scale, color);
    }
    
    return result;
}

internal void debug_build_tab_asset_inspector(debug_memory* debug)
{
    cdl_ui_widget* w = debug->emWPWidgets;
    char buffer[256] = { };
    
    cdl_ui_widget_attributes emWButtonAttrib = cdl_ui_widget_attributes_new();
    emWButtonAttrib.constraints.type = UIWidgetConstraint_SizeRelativeX
        | UIWidgetConstraint_SizeFixedY
        | UIWidgetConstraint_MarginFixedLeft;
    emWButtonAttrib.constraints.size = v2f_new(0.45, 15);
    emWButtonAttrib.constraints.margins.left = 10;
    
    cdl_ui_widget_attributes emWHeaderAttrib = emWButtonAttrib;
    emWHeaderAttrib.theme = UITheme_Danger;
    
    // Shaders
    {
        cdl_ui_widget* shaderGroupWidget = w++;
        cdl_ui_widget_new(shaderGroupWidget, &debug->emWPanel, "Shaders", UIWidgetType_Group, emWHeaderAttrib);
        cdl_ui_widget* shaderFirstWidget = w;
        
        emWButtonAttrib.theme = UITheme_Base;
        for (u32 i = 0; i < core->assets->nextShader; i++) {
            cdl_assets_shader* s = core->assets->shaders + i;
            cdl_ui_widget* groupWidget = w++;
            cdl_ui_widget_new(groupWidget, &debug->emWPanel, s->head.path, UIWidgetType_Group, emWButtonAttrib);
            
            cdl_ui_widget* firstWidget = w++;
            cdl_ui_widget_new(firstWidget, &debug->emWPanel, ShaderType_to_string[s->type], UIWidgetType_Label, emWButtonAttrib);
            
            cdl_ui_widget* reloadWidget = w++;
            cdl_ui_widget_new(reloadWidget, &debug->emWPanel, "Reload", UIWidgetType_Button, emWButtonAttrib);
            reloadWidget->onPressed = debug_tab_assets_reload_shader;
            reloadWidget->userData1 = debug;
            reloadWidget->userData2 = s;
            
            guid uuid = s->head.uuid;
            sprintf(buffer, "UUID generated {%02hhX%02hhX-%02hhX%02hhX-%02hhX%02hhX-%02hhX%02hhX}\n", 
                    *(u8*)&uuid.bytes[0], *(u8*)&uuid.bytes[2], *(u8*)&uuid.bytes[4], *(u8*)&uuid.bytes[6],
                    *(u8*)&uuid.bytes[8], *(u8*)&uuid.bytes[10], *(u8*)&uuid.bytes[12], *(u8*)&uuid.bytes[14]);
            cdl_ui_widget* lastWidget = w++;
            cdl_ui_widget_new(lastWidget, &debug->emWPanel, buffer, UIWidgetType_Label, emWButtonAttrib);
            
            cdl_ui_group(groupWidget, firstWidget, lastWidget);
        }
        
        cdl_ui_widget* shaderLastWidget = w - 1;
        cdl_ui_group(shaderGroupWidget, shaderFirstWidget, shaderLastWidget);
    }
    
    // Textures
    {
        cdl_ui_widget* textureGroupWidget = w++;
        cdl_ui_widget_new(textureGroupWidget, &debug->emWPanel, "Textures", UIWidgetType_Group, emWHeaderAttrib);
        
        cdl_ui_widget* textureFirstWidget = w;
        
        for (u32 i = 0; i < core->assets->nextTexture; i++) {
            cdl_assets_texture* t = core->assets->textures + i;
            cdl_ui_widget* groupWidget = w++;
            cdl_ui_widget_new(groupWidget, &debug->emWPanel, t->head.path, UIWidgetType_Group, emWButtonAttrib);
            
            cdl_ui_widget* firstWidget = w++;
            cdl_ui_widget_new(firstWidget, &debug->emWPanel, "Show", UIWidgetType_Button, emWButtonAttrib);
            firstWidget->onPressed = debug_tab_assets_show_texture;
            firstWidget->userData1 = debug;
            firstWidget->userData2 = (void*)t;
            
            cdl_ui_widget_new(w++, &debug->emWPanel, TextureType_to_string[t->type], UIWidgetType_Label, emWButtonAttrib);
            
            sprintf(buffer, "Dim: %d x %d\n", t->width, t->height);
            cdl_ui_widget_new(w++, &debug->emWPanel, buffer, UIWidgetType_Label, emWButtonAttrib);
            
            cdl_ui_widget* lastWidget = w++;
            guid uuid = t->head.uuid;
            sprintf(buffer, "UUID generated {%02hhX%02hhX-%02hhX%02hhX-%02hhX%02hhX-%02hhX%02hhX}\n", 
                    *(u8*)&uuid.bytes[0], *(u8*)&uuid.bytes[2], *(u8*)&uuid.bytes[4], *(u8*)&uuid.bytes[6],
                    *(u8*)&uuid.bytes[8], *(u8*)&uuid.bytes[10], *(u8*)&uuid.bytes[12], *(u8*)&uuid.bytes[14]);
            cdl_ui_widget_new(lastWidget, &debug->emWPanel, buffer, UIWidgetType_Label, emWButtonAttrib);
            
            cdl_ui_group(groupWidget, firstWidget, lastWidget);
        }
        
        cdl_ui_widget* textureLastWidget = w - 1;
        cdl_ui_group(textureGroupWidget, textureFirstWidget, textureLastWidget);
    }
    
    // Framebuffers
    {
        cdl_ui_widget* fbGroupWidget = w++;
        cdl_ui_widget_new(fbGroupWidget, &debug->emWPanel, "Framebuffers", UIWidgetType_Group, emWHeaderAttrib);
        
        cdl_ui_widget* fbFirstWidget = w;
        
        // View Framebuffers
        for (cdl_renderer_scene_view* v = renderer->storage.views; v < renderer->storage.nextView; v++) {
            cdl_ui_widget* groupWidget = w++;
            
            if ( v->debugName[0] != 0 ) { 
                cdl_ui_widget_new(groupWidget, &debug->emWPanel, v->debugName, UIWidgetType_Group, emWButtonAttrib);
            } else {
                cdl_ui_widget_new(groupWidget, &debug->emWPanel, "View Framebuffer", UIWidgetType_Group, emWButtonAttrib);
            }
            
            cdl_ui_widget *first, *last;
            first = last = w;
            cdl_ui_widget_new(w++, &debug->emWPanel, "Show", UIWidgetType_Button, emWButtonAttrib);
            first->onPressed = debug_tab_assets_show_texture;
            first->userData1 = debug;
            first->userData3 = (void*)&v->framebuffer->transient->colorTexture;
            
            cdl_ui_group(groupWidget, first, last);
        }
        
        // Effects Framebuffers
        {
            for (u32 i = 0; i < renderer->post.effectCount; i++) {
                cdl_renderer_effect* e = renderer->post.effects + i;
                
                cdl_ui_widget* groupWidget = w++;
                cdl_ui_widget_new(groupWidget, &debug->emWPanel, e->shader->asset->head.path, UIWidgetType_Group, emWButtonAttrib);
                
                cdl_ui_widget *first, *last;
                first = last = w;
                cdl_ui_widget_new(w++, &debug->emWPanel, "Show", UIWidgetType_Button, emWButtonAttrib);
                first->onPressed = debug_tab_assets_show_texture;
                first->userData1 = debug;
                first->userData3 = (void*)&e->framebuffer->transient->colorTexture;
                
                cdl_ui_group(groupWidget, first, last);
            }
        }
        
        cdl_ui_widget* fbLastWidget = w - 1;
        cdl_ui_group(fbGroupWidget, fbFirstWidget, fbLastWidget);
    }
    
    // Raw Models
    {
        cdl_ui_widget* modelGroupWidget = w++;
        cdl_ui_widget_new(modelGroupWidget, &debug->emWPanel, "Models", UIWidgetType_Group, emWHeaderAttrib);
        
        cdl_ui_widget* modelFirstWidget = w;
        
        for (u32 i = 0; i < core->assets->nextRawModel; i++) {
            cdl_assets_model_raw* m = core->assets->rawModels + i;
            
            cdl_ui_widget* groupWidget = w++;
            cdl_ui_widget_new(groupWidget, &debug->emWPanel, m->head.path, UIWidgetType_Group, emWButtonAttrib);
            
            cdl_ui_widget* firstWidget = w++;
            sprintf(buffer, "Vertex count: %d\n", m->vertexCount);
            cdl_ui_widget_new(firstWidget, &debug->emWPanel, buffer, UIWidgetType_Label, emWButtonAttrib);
            
            cdl_ui_widget* lastWidget = w++;
            guid uuid = m->head.uuid;
            sprintf(buffer, "UUID generated {%02hhX%02hhX-%02hhX%02hhX-%02hhX%02hhX-%02hhX%02hhX}\n", 
                    *(u8*)&uuid.bytes[0], *(u8*)&uuid.bytes[2], *(u8*)&uuid.bytes[4], *(u8*)&uuid.bytes[6],
                    *(u8*)&uuid.bytes[8], *(u8*)&uuid.bytes[10], *(u8*)&uuid.bytes[12], *(u8*)&uuid.bytes[14]);
            cdl_ui_widget_new(lastWidget, &debug->emWPanel, buffer, UIWidgetType_Label, emWButtonAttrib);
            
            cdl_ui_group(groupWidget, firstWidget, lastWidget);
        }
        
        cdl_ui_widget* modelLastWidget = w - 1;
        cdl_ui_group(modelGroupWidget, modelFirstWidget, modelLastWidget);
    }
    
    cdl_ui_widget_attributes textureTargetAttrib = cdl_ui_widget_attributes_new();
    textureTargetAttrib.flags = UIWidgetFlags_AbsolutePosition;
    textureTargetAttrib.constraints.type = UIWidgetConstraint_SizeByAspect_HardX
        | UIWidgetConstraint_MarginRelativeLeft
        | UIWidgetConstraint_MarginFixedTop;
    textureTargetAttrib.constraints.size.x = 0.45;
    textureTargetAttrib.constraints.aspect = 1;
    textureTargetAttrib.constraints.margins.top = 10;
    textureTargetAttrib.constraints.margins.left = 0.5;
    
    cdl_ui_widget_new(&debug->emWPTextureTarget, &debug->emWPanel, "", UIWidgetType_TextureTarget, textureTargetAttrib);
}

internal void debug_build_tab_profiler(debug_memory* debug)
{
    cdl_ui_widget* w = debug->emWPWidgets;
    
    cdl_ui_widget_attributes buttonAttr = cdl_ui_widget_attributes_new();
    buttonAttr.constraints.type = UIWidgetConstraint_MarginFixedTop
        | UIWidgetConstraint_MarginFixedLeft
        | UIWidgetConstraint_SizeFixedX
        | UIWidgetConstraint_SizeFixedY;
    buttonAttr.constraints.margins.left = 5;
    buttonAttr.constraints.margins.top = 5;
    buttonAttr.constraints.size = v2f_new(150, 20);
    
    cdl_ui_widget* saveFrameButton = w++;
    cdl_ui_widget_new(saveFrameButton, &debug->emWPanel, "Save Frame", UIWidgetType_Button, buttonAttr);
    saveFrameButton->onPressed = debug_tab_profiler_save;
    saveFrameButton->userData1 = debug;
    
    cdl_ui_widget* memoryWidget = w++;
    cdl_ui_widget_new(memoryWidget, &debug->emWPanel, "Profiler", UIWidgetType_DrawArea, {});
    memoryWidget->onDraw = debug_tab_profiler_on_draw;
    memoryWidget->userData1 = debug;
}

internal void debug_build_tab_memory(debug_memory* debug)
{
    cdl_ui_widget* w = debug->emWPWidgets;
    
    cdl_ui_widget* memoryWidget = w++;
    cdl_ui_widget_new(memoryWidget, &debug->emWPanel, "Memory", UIWidgetType_DrawArea, {});
    
    memoryWidget->onDraw = debug_tab_memory_on_draw;
    memoryWidget->userData1 = debug;
}

internal void debug_build_tab_console(debug_memory* debug)
{
    cdl_ui_widget* w = debug->emWPWidgets;
    
    cdl_ui_widget* consoleWidget = w++;
    cdl_ui_widget_new(consoleWidget, &debug->emWPanel, "Console", UIWidgetType_DrawArea, {});
    
    consoleWidget->onDraw = debug_tab_console_on_draw;
    consoleWidget->userData1 = debug;
}

internal void debug_build_management_window(debug_memory* debug)
{
    debug->emWPanel.child = 0;
    debug->emWPanel.dirty = 1;
    
    if ( w_toggled(debug->emWMBAssetInspectorRadio) ) {
        debug_build_tab_asset_inspector(debug);
    } else if ( w_toggled(debug->emWMBProfilerRadio) ) {
        debug_build_tab_profiler(debug);
    } else if ( w_toggled(debug->emWMBMemoryRadio) ) {
        debug_build_tab_memory(debug);
    } else if ( w_toggled(debug->emWMBConsoleRadio) ) {
        debug_build_tab_console(debug);
    }
}

internal void debug_update_profiled(debug_memory* debug)
{
    PROFILED_BLOCK("debug_update_profiled");
    
    if ( !debug->initialised ) {
        debug_init(debug);
    }
    
    //- Debug UI Widgets
    {
        char textBuffer[64] = { };
        sprintf(textBuffer, "dt: %.1f ms\n", core->deltaTime * 1000.f);
        MemoryCopy(debug->bottomBarDeltaTimeDisplay.label, textBuffer, ARRAY_COUNT(textBuffer));
        sprintf(textBuffer, "engine dt: %.1f ms\n", core->processingTime * 1000.f);
        MemoryCopy(debug->bottomBarElapsedTimeDisplay.label, textBuffer, ARRAY_COUNT(textBuffer));
        cdl_ui_update(&debug->UI);
    }
    
    // ----------- Graphs & Debug Views -------------
    
#if 0
    //- Batch Rendering Buffers
    if ( debug->vars[DebugVar_BatchRenderingToggle].toggled ) {
        local color8 colors[2 * RENDERER_BATCH_MAX_ENTRIES] = { };
        local bool8 colors_set = 0;
        local i32 slotPerRow = 60;
        
        if ( !colors_set ) {
            random_series* s = &debug->rdm;
            for (i32 i = 0; i < ARRAY_COUNT(colors); i++) {
                colors[i] = cdl_rgb(random_u32(s), random_u32(s), random_u32(s));
            }
            
            colors_set = 1;
        }
        
        bool8 printInfo = 0;
        u32 printID = 0;
        i32 maxYPos = 100;
        
        for (i32 i = 0; i < RENDERER_BATCH_MAX_ENTRIES; i++) {
            
            cdl_renderer_batch_entry* entry = &renderer->world.staticTriangles.entries[i];
            if ( entry->id == 0 ) continue;
            
            v2f size = v2f_new(8.f, 15.f);
            v2f pos = v2f_new(15.f + ( i % slotPerRow ) * ( size.width + 2 ),
                              100.f + ( i / slotPerRow ) * ( size.height + 2 ));
            rect2 rectangle = rect2_from_size(pos, size);
            
            if ( pos.y + size.y / 2 > maxYPos ) {
                maxYPos = pos.y;
            }
            
            if ( collisions_v2f_in_rect(mousePosition, rectangle) ) {
                printID = i;
                printInfo = 1;
                size = v2f_times(size, 1.2f);
                
                if ( mouseLeftOnDown ) {
                    cdl_render_scene_remove_static_model(&renderer->world, entry->id);
                    printInfo = 0;
                }
            }
            
            debug_ui_quad(pos, size, colors[entry->id]);
        }
        
        if ( printInfo ) {
            cdl_renderer_batch_entry* entry = &renderer->world.staticTriangles.entries[printID];
            char buffer[300] = { };
            sprintf(buffer, "Static model:\n"
                    " - id: %d\n"
                    " - vertexStart offset: %d\n"
                    " - vertexCount: %d\n"
                    " - indexStart offset: %d\n"
                    " - indexCount: %d\n",
                    entry->id, (i32)(entry->vertexStart - renderer->world.staticTriangles.vertexBuffer), entry->vertexCount, (i32)(entry->indexStart - renderer->world.staticTriangles.indexBuffer), entry->indexCount);
            
            cdl_text_atb attributes = cdl_text_atb_new();
            attributes.font = renderer->currentFont;
            attributes.anchor = TextAnchor_TopLeft;
            v2f textDim = cdl_text_get_dimensions(buffer, attributes);
            v2f quadDim = v2f_times(textDim, 1.05f);
            v2f quadPos = v2f_sub(mousePosition, v2f_times(quadDim, -0.5f));
            color8 quadColor = cdl_rgba(25, 25, 25, 190);
            debug_ui_quad(quadPos, quadDim, quadColor);
            debug_ui_text(buffer, mousePosition, attributes);
        }
        
        // Draw static world memory occupancy
        v2f staticBatchSize = v2f_new(200, 15);
        maxYPos += staticBatchSize.height;
        v2f staticBatchPos = v2f_new(15 + staticBatchSize.width / 2, maxYPos + staticBatchSize.height / 2);
        
        f32 amountOccupied = renderer->world.staticTriangles.vertexCount / (f32)(RENDERER_BATCH_MAX_TRIANGLE_VERTEX_COUNT);
        v2f staticBatchOccupiedSize = v2f_new(amountOccupied * 200, 15);
        v2f staticBatchOccupiedPos = v2f_new(15 + staticBatchOccupiedSize.width / 2, maxYPos + staticBatchSize.height / 2);
        
        debug_ui_quad(staticBatchPos, staticBatchSize, color_gray);
        debug_ui_quad(staticBatchOccupiedPos, staticBatchOccupiedSize, color_red);
    }
#endif
    
    //- Asset Inspector
    debug->emWindow.hide = !w_toggled(debug->bottomBarToggleEngineManagement);
    
    if ( w_pressed(debug->bottomBarToggleEngineManagement)
        || w_pressed(debug->emWMBAssetInspectorRadio)
        || w_pressed(debug->emWMBProfilerRadio)
        || w_pressed(debug->emWMBMemoryRadio)
        || w_pressed(debug->emWMBConsoleRadio) ) {
        debug_build_management_window(debug);
    }
    
    if ( keyboard_key_on_down(KEY_F10) ) {
        debug_profiler_save_frame(debug);
    }
}

internal void debug_update(debug_memory* debug)
{
    debug_update_profiled(debug);
}