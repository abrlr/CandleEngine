internal cdl_audio_track* cdl_audio_track_queue(cdl_assets_audio* trackAsset)
{
    assert(trackAsset);
    PROFILED_FUNC;
    
    cdl_audio_track* result = 0;
    cdl_audio_mixer* mixer = &core->mixer;
    
    for (u32 trackIndex = 0; trackIndex < ARRAY_COUNT(mixer->playing); trackIndex++) {
        cdl_audio_track* current = mixer->playing + trackIndex;
        if ( !current->asset ) {
            current->asset = trackAsset;
            result = &mixer->playing[trackIndex];
            result->currentSample = 0;
            result->volume = 1;
            break;
        }
    }
    
    return result;
}

//- [SECTION] Audio Mixer
internal void cdl_audio_play()
{
    PROFILED_FUNC;
    
    cdl_audio_mixer* mixer = &core->mixer;
    u32 sampleCount = mixer->native.sampleCount;
    
    
    i16* samplePtr = mixer->native.samples;
    for (u32 sampleIndex = 0; sampleIndex < sampleCount; sampleIndex++) {
        *samplePtr++ = 0;
        *samplePtr++ = 0;
    }
    
    
    for (u32 trackIndex = 0; trackIndex < ARRAY_COUNT(mixer->playing); trackIndex++) {
        cdl_audio_track* track = mixer->playing + trackIndex;
        if ( !track->asset ) { continue; }
        
        samplePtr = mixer->native.samples;
        i16 volume = track->volume * mixer->volume;
        
        i16* leftSamples = track->asset->leftSamples;
        i16* rightSamples = 0;
        
        switch ( track->asset->channels ) {
            case 1:
            {
                rightSamples = track->asset->leftSamples;
            } break;
            
            default:
            {
                rightSamples = track->asset->rightSamples;
            } break;
        }
        
        for (u32 sampleIndex = 0; sampleIndex < sampleCount; sampleIndex++) {
            u32 offset = ( track->currentSample + sampleIndex ) % track->asset->sampleCount;
            
            if ( track->loop || track->currentSample + sampleIndex < track->asset->sampleCount ) {
                i16 sampleLeft = *(leftSamples + offset);
                i16 sampleRight = *(rightSamples + offset);
                *samplePtr++ += sampleLeft * volume;
                *samplePtr++ += sampleRight * volume;
            }
            
            else {
                mixer->playing[trackIndex] = { };
                break;
            }
        }
        
        track->currentSample += sampleCount;
    }
}