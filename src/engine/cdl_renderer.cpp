cdl_renderer* renderer;

#ifdef CDL_DEBUG_GL
#define check_gl_proc(glCall) glCall; cdl_renderer_api_flush_errors(STRINGIFY(glCall), __LINE__)
#else
#define check_gl_proc(glCall) glCall;
#endif

//- [SECTION] Misc
inline v2f pixel_to_gl_size(v2i pixelSize) 
{
	return {
		pixelSize.width * 2.f / core->frameWidth,
		pixelSize.height * 2.f / core->frameWidth
	};
}

inline v2f pixel_to_gl_coordinates(v2i pixelCoordinates) 
{
	return {
		( ( pixelCoordinates.x + 0.5f ) / core->frameWidth ) * 2.f - 1.f,
		1.f - ( ( pixelCoordinates.y + 0.5f ) / core->frameHeight ) * 2.f
	};
}

inline v2f pixelf_to_gl_size(v2f pixelSize) 
{
	return {
		pixelSize.width * 2.f / core->frameWidth,
		pixelSize.height * 2.f / core->frameWidth
	};
}

inline v2f pixelf_to_gl_coordinates(v2f pixelCoordinates) 
{
	return {
		( ( pixelCoordinates.x + 0.5f ) / core->frameWidth ) * 2.f - 1.f,
		1.f - ( ( pixelCoordinates.y + 0.5f ) / core->frameHeight ) * 2.f
	};
}


//- [SECTION] Colors
inline color8 cdl_rgb(u8 r, u8 g, u8 b)
{
    return { r, g, b, 255 };
}

inline color8 cdl_rgba(u8 r, u8 g, u8 b, u8 a)
{
    return { r, g, b, a };
}

inline color8 cdl_rgba_from_v4f_new(v4f color)
{
    PROFILED_FUNC;
    
    color8 result = {
        (u8)(color.r * 255),
        (u8)(color.g * 255),
        (u8)(color.b * 255),
        (u8)(color.a * 255)
    };
    
    return result;
}

internal color8 cdl_rgba_lerp(color8 target, color8 source, f32 t)
{
    PROFILED_FUNC;
    
    color8 result = source;
    result.r = f32_ceil(f32_lerp(target.r, source.r, t));
    result.g = f32_ceil(f32_lerp(target.g, source.g, t));
    result.b = f32_ceil(f32_lerp(target.b, source.b, t));
    result.a = f32_ceil(f32_lerp(target.a, source.a, t));
    return result;
}

internal v4f cdl_rgba_adjust_hue(color8 rgbColor, f32 hueAdjust)
{
    PROFILED_FUNC;
    
    v4f result = ZeroStruct;
    
    // Constants
    v4f kRGBToYPrime = v4f_new(0.299, 0.587, 0.114, 0.0);
    v4f kRGBToI = v4f_new(0.596, -0.275, -0.321, 0.0);
    v4f kRGBToQ = v4f_new(0.212, -0.523, 0.311, 0.0);
    
    v4f kYIQToR = v4f_new(1.0, 0.956, 0.621, 0.0);
    v4f kYIQToG = v4f_new(1.0, -0.272, -0.647, 0.0);
    v4f kYIQToB = v4f_new(1.0, -1.107, 1.704, 0.0);
    
    // Convert to YIQ
    v4f v4fColor = v4f_new(rgbColor.r / 255.f, rgbColor.g / 255.f, rgbColor.b / 255.f, rgbColor.a / 255.f);
    f32 YPrime = v4f_dot(v4fColor, kRGBToYPrime);
    f32 I = v4f_dot(v4fColor, kRGBToI);
    f32 Q = v4f_dot(v4fColor, kRGBToQ);
    
    // Calculate the hue and chroma
    f32 hue = 0;
    if ( !f32_equals(I, 0) ) {
        hue = atan(Q / I);
    }
    f32 chroma = f32_square_root_approx(I * I + Q * Q);
    
    // Make the user's adjustments
    hue += hueAdjust;
    
    // Convert back to YIQ
    Q = chroma * sin(hue);
    I = chroma * cos(hue);
    
    // Convert back to RGB
    v4f yIQ = v4f_new(YPrime, I, Q, 0.0);
    result.r = v4f_dot(yIQ, kYIQToR);
    result.g = v4f_dot(yIQ, kYIQToG);
    result.b = v4f_dot(yIQ, kYIQToB);
    result.a = rgbColor.a / 255.f;
    
    return result;
}

internal color8 cdl_rgb_desaturate(color8 color, f32 desaturation)
{
    PROFILED_FUNC;
    
    color8 result = color;
    f32 L = 0.3 * color.r + 0.6 * color.g + 0.1 * color.b;
    result.r = color.r + desaturation * ( L - color.r );
    result.g = color.g + desaturation * ( L - color.g );
    result.b = color.b + desaturation * ( L - color.b );
    return result;
}

internal color8 cdl_hex(u32 hexValue)
{
    color8 result = ZeroStruct;
    result.r = (hexValue >> 16) & 0xFF;
    result.g = (hexValue >> 8) & 0xFF;
    result.b = hexValue & 0xFF;
    result.a = 255;
    return result;
}


//- [SECTION] Textures
internal cdl_renderer_texture* cdl_renderer_texture_new(cdl_assets_texture* asset)
{
    assert(renderer->storage.nextTexture);
    assert(asset);
    assert(asset->head.id != INVALID_ASSET);
    PROFILED_FUNC;
    
    cdl_renderer_texture* result = renderer->storage.nextTexture++;
    result->asset = asset;
    if ( asset->head.state != AssetState_Loaded ) {
        cdl_assets_texture_load_pixels(asset);
    }
    cdl_renderer_texture_load(result);
    return result;
}

internal void cdl_renderer_texture_load(cdl_renderer_texture* texture)
{
    assert(texture);
    PROFILED_FUNC;
    
    check_gl_proc(glGenTextures(1, &texture->id));
    check_gl_proc(glActiveTexture(GL_TEXTURE0));
    check_gl_proc(glBindTexture(GL_TEXTURE_2D, texture->id));
    
    // Texture loading
    check_gl_proc(glEnable(GL_BLEND));
    check_gl_proc(glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA));
    texture->loaded = 1;
    
    if (texture->asset->pixels) {
        u32 channels = texture->asset->channels;
        
        switch (channels) {
            case 1:
            {
                check_gl_proc(glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, texture->asset->width, texture->asset->height, 0, GL_RED, GL_UNSIGNED_BYTE, texture->asset->pixels));
                check_gl_proc(glGenerateMipmap(GL_TEXTURE_2D));
                check_gl_proc(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR));
                check_gl_proc(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR));
                check_gl_proc(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE));
                check_gl_proc(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE));
                
                log_info("Asset %d: Texture %d (%s) was loaded: (%d x %d) pixels and %d channels\n",
                         texture->asset->head.id, texture->id, texture->asset->head.path, texture->asset->width, texture->asset->height, texture->asset->channels);
            } break;
            
            case 3:
            {
                check_gl_proc(glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, texture->asset->width, texture->asset->height, 0, GL_RGB, GL_UNSIGNED_BYTE, texture->asset->pixels));
                check_gl_proc(glGenerateMipmap(GL_TEXTURE_2D));
                check_gl_proc(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR));
                check_gl_proc(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR));
                check_gl_proc(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE));
                check_gl_proc(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE));
                
                log_info("Asset %d: Texture %d (%s) was loaded: (%d x %d) pixels and %d channels\n",
                         texture->asset->head.id, texture->id, texture->asset->head.path, texture->asset->width, texture->asset->height, texture->asset->channels);
            } break;
            
            case 4:
            {
                check_gl_proc(glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, texture->asset->width, texture->asset->height, 0, GL_RGBA, GL_UNSIGNED_BYTE, texture->asset->pixels));
                check_gl_proc(glGenerateMipmap(GL_TEXTURE_2D));
                check_gl_proc(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR));
                check_gl_proc(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR));
                check_gl_proc(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE));
                check_gl_proc(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE));
                
                log_info("Asset %d: Texture %d (%s) was loaded: (%d x %d) pixels and %d channels\n",
                         texture->asset->head.id, texture->id, texture->asset->head.path, texture->asset->width, texture->asset->height, texture->asset->channels);
            } break;
            
            default:
            {
                log_err("Loading texture with %d channels is not supported\n", channels);
                log_err("%s was not loaded properly\n", texture->asset->head.path);
                check_gl_proc(glDeleteTextures(1, &texture->id));
                texture->asset->head.id = INVALID_ASSET;
                texture->loaded = 0;
            } break;
        }
        
        TranRelease(texture->asset->pixels);
        texture->asset->pixels = 0;
    } else {
        log_err("%s was not loaded properly\n", texture->asset->head.path);
        check_gl_proc(glDeleteTextures(1, &texture->id));
        texture->asset->head.id = INVALID_ASSET;
        texture->loaded = 0;
    }
    
    check_gl_proc(glBindTexture(GL_TEXTURE_2D, 0));
}

// TODO(abe): remove this function
internal cdl_renderer_texture* cdl_renderer_texture_get(cdl_assets_texture* asset)
{
    assert(asset);
    PROFILED_FUNC;
    
    cdl_renderer_texture* result = 0;
    cdl_renderer_texture* current = renderer->storage.textures;
    while ( current && (current - renderer->storage.textures) < ARRAY_COUNT(renderer->storage.textures) ) {
        if ( current->asset == asset ) {
            result = current;
            break;
        }
        current++;
    }
    
    if ( !result ) {
        result = cdl_renderer_texture_new(asset);
    }
    
    assert(result);
    return result;
}

internal i32 cdl_renderer_texture_push(cdl_render_scene* s, cdl_renderer_texture* texture)
{
    assert(texture);
    PROFILED_FUNC;
    
    if ( !texture->loaded ) {
        if ( texture->asset->head.id == INVALID_ASSET ) {
            return -1;
        }
        
        log_info("Loading in cdl_renderer_texture_push\n");
        cdl_renderer_texture_load(texture);
    }
    
    for (i32 i = 0; i < RENDERER_MAX_TEXTURE_UNIT_PER_FRAME; i++) {
        if ( s->textures[i] == 0 || s->textures[i] == texture ) {
            s->textures[i] = texture;
            return i + 1;
        }
    }
    
    // NOTE(abe): Trying to push more textures than allowed in one frame
    assert(0);
    return -1;
}


//- [SECTION] Shaders
internal cdl_renderer_shader* cdl_renderer_shader_new(RendererShaderType type, cdl_assets_shader* asset)
{
    assert(asset);
    PROFILED_FUNC;
    
    cdl_renderer_shader* result = renderer->storage.nextShader++;
    result->type = type;
    result->asset = asset;
    return result;
}

internal void cdl_renderer_shader_load(cdl_renderer_shader* shader)
{
    assert(shader);
    PROFILED_FUNC;
    
    char* shaderName = shader->asset->head.path + GetLastSlashLoc(shader->asset->head.path) + 1;
    shader->isValid = 1;
    
    if ( shader->ID ) {
        check_gl_proc(glDeleteShader(shader->ID));
        cdl_assets_shader_load_code(shader->asset);
    }
    
    u32 vertexID;
    vertexID = check_gl_proc(glCreateShader(GL_VERTEX_SHADER));
    check_gl_proc(glShaderSource(vertexID, 1, &shader->asset->vertexCode.string, NULL));
    check_gl_proc(glCompileShader(vertexID));
    
    int success;
    char infoLog[512] = { };
    check_gl_proc(glGetShaderiv(vertexID, GL_COMPILE_STATUS, &success));
    if( !success ) {
        check_gl_proc(glGetShaderInfoLog(vertexID, 512, NULL, infoLog));
        for (i32 i = 0; i < 512; i++) if (infoLog[i] == '\n') infoLog[i] = ':';
        log_err("Error loading vertex shader %s.\n", shaderName);
        log_msg("Additionnal information: %s.\n", infoLog);
        shader->isValid = 0;
    }
    
    u32 fragmentID;
    fragmentID = check_gl_proc(glCreateShader(GL_FRAGMENT_SHADER));
    check_gl_proc(glShaderSource(fragmentID, 1, &shader->asset->fragmentCode.string, NULL));
    check_gl_proc(glCompileShader(fragmentID));
    
    check_gl_proc(glGetShaderiv(fragmentID, GL_COMPILE_STATUS, &success));
    if( !success ) {
        check_gl_proc(glGetShaderInfoLog(vertexID, 512, NULL, infoLog));
        for (i32 i = 0; i < 512; i++) if (infoLog[i] == '\n') infoLog[i] = ':';
        log_err("Error loading fragment shader %s.\n", shaderName);
        log_msg("Additionnal information: %s.\n", infoLog);
        shader->isValid = 0;
    }
    
    shader->ID = check_gl_proc(glCreateProgram());
    check_gl_proc(glAttachShader(shader->ID, vertexID));
    check_gl_proc(glAttachShader(shader->ID, fragmentID));
    check_gl_proc(glLinkProgram(shader->ID));
    
    check_gl_proc(glGetProgramiv(shader->ID, GL_LINK_STATUS, &success));
    if( !success ) {
        check_gl_proc(glGetProgramInfoLog(shader->ID, 512, NULL, infoLog));
        for (i32 i = 0; i < 512; i++) if (infoLog[i] == '\n') infoLog[i] = ':';
        log_err("Error linking shader %s.\n", shaderName);
        log_msg("Additionnal information: %s.\n", infoLog);
        shader->isValid = 0;
    }
    
    check_gl_proc(glDeleteShader(vertexID));
    check_gl_proc(glDeleteShader(fragmentID));
    
    if ( shader->isValid ) {
        check_gl_proc(glUseProgram(shader->ID));
        i32 u_Textures[RENDERER_MAX_TEXTURE_UNIT_PER_FRAME] = { };
        for (i32 i = 0; i < RENDERER_MAX_TEXTURE_UNIT_PER_FRAME; i++) {
            u_Textures[i] = i;
        }
        int textUnitLoc = check_gl_proc(glGetUniformLocation(shader->ID, "uTextures"));
        check_gl_proc(glUniform1iv(textUnitLoc, RENDERER_MAX_TEXTURE_UNIT_PER_FRAME, u_Textures));
        
        TranRelease(shader->asset->vertexCode.string);
        TranRelease(shader->asset->fragmentCode.string);
    }
}

internal void cdl_renderer_shader_use(cdl_renderer_shader* shader, cdl_renderer_scene_view* view)
{
    PROFILED_FUNC;
    
    if ( shader->asset->head.state != AssetState_Loaded ) {
        cdl_assets_shader_load_code(shader->asset);
    }
    
    if ( !shader->ID ) {
        cdl_renderer_shader_load(shader);
    }
    
    if ( !shader->isValid ) {
        check_gl_proc(glUseProgram(0));
        return;
    }

    check_gl_proc(glUseProgram(shader->ID));
    renderer->currentShader = shader;
    
    // Common uniforms defined
    cdl_renderer_shader_uniform_set_float2(shader, "g_uResolution", (f32)core->frameWidth, (f32)core->frameHeight);
    cdl_renderer_shader_uniform_set_float(shader, "g_uAspectRatio", cdl_renderer_aspect_ratio());
    cdl_renderer_shader_uniform_set_float(shader, "g_uTime", core->totalTime);
    
    if ( shader->type == RendererShaderType_World ) {
        
        if ( shader->lit ) {
            // Load Lights
            if ( view->scene->useLights ) {
                for (cdl_renderer_light* l = view->scene->lights; l < view->scene->lights + 8; l++) {
                    char uniformName[64] = { };
                    sprintf(uniformName, "u_Lights[%d].position", (u32)(l - view->scene->lights));
                    cdl_renderer_shader_uniform_set_float3(shader, uniformName, l->position.x, l->position.y, l->position.z);
                    sprintf(uniformName, "u_Lights[%d].intensity", (u32)(l - view->scene->lights));
                    cdl_renderer_shader_uniform_set_float(shader, uniformName, l->intensity);
                    sprintf(uniformName, "u_Lights[%d].color", (u32)(l - view->scene->lights));
                    u32 color = *(u32*)&l->color;
                    cdl_renderer_shader_uniform_set_uint(shader, uniformName, color);
                }
            } else {
                // TODO(abe): maybe clear the lights if we don't use them 
                // to avoid having bad light data in the shader
            }
            
            // Load Environment
            {
                v3f dir = view->scene->environment.ambientDirection;
                cdl_renderer_shader_uniform_set_float3(shader, "u_Environment.direction", dir.x, dir.y, dir.z);
                cdl_renderer_shader_uniform_set_uint(shader, "u_Environment.color", *(u32*)&view->scene->environment.ambientColor);
                cdl_renderer_shader_uniform_set_float(shader, "u_Environment.intensity", view->scene->environment.ambientIntensity);
            }
        }
        
        if ( view->camera ) {
            cdl_renderer_camera* cam = view->camera;
            m4f viewMatrix = cdl_renderer_camera_get_view(cam);
            m4f proj_view_mat = m4f_mult(&cam->projection, &viewMatrix);
            
            // Transpose matrices because OpenGL matrice are row major, not column major
            proj_view_mat = m4f_transpose(&proj_view_mat);
            cdl_renderer_shader_uniform_set_mat4(shader, "u_ProjViewMat", proj_view_mat);
            
            v3f camPos = cam->position;
            cdl_renderer_shader_uniform_set_float3(shader, "u_CamPosition", camPos.x, camPos.y, camPos.z);
        }
    }
}

internal void cdl_renderer_shader_uniform_set_uint(cdl_renderer_shader* shader, char* uniform, u32 value)
{
    PROFILED_FUNC;
    int uniformLocation = check_gl_proc(glGetUniformLocation(shader->ID, uniform));
    check_gl_proc(glUniform1ui(uniformLocation, value));
}

internal void cdl_renderer_shader_uniform_set_int(cdl_renderer_shader* shader, char* uniform, i32 value)
{
    PROFILED_FUNC;
    int uniformLocation = check_gl_proc(glGetUniformLocation(shader->ID, uniform));
    check_gl_proc(glUniform1i(uniformLocation, value));
}

internal void cdl_renderer_shader_uniform_set_int2(cdl_renderer_shader* shader, char* uniform, i32 x, i32 y)
{
    PROFILED_FUNC;
    int uniformLocation = check_gl_proc(glGetUniformLocation(shader->ID, uniform));
    check_gl_proc(glUniform2i(uniformLocation, x, y));
}

internal void cdl_renderer_shader_uniform_set_int3(cdl_renderer_shader* shader, char* uniform, i32 x, i32 y, i32 z)
{
    PROFILED_FUNC;
    int uniformLocation = check_gl_proc(glGetUniformLocation(shader->ID, uniform));
    check_gl_proc(glUniform3i(uniformLocation, x, y, z));
}

internal void cdl_renderer_shader_uniform_set_int4(cdl_renderer_shader* shader, char* uniform, i32 x, i32 y, i32 z, i32 w)
{
    PROFILED_FUNC;
    int uniformLocation = check_gl_proc(glGetUniformLocation(shader->ID, uniform));
    check_gl_proc(glUniform4i(uniformLocation, x, y, z, w));
}

internal void cdl_renderer_shader_uniform_set_float(cdl_renderer_shader* shader, char* uniform, f32 value)
{
    PROFILED_FUNC;
    int uniformLocation = check_gl_proc(glGetUniformLocation(shader->ID, uniform));
    check_gl_proc(glUniform1f(uniformLocation, value));
}

internal void cdl_renderer_shader_uniform_set_float2(cdl_renderer_shader* shader, char* uniform, f32 x, f32 y)
{
    PROFILED_FUNC;
    int uniformLocation = check_gl_proc(glGetUniformLocation(shader->ID, uniform));
    check_gl_proc(glUniform2f(uniformLocation, x, y));
}

internal void cdl_renderer_shader_uniform_set_float3(cdl_renderer_shader* shader, char* uniform, f32 x, f32 y, f32 z)
{
    PROFILED_FUNC;
    int uniformLocation = check_gl_proc(glGetUniformLocation(shader->ID, uniform));
    check_gl_proc(glUniform3f(uniformLocation, x, y, z));
}

internal void cdl_renderer_shader_uniform_set_float4(cdl_renderer_shader* shader, char* uniform, f32 x, f32 y, f32 z, f32 w)
{
    PROFILED_FUNC;
    int uniformLocation = check_gl_proc(glGetUniformLocation(shader->ID, uniform));
    check_gl_proc(glUniform4f(uniformLocation, x, y, z, w));
}

internal void cdl_renderer_shader_uniform_set_mat4(cdl_renderer_shader* shader, char* uniform, const m4f& mat)
{
    PROFILED_FUNC;
    int uniformLocation = check_gl_proc(glGetUniformLocation(shader->ID, uniform));
    check_gl_proc(glUniformMatrix4fv(uniformLocation, 1, GL_FALSE, (GLfloat*)&mat));
}


//- [SECTION] Materials
internal cdl_renderer_material* cdl_renderer_material_new(cdl_renderer_shader* shader)
{
    PROFILED_FUNC;
    
    cdl_renderer_material* result = renderer->storage.nextMaterial++;
    
    if ( shader == 0 ) {
        result->shader = renderer->defaultMaterial->shader;
    } else {
        result->shader = shader;
    }
    
    return result;
}

internal void cdl_renderer_material_use(cdl_render_scene* s, cdl_renderer_material* material)
{
    assert(material);
    PROFILED_FUNC;
    
    if ( material->wireframe ) {
        check_gl_proc(glPolygonMode(GL_FRONT_AND_BACK, GL_LINE));
    } else {
        check_gl_proc(glPolygonMode(GL_FRONT_AND_BACK, GL_FILL));
    }
    
    //- Load Uniforms
    if ( material->shader->isValid ) {
        switch (material->shader->type) {
            case RendererShaderType_World:
            {
                // Set material uniforms (metalness, roughness, etc..)
                cdl_renderer_shader_uniform_set_uint(material->shader, "u_Material.tint", *(u32*)&material->color);
                cdl_renderer_shader_uniform_set_uint(material->shader, "u_Material.textures", material->textureUniform);
            } break;
            
            case RendererShaderType_UI:
            case RendererShaderType_Text:
            {
            } break;
            
            default:
            {
                assert(0); // Invalid Code Path
            } break;
        }
    }
}


//- [SECTION] Camera
internal cdl_renderer_camera* cdl_renderer_camera_new(CameraType type)
{
    PROFILED_FUNC;
    
    cdl_renderer_camera* result = renderer->storage.nextCamera++;
    result->type = type;
    result->nearPlane = 0.1;
    result->farPlane = 1000;
    
    switch (type) {
        case CameraType_Perspective:
        {
            result->fov = 90.0f;
        } break;
        
        case CameraType_Orthographic:
        {
            result->zoom = 1.f;
        } break;
    }
    
    cdl_renderer_camera_update_projection(result);
    
    return result;
}

internal void cdl_renderer_camera_update_projection(cdl_renderer_camera* camera)
{
    PROFILED_FUNC;
    
    switch (camera->type) {
        case CameraType_Perspective:
        {
            camera->projection = m4f_perspective(f32_deg_to_rad(camera->fov), cdl_renderer_aspect_ratio(), camera->farPlane, camera->nearPlane);
        } break;
        
        case CameraType_Orthographic:
        {
            camera->projection = m4f_orthographic(cdl_renderer_aspect_ratio() * camera->zoom, -cdl_renderer_aspect_ratio() * camera->zoom, camera->zoom, -camera->zoom, camera->nearPlane, camera->farPlane);
            
        } break;
    }
}

internal m4f cdl_renderer_camera_get_view(cdl_renderer_camera* camera)
{
    PROFILED_FUNC;
    
    m4f viewMat = m4f_identity();
    v3f xAxis = v3f_new(1.f, 0.f, 0.f);
    v3f yAxis = v3f_new(0.f, 1.f, 0.f);
    v3f zAxis = v3f_new(0.f, 0.f, 1.f);
    
    v3f negCamPos = v3f_times(camera->position, -1);
    m4f translation = m4f_translation_matrix(negCamPos);
    viewMat = m4f_mult(&translation, &viewMat);
    
    m4f rotationZ = m4f_rotation_matrix(zAxis, camera->rotation.z);
    viewMat = m4f_mult(&rotationZ, &viewMat);
    
    m4f rotationY = m4f_rotation_matrix(yAxis, camera->rotation.y);
    viewMat = m4f_mult(&rotationY, &viewMat);
    
    m4f rotationX = m4f_rotation_matrix(xAxis, camera->rotation.x);
    viewMat = m4f_mult(&rotationX, &viewMat);
    
    return viewMat;
}


//- [SECTION] Framebuffers
internal cdl_renderer_api_framebuffer* cdl_renderer_internal_api_framebuffer_get_empty()
{
    PROFILED_FUNC;
    
    cdl_renderer_api_framebuffer* result = 0;
    for (u32 i = 0; i < ARRAY_COUNT(renderer->storage.apiFramebuffers); i++) {
        if ( !renderer->storage.apiFramebuffers[i].valid ) {
            result = renderer->storage.apiFramebuffers + i;
            break;
        }
    }
    
    assert(result);
    return result;
}

internal cdl_renderer_api_framebuffer* cdl_renderer_api_framebuffer_new()
{
    PROFILED_FUNC;
    
    cdl_renderer_api_framebuffer* result = cdl_renderer_internal_api_framebuffer_get_empty();
    
    result->id = INVALID_FRAMEBUFFER;
    result->color = INVALID_TEXTURE;
    result->depthStencil = INVALID_TEXTURE;
    result->dim = v2f_new(core->frameWidth, core->frameHeight);
    result->samples = 1;
    
    check_gl_proc(glGenFramebuffers(1, &result->id));
    check_gl_proc(glBindFramebuffer(GL_FRAMEBUFFER, result->id));
    
    // Create Color Attachment
    check_gl_proc(glGenTextures(1, &result->color));
    check_gl_proc(glBindTexture(GL_TEXTURE_2D, result->color));
    
    check_gl_proc(glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, result->dim.width, result->dim.height, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0));
    check_gl_proc(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR));
    check_gl_proc(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR));
    check_gl_proc(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE));
    check_gl_proc(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE));
    
    check_gl_proc(glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, result->color, 0));
    
    result->colorTexture.loaded = 1;
    result->colorTexture.id = result->color;
    
    // Create Depth & Stencil Attachment
    check_gl_proc(glGenTextures(1, &result->depthStencil));
    check_gl_proc(glBindTexture(GL_TEXTURE_2D, result->depthStencil));
    
    check_gl_proc(glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH24_STENCIL8, result->dim.width, result->dim.height, 0, GL_DEPTH_STENCIL, GL_UNSIGNED_INT_24_8, 0));
    check_gl_proc(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR));
    check_gl_proc(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR));
    check_gl_proc(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE));
    check_gl_proc(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE));
    
    check_gl_proc(glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_TEXTURE_2D, result->depthStencil, 0));
    check_gl_proc(glBindTexture(GL_TEXTURE_2D, 0));
    
    result->depthStencilTexture.loaded = 1;
    result->depthStencilTexture.id = result->depthStencil;
    
    u32 framebufferStatus = check_gl_proc(glCheckFramebufferStatus(GL_FRAMEBUFFER));
    if ( framebufferStatus == GL_FRAMEBUFFER_COMPLETE ) {
        result->valid = 1;
        log_msg("Framebuffer %d complete (color: %d, depth/stencil: %d)\n", result->id, result->color, result->depthStencil);
    } else {
        log_err("Framebuffer %d incomplete (color: %d, depth/stencil: %d)\n", result->id, result->color, result->depthStencil);
    }
    
    check_gl_proc(glBindFramebuffer(GL_FRAMEBUFFER, 0));
    
    return result;
}

internal cdl_renderer_api_framebuffer* cdl_renderer_api_framebuffer_multisampled_new(u8 samples)
{
    PROFILED_FUNC;
    
    cdl_renderer_api_framebuffer* result = cdl_renderer_internal_api_framebuffer_get_empty();
    
    result->id = INVALID_FRAMEBUFFER;
    result->color = INVALID_TEXTURE;
    result->depthStencil = INVALID_TEXTURE;
    result->dim = v2f_new(core->frameWidth, core->frameHeight);
    result->samples = samples;
    
    check_gl_proc(glGenFramebuffers(1, &result->id));
    check_gl_proc(glBindFramebuffer(GL_FRAMEBUFFER, result->id));
    
    // Multisampled Color Attachment
    check_gl_proc(glGenTextures(1, &result->color));
    check_gl_proc(glBindTexture(GL_TEXTURE_2D_MULTISAMPLE, result->color));
    
    check_gl_proc(glTexImage2DMultisample(GL_TEXTURE_2D_MULTISAMPLE, samples, GL_RGBA16F, result->dim.width, result->dim.height, GL_TRUE));
    check_gl_proc(glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D_MULTISAMPLE, result->color, 0));
    check_gl_proc(glBindTexture(GL_TEXTURE_2D_MULTISAMPLE, 0));
    
    // Multisampled Depth & Stencil Attachment
    check_gl_proc(glGenRenderbuffers(1, &result->depthStencil));
    check_gl_proc(glBindRenderbuffer(GL_RENDERBUFFER, result->depthStencil));
    
    check_gl_proc(glRenderbufferStorageMultisample(GL_RENDERBUFFER, samples, GL_DEPTH24_STENCIL8, result->dim.width, result->dim.height));
    check_gl_proc(glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, result->depthStencil));
    check_gl_proc(glBindRenderbuffer(GL_RENDERBUFFER, 0));
    
    u32 framebufferStatus = check_gl_proc(glCheckFramebufferStatus(GL_FRAMEBUFFER));
    if ( framebufferStatus == GL_FRAMEBUFFER_COMPLETE ) {
        result->valid = 1;
        log_msg("Multisampled Framebuffer %d complete (color: %d, depth/stencil: %d)\n", result->id, result->color, result->depthStencil);
    } else {
        log_err("Multisampled Framebuffer %d incomplete (color: %d, depth/stencil: %d)\n", result->id, result->color, result->depthStencil);
    }
    
    check_gl_proc(glBindFramebuffer(GL_FRAMEBUFFER, 0));
    
    return result;
}

internal void cdl_renderer_api_framebuffer_clear(cdl_renderer_api_framebuffer* framebuffer)
{
    PROFILED_FUNC;
    
    if ( framebuffer->color != INVALID_TEXTURE ) {
        log_msg("Deleting framebuffer texture %d\n", framebuffer->color);
        check_gl_proc(glDeleteTextures(1, &framebuffer->color));
        framebuffer->color = INVALID_TEXTURE;
    }
    
    if ( framebuffer->depthStencil != INVALID_TEXTURE ) {
        log_msg("Deleting framebuffer depth stencil %d\n", framebuffer->depthStencil);
        check_gl_proc(glDeleteTextures(1, &framebuffer->depthStencil));
        framebuffer->depthStencil = INVALID_TEXTURE;
    }
    
    if ( framebuffer->id != INVALID_FRAMEBUFFER ) {
        log_msg("Deleting framebuffer %d\n", framebuffer->id);
        check_gl_proc(glDeleteFramebuffers(1, &framebuffer->id));
    }
    framebuffer->id = INVALID_FRAMEBUFFER;
    
    *framebuffer = ZeroStruct;
}

internal cdl_renderer_framebuffer* cdl_renderer_internal_framebuffer_get_empty()
{
    PROFILED_FUNC;
    
    cdl_renderer_framebuffer* result = 0;
    for (u32 i = 0; i < ARRAY_COUNT(renderer->storage.framebuffers); i++) {
        if ( !renderer->storage.framebuffers[i].valid ) {
            result = renderer->storage.framebuffers + i;
            break;
        }
    }
    
    assert(result);
    return result;
}

internal cdl_renderer_framebuffer* cdl_renderer_framebuffer_new(u8 samples)
{
    PROFILED_FUNC;
    
    cdl_renderer_framebuffer* result = cdl_renderer_internal_framebuffer_get_empty();
    result->transient = cdl_renderer_api_framebuffer_new();
    result->valid = result->transient->valid;
    
    if ( samples > 1 ) {
        result->multisampled = cdl_renderer_api_framebuffer_multisampled_new(samples);
        result->valid &= result->multisampled->valid;
        result->samples = samples;
    }
    
    return result;
}

internal void cdl_renderer_framebuffer_render(cdl_renderer_framebuffer* framebuffer)
{
    PROFILED_FUNC;
    
    if ( framebuffer->samples > 1 ) {
        check_gl_proc(glBindFramebuffer(GL_READ_FRAMEBUFFER, framebuffer->multisampled->id));
        check_gl_proc(glBindFramebuffer(GL_DRAW_FRAMEBUFFER, framebuffer->transient->id));
        check_gl_proc(glBlitFramebuffer(0, 0, framebuffer->multisampled->dim.width, framebuffer->multisampled->dim.height,
                                        0, 0, framebuffer->transient->dim.width, framebuffer->transient->dim.height,
                                        GL_COLOR_BUFFER_BIT, GL_NEAREST));
    }
    
    check_gl_proc(glBindVertexArray(renderer->fsq));
    check_gl_proc(glBindFramebuffer(GL_FRAMEBUFFER, 0));
    check_gl_proc(glBindTextureUnit(0, framebuffer->transient->color));
    check_gl_proc(glDrawArrays(GL_TRIANGLES, 0, 6));
}

internal void cdl_renderer_framebuffer_use(cdl_renderer_framebuffer* framebuffer)
{
    PROFILED_FUNC;
    
    u32 target = INVALID_FRAMEBUFFER;
    if ( framebuffer->samples > 1 ) {
        target = framebuffer->multisampled->id;
    } else {
        target = framebuffer->transient->id;
    }
    
    check_gl_proc(glBindFramebuffer(GL_FRAMEBUFFER, target));
}

internal void cdl_renderer_framebuffer_clear(cdl_renderer_framebuffer* framebuffer)
{
    PROFILED_FUNC;
    
    cdl_renderer_api_framebuffer_clear(framebuffer->transient);
    if ( framebuffer->samples > 1 ) {
        cdl_renderer_api_framebuffer_clear(framebuffer->multisampled);
    }
    
    framebuffer->valid = 0;
}

//- [SECTION] Instanced Rendring
internal cdl_renderer_model_transform cdl_renderer_model_transform_new()
{
    PROFILED_FUNC;
    
    cdl_renderer_model_transform result = ZeroStruct;
    result.scale = 1;
    
    return result;
}

internal cdl_renderer_model_instance_list* cdl_renderer_instance_list_get(cdl_render_scene* s, cdl_assets_model_raw* model,
                                                                          cdl_renderer_material* material)
{
    assert(s->instanceListsCount + 1 < ARRAY_COUNT(s->instanceLists));
    PROFILED_FUNC;
    
    cdl_renderer_model_instance_list* result = 0;
    
    cdl_renderer_material* m = material;
    if ( !m ) {
        m = renderer->defaultMaterial;
    }
    
    for (u32 i = 0; i < s->instanceListsCount; i++) {
        if ( s->instanceLists[i].asset == model && s->instanceLists[i].material == m ) {
            result = s->instanceLists + i;
            break;
        }
    }
    
    if ( !result ) {
        if ( !s->instanceLists[s->instanceListsCount].modelLoaded ) {
            result = s->instanceLists + s->instanceListsCount;
            result->id = s->instanceListsCount++;
            result->asset = model;
            result->material = m;
            result->clearEveryFrame = 1;
        } else {
            result = s->instanceLists;
        }
    }
    
    assert(result);
    return result;
}

internal void cdl_renderer_internal_load_model_from_list(cdl_renderer_model_instance_list* list)
{
    assert(list);
    PROFILED_FUNC;
    
    if ( !list->asset ) {
        log_err("Error: can't load model because the asset isn't loaded or doesn't exist\n");
        return;
    }
    
    u32 dummyVertexBO = 0;
    u32 dummyIndexBO = 0;
    
    //- Generate the buffers
    check_gl_proc(glGenVertexArrays(1, &list->vao));
    check_gl_proc(glGenBuffers(1, &dummyVertexBO));
    check_gl_proc(glGenBuffers(1, &dummyIndexBO));
    check_gl_proc(glGenBuffers(1, &list->dataVBO));
    
    //- Create cdl_renderer_batch_vertex array from the asset_vertex data
    cdl_renderer_batch_vertex* vertices = 0;
    TranAllocArray(&vertices, "Instance Model Vertices", cdl_renderer_batch_vertex, list->asset->vertexCount);
    
    for (u32 i = 0; i < list->asset->vertexCount; i++) {
        cdl_assets_model_vertex* mv = list->asset->vertices + i;
        vertices[i].position = mv->localPosition;
        vertices[i].uvs = mv->uv;
        vertices[i].normal = mv->normal;
        vertices[i].color = mv->color;
        vertices[i].flags = 0;
        vertices[i].flags |= BatchVertexFlags_UseModelMat;
    }
    
    //- Bind and fill the buffers
    check_gl_proc(glBindVertexArray(list->vao));
    check_gl_proc(glBindBuffer(GL_ARRAY_BUFFER, dummyVertexBO));
    check_gl_proc(glBufferData(GL_ARRAY_BUFFER, list->asset->vertexCount * sizeof(cdl_renderer_batch_vertex), vertices, GL_STATIC_DRAW));
    check_gl_proc(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, dummyIndexBO));
    check_gl_proc(glBufferData(GL_ELEMENT_ARRAY_BUFFER, list->asset->indexCount * sizeof(u32), list->asset->indices, GL_STATIC_DRAW));
    
    //- Model Attributes
    check_gl_proc(glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(cdl_renderer_batch_vertex), (void*)struct_offset(cdl_renderer_batch_vertex, position))); // position attribute
    check_gl_proc(glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(cdl_renderer_batch_vertex), (void*)struct_offset(cdl_renderer_batch_vertex, uvs))); // uvs attribute
    check_gl_proc(glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, sizeof(cdl_renderer_batch_vertex), (void*)struct_offset(cdl_renderer_batch_vertex, normal))); // uvs attribute
    check_gl_proc(glVertexAttribIPointer(3, 1, GL_UNSIGNED_INT, sizeof(cdl_renderer_batch_vertex), (void*)struct_offset(cdl_renderer_batch_vertex, color))); // color attribute
    check_gl_proc(glVertexAttribIPointer(4, 1, GL_UNSIGNED_INT, sizeof(cdl_renderer_batch_vertex),(void*)struct_offset(cdl_renderer_batch_vertex, flags)));
    check_gl_proc(glVertexAttribPointer(5, 4, GL_FLOAT, GL_FALSE, sizeof(cdl_renderer_batch_vertex),(void*)struct_offset(cdl_renderer_batch_vertex, misc))); // misc attribute 1 (texture id)
    
    check_gl_proc(glEnableVertexAttribArray(0));
    check_gl_proc(glEnableVertexAttribArray(1));
    check_gl_proc(glEnableVertexAttribArray(2));
    check_gl_proc(glEnableVertexAttribArray(3));
    check_gl_proc(glEnableVertexAttribArray(4));
    check_gl_proc(glEnableVertexAttribArray(5));
    
    //- Per Instance Data
    check_gl_proc(glBindBuffer(GL_ARRAY_BUFFER, list->dataVBO));
    check_gl_proc(glBufferData(GL_ARRAY_BUFFER, list->capacity * sizeof(cdl_renderer_model_instance_data), 0, GL_DYNAMIC_DRAW));
    
    u32 attributeIndex = 6;
    for (u32 i = 0; i < 4; i++) {
        check_gl_proc(glVertexAttribPointer(attributeIndex, 4, GL_FLOAT, GL_FALSE, sizeof(cdl_renderer_model_instance_data), (void*)(sizeof(v4f) * i)));
        check_gl_proc(glEnableVertexAttribArray(attributeIndex));
        check_gl_proc(glVertexAttribDivisor(attributeIndex, 1));
        attributeIndex++;
    }
    
    check_gl_proc(glVertexAttribIPointer(attributeIndex, 1, GL_UNSIGNED_INT, sizeof(cdl_renderer_model_instance_data), (void*)struct_offset(cdl_renderer_model_instance_data, modelColor)));
    check_gl_proc(glEnableVertexAttribArray(attributeIndex));
    check_gl_proc(glVertexAttribDivisor(attributeIndex, 1));
    attributeIndex++;
    
    //- Clean up
    TranRelease(vertices);
    TranRelease(list->asset->vertices);
    TranRelease(list->asset->indices);
    list->modelLoaded = 1;
}

internal void cdl_renderer_create_model_instance_list(cdl_render_scene* s, cdl_assets_model_raw* model, cdl_renderer_material* material, bool8 clearEveryFrame)
{
    PROFILED_FUNC;
    
    cdl_assets_model_raw* m = model;
    if ( !m ) {
        m = s->defaultModel;
    }
    
    assert(m);
    assert(m->head.state == AssetState_Loaded);
    
    cdl_renderer_model_instance_list* list = cdl_renderer_instance_list_get(s, m, material);
    list->asset = m;
    list->material = material;
    if ( !material ) {
        list->material = renderer->defaultMaterial;
    }
    list->count = 0;
    list->capacity = 16;
    TranAllocArray(&list->instanceData, "Instance Data Alloc", cdl_renderer_model_instance_data, list->capacity);
    list->clearEveryFrame = clearEveryFrame;
    cdl_renderer_internal_load_model_from_list(list);
}


//- [SECTION] Render States
internal cdl_render_state* cdl_render_scene_get_next_state(cdl_render_scene* s)
{
    assert(s);
    PROFILED_FUNC;
    
    cdl_render_state* result = 0;
    
    bool8 currentStateUsed = (s->currentState->vertexCount != 0) 
        || (s->currentState->lineVertexCount != 0);
    
    if ( !currentStateUsed ) {
        result = s->currentState;
    }
    
    else {
        assert(s->statesCount < ARRAY_COUNT(s->states));
        cdl_render_state* previous = s->currentState;
        result = &s->states[s->statesCount++];
        
        *result = ZeroStruct;
        result->vertexStart = previous->vertexStart + previous->vertexCount;
        result->indexStart = previous->indexStart + previous->indexCount;
        result->lineVertexStart = previous->lineVertexStart + previous->lineVertexCount;
        result->lineIndexStart = previous->lineIndexStart + previous->lineIndexCount;
        
        s->currentState = result;
    }
    
    return result;
}

internal void cdl_render_scene_push_state_var(cdl_render_scene* s, RenderStateVarType varType, void* data)
{
    assert(s);
    assert(data);
    PROFILED_FUNC;
    
    switch (varType) {
        case RenderStateVarType_LineWidth:
        {
            u32 lineWidth = *(u32*)data;
            bool8 sameLineWidth = ( s->currentState->lineWidth && *s->currentState->lineWidth == lineWidth );
            if ( sameLineWidth )  { return; }
        } break;
        
        case RenderStateVarType_Wireframe:
        {
            if ( s->currentState->wireframe ) { return; }
        } break;
        
        default:
        {
        } break;
    }
    
    cdl_render_state* prev = s->currentState;
    cdl_render_state* next = cdl_render_scene_get_next_state(s);
    cdl_render_state_vars* vars = &s->statesVars;
    assert(next);
    
    next->varType = varType;
    next->hasClipRect = prev->hasClipRect;
    next->clipRect = prev->clipRect;
    next->lineWidth = prev->lineWidth;
    next->wireframe = prev->wireframe;
    
    switch (varType) {
        case RenderStateVarType_ClipRect:
        {
            cdl_render_state_clip_rect rsClipRect = {
                vars->currentDepth++,
                *(rect2*)data
            };
            
            *vars->currentClipRect = rsClipRect;
            next->clipRect = vars->currentClipRect++;
            next->hasClipRect = 1;
        } break;
        
        case RenderStateVarType_LineWidth:
        {
            u32 lineWidth = *(u32*)data;
            *vars->currentLineWidth = lineWidth;
            next->lineWidth = vars->currentLineWidth++;
        } break;
        
        case RenderStateVarType_Wireframe:
        {
            next->wireframe = 1;
        } break;
        
        default:
        {
        } break;
    }
}

internal void cdl_render_scene_pop_state_var(cdl_render_scene* s, RenderStateVarType varType)
{
    assert(s);
    PROFILED_FUNC;
    
    cdl_render_state* stateArray = s->states;
    cdl_render_state* prevPrev = s->currentState - 1;
    cdl_render_state* prev = s->currentState;
    cdl_render_state next = ZeroStruct;
    cdl_render_state_vars* vars = &s->statesVars;
    
    bool8 varWasPoped = 0;
    while ( !varWasPoped && (prevPrev >= stateArray) ) {
        switch (varType) {
            case RenderStateVarType_ClipRect:
            {
                if ( prevPrev->hasClipRect ) {
                    if ( prev->hasClipRect && ( prevPrev->clipRect->depth < prev->clipRect->depth ) ) {
                        next.clipRect = prevPrev->clipRect;
                        next.hasClipRect = 1;
                        varWasPoped = 1;
                    }
                } else {
                    varWasPoped = 1;
                }
            } break;
            
            case RenderStateVarType_LineWidth:
            {
                if ( prevPrev->lineWidth && ( prevPrev->lineWidth != prev->lineWidth ) ) {
                    next.lineWidth = prevPrev->lineWidth;
                    varWasPoped = 1;
                }
            } break;
            
            case RenderStateVarType_Wireframe:
            {
                if ( prevPrev->wireframe && ( prevPrev->wireframe != prev->wireframe ) ) {
                    next.wireframe = prevPrev->wireframe;
                    varWasPoped = 1;
                }
            } break;
            
            default:
            {
            } break;
        }
        
        prevPrev--;
    }
    
    if ( varWasPoped ) {
        cdl_render_state* nextptr = cdl_render_scene_get_next_state(s);
        
        nextptr->varType = varType;
        nextptr->hasClipRect = prev->hasClipRect;
        nextptr->clipRect = prev->clipRect;
        nextptr->lineWidth = prev->lineWidth;
        nextptr->wireframe = prev->wireframe;
        
        switch (varType) {
            case RenderStateVarType_ClipRect: 
            {
                nextptr->clipRect = next.clipRect;
                nextptr->hasClipRect = next.hasClipRect;
                vars->currentDepth--;
            } break;
            
            case RenderStateVarType_LineWidth:
            {
                nextptr->lineWidth = next.lineWidth;
            } break;
            
            case RenderStateVarType_Wireframe:
            {
                nextptr->wireframe = next.wireframe;
            } break;
            
            default:
            {
            } break;
        }
    }
}

internal void cdl_renderer_state_process(cdl_render_state* state)
{
    PROFILED_FUNC;
    
    if ( state->wireframe ) {
        check_gl_proc(glPolygonMode(GL_FRONT_AND_BACK, GL_LINE));
    } else {
        check_gl_proc(glPolygonMode(GL_FRONT_AND_BACK, GL_FILL));
    }
    
    if ( state->clipRect ) {
        check_gl_proc(glEnable(GL_SCISSOR_TEST));
        rect2 rect = state->clipRect->rect;
        v2f bottomLeft = v2f_new(rect.min.x, core->windowHeight - rect.max.y);
        v2f size = v2f_new(rect.max.x - rect.min.x, rect.max.y - rect.min.y);
        check_gl_proc(glScissor(bottomLeft.x, bottomLeft.y, size.x, size.y));
    } else {
        check_gl_proc(glDisable(GL_SCISSOR_TEST));
    }
    
    if ( state->lineWidth ) {
        check_gl_proc(glLineWidth(*state->lineWidth));
    } else {
        check_gl_proc(glLineWidth(2));
    }
}

internal void cdl_renderer_state_reset()
{
    PROFILED_FUNC;
    
    check_gl_proc(glPolygonMode(GL_FRONT_AND_BACK, GL_FILL));
    check_gl_proc(glDisable(GL_SCISSOR_TEST));
    check_gl_proc(glPolygonMode(GL_FRONT_AND_BACK, GL_FILL));
    check_gl_proc(glLineWidth(2));
}


//- [SECTION] Render Scenes
internal cdl_render_scene* cdl_render_scene_new(RenderSceneType type)
{
    PROFILED_FUNC;
    
    cdl_render_scene* result = renderer->storage.nextScene++;
    
    result->type = type;
    result->nextLight = result->lights;
    
    //- Batches 
    // Scene Static Triangles Batch
    {
        check_gl_proc(glGenVertexArrays(1, &result->staticTriangles.VAO));
        check_gl_proc(glGenBuffers(1, &result->staticTriangles.VBO));
        check_gl_proc(glGenBuffers(1, &result->staticTriangles.EBO));
        
        check_gl_proc(glBindVertexArray(result->staticTriangles.VAO));
        check_gl_proc(glBindBuffer(GL_ARRAY_BUFFER, result->staticTriangles.VBO));
        check_gl_proc(glBufferData(GL_ARRAY_BUFFER, sizeof(result->staticTriangles.vertexBuffer), 0, GL_DYNAMIC_DRAW));
        check_gl_proc(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, result->staticTriangles.EBO));
        check_gl_proc(glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(result->staticTriangles.indexBuffer), 0, GL_DYNAMIC_DRAW));
        
        check_gl_proc(glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(cdl_renderer_batch_vertex), (void*)struct_offset(cdl_renderer_batch_vertex, position))); // position attribute
        check_gl_proc(glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(cdl_renderer_batch_vertex), (void*)struct_offset(cdl_renderer_batch_vertex, uvs))); // uvs attribute
        check_gl_proc(glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, sizeof(cdl_renderer_batch_vertex), (void*)struct_offset(cdl_renderer_batch_vertex, normal))); // uvs attribute
        check_gl_proc(glVertexAttribIPointer(3, 1, GL_UNSIGNED_INT, sizeof(cdl_renderer_batch_vertex), (void*)struct_offset(cdl_renderer_batch_vertex, color))); // color attribute
        check_gl_proc(glVertexAttribIPointer(4, 1, GL_UNSIGNED_INT, sizeof(cdl_renderer_batch_vertex),(void*)struct_offset(cdl_renderer_batch_vertex, flags)));
        check_gl_proc(glVertexAttribPointer(5, 4, GL_FLOAT, GL_FALSE, sizeof(cdl_renderer_batch_vertex),(void*)struct_offset(cdl_renderer_batch_vertex, misc))); // misc attribute 1 (texture id)
        
        check_gl_proc(glEnableVertexAttribArray(0));
        check_gl_proc(glEnableVertexAttribArray(1));
        check_gl_proc(glEnableVertexAttribArray(2));
        check_gl_proc(glEnableVertexAttribArray(3));
        check_gl_proc(glEnableVertexAttribArray(4));
        
        result->staticTriangles.nextVertex = result->staticTriangles.vertexBuffer;
        result->staticTriangles.nextIndex = result->staticTriangles.indexBuffer;
        result->staticTriangles.lastEntry = result->staticTriangles.entries;
    }
    
    // Scene Triangle Batch
    {
        check_gl_proc(glGenVertexArrays(1, &result->triangles.VAO));
        check_gl_proc(glGenBuffers(1, &result->triangles.VBO));
        check_gl_proc(glGenBuffers(1, &result->triangles.EBO));
        
        check_gl_proc(glBindVertexArray(result->triangles.VAO));
        check_gl_proc(glBindBuffer(GL_ARRAY_BUFFER, result->triangles.VBO));
        check_gl_proc(glBufferData(GL_ARRAY_BUFFER, sizeof(result->triangles.vertexBuffer), 0, GL_DYNAMIC_DRAW));
        check_gl_proc(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, result->triangles.EBO));
        check_gl_proc(glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(result->triangles.indexBuffer), 0, GL_DYNAMIC_DRAW));
        
        check_gl_proc(glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(cdl_renderer_batch_vertex), (void*)struct_offset(cdl_renderer_batch_vertex, position))); // position attribute
        check_gl_proc(glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(cdl_renderer_batch_vertex), (void*)struct_offset(cdl_renderer_batch_vertex, uvs))); // uvs attribute
        check_gl_proc(glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, sizeof(cdl_renderer_batch_vertex), (void*)struct_offset(cdl_renderer_batch_vertex, normal))); // normal attribute
        check_gl_proc(glVertexAttribIPointer(3, 1, GL_UNSIGNED_INT, sizeof(cdl_renderer_batch_vertex), (void*)struct_offset(cdl_renderer_batch_vertex, color))); // color attribute
        check_gl_proc(glVertexAttribIPointer(4, 1, GL_UNSIGNED_INT, sizeof(cdl_renderer_batch_vertex),(void*)struct_offset(cdl_renderer_batch_vertex, flags)));
        check_gl_proc(glVertexAttribPointer(5, 4, GL_FLOAT, GL_FALSE, sizeof(cdl_renderer_batch_vertex),(void*)struct_offset(cdl_renderer_batch_vertex, misc))); // misc attribute 2 (use model matrix, etc...)
        
        check_gl_proc(glEnableVertexAttribArray(0));
        check_gl_proc(glEnableVertexAttribArray(1));
        check_gl_proc(glEnableVertexAttribArray(2));
        check_gl_proc(glEnableVertexAttribArray(3));
        check_gl_proc(glEnableVertexAttribArray(4));
        check_gl_proc(glEnableVertexAttribArray(5));
    }
    
    // Scene Line Batch
    {
        check_gl_proc(glGenVertexArrays(1, &result->lines.VAO));
        check_gl_proc(glGenBuffers(1, &result->lines.VBO));
        check_gl_proc(glGenBuffers(1, &result->lines.EBO));
        
        check_gl_proc(glBindVertexArray(result->lines.VAO));
        check_gl_proc(glBindBuffer(GL_ARRAY_BUFFER, result->lines.VBO));
        check_gl_proc(glBufferData(GL_ARRAY_BUFFER, sizeof(result->lines.vertexBuffer), 0, GL_DYNAMIC_DRAW));
        check_gl_proc(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, result->lines.EBO));
        check_gl_proc(glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(result->lines.indexBuffer), 0, GL_DYNAMIC_DRAW));
        
        check_gl_proc(glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(cdl_renderer_batch_vertex), (void*)struct_offset(cdl_renderer_batch_vertex, position))); // position attribute
        check_gl_proc(glVertexAttribIPointer(3, 1, GL_UNSIGNED_INT, sizeof(cdl_renderer_batch_vertex), (void*)struct_offset(cdl_renderer_batch_vertex, color))); // color attribute
        
        check_gl_proc(glEnableVertexAttribArray(0));
        check_gl_proc(glEnableVertexAttribArray(3));
    }
    
    return result;
}

internal void cdl_render_scene_frame_begin(cdl_render_scene* s)
{
    PROFILED_FUNC;
    
    s->dirty = 0;
    
    //- Render States
    cdl_render_state defaultRenderState = ZeroStruct;
    defaultRenderState.vertexCount = 0;
    defaultRenderState.indexCount = 0;
    defaultRenderState.lineVertexCount = 0;
    defaultRenderState.lineIndexCount = 0;
    
    defaultRenderState.vertexStart = s->triangles.vertexBuffer;
    defaultRenderState.indexStart = s->triangles.indexBuffer;
    defaultRenderState.lineVertexStart = s->lines.vertexBuffer;
    defaultRenderState.lineIndexStart = s->lines.indexBuffer;
    
    s->statesCount = 1;
    s->states[0] = defaultRenderState;
    s->currentState = s->states;
    
    s->statesVars.currentDepth = 0;
    s->statesVars.currentClipRect = s->statesVars.clipRects;
    *s->statesVars.currentClipRect = ZeroStruct;
    s->currentState->hasClipRect = 0;
    
    s->statesVars.currentLineWidth = s->statesVars.lineWidth;
    *s->statesVars.currentLineWidth = 2;
    s->currentState->lineWidth = s->statesVars.currentLineWidth;
    
    //- Instance Lists 
    for (u32 i = 0; i < s->instanceListsCount; i++) {
        if ( s->instanceLists[i].clearEveryFrame ) {
            s->instanceLists[i].count = 0;
        }
    }
    
    //- Batches 
    // Triangles
    s->triangles.nextVertex = s->triangles.vertexBuffer;
    s->triangles.nextIndex = s->triangles.indexBuffer;
    s->triangles.vertexCount = 0;
    
    // Lines
    s->lines.nextVertex = s->lines.vertexBuffer;
    s->lines.nextIndex = s->lines.indexBuffer;
    s->lines.vertexCount = 0;
    
    //- Shaders
    renderer->currentShader = 0;
    
    //- Textures
    MemoryZero(s->textures, sizeof(s->textures));
}

internal void cdl_render_scene_frame_end(cdl_render_scene* s)
{
    
}

internal void cdl_render_scene_prepare_textures(cdl_render_scene* s)
{
    PROFILED_FUNC;
    
    for (u32 i = 0; i < RENDERER_MAX_TEXTURE_UNIT_PER_FRAME; i++) {
        cdl_renderer_texture* texture = s->textures[i];
        if ( texture != 0 ) {
            
            if ( !texture->loaded ) {
                cdl_renderer_texture_load(texture);
            }
            
            if ( !glIsTexture(texture->id) ) {
                log_err("Texture %s (id: %d) has an issue, reloading\n", texture->asset->head.path, texture->id);
                cdl_assets_texture_load_pixels(texture->asset);
                cdl_renderer_texture_load(texture);
            }
            
            check_gl_proc(glBindTextureUnit(i, texture->id));
            //glActiveTexture(GL_TEXTURE0 + i);
            //glBindTexture(GL_TEXTURE_2D, texture->id);
        }
    }
}

//- [SECTION] Render Scene Variables
internal void cdl_render_scene_push_clip_rect(cdl_render_scene* s, rect2 clipRect)
{
    PROFILED_FUNC;
    cdl_render_scene_push_state_var(s, RenderStateVarType_ClipRect, (void*)&clipRect);
}

internal void cdl_render_scene_pop_clip_rect(cdl_render_scene* s)
{
    PROFILED_FUNC;
    cdl_render_scene_pop_state_var(s, RenderStateVarType_ClipRect);
}

internal void cdl_render_scene_push_line_width(cdl_render_scene* s, u32 lineWidth)
{
    PROFILED_FUNC;
    cdl_render_scene_push_state_var(s, RenderStateVarType_LineWidth, (void*)&lineWidth);
}

internal void cdl_render_scene_pop_line_width(cdl_render_scene* s)
{
    PROFILED_FUNC;
    cdl_render_scene_pop_state_var(s, RenderStateVarType_LineWidth);
}

internal void cdl_render_scene_push_wireframe(cdl_render_scene* s)
{
    PROFILED_FUNC;
    i32 dummy = 1;
    cdl_render_scene_push_state_var(s, RenderStateVarType_Wireframe, (void*)&dummy);
}

internal void cdl_render_scene_pop_wireframe(cdl_render_scene* s)
{
    PROFILED_FUNC;
    cdl_render_scene_pop_state_var(s, RenderStateVarType_Wireframe);
}

//- World Macros
#define world_push_clip_rect(rect) (cdl_render_scene_push_clip_rect(renderer->world, rect))
#define world_pop_clip_rect() (cdl_render_scene_pop_clip_rect(renderer->world))
#define world_push_line_width(width) (cdl_render_scene_push_line_width(renderer->world, width))
#define world_pop_line_width() (cdl_render_scene_pop_clip_rect(renderer->world))
#define world_push_wireframe() (cdl_render_scene_push_wireframe(renderer->world))
#define world_pop_wireframe() (cdl_render_scene_pop_wireframe(renderer->world))

//- UI Macros
#define ui_push_clip_rect(rect) (cdl_render_scene_push_clip_rect(renderer->ui, rect))
#define ui_pop_clip_rect() (cdl_render_scene_pop_clip_rect(renderer->ui))
#define ui_push_line_width(width) (cdl_render_scene_push_line_width(renderer->ui, width))
#define ui_pop_line_width() (cdl_render_scene_pop_clip_rect(renderer->ui))


//- [SECTION] World Render Scene Points 
internal void cdl_render_scene_push_point(cdl_render_scene* s, v3f p, f32 size, color8 color)
{
    assert(s);
    PROFILED_FUNC;
    
    v2f scale = v2f_new(size);
    cdl_render_scene_push_quads(s, 1, &p, &scale, &color);
}

//- World Render Scene Lines
internal void cdl_render_scene_push_line(cdl_render_scene* s, v3f p1, v3f p2, color8 color)
{
    assert(s);
    assert( RENDERER_BATCH_MAX_LINE_VERTEX_COUNT  > s->lines.vertexCount + 2 );
    PROFILED_FUNC;
    
    u32 offset = (u32)(s->lines.nextVertex - s->lines.vertexBuffer);
    *s->lines.nextIndex++ = offset + 0;
    *s->lines.nextIndex++ = offset + 1;
    
    s->lines.nextVertex->position = p1;
    s->lines.nextVertex->uvs = v2f_new(0);
    s->lines.nextVertex->color = color;
    s->lines.nextVertex++;
    
    s->lines.nextVertex->position = p2;
    s->lines.nextVertex->uvs = v2f_new(0);
    s->lines.nextVertex->color = color;
    s->lines.nextVertex++;
    
    s->lines.vertexCount += 2;
    s->currentState->lineVertexCount += 2;
    s->currentState->lineIndexCount += 2;
    s->dirty = 1;
}


//- [SECTION] World Render Scene Curves
internal void cdl_render_scene_push_bezier_quadratic(cdl_render_scene* s, v3f p1, v3f p2, v3f p3, color8 color, u32 steps)
{
    PROFILED_FUNC;
    
    v3f previous = p1;
    for (u32 dt = 0; dt <= steps; dt++) {
        v3f point = bezier3_point_on_quadratic(p1, p2, p3, dt / (f32)steps);
        cdl_render_scene_push_line(s, point, previous, color);
        previous = point;
    }
}

internal void cdl_render_scene_push_bezier_cubic(cdl_render_scene* s, v3f p1, v3f p2, v3f p3, v3f p4, color8 color, u32 steps)
{
    PROFILED_FUNC;
    
    v3f previous = p1;
    for (u32 dt = 0; dt <= steps; dt++) {
        v3f point = bezier3_point_on_cubic(p1, p2, p3, p4, dt / (f32)steps);
        cdl_render_scene_push_line(s, point, previous, color);
        previous = point;
    }
}


//- [SECTION] World Render Scene Triangles
internal void cdl_render_scene_push_triangles(cdl_render_scene* s, u32 triangleCount, v3f* positions, color8* colors)
{
    assert(s);
    assert(positions);
    assert(colors);
    assert( RENDERER_BATCH_MAX_TRIANGLE_VERTEX_COUNT > s->triangles.vertexCount + triangleCount * 3 );
    PROFILED_FUNC;
    
    for (u32 i = 0; i < triangleCount * 3; i++) {
        u32 offset = (u32)(s->triangles.nextVertex - s->triangles.vertexBuffer);
        *s->triangles.nextIndex++ = offset + 0;
        *s->triangles.nextIndex++ = offset + 1;
        *s->triangles.nextIndex++ = offset + 2;
    }
    
    for (u32 i = 0; i < triangleCount * 3; i++) {
        s->triangles.nextVertex->position = positions[i];
        s->triangles.nextVertex->uvs = v2f_new(0);
        s->triangles.nextVertex->color = colors[i];
        s->triangles.nextVertex++;
    }
    
    s->triangles.vertexCount += triangleCount * 3;
    s->currentState->vertexCount += triangleCount * 3;
    s->currentState->indexCount += triangleCount * 3;
    s->dirty = 1;
}

internal void cdl_render_scene_push_quads(cdl_render_scene* s, u32 quadCount, v3f* positions, v2f* scales, color8* colors)
{
    // TODO(abe): Add rotations
    assert(s);
    assert(positions);
    assert(scales);
    assert(colors);
    assert( RENDERER_BATCH_MAX_TRIANGLE_VERTEX_COUNT > s->triangles.vertexCount + quadCount * 4 );
    PROFILED_FUNC;
    
    cdl_renderer_batch_vertex* nextVertex = (cdl_renderer_batch_vertex*)InterlockedExchangePtr(&s->triangles.nextVertex, (s->triangles.nextVertex + (4 * quadCount)));
    
    u32* nextIndex = (u32*)InterlockedExchangePtr(&s->triangles.nextIndex, (s->triangles.nextIndex + (6 * quadCount)));
    
    s->triangles.vertexCount += 4 * quadCount;
    s->currentState->vertexCount += 4 * quadCount;
    s->currentState->indexCount += 6 * quadCount;
    
    cdl_renderer_batch_vertex* vertexBuffer = s->triangles.vertexBuffer;
    for (u32 i = 0; i < quadCount; i++) {
        u32 offset = (u32)(nextVertex - vertexBuffer);
        *nextIndex++ = offset + 0;
        *nextIndex++ = offset + 1;
        *nextIndex++ = offset + 3;
        *nextIndex++ = offset + 1;
        *nextIndex++ = offset + 2;
        *nextIndex++ = offset + 3;
    }
    
    for (u32 i = 0; i < quadCount; i++) {
        v3f position = positions[i];
        v2f scale = scales[i];
        color8 color = colors[i];
        
        nextVertex->position = v3f_new( position.x + scale.width / 2, position.y + scale.height / 2, position.z );
        nextVertex->uvs = v2f_new(0);
        nextVertex->color = color;
        nextVertex++;
        
        nextVertex->position = v3f_new( position.x + scale.width / 2, position.y - scale.height / 2, position.z );
        nextVertex->uvs = v2f_new(0);
        nextVertex->color = color;
        nextVertex++;
        
        nextVertex->position = v3f_new( position.x - scale.width / 2, position.y - scale.height / 2, position.z );
        nextVertex->uvs = v2f_new(0);
        nextVertex->color = color;
        nextVertex++;
        
        nextVertex->position = v3f_new( position.x - scale.width / 2, position.y + scale.height / 2, position.z );
        nextVertex->uvs = v2f_new(0);
        nextVertex->color = color;
        nextVertex++;
    }
    
    s->dirty = 1;
}

internal void cdl_render_scene_push_quad_gradient(cdl_render_scene* s, v3f position, v2f scale, color8 topRightColor, color8 bottomRightColor, color8 bottomLeftColor, color8 topLeftColor)
{
    assert(s);
    assert( RENDERER_BATCH_MAX_TRIANGLE_VERTEX_COUNT > s->triangles.vertexCount + 4 );
    PROFILED_FUNC;
    
    u32 offset = (u32)(s->triangles.nextVertex - s->triangles.vertexBuffer);
    *s->triangles.nextIndex++ = offset + 0;
    *s->triangles.nextIndex++ = offset + 1;
    *s->triangles.nextIndex++ = offset + 3;
    *s->triangles.nextIndex++ = offset + 1;
    *s->triangles.nextIndex++ = offset + 2;
    *s->triangles.nextIndex++ = offset + 3;
    
    f32 aspectRatio = cdl_renderer_aspect_ratio();;
    
    s->triangles.nextVertex->position = v3f_new( position.x + scale.width / 2, position.y + aspectRatio * scale.height / 2, position.z );
    s->triangles.nextVertex->uvs = v2f_new(0);
    s->triangles.nextVertex->color = topRightColor;
    s->triangles.nextVertex++;
    
    s->triangles.nextVertex->position = v3f_new( position.x + scale.width / 2, position.y - aspectRatio * scale.height / 2, position.z );
    s->triangles.nextVertex->uvs = v2f_new(0);
    s->triangles.nextVertex->color = bottomRightColor;
    s->triangles.nextVertex++;
    
    s->triangles.nextVertex->position = v3f_new( position.x - scale.width / 2, position.y - aspectRatio * scale.height / 2, position.z );
    s->triangles.nextVertex->uvs = v2f_new(0);
    s->triangles.nextVertex->color = bottomLeftColor;
    s->triangles.nextVertex++;
    
    s->triangles.nextVertex->position = v3f_new( position.x - scale.width / 2, position.y + aspectRatio * scale.height / 2, position.z );
    s->triangles.nextVertex->uvs = v2f_new(0);
    s->triangles.nextVertex->color = topLeftColor;
    s->triangles.nextVertex++;
    
    s->triangles.vertexCount += 4;
    s->currentState->vertexCount += 4;
    s->currentState->indexCount += 6;
    s->dirty = 1;
}

internal void cdl_render_scene_push_quad_textured(cdl_render_scene* s, v3f position, v2f scale, v4f uvs, u32 texture)
{
    assert(s);
    assert( RENDERER_BATCH_MAX_TRIANGLE_VERTEX_COUNT > s->triangles.vertexCount + 4 );
    PROFILED_FUNC;
    
    u32 offset = (u32)(s->triangles.nextVertex - s->triangles.vertexBuffer);
    *s->triangles.nextIndex++ = offset + 0;
    *s->triangles.nextIndex++ = offset + 1;
    *s->triangles.nextIndex++ = offset + 3;
    *s->triangles.nextIndex++ = offset + 1;
    *s->triangles.nextIndex++ = offset + 2;
    *s->triangles.nextIndex++ = offset + 3;
    
    s->triangles.nextVertex->position = v3f_new( position.x + scale.width / 2, position.y + scale.height / 2, position.z );
    s->triangles.nextVertex->uvs = v2f_new( uvs.y, uvs.w );
    s->triangles.nextVertex->color = color_white;
    s->triangles.nextVertex->flags |= BatchVertexFlags_UseTexture;
    s->triangles.nextVertex->misc.x = (f32)(texture);
    s->triangles.nextVertex++;
    
    s->triangles.nextVertex->position = v3f_new( position.x + scale.width / 2, position.y - scale.height / 2, position.z );
    s->triangles.nextVertex->uvs = v2f_new( uvs.y, uvs.z );
    s->triangles.nextVertex->color = color_white;
    s->triangles.nextVertex->flags |= BatchVertexFlags_UseTexture;
    s->triangles.nextVertex->misc.x = (f32)(texture);
    s->triangles.nextVertex++;
    
    s->triangles.nextVertex->position = v3f_new( position.x - scale.width / 2, position.y - scale.height / 2, position.z );
    s->triangles.nextVertex->uvs = v2f_new( uvs.x, uvs.z );
    s->triangles.nextVertex->color = color_white;
    s->triangles.nextVertex->flags |= BatchVertexFlags_UseTexture;
    s->triangles.nextVertex->misc.x = (f32)(texture);
    s->triangles.nextVertex++;
    
    s->triangles.nextVertex->position = v3f_new( position.x - scale.width / 2, position.y + scale.height / 2, position.z );
    s->triangles.nextVertex->uvs = v2f_new( uvs.x, uvs.w );
    s->triangles.nextVertex->color = color_white;
    s->triangles.nextVertex->flags |= BatchVertexFlags_UseTexture;
    s->triangles.nextVertex->misc.x = (f32)(texture);
    s->triangles.nextVertex++;
    
    s->triangles.vertexCount += 4;
    s->currentState->vertexCount += 4;
    s->currentState->indexCount += 6;
    s->dirty = 1;
}


//- [SECTION] World Render Scene Models 
internal void cdl_render_scene_push_model(cdl_render_scene* s, u32 vertexCount, v3f* positions, v2f* uvs, v3f* normals, color8* colors, u32 texture, u32 indexCount, u32* indices)
{
    assert(s);
    assert(positions);
    assert(indices);
    assert( RENDERER_BATCH_MAX_TRIANGLE_VERTEX_COUNT > s->triangles.vertexCount + vertexCount );
    PROFILED_FUNC;
    
    cdl_renderer_triangle_batch* batch = &s->triangles;
    
    u32 offset = (u32)(batch->nextVertex - batch->vertexBuffer);
    for (u32 i = 0; i < indexCount; i++) {
        *batch->nextIndex++ = offset + indices[i];
    }
    
    for (u32 i = 0; i < vertexCount; i++) {
        batch->nextVertex->position = positions[i];
        
        if ( uvs != 0 ) {
            batch->nextVertex->uvs = uvs[i];
        } else {
            batch->nextVertex->uvs = v2f_new(0);
        }
        
        if ( normals != 0 ) {
            batch->nextVertex->normal = normals[i];
        } else {
            batch->nextVertex->normal = v3f_new(0);
        }
        
        if ( colors != 0 ) {
            batch->nextVertex->color = colors[i];
        } else {
            batch->nextVertex->color = color_white;
        }
        
        batch->nextVertex->flags = 0;
        batch->nextVertex->misc = v4f_new(0);
        
        if ( texture != INVALID_TEXTURE ) {
            batch->nextVertex->flags |= BatchVertexFlags_UseTexture;
            batch->nextVertex->misc.x = (f32)(texture);
        }
        
        batch->nextVertex++;
    }
    
    batch->vertexCount += vertexCount;
    s->currentState->vertexCount += vertexCount;
    s->currentState->indexCount += indexCount;
    s->dirty = 1;
}

internal void cdl_render_scene_push_model_textured(cdl_render_scene* s, u32 vertexCount, v3f* positions, v2f* uvs, v3f* normals, u32 texture, u32 indexCount, u32* indices)
{
    assert(s);
    assert(positions);
    assert(indices);
    PROFILED_FUNC;
    
    cdl_render_scene_push_model(renderer->world, vertexCount, positions, uvs, normals, 0, texture, indexCount, indices); 
}

internal void cdl_render_scene_push_model_instance(cdl_render_scene* s, cdl_assets_model_raw* model, cdl_renderer_material* material, cdl_renderer_model_transform transform)
{
    assert(s);
    PROFILED_FUNC;
    
    if ( !model ) {
        if ( s->defaultModel ) {
            cdl_render_scene_push_model_instance(s, s->defaultModel, material, transform);
        }
        
        return;
    }
    
    assert(model);
    
    cdl_renderer_model_instance_list* list = cdl_renderer_instance_list_get(s, model, material);
    if ( !list ) { return; }
    
    if ( list->capacity == 0 ) {
        cdl_renderer_create_model_instance_list(s, model, material, 1);
    }
    
    m4f modelMatrix = m4f_identity();
    
    bool8 useScale = !f32_equals(transform.scale, 1.f);
    bool8 useRotation = !f32_equals(v3f_magnitude(transform.rotation), 0);
    bool8 useTranslation =!f32_equals(v3f_magnitude(transform.position), 0);
    
    if ( useScale ) {
        m4f scaleMatrix = m4f_scale_matrix(transform.scale);
        modelMatrix = m4f_mult(&scaleMatrix, &modelMatrix);
    }
    
    if ( useRotation ) {
        m4f rotX = m4f_rotation_matrix(v3f_new(1, 0, 0), transform.rotation.x);
        m4f rotY = m4f_rotation_matrix(v3f_new(0, 1, 0), transform.rotation.y);
        m4f rotZ = m4f_rotation_matrix(v3f_new(0, 0, 1), transform.rotation.z);
        
        modelMatrix = m4f_mult(&rotX, &modelMatrix);
        modelMatrix = m4f_mult(&rotY, &modelMatrix);
        modelMatrix = m4f_mult(&rotZ, &modelMatrix);
    }
    
    if ( useTranslation ) {
        m4f translation = m4f_translation_matrix(transform.position);
        modelMatrix = m4f_mult(&translation, &modelMatrix);
    }
    
    if ( list->count + 1 >= list->capacity ) {
        u32 newCapacity = 2 * list->capacity;
        
        cdl_renderer_model_instance_data* newDataArray = 0;
        TranAllocArray(&newDataArray, "Instance Data Realloc", cdl_renderer_model_instance_data, newCapacity);
        
        MemoryCopy(newDataArray, list->instanceData, list->count * sizeof(cdl_renderer_model_instance_data));
        
        TranRelease(list->instanceData);
        
        list->capacity = newCapacity;
        list->instanceData = newDataArray;
        
        glBindBuffer(GL_ARRAY_BUFFER, list->dataVBO);
        glBufferData(GL_ARRAY_BUFFER, list->capacity * sizeof(cdl_renderer_model_instance_data), 0, GL_DYNAMIC_DRAW);
    }
    
    list->instanceData[list->count].modelMatrix = m4f_transpose(&modelMatrix);
    
    cdl_renderer_material* m = material;
    if ( !m ) {
        m = renderer->defaultMaterial;
    }
    list->instanceData[list->count].modelColor = m->color;
    
    u32 materialTextureUniform = 0;
    if ( m->diffuse ) {
        u32 textureID = cdl_renderer_texture_push(s, m->diffuse);
        materialTextureUniform |= textureID << 24;
    }
    
    if ( m->normal ) {
        u32 normalID = cdl_renderer_texture_push(s, m->normal);
        materialTextureUniform |= normalID << 16;
    }
    
    m->textureUniform = materialTextureUniform;
    
    list->count++;
    list->dirty = 1;
    s->dirty = 1;
}


//- [SECTION] World Render Scene Static Models
internal u32 cdl_render_scene_push_static_model(cdl_render_scene* s, u32 vertexCount, v3f* positions, v2f* uvs, v3f* normals, color8* colors, u32 texture, u32 indexCount, u32* indices)
{
    assert(s);
    assert(positions);
    assert(indices);
    assert( RENDERER_BATCH_MAX_TRIANGLE_VERTEX_COUNT > s->staticTriangles.vertexCount + vertexCount );
    PROFILED_FUNC;
    
    cdl_renderer_triangle_batch* batch = &s->staticTriangles;
    
    u32 offset = (u32)(batch->nextVertex - batch->vertexBuffer);
    for (u32 i = 0; i < indexCount; i++) {
        *batch->nextIndex++ = offset + indices[i];
    }
    
    for (u32 i = 0; i < vertexCount; i++) {
        batch->nextVertex->position = positions[i];
        
        if ( uvs != 0 ) {
            batch->nextVertex->uvs = uvs[i];
        } else {
            batch->nextVertex->uvs = v2f_new(0);
        }
        
        if ( normals != 0 ) {
            batch->nextVertex->normal = normals[i];
        } else {
            batch->nextVertex->normal = v3f_new(0);
        }
        
        if ( colors != 0 ) {
            batch->nextVertex->color = colors[i];
        } else {
            batch->nextVertex->color = color_white;
        }
        
        batch->nextVertex->flags = 0;
        batch->nextVertex->misc = v4f_new(0);
        
        if ( texture != INVALID_TEXTURE ) {
            batch->nextVertex->flags |= BatchVertexFlags_UseTexture;
            batch->nextVertex->misc.x = (f32)(texture);
        }
        
        batch->nextVertex++;
    }
    
    batch->vertexCount += vertexCount;
    s->currentState->vertexCount += vertexCount;
    s->currentState->indexCount += indexCount;
    s->dirty = 1;
    
    return U32Max; // TODO(abe): // TODO(abe): // TODO(abe): // TODO(abe): // TODO(abe): 
}

internal void cdl_render_scene_remove_static_model(cdl_render_scene* s, u32 modelEntryID)
{
    PROFILED_FUNC;
    
    cdl_renderer_triangle_batch* sw = &s->staticTriangles;
    u32 entryIndex = U32Max;
    
    for (u32 i = 0; i < RENDERER_BATCH_MAX_ENTRIES; i++) {
        if ( sw->entries[i].id == modelEntryID ) {
            entryIndex = i;
            break;
        }
    }
    // NOTE(abe): No valid entry was found
    assert( entryIndex < RENDERER_BATCH_MAX_ENTRIES );
    
    cdl_renderer_batch_entry* e = &sw->entries[entryIndex];
    cdl_renderer_batch_entry e_values = *e;
    assert( e->id != 0 );
    
    
    // Retrieve the offset of the model's indices in the batch's index buffer
    // Only if the model removed is not the last model in the entries list
    if ( e != sw->lastEntry ) {
        u32 offsetToRemoveFromNextIndices = e->vertexCount;
        
        for (u32* i = sw->entries[entryIndex + 1].indexStart; i < sw->lastEntry->indexStart + sw->lastEntry->indexCount; i++) {
            *i -= offsetToRemoveFromNextIndices;
        }
    }
    
    
    // Move the data up in the batch index and vertex buffers
    // (erase index and vertex data of the removed model)
    for (u32 i = entryIndex + 1; i < RENDERER_BATCH_MAX_ENTRIES; i++) {
        if ( sw->entries[i].id > 0 ) {
            u64 vertexSizeToMove = (u64)(sw->lastEntry->vertexStart + sw->lastEntry->vertexCount) - (u64)(e->vertexStart);
            memmove(e->vertexStart, sw->entries[i].vertexStart, vertexSizeToMove);
            
            u64 indexSizeToMove = (u64)(sw->lastEntry->indexStart + sw->lastEntry->indexCount) - (u64)(e->indexStart);
            memmove(e->indexStart, sw->entries[i].indexStart, indexSizeToMove);
            
            break;
        }
    }
    
    // Update the entries pointers since the data has been be moved up in the batch
    for (u32 i = ARRAY_COUNT(sw->entries) - 1; i > entryIndex; i--) {
        if ( sw->entries[i].id == 0 ) continue;
        sw->entries[i].vertexStart -= e_values.vertexCount;
        sw->entries[i].indexStart -= e_values.indexCount;
    }
    sw->entries[entryIndex].vertexStart -= e_values.vertexCount;
    sw->entries[entryIndex].indexStart -= e_values.indexCount;
    
    
    // Update the batch pointers accordingly
    sw->vertexCount -= e_values.vertexCount;
    sw->nextVertex -= e_values.vertexCount;
    sw->nextIndex -= e_values.indexCount;
    
    
    // Move the entry data up in the entries list and clear the end of the buffer
    memmove(&sw->entries[entryIndex], &sw->entries[entryIndex+1], (u64)(sw->entries + ARRAY_COUNT(sw->entries)) - (u64)sw->lastEntry);
    MemoryZero(sw->lastEntry, (u64)(sw->entries + ARRAY_COUNT(sw->entries)) - (u64)sw->lastEntry);
    
    sw->lastEntry--;
    sw->dirty = 1;
}


//- [SECTION] World Environment
internal cdl_renderer_light* cdl_render_scene_push_light(cdl_render_scene* s, v3f pos, color8 color)
{
    assert(s->nextLight < s->lights + ARRAY_COUNT(s->lights));
    PROFILED_FUNC;
    
    s->useLights = 1;
    s->nextLight->position = pos;
    s->nextLight->color = color;
    s->nextLight->intensity = 1;
    return s->nextLight++;
}


//- [SECTION] UI Scenes Points
internal void cdl_render_scene_ui_push_point(cdl_render_scene* s, v2f p, f32 size, color8 color)
{
    assert(s);
    PROFILED_FUNC;
    cdl_render_scene_ui_push_quad(s, p, v2f_new(size), color);
}


//- [SECTION] UI Scenes Lines
internal void cdl_render_scene_ui_push_line(cdl_render_scene* s, v2f p1, v2f p2, f32 thickness, color8 color)
{
    assert(s);
    assert( RENDERER_BATCH_MAX_LINE_VERTEX_COUNT > s->triangles.vertexCount + 2 );
    PROFILED_FUNC;
    
    v2f p1p2 = v2f_normalize(v2f_sub(p2, p1));
    v2f orthoP1P2 = v2f_new(-p1p2.y, p1p2.x);
    
    v2f pq0 = v2f_add(p1, v2f_times(orthoP1P2, thickness / 2));
    v2f pq1 = v2f_sub(p1, v2f_times(orthoP1P2, thickness / 2));
    v2f pq2 = v2f_add(p2, v2f_times(orthoP1P2, thickness / 2));
    v2f pq3 = v2f_sub(p2, v2f_times(orthoP1P2, thickness / 2));
    
    v2f glpq0 = pixelf_to_gl_coordinates(pq0);
    v2f glpq1 = pixelf_to_gl_coordinates(pq1);
    v2f glpq2 = pixelf_to_gl_coordinates(pq2);
    v2f glpq3 = pixelf_to_gl_coordinates(pq3);
    
    u32 offset = (u32)(s->triangles.nextVertex - s->triangles.vertexBuffer);
    *s->triangles.nextIndex++ = offset + 0;
    *s->triangles.nextIndex++ = offset + 1;
    *s->triangles.nextIndex++ = offset + 3;
    *s->triangles.nextIndex++ = offset + 1;
    *s->triangles.nextIndex++ = offset + 2;
    *s->triangles.nextIndex++ = offset + 3;
    
    s->triangles.nextVertex->position = v3f_new(glpq0.x, glpq0.y, 0);
    s->triangles.nextVertex->uvs = v2f_new(0);
    s->triangles.nextVertex->color = color;
    s->triangles.nextVertex->flags = 0;
    s->triangles.nextVertex->misc = v4f_new(0);
    s->triangles.nextVertex++;
    
    s->triangles.nextVertex->position = v3f_new(glpq1.x, glpq1.y, 0);
    s->triangles.nextVertex->uvs = v2f_new(0);
    s->triangles.nextVertex->color = color;
    s->triangles.nextVertex->flags = 0;
    s->triangles.nextVertex->misc = v4f_new(0);
    s->triangles.nextVertex++;
    
    s->triangles.nextVertex->position = v3f_new(glpq3.x, glpq3.y, 0);
    s->triangles.nextVertex->uvs = v2f_new(0);
    s->triangles.nextVertex->color = color;
    s->triangles.nextVertex->flags = 0;
    s->triangles.nextVertex->misc = v4f_new(0);
    s->triangles.nextVertex++;
    
    s->triangles.nextVertex->position = v3f_new(glpq2.x, glpq2.y, 0);
    s->triangles.nextVertex->uvs = v2f_new(0);
    s->triangles.nextVertex->color = color;
    s->triangles.nextVertex->flags = 0;
    s->triangles.nextVertex->misc = v4f_new(0);
    s->triangles.nextVertex++;
    
    s->triangles.vertexCount += 4;
    s->currentState->vertexCount += 4;
    s->currentState->indexCount += 6;
    s->dirty = 1;
}


//- [SECTION] UI Scenes Curves
internal void cdl_render_scene_ui_push_bezier_quadratic(cdl_render_scene* s, v2f p1, v2f p2, v2f p3, color8 color, f32 thickness, u32 steps)
{
    PROFILED_FUNC;
    
    v2f previous = p1;
    for (u32 dt = 0; dt <= steps; dt++) {
        v2f point = bezier_point_on_quadratic(p1, p2, p3, dt / (f32)steps);
        cdl_render_scene_ui_push_line(s, point, previous, thickness, color);
        previous = point;
    }
}

internal void cdl_render_scene_ui_push_bezier_cubic(cdl_render_scene* s, v2f p1, v2f p2, v2f p3, v2f p4, color8 color, f32 thickness, u32 steps)
{
    PROFILED_FUNC;
    
    v2f previous = p1;
    for (u32 dt = 0; dt <= steps; dt++) {
        v2f point = bezier_point_on_cubic(p1, p2, p3, p4, dt / (f32)steps);
        cdl_render_scene_ui_push_line(s, point, previous, thickness, color);
        previous = point;
    }
}


//- [SECTION] UI Scenes Triangles
internal void cdl_render_scene_ui_push_triangles(cdl_render_scene* s, u32 count, v2f* positions, color8* colors)
{
    assert(s);
    assert(positions);
    assert(colors);
    assert( RENDERER_BATCH_MAX_TRIANGLE_VERTEX_COUNT > s->triangles.vertexCount + 3 );
    PROFILED_FUNC;
    
    for (u32 i = 0; i < count; i++) {
        u32 offset = (u32)(s->triangles.nextVertex - s->triangles.vertexBuffer);
        *s->triangles.nextIndex++ = offset + 0;
        *s->triangles.nextIndex++ = offset + 1;
        *s->triangles.nextIndex++ = offset + 2;
        
        v2f glPos = pixelf_to_gl_coordinates(positions[i * 3 + 0]);
        s->triangles.nextVertex->position = v3f_new( glPos.x, glPos.y, 0.f );
        s->triangles.nextVertex->uvs = v2f_new(0);
        s->triangles.nextVertex->color = colors[i * 3 + 0];
        s->triangles.nextVertex->flags = 0;
        s->triangles.nextVertex->misc = v4f_new(0);
        s->triangles.nextVertex++;
        
        glPos = pixelf_to_gl_coordinates(positions[i * 3 + 1]);
        s->triangles.nextVertex->position = v3f_new( glPos.x, glPos.y, 0.f );
        s->triangles.nextVertex->uvs = v2f_new(0);
        s->triangles.nextVertex->color = colors[i * 3 + 1];
        s->triangles.nextVertex->flags = 0;
        s->triangles.nextVertex->misc = v4f_new(0);
        s->triangles.nextVertex++;
        
        glPos = pixelf_to_gl_coordinates(positions[i * 3 + 2]);
        s->triangles.nextVertex->position = v3f_new( glPos.x, glPos.y, 0.f );
        s->triangles.nextVertex->uvs = v2f_new(0);
        s->triangles.nextVertex->color = colors[i * 3 + 2];
        s->triangles.nextVertex->flags = 0;
        s->triangles.nextVertex->misc = v4f_new(0);
        s->triangles.nextVertex++;
        
        s->triangles.vertexCount += 3;
        s->currentState->vertexCount += 3;
        s->currentState->indexCount += 3;
    }
    
    s->dirty = 1;
}

internal void cdl_render_scene_ui_push_quads(cdl_render_scene* s, u32 count, v2f* positions, v2f* sizes, color8* colors)
{
    assert(s);
    assert(positions);
    assert(sizes);
    assert(colors);
    assert( RENDERER_BATCH_MAX_TRIANGLE_VERTEX_COUNT > s->triangles.vertexCount + count * 4 );
    PROFILED_FUNC;
    
    f32 aspectRatio = cdl_renderer_aspect_ratio();
    
    for (u32 i = 0; i < count; i++) {
        v2f position = pixelf_to_gl_coordinates(positions[i]);
        v2f size = pixelf_to_gl_size(sizes[i]);
        color8 color = colors[i];
        
        u32 offset = (u32)(s->triangles.nextVertex - s->triangles.vertexBuffer);
        *s->triangles.nextIndex++ = offset + 0;
        *s->triangles.nextIndex++ = offset + 1;
        *s->triangles.nextIndex++ = offset + 3;
        *s->triangles.nextIndex++ = offset + 1;
        *s->triangles.nextIndex++ = offset + 2;
        *s->triangles.nextIndex++ = offset + 3;
        
        s->triangles.nextVertex->position = v3f_new( position.x + size.width / 2, position.y + aspectRatio * size.height / 2, 0.f );
        s->triangles.nextVertex->uvs = v2f_new(0);
        s->triangles.nextVertex->color = color;
        s->triangles.nextVertex->flags = 0;
        s->triangles.nextVertex->misc = v4f_new(0);
        s->triangles.nextVertex++;
        
        s->triangles.nextVertex->position = v3f_new( position.x + size.width / 2, position.y - aspectRatio * size.height / 2, 0.f );
        s->triangles.nextVertex->uvs = v2f_new(0);
        s->triangles.nextVertex->color = color;
        s->triangles.nextVertex->flags = 0;
        s->triangles.nextVertex->misc = v4f_new(0);
        s->triangles.nextVertex++;
        
        s->triangles.nextVertex->position = v3f_new( position.x - size.width / 2, position.y - aspectRatio * size.height / 2, 0.f );
        s->triangles.nextVertex->uvs = v2f_new(0);
        s->triangles.nextVertex->color = color;
        s->triangles.nextVertex->flags = 0;
        s->triangles.nextVertex->misc = v4f_new(0);
        s->triangles.nextVertex++;
        
        s->triangles.nextVertex->position = v3f_new( position.x - size.width / 2, position.y + aspectRatio * size.height / 2, 0.f );
        s->triangles.nextVertex->uvs = v2f_new(0);
        s->triangles.nextVertex->color = color;
        s->triangles.nextVertex->flags = 0;
        s->triangles.nextVertex->misc = v4f_new(0);
        s->triangles.nextVertex++;
        
        s->triangles.vertexCount += 4;
        s->currentState->vertexCount += 4;
        s->currentState->indexCount += 6;
    }
    
    s->dirty = 1;
}

internal void cdl_render_scene_ui_push_quad(cdl_render_scene* s, v2f position, v2f size, color8 color)
{
    PROFILED_FUNC;
    cdl_render_scene_ui_push_quads(s, 1, &position, &size, &color);
}

internal void cdl_render_scene_ui_push_quad_textured(cdl_render_scene* s, v2f position, v2f size, v4f uvs, u32 texture)
{
    assert(s);
    assert( RENDERER_BATCH_MAX_TRIANGLE_VERTEX_COUNT > s->triangles.vertexCount + 4 );
    PROFILED_FUNC;
    
    v2f glPos = pixelf_to_gl_coordinates(position);
    v2f glSz = pixelf_to_gl_size(size);
    
    u32 offset = (u32)(s->triangles.nextVertex - s->triangles.vertexBuffer);
    *s->triangles.nextIndex++ = offset + 0;
    *s->triangles.nextIndex++ = offset + 1;
    *s->triangles.nextIndex++ = offset + 3;
    *s->triangles.nextIndex++ = offset + 1;
    *s->triangles.nextIndex++ = offset + 2;
    *s->triangles.nextIndex++ = offset + 3;
    
    f32 aspectRatio = cdl_renderer_aspect_ratio();
    
    s->triangles.nextVertex->position = v3f_new( glPos.x + glSz.width / 2, glPos.y + aspectRatio * glSz.height / 2, 0.f );
    s->triangles.nextVertex->uvs = v2f_new( uvs.y, uvs.w );
    s->triangles.nextVertex->color = color_white;
    s->triangles.nextVertex->flags = 0;
    s->triangles.nextVertex->flags |= BatchUIVertexFlags_UseTexture;
    s->triangles.nextVertex->misc.x = (f32)(texture);
    s->triangles.nextVertex++;
    
    s->triangles.nextVertex->position = v3f_new( glPos.x + glSz.width / 2, glPos.y - aspectRatio * glSz.height / 2, 0.f );
    s->triangles.nextVertex->uvs = v2f_new( uvs.y, uvs.z );
    s->triangles.nextVertex->color = color_white;
    s->triangles.nextVertex->flags = 0;
    s->triangles.nextVertex->flags |= BatchUIVertexFlags_UseTexture;
    s->triangles.nextVertex->misc.x = (f32)(texture);
    s->triangles.nextVertex++;
    
    s->triangles.nextVertex->position = v3f_new( glPos.x - glSz.width / 2, glPos.y - aspectRatio * glSz.height / 2, 0.f );
    s->triangles.nextVertex->uvs = v2f_new( uvs.x, uvs.z );
    s->triangles.nextVertex->color = color_white;
    s->triangles.nextVertex->flags = 0;
    s->triangles.nextVertex->flags |= BatchUIVertexFlags_UseTexture;
    s->triangles.nextVertex->misc.x = (f32)(texture);
    s->triangles.nextVertex++;
    
    s->triangles.nextVertex->position = v3f_new( glPos.x - glSz.width / 2, glPos.y + aspectRatio * glSz.height / 2, 0.f );
    s->triangles.nextVertex->uvs = v2f_new( uvs.x, uvs.w );
    s->triangles.nextVertex->color = color_white;
    s->triangles.nextVertex->flags = 0;
    s->triangles.nextVertex->flags |= BatchUIVertexFlags_UseTexture;
    s->triangles.nextVertex->misc.x = (f32)(texture);
    s->triangles.nextVertex++;
    
    s->triangles.vertexCount += 4;
    s->currentState->vertexCount += 4;
    s->currentState->indexCount += 6;
    s->dirty = 1;
}

internal void cdl_render_scene_ui_push_quad_textured_tinted(cdl_render_scene* s, v2f position, v2f size, v4f uvs, color8 color, u32 texture)
{
    assert(s);
    assert( RENDERER_BATCH_MAX_TRIANGLE_VERTEX_COUNT > s->triangles.vertexCount + 4 );
    PROFILED_FUNC;
    
    v2f glPos = pixelf_to_gl_coordinates(position);
    v2f glSz = pixelf_to_gl_size(size);
    
    u32 offset = (u32)(s->triangles.nextVertex - s->triangles.vertexBuffer);
    *s->triangles.nextIndex++ = offset + 0;
    *s->triangles.nextIndex++ = offset + 1;
    *s->triangles.nextIndex++ = offset + 3;
    *s->triangles.nextIndex++ = offset + 1;
    *s->triangles.nextIndex++ = offset + 2;
    *s->triangles.nextIndex++ = offset + 3;
    
    f32 aspectRatio = cdl_renderer_aspect_ratio();
    
    s->triangles.nextVertex->position = v3f_new( glPos.x + glSz.width / 2, glPos.y + aspectRatio * glSz.height / 2, 0.f );
    s->triangles.nextVertex->uvs = v2f_new( uvs.y, uvs.w );
    s->triangles.nextVertex->color = color;
    s->triangles.nextVertex->flags = 0;
    s->triangles.nextVertex->flags |= BatchUIVertexFlags_UseTexture;
    s->triangles.nextVertex->misc.x = (f32)(texture);
    s->triangles.nextVertex++;
    
    s->triangles.nextVertex->position = v3f_new( glPos.x + glSz.width / 2, glPos.y - aspectRatio * glSz.height / 2, 0.f );
    s->triangles.nextVertex->uvs = v2f_new( uvs.y, uvs.z );
    s->triangles.nextVertex->color = color;
    s->triangles.nextVertex->flags = 0;
    s->triangles.nextVertex->flags |= BatchUIVertexFlags_UseTexture;
    s->triangles.nextVertex->misc.x = (f32)(texture);
    s->triangles.nextVertex++;
    
    s->triangles.nextVertex->position = v3f_new( glPos.x - glSz.width / 2, glPos.y - aspectRatio * glSz.height / 2, 0.f );
    s->triangles.nextVertex->uvs = v2f_new( uvs.x, uvs.z );
    s->triangles.nextVertex->color = color;
    s->triangles.nextVertex->flags = 0;
    s->triangles.nextVertex->flags |= BatchUIVertexFlags_UseTexture;
    s->triangles.nextVertex->misc.x = (f32)(texture);
    s->triangles.nextVertex++;
    
    s->triangles.nextVertex->position = v3f_new( glPos.x - glSz.width / 2, glPos.y + aspectRatio * glSz.height / 2, 0.f );
    s->triangles.nextVertex->uvs = v2f_new( uvs.x, uvs.w );
    s->triangles.nextVertex->color = color;
    s->triangles.nextVertex->flags = 0;
    s->triangles.nextVertex->flags |= BatchUIVertexFlags_UseTexture;
    s->triangles.nextVertex->misc.x = (f32)(texture);
    s->triangles.nextVertex++;
    
    s->triangles.vertexCount += 4;
    s->currentState->vertexCount += 4;
    s->currentState->indexCount += 6;
    s->dirty = 1;
}

internal void cdl_render_scene_ui_push_quad_sdf(cdl_render_scene* s, v2f position, v2f size, v4f uvs, u32 texture)
{
    assert(s);
    assert( RENDERER_BATCH_MAX_TRIANGLE_VERTEX_COUNT > s->triangles.vertexCount + 4 );
    PROFILED_FUNC;
    
    v2f glPos = pixelf_to_gl_coordinates(position);
    v2f glSz = pixelf_to_gl_size(size);
    
    u32 offset = (u32)(s->triangles.nextVertex - s->triangles.vertexBuffer);
    *s->triangles.nextIndex++ = offset + 0;
    *s->triangles.nextIndex++ = offset + 1;
    *s->triangles.nextIndex++ = offset + 3;
    *s->triangles.nextIndex++ = offset + 1;
    *s->triangles.nextIndex++ = offset + 2;
    *s->triangles.nextIndex++ = offset + 3;
    
    f32 aspectRatio = cdl_renderer_aspect_ratio();
    
    s->triangles.nextVertex->position = v3f_new( glPos.x + glSz.width / 2, glPos.y + aspectRatio * glSz.height / 2, 0.f );
    s->triangles.nextVertex->uvs = v2f_new( uvs.y, uvs.w );
    s->triangles.nextVertex->color = color_white;
    s->triangles.nextVertex->flags = 0;
    s->triangles.nextVertex->flags |= BatchUIVertexFlags_UseTexture | BatchUIVertexFlags_UseSDF;
    s->triangles.nextVertex->misc.x = (f32)(texture);
    s->triangles.nextVertex->misc.w = (f32)(f32_floor(size.width) + (f32_floor(size.height) << 16));
    s->triangles.nextVertex++;
    
    s->triangles.nextVertex->position = v3f_new( glPos.x + glSz.width / 2, glPos.y - aspectRatio * glSz.height / 2, 0.f );
    s->triangles.nextVertex->uvs = v2f_new( uvs.y, uvs.z );
    s->triangles.nextVertex->color = color_white;
    s->triangles.nextVertex->flags = 0;
    s->triangles.nextVertex->flags |= BatchUIVertexFlags_UseTexture | BatchUIVertexFlags_UseSDF;
    s->triangles.nextVertex->misc.x = (f32)(texture);
    s->triangles.nextVertex->misc.w = (f32)(f32_floor(size.width) + (f32_floor(size.height) << 16));
    s->triangles.nextVertex++;
    
    s->triangles.nextVertex->position = v3f_new( glPos.x - glSz.width / 2, glPos.y - aspectRatio * glSz.height / 2, 0.f );
    s->triangles.nextVertex->uvs = v2f_new( uvs.x, uvs.z );
    s->triangles.nextVertex->color = color_white;
    s->triangles.nextVertex->flags = 0;
    s->triangles.nextVertex->flags |= BatchUIVertexFlags_UseTexture | BatchUIVertexFlags_UseSDF;
    s->triangles.nextVertex->misc.x = (f32)(texture);
    s->triangles.nextVertex->misc.w = (f32)(f32_floor(size.width) + (f32_floor(size.height) << 16));
    s->triangles.nextVertex++;
    
    s->triangles.nextVertex->position = v3f_new( glPos.x - glSz.width / 2, glPos.y + aspectRatio * glSz.height / 2, 0.f );
    s->triangles.nextVertex->uvs = v2f_new( uvs.x, uvs.w );
    s->triangles.nextVertex->color = color_white;
    s->triangles.nextVertex->flags = 0;
    s->triangles.nextVertex->flags |= BatchUIVertexFlags_UseTexture | BatchUIVertexFlags_UseSDF;
    s->triangles.nextVertex->misc.x = (f32)(texture);
    s->triangles.nextVertex->misc.w = (f32)(f32_floor(size.width) + (f32_floor(size.height) << 16));
    s->triangles.nextVertex++;
    
    s->triangles.vertexCount += 4;
    s->currentState->vertexCount += 4;
    s->currentState->indexCount += 6;
    s->dirty = 1;
}


internal void cdl_render_scene_ui_push_quad_gradient(cdl_render_scene* s, v2f position, v2f size, color8 topRightColor, color8 bottomRightColor, color8 bottomLeftColor, color8 topLeftColor)
{
    assert(s);
    assert( RENDERER_BATCH_MAX_TRIANGLE_VERTEX_COUNT > s->triangles.vertexCount + 4 );
    PROFILED_FUNC;
    
    v2f glPos = pixelf_to_gl_coordinates(position);
    v2f glSz = pixelf_to_gl_size(size);
    
    u32 offset = (u32)(s->triangles.nextVertex - s->triangles.vertexBuffer);
    *s->triangles.nextIndex++ = offset + 0;
    *s->triangles.nextIndex++ = offset + 1;
    *s->triangles.nextIndex++ = offset + 3;
    *s->triangles.nextIndex++ = offset + 1;
    *s->triangles.nextIndex++ = offset + 2;
    *s->triangles.nextIndex++ = offset + 3;
    
    f32 aspectRatio = cdl_renderer_aspect_ratio();
    
    s->triangles.nextVertex->position = v3f_new( glPos.x + glSz.width / 2, glPos.y + aspectRatio * glSz.height / 2, 0.f );
    s->triangles.nextVertex->uvs = v2f_new(0);
    s->triangles.nextVertex->color = topRightColor;
    s->triangles.nextVertex->flags = 0;
    s->triangles.nextVertex->misc = v4f_new(0);
    s->triangles.nextVertex++;
    
    s->triangles.nextVertex->position = v3f_new( glPos.x + glSz.width / 2, glPos.y - aspectRatio * glSz.height / 2, 0.f );
    s->triangles.nextVertex->uvs = v2f_new(0);
    s->triangles.nextVertex->color = bottomRightColor;
    s->triangles.nextVertex->flags = 0;
    s->triangles.nextVertex->misc = v4f_new(0);
    s->triangles.nextVertex++;
    
    s->triangles.nextVertex->position = v3f_new( glPos.x - glSz.width / 2, glPos.y - aspectRatio * glSz.height / 2, 0.f );
    s->triangles.nextVertex->uvs = v2f_new(0);
    s->triangles.nextVertex->color = bottomLeftColor;
    s->triangles.nextVertex->flags = 0;
    s->triangles.nextVertex->misc = v4f_new(0);
    s->triangles.nextVertex++;
    
    s->triangles.nextVertex->position = v3f_new( glPos.x - glSz.width / 2, glPos.y + aspectRatio * glSz.height / 2, 0.f );
    s->triangles.nextVertex->uvs = v2f_new(0);
    s->triangles.nextVertex->color = topLeftColor;
    s->triangles.nextVertex->flags = 0;
    s->triangles.nextVertex->misc = v4f_new(0);
    s->triangles.nextVertex++;
    
    s->triangles.vertexCount += 4;
    s->currentState->vertexCount += 4;
    s->currentState->indexCount += 6;
    s->dirty = 1;
}

internal void cdl_render_scene_ui_push_quad_color_picker(cdl_render_scene* s, v2f position, v2f size, f32 hueAdjust)
{
    assert(s);
    assert( RENDERER_BATCH_MAX_TRIANGLE_VERTEX_COUNT > s->triangles.vertexCount + 4 );
    PROFILED_FUNC;
    
    v2f glPos = pixelf_to_gl_coordinates(position);
    v2f glSz = pixelf_to_gl_size(size);
    
    u32 offset = (u32)(s->triangles.nextVertex - s->triangles.vertexBuffer);
    *s->triangles.nextIndex++ = offset + 0;
    *s->triangles.nextIndex++ = offset + 1;
    *s->triangles.nextIndex++ = offset + 3;
    *s->triangles.nextIndex++ = offset + 1;
    *s->triangles.nextIndex++ = offset + 2;
    *s->triangles.nextIndex++ = offset + 3;
    
    f32 aspectRatio = cdl_renderer_aspect_ratio();
    
    s->triangles.nextVertex->position = v3f_new( glPos.x + glSz.width / 2, glPos.y + aspectRatio * glSz.height / 2, 0.f );
    s->triangles.nextVertex->uvs = v2f_new(1, 1);
    s->triangles.nextVertex->color = color_white;
    s->triangles.nextVertex->flags = 0;
    s->triangles.nextVertex->flags |= BatchUIVertexFlags_ColorPicker;
    s->triangles.nextVertex->misc.w = hueAdjust;
    s->triangles.nextVertex++;
    
    s->triangles.nextVertex->position = v3f_new( glPos.x + glSz.width / 2, glPos.y - aspectRatio * glSz.height / 2, 0.f );
    s->triangles.nextVertex->uvs = v2f_new(1, 0);
    s->triangles.nextVertex->color = color_white;
    s->triangles.nextVertex->flags = 0;
    s->triangles.nextVertex->flags |= BatchUIVertexFlags_ColorPicker;
    s->triangles.nextVertex->misc.w = hueAdjust;
    s->triangles.nextVertex++;
    
    s->triangles.nextVertex->position = v3f_new( glPos.x - glSz.width / 2, glPos.y - aspectRatio * glSz.height / 2, 0.f );
    s->triangles.nextVertex->uvs = v2f_new(0);
    s->triangles.nextVertex->color = color_white;
    s->triangles.nextVertex->flags = 0;
    s->triangles.nextVertex->flags |= BatchUIVertexFlags_ColorPicker;
    s->triangles.nextVertex->misc.w = hueAdjust;
    s->triangles.nextVertex++;
    
    s->triangles.nextVertex->position = v3f_new( glPos.x - glSz.width / 2, glPos.y + aspectRatio * glSz.height / 2, 0.f );
    s->triangles.nextVertex->uvs = v2f_new(0, 1);
    s->triangles.nextVertex->color = color_white;
    s->triangles.nextVertex->flags = 0;
    s->triangles.nextVertex->flags |= BatchUIVertexFlags_ColorPicker;
    s->triangles.nextVertex->misc.w = hueAdjust;
    s->triangles.nextVertex++;
    
    s->triangles.vertexCount += 4;
    s->currentState->vertexCount += 4;
    s->currentState->indexCount += 6;
    s->dirty = 1;
}

internal void cdl_render_scene_ui_push_quad_rounded(cdl_render_scene* s, v2f position, v2f size, color8 color, i32 cornerRadius)
{
    assert(s);
    assert( RENDERER_BATCH_MAX_TRIANGLE_VERTEX_COUNT > s->triangles.vertexCount + 4 );
    PROFILED_FUNC;
    
    v2f glPos = pixelf_to_gl_coordinates(position);
    v2f glSz = pixelf_to_gl_size(size);
    
    u32 offset = (u32)(s->triangles.nextVertex - s->triangles.vertexBuffer);
    *s->triangles.nextIndex++ = offset + 0;
    *s->triangles.nextIndex++ = offset + 1;
    *s->triangles.nextIndex++ = offset + 3;
    *s->triangles.nextIndex++ = offset + 1;
    *s->triangles.nextIndex++ = offset + 2;
    *s->triangles.nextIndex++ = offset + 3;
    
    f32 aspectRatio = cdl_renderer_aspect_ratio();
    
    s->triangles.nextVertex->position = v3f_new( glPos.x + glSz.width / 2, glPos.y + aspectRatio * glSz.height / 2, 0.f );
    s->triangles.nextVertex->uvs = v2f_new(0);
    s->triangles.nextVertex->color = color;
    s->triangles.nextVertex->flags = BatchUIVertexFlags_RoundCorners;
    s->triangles.nextVertex->misc = ZeroStruct;
    s->triangles.nextVertex->misc.y = (f32)cornerRadius;
    s->triangles.nextVertex->misc.z = (f32)(f32_floor(position.x) + (f32_floor(position.y) << 16));
    s->triangles.nextVertex->misc.w = (f32)(f32_floor(size.width) + (f32_floor(size.height) << 16));
    s->triangles.nextVertex++;
    
    s->triangles.nextVertex->position = v3f_new( glPos.x + glSz.width / 2, glPos.y - aspectRatio * glSz.height / 2, 0.f );
    s->triangles.nextVertex->uvs = v2f_new(0);
    s->triangles.nextVertex->color = color;
    s->triangles.nextVertex->flags = BatchUIVertexFlags_RoundCorners;
    s->triangles.nextVertex->misc = ZeroStruct;
    s->triangles.nextVertex->misc.y = (f32)cornerRadius;
    s->triangles.nextVertex->misc.z = (f32)(f32_floor(position.x) + (f32_floor(position.y) << 16));
    s->triangles.nextVertex->misc.w = (f32)(f32_floor(size.width) + (f32_floor(size.height) << 16));
    s->triangles.nextVertex++;
    
    s->triangles.nextVertex->position = v3f_new( glPos.x - glSz.width / 2, glPos.y - aspectRatio * glSz.height / 2, 0.f );
    s->triangles.nextVertex->uvs = v2f_new(0);
    s->triangles.nextVertex->color = color;
    s->triangles.nextVertex->flags = BatchUIVertexFlags_RoundCorners;
    s->triangles.nextVertex->misc = ZeroStruct;
    s->triangles.nextVertex->misc.y = (f32)cornerRadius;
    s->triangles.nextVertex->misc.z = (f32)(f32_floor(position.x) + (f32_floor(position.y) << 16));
    s->triangles.nextVertex->misc.w = (f32)(f32_floor(size.width) + (f32_floor(size.height) << 16));
    s->triangles.nextVertex++;
    
    s->triangles.nextVertex->position = v3f_new( glPos.x - glSz.width / 2, glPos.y + aspectRatio * glSz.height / 2, 0.f );
    s->triangles.nextVertex->uvs = v2f_new(0);
    s->triangles.nextVertex->color = color;
    s->triangles.nextVertex->flags = BatchUIVertexFlags_RoundCorners;
    s->triangles.nextVertex->misc = ZeroStruct;
    s->triangles.nextVertex->misc.y = (f32)cornerRadius;
    s->triangles.nextVertex->misc.z = (f32)(f32_floor(position.x) + (f32_floor(position.y) << 16));
    s->triangles.nextVertex->misc.w = (f32)(f32_floor(size.width) + (f32_floor(size.height) << 16));
    s->triangles.nextVertex++;
    
    s->triangles.vertexCount += 4;
    s->currentState->vertexCount += 4;
    s->currentState->indexCount += 6;
    s->dirty = 1;
}

internal void cdl_render_scene_ui_push_circle(cdl_render_scene* s, v2f position, f32 radius, f32 width, color8 color)
{
    assert(s);
    assert( RENDERER_BATCH_MAX_TRIANGLE_VERTEX_COUNT > s->triangles.vertexCount + 4 );
    PROFILED_FUNC;
    
    v2f glPos = pixelf_to_gl_coordinates(position);
    v2f glSz = pixelf_to_gl_size(v2f_new(radius * 2, radius * 2));
    
    u32 offset = (u32)(s->triangles.nextVertex - s->triangles.vertexBuffer);
    *s->triangles.nextIndex++ = offset + 0;
    *s->triangles.nextIndex++ = offset + 1;
    *s->triangles.nextIndex++ = offset + 3;
    *s->triangles.nextIndex++ = offset + 1;
    *s->triangles.nextIndex++ = offset + 2;
    *s->triangles.nextIndex++ = offset + 3;
    
    f32 aspectRatio = cdl_renderer_aspect_ratio();
    
    s->triangles.nextVertex->position = v3f_new( glPos.x + glSz.width / 2, glPos.y + aspectRatio * glSz.height / 2, 0.f );
    s->triangles.nextVertex->uvs = v2f_new(0);
    s->triangles.nextVertex->color = color;
    s->triangles.nextVertex->flags = BatchUIVertexFlags_Circle;
    s->triangles.nextVertex->misc = ZeroStruct;
    s->triangles.nextVertex->misc.y = radius;
    s->triangles.nextVertex->misc.z = (f32)(f32_floor(position.x) + (f32_floor(position.y) << 16));
    s->triangles.nextVertex->misc.w = f32_is_zero(width) ? radius : width;
    s->triangles.nextVertex++;
    
    s->triangles.nextVertex->position = v3f_new( glPos.x + glSz.width / 2, glPos.y - aspectRatio * glSz.height / 2, 0.f );
    s->triangles.nextVertex->uvs = v2f_new(0);
    s->triangles.nextVertex->color = color;
    s->triangles.nextVertex->flags = BatchUIVertexFlags_Circle;
    s->triangles.nextVertex->misc = ZeroStruct;
    s->triangles.nextVertex->misc.y = (f32)radius;
    s->triangles.nextVertex->misc.z = (f32)(f32_floor(position.x) + (f32_floor(position.y) << 16));
    s->triangles.nextVertex->misc.w = f32_is_zero(width) ? radius : width;
    s->triangles.nextVertex++;
    
    s->triangles.nextVertex->position = v3f_new( glPos.x - glSz.width / 2, glPos.y - aspectRatio * glSz.height / 2, 0.f );
    s->triangles.nextVertex->uvs = v2f_new(0);
    s->triangles.nextVertex->color = color;
    s->triangles.nextVertex->flags = BatchUIVertexFlags_Circle;
    s->triangles.nextVertex->misc = ZeroStruct;
    s->triangles.nextVertex->misc.y = radius;
    s->triangles.nextVertex->misc.z = (f32)(f32_floor(position.x) + (f32_floor(position.y) << 16));
    s->triangles.nextVertex->misc.w = f32_is_zero(width) ? radius : width;
    s->triangles.nextVertex++;
    
    s->triangles.nextVertex->position = v3f_new( glPos.x - glSz.width / 2, glPos.y + aspectRatio * glSz.height / 2, 0.f );
    s->triangles.nextVertex->uvs = v2f_new(0);
    s->triangles.nextVertex->color = color;
    s->triangles.nextVertex->flags = BatchUIVertexFlags_Circle;
    s->triangles.nextVertex->misc = ZeroStruct;
    s->triangles.nextVertex->misc.y = radius;
    s->triangles.nextVertex->misc.z = (f32)(f32_floor(position.x) + (f32_floor(position.y) << 16));
    s->triangles.nextVertex->misc.w = f32_is_zero(width) ? radius : width;
    s->triangles.nextVertex++;
    
    s->triangles.vertexCount += 4;
    s->currentState->vertexCount += 4;
    s->currentState->indexCount += 6;
    s->dirty = 1;
}


//- [SECTION] UI Scenes Rect
internal void cdl_render_scene_ui_push_rect(cdl_render_scene* s, rect2 rect, color8 color)
{
    PROFILED_FUNC;
    
    v2f position = rect2_mid(rect);
    v2f size = rect2_dim(rect);
    
    cdl_render_scene_ui_push_quads(s, 1, &position, &size, &color);
}

internal void cdl_render_scene_ui_push_rect_outline(cdl_render_scene* s, rect2 rect, color8 color, f32 thickness)
{
    PROFILED_FUNC;
    
    v2f p0 = rect.min;
    v2f p1 = v2f_new(rect.min.x, rect.max.y);
    v2f p2 = rect.max;
    v2f p3 = v2f_new(rect.max.x, rect.min.y);
    
    cdl_render_scene_ui_push_line(s, p0, p1, thickness, color);
    cdl_render_scene_ui_push_line(s, p1, p2, thickness, color);
    cdl_render_scene_ui_push_line(s, p2, p3, thickness, color);
    cdl_render_scene_ui_push_line(s, p3, p0, thickness, color);
}


//- [SECTION] UI Scenes Text
internal void cdl_render_scene_ui_push_glyph(cdl_render_scene* s, u32 texture, v3f position, v2f scale, v4f uvs, cdl_text_atb attributes)
{
    assert( RENDERER_BATCH_MAX_TRIANGLE_VERTEX_COUNT * 4 > s->triangles.vertexCount + 4 );
    PROFILED_FUNC;
    
    u32 offset = (u32)(s->triangles.nextVertex - s->triangles.vertexBuffer);
    *s->triangles.nextIndex++ = offset + 0;
    *s->triangles.nextIndex++ = offset + 1;
    *s->triangles.nextIndex++ = offset + 3;
    *s->triangles.nextIndex++ = offset + 1;
    *s->triangles.nextIndex++ = offset + 2;
    *s->triangles.nextIndex++ = offset + 3;
    
    f32 aspectRatio = cdl_renderer_aspect_ratio();
    u32 additionnalUIVertexFlags = 0;
    f32 fontSize = attributes.size;
    f32 fontWeight = attributes.weight;
    color8 color = attributes.color;
    
    if ( attributes.style & TextStyle_Bold ) {
        additionnalUIVertexFlags |= BatchUIVertexFlags_TextBold;
    }
    
    s->triangles.nextVertex->position = v3f_new( position.x + scale.width / 2, position.y + aspectRatio * scale.height / 2, 0.f );
    s->triangles.nextVertex->uvs = v2f_new( uvs.y, uvs.w );
    s->triangles.nextVertex->color = color;
    s->triangles.nextVertex->flags = 0;
    s->triangles.nextVertex->flags |= BatchUIVertexFlags_UseTexture | additionnalUIVertexFlags;
    s->triangles.nextVertex->misc.x = (f32)(texture);
    s->triangles.nextVertex->misc.y = fontSize;
    s->triangles.nextVertex->misc.w = fontWeight;
    s->triangles.nextVertex++;
    
    s->triangles.nextVertex->position = v3f_new( position.x + scale.width / 2, position.y - aspectRatio * scale.height / 2, 0.f );
    s->triangles.nextVertex->uvs = v2f_new( uvs.y, uvs.z );
    s->triangles.nextVertex->color = color;
    s->triangles.nextVertex->flags = 0;
    s->triangles.nextVertex->flags |= BatchUIVertexFlags_UseTexture | additionnalUIVertexFlags;
    s->triangles.nextVertex->misc.x = (f32)(texture);
    s->triangles.nextVertex->misc.y = fontSize;
    s->triangles.nextVertex->misc.w = fontWeight;
    s->triangles.nextVertex++;
    
    s->triangles.nextVertex->position = v3f_new( position.x - scale.width / 2, position.y - aspectRatio * scale.height / 2, 0.f );
    s->triangles.nextVertex->uvs = v2f_new( uvs.x, uvs.z );
    s->triangles.nextVertex->color = color;
    s->triangles.nextVertex->flags = 0;
    s->triangles.nextVertex->flags |= BatchUIVertexFlags_UseTexture | additionnalUIVertexFlags;
    s->triangles.nextVertex->misc.x = (f32)(texture);
    s->triangles.nextVertex->misc.y = fontSize;
    s->triangles.nextVertex->misc.w = fontWeight;
    s->triangles.nextVertex++;
    
    s->triangles.nextVertex->position = v3f_new( position.x - scale.width / 2, position.y + aspectRatio * scale.height / 2, 0.f );
    s->triangles.nextVertex->uvs = v2f_new( uvs.x, uvs.w );
    s->triangles.nextVertex->color = color;
    s->triangles.nextVertex->flags = 0;
    s->triangles.nextVertex->flags |= BatchUIVertexFlags_UseTexture | additionnalUIVertexFlags;
    s->triangles.nextVertex->misc.x = (f32)(texture);
    s->triangles.nextVertex->misc.y = fontSize;
    s->triangles.nextVertex->misc.w = fontWeight;
    s->triangles.nextVertex++;
    
    s->triangles.vertexCount += 4;
    s->currentState->vertexCount += 4;
    s->currentState->indexCount += 6;
    s->dirty = 1;
}


//- [SECTION] Text Rendering
internal cdl_renderer_font* cdl_renderer_font_new(cdl_assets_font* asset)
{
    assert( (u32)(renderer->storage.nextFont - renderer->storage.fonts) + 1 < ARRAY_COUNT(renderer->storage.fonts) );
    PROFILED_FUNC;
    
    cdl_renderer_font* result = renderer->storage.nextFont++;
    result->asset = asset;
    
    return result;
}

internal v2f cdl_renderer_internal_text_apply_justify(char* text, v2f cursor, v2f textSize, cdl_assets_font* font, cdl_text_atb attributes)
{
    v2f result = cursor;
    
    f32 lineWidthForAlign = cdl_text_get_line_width(text, font, attributes);
    if ( FLAG_HAS(attributes.style, TextStyle_AlignLeft) ) {
        result.x -= textSize.x / 2;
        // Do nothing
    }
    
    else if ( FLAG_HAS(attributes.style, TextStyle_AlignRight) ) {
        result.x -= lineWidthForAlign - textSize.x / 2;
    }
    
    else {
        result.x -= lineWidthForAlign / 2;
    }
    
    return result;
}

internal v4f cdl_renderer_internal_compute_glyph_uvs(u32 ascii, cdl_renderer_font* renderFont)
{
    v4f result = ZeroStruct;
    cdl_assets_font* font = renderFont->asset;
    cdl_assets_glyph* cmap = font->glyphs;
    result.x = (f32)(cmap[ascii].xpos) / font->texture->width;
    result.y = (f32)(cmap[ascii].xpos + cmap[ascii].width) / font->texture->width;
    result.z = (f32)(font->texture->height - cmap[ascii].ypos - cmap[ascii].height) / font->texture->height;
    result.w = (f32)(font->texture->height - cmap[ascii].ypos) / font->texture->height;
    return result;
}

// TODO(abe): reintroduce text margins
// TODO(abe): reintroduce text anchors
internal v2f cdl_render_scene_ui_text(cdl_render_scene* s, char* text, v2f position, cdl_renderer_font* renderFont, cdl_text_atb attributes)
{
    PROFILED_FUNC;
    
    u32 length = StringLength(text);
    v2f result = position;
    
    if ( length > 0 ) {
        cdl_assets_font* font = renderFont->asset;
        cdl_assets_glyph* cmap = font->glyphs;
        
        // Graphics
        {
            // Load font if not loaded
            if ( font->head.state != AssetState_Loaded ) {
                log_info("Loading font asset\n");
                cdl_assets_font_load_data(font);
                
                if ( !renderFont->texture ) {
                    log_info("Loading texture asset\n");
                    renderFont->texture = cdl_renderer_texture_new(font->texture);
                }
            }
        }
        
        u32 texture = cdl_renderer_texture_push(s, renderFont->texture);
        
        // Text rendering
        i32 currentLineWidth = 0;
        v2f textSize = cdl_text_get_dimensions(text, font, attributes);
        
        cdl_text_cursor cursor = cdl_text_cursor_new(attributes);
        cursor.current = cdl_renderer_internal_text_apply_justify(text, position, textSize, renderFont->asset, attributes);
        
        // Align cursor depending on the text anchors
        cursor.current.x += ( (attributes.anchor & TextAnchor_x_left) > 0 ) * textSize.width / 2;
        cursor.current.x -= ( (attributes.anchor & TextAnchor_x_right) > 0 ) * textSize.width / 2;
        
        // Align cursor vertically ( done only once )
        cursor.current.y -= ( (attributes.anchor & TextAnchor_y_center) > 0 ) * textSize.height / 2;
        cursor.current.y -= ( (attributes.anchor & TextAnchor_y_bottom ) > 0 ) * textSize.height;
        
        if ( FLAG_HAS(attributes.style, TextStyle_BackgroundFill) ) {
            rect2 bgRect = { cursor.current, v2f_add(cursor.current, textSize) };
            cdl_render_scene_ui_push_rect(s, bgRect, attributes.background);
        }
        
        //- Loop over all the chars in the text
        for (u32 i = 0; i < length; i++) {
            char* c = text + i;
            
            if ( *c == '\n' ) {
                currentLineWidth = 0;
                cursor.current.x = position.x;
                cursor.current.y += attributes.size;
                cursor.current = cdl_renderer_internal_text_apply_justify(c + 1, cursor.current, textSize, renderFont->asset, attributes);
                
                // Align cursor depending on the text anchors
                cursor.current.x += ( (attributes.anchor & TextAnchor_x_left) > 0 ) * textSize.width / 2;
                cursor.current.x -= ( (attributes.anchor & TextAnchor_x_right) > 0 ) * textSize.width / 2;
                continue;
            }
            
            i32 asciiId = i32_clamp(text[i], 0, 125);
            
            v2f lineCursor = cursor.current;
            v2f quadPixPos = {
                lineCursor.x + attributes.size / font->glyphSize * ( cmap[asciiId].xoffset + cmap[asciiId].width / 2.f ),
                lineCursor.y + attributes.size / font->glyphSize * ( cmap[asciiId].yoffset + cmap[asciiId].height / 2.f )
            };
            
            v2f quadPixSize = { (f32)cmap[asciiId].width, (f32)cmap[asciiId].height };
            quadPixSize = v2f_times(quadPixSize, attributes.size / font->glyphSize);
            
            v4f fontUv = cdl_renderer_internal_compute_glyph_uvs(asciiId, renderFont);
            v2f quadPos = pixelf_to_gl_coordinates(quadPixPos);
            v2f quadSize = pixelf_to_gl_size(quadPixSize);
            
            if ( *c != ' ' && *c != '\n' && *c != '\t' ) {
                cdl_render_scene_ui_push_glyph(s, texture, v3f_new(quadPos.x, quadPos.y, 0.f), quadSize, fontUv, attributes);
            }
            
            f32 advance = cmap[asciiId].xadvance * attributes.size / font->glyphSize;
            currentLineWidth += advance;
            cursor.current.x += advance;
            
            if ( currentLineWidth > attributes.lineWidth ) {
                currentLineWidth = 0;
                cursor.current.x = position.x;
                cursor.current.y += attributes.size;
                cursor.current = cdl_renderer_internal_text_apply_justify(c, cursor.current, textSize, renderFont->asset, attributes);
                
                // Align cursor depending on the text anchors
                cursor.current.x += ( (attributes.anchor & TextAnchor_x_left) > 0 ) * textSize.width / 2;
                cursor.current.x -= ( (attributes.anchor & TextAnchor_x_right) > 0 ) * textSize.width / 2;
            }
            
        }
        
        // If the text fit on one line, cursor.max.x is never updated
        if ( cursor.max.x < cursor.current.x ) {
            cursor.max.x = cursor.current.x + attributes.hMargin;
        }
        cursor.max.y = cursor.lineCount * attributes.size;
        
        result = cursor.max;
    }
    
    return result;
}


//- [SECTION] Post Processing
internal cdl_renderer_effect* cdl_renderer_effect_new(cdl_assets_shader* shaderAsset, cdl_renderer_effect* parents[4])
{
    assert(shaderAsset);
    PROFILED_FUNC;
    
    cdl_renderer_effect* result = renderer->post.effects + renderer->post.effectCount++;
    result->shader = cdl_renderer_shader_new(RendererShaderType_FullScreenEffect, shaderAsset);
    result->framebuffer = cdl_renderer_framebuffer_new(1);
    
    if ( parents ) {
        result->hasParents = 1;
        for (u32 i = 0; i < 4; i++) {
            result->parents[i] = parents[i];
        }
    }
    
    return result;
}

internal void cdl_renderer_effect_process(cdl_renderer_scene_view* view, cdl_renderer_effect* effect, bool8 isFinal)
{
    assert(view);
    assert(effect);
    PROFILED_FUNC;
    
    u32 indexInEffectsArray = (u32)(effect - renderer->post.effects);
    bool8 processed = renderer->post.processed[indexInEffectsArray];
    if ( processed ) { return; }
    
    {
        PROFILED_BLOCK("glClear");
        check_gl_proc(glBindFramebuffer(GL_FRAMEBUFFER, effect->framebuffer->transient->id));
        check_gl_proc(glClearColor(0, 0, 0, 0));
        check_gl_proc(glClear(GL_COLOR_BUFFER_BIT));
    }
    
    //- Process parent effects
    if ( effect->hasParents ) {
        for (u32 i = 0; i < ARRAY_COUNT(effect->parents); i++) {
            cdl_renderer_effect* e = *(effect->parents + i);
            if ( !e ) { continue; }
            
            cdl_renderer_effect_process(view, e, 0);
        }
    }
    
    //- Bind parents & scene framebuffer
    check_gl_proc(glBindFramebuffer(GL_FRAMEBUFFER, effect->framebuffer->transient->id));
    
    cdl_renderer_shader_use(effect->shader, view);
    
    {
        PROFILED_BLOCK("cdl_renderer_effect_process::textures");
        u32 unit = 0;
        check_gl_proc(glBindTextureUnit(unit++, view->framebuffer->transient->color));
        
        if ( effect->hasParents ) {
            for (u32 i = 0; i < ARRAY_COUNT(effect->parents); i++) {
                cdl_renderer_effect* e = *(effect->parents + i);
                
                if ( !e ) { 
                    check_gl_proc(glBindTextureUnit(unit++, view->framebuffer->transient->color));
                } else {
                    check_gl_proc(glBindTextureUnit(unit++, e->framebuffer->transient->color));
                }
            }
        }
    }
    
    check_gl_proc(glBindVertexArray(renderer->fsq));
    {
        PROFILED_BLOCK("glDrawArrays");
        check_gl_proc(glDrawArrays(GL_TRIANGLES, 0, 6));
    }
    
    renderer->post.processed[indexInEffectsArray] = 1;
}


//- [SECTION] Scene View
internal cdl_renderer_scene_view* cdl_renderer_scene_view_new(i32 spot, cdl_render_scene* scene, cdl_renderer_camera* camera, cdl_renderer_shader* defaultShader, u32 msaaSamples)
{
    assert(renderer->storage.nextView + 1 < renderer->storage.views + ARRAY_COUNT(renderer->storage.views));
    assert(scene);
    PROFILED_FUNC;
    
    cdl_renderer_scene_view* result = renderer->storage.nextView++;
    result->renderOrder = spot;
    result->scene = scene;
    result->camera = camera;
    result->framebuffer = cdl_renderer_framebuffer_new(msaaSamples);
    result->defaultShader = defaultShader;
    
    return result;
}

internal void cdl_renderer_scene_view_begin(cdl_renderer_scene_view* view)
{
    PROFILED_FUNC;
    
    if ( view->framebuffer->samples > 1 ) {
        check_gl_proc(glBindFramebuffer(GL_FRAMEBUFFER, view->framebuffer->multisampled->id));
    } else {
        check_gl_proc(glBindFramebuffer(GL_FRAMEBUFFER, view->framebuffer->transient->id));
    }
    
    {
        PROFILED_BLOCK("glClear");
        check_gl_proc(glClearColor( 1.f / 255, 1.f / 255, 1.f / 255, 0.f ));
        check_gl_proc(glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT));
    }
}

internal void cdl_renderer_scene_view_render(cdl_renderer_scene_view* view)
{
    PROFILED_FUNC;
    
    if ( view->scene->dirty ) {
        cdl_renderer_scene_view_begin(view);
        
        check_gl_proc(glViewport(0, 0, core->frameWidth, core->frameHeight));
        
        cdl_renderer_framebuffer_use(view->framebuffer);
        cdl_render_scene_prepare_textures(view->scene);
        cdl_renderer_internal_render_scene_instances_and_static(view);
        
        cdl_renderer_shader_use(view->defaultShader, view);
        cdl_renderer_internal_render_scene(view->scene);
    }
}


//- [SECTION] Renderer
inline f32 cdl_renderer_aspect_ratio()
{
    return (core->frameWidth / (f32)core->frameHeight);
}

inline rect2 cdl_renderer_frame_rect()
{
    return rect2_new(v2f_new(0, 0), v2f_new(core->frameWidth, core->frameHeight));
}

internal bool8 cdl_renderer_api_init()
{
    PROFILED_FUNC;
    
    GLenum err = glewInit();
    if ( err != GLEW_OK ) {
        log_err("Error: %s\n", glewGetErrorString(err));
        return false;
    }
    log_info("Status: Using GLEW %s\n", glewGetString(GLEW_VERSION));
    
    return true;
}

inline internal bool8 cdl_renderer_api_flush_errors(const char* funcCall, u32 line)
{
    bool8 result = 0;
    
    GLenum error = glGetError();
    while ( error != GL_NO_ERROR ) {
        result = 1;
        log_err("OPENGL: cdl_renderer.cpp:%d : %s\n", line, funcCall, error);
        error = glGetError();
    }
    
    return result;
}

internal void cdl_renderer_create(cdl_renderer* rendererPtr)
{
    PROFILED_FUNC;
    
    if ( !cdl_renderer_api_init() ) return;
    
    //- Storage Initialisation
    rendererPtr->storage.nextCamera = rendererPtr->storage.cameras;
    rendererPtr->storage.nextShader = rendererPtr->storage.shaders;
    rendererPtr->storage.nextMaterial = rendererPtr->storage.materials;
    rendererPtr->storage.nextTexture = rendererPtr->storage.textures;
    rendererPtr->storage.nextFont = rendererPtr->storage.fonts;
    rendererPtr->storage.nextScene = rendererPtr->storage.scenes;
    rendererPtr->storage.nextView = rendererPtr->storage.views;
    
    //- Shaders
    
    // World
    cdl_assets_shader* worldShaderAsset = cdl_assets_shader_get(ShaderType_World, ASSET_SHADER(world));
    rendererPtr->defaultWorldShader = cdl_renderer_shader_new(RendererShaderType_World, worldShaderAsset);
    rendererPtr->defaultWorldShader->lit = 1;
    cdl_renderer_shader_load(rendererPtr->defaultWorldShader);
    
    // UI
    cdl_assets_shader* uiShaderAsset = cdl_assets_shader_get(ShaderType_UI, ASSET_SHADER(ui));
    rendererPtr->defaultUIShader = cdl_renderer_shader_new(RendererShaderType_UI, uiShaderAsset);
    cdl_renderer_shader_load(rendererPtr->defaultUIShader);
    
    // Full Screen Quad Shader (framebuffer)
    cdl_assets_shader* fsQuadShaderAsset = cdl_assets_shader_get(ShaderType_FullScreenQuad, ASSET_SHADER(fsquad));
    rendererPtr->fsQuadShader = cdl_renderer_shader_new(RendererShaderType_FullScreenQuad, fsQuadShaderAsset);
    cdl_renderer_shader_load(rendererPtr->fsQuadShader);
    
    
    //- Materials 
    rendererPtr->defaultMaterial = cdl_renderer_material_new(rendererPtr->defaultWorldShader);
    
    
    //- Fonts
    rendererPtr->arial = cdl_renderer_font_new(cdl_assets_font_get(ASSET_FONT_FILE(arial)));
    rendererPtr->segoe_ui = cdl_renderer_font_new(cdl_assets_font_get(ASSET_FONT_FILE(segoe_ui)));
    rendererPtr->currentFont = rendererPtr->segoe_ui;
    
    
    //- Render Scenes
    rendererPtr->world = cdl_render_scene_new(RenderSceneType_3D);
    rendererPtr->worldView = cdl_renderer_scene_view_new(0, rendererPtr->world, 0, rendererPtr->defaultWorldShader, 4);
    
    rendererPtr->ui = cdl_render_scene_new(RenderSceneType_2D);
    rendererPtr->uiView = cdl_renderer_scene_view_new(1, rendererPtr->ui, 0, rendererPtr->defaultUIShader, 4);
    
#ifdef CDL_DEBUG
    MemoryCopy(rendererPtr->worldView->debugName, "World View", StringLength("World View"));
    MemoryCopy(rendererPtr->uiView->debugName, "UI View", StringLength("UI View"));
#endif
    
    //- Models
    
    v3f defaultCubePositions[] = {
        v3f_new(-0.5f, -0.5f,  0.5f),
        v3f_new(-0.5f,  0.5f,  0.5f),
        v3f_new(-0.5f, -0.5f, -0.5f),
        v3f_new(-0.5f,  0.5f, -0.5f),
        
        v3f_new( 0.5f, -0.5f,  0.5f),
        v3f_new( 0.5f,  0.5f,  0.5f),
        v3f_new( 0.5f, -0.5f, -0.5f),
        v3f_new( 0.5f,  0.5f, -0.5f)
    };
    
    v2f defaultCubeUVs[] = {
        v2f_new(0.625000, 0.000000),
        v2f_new(0.375000, 0.250000),
        v2f_new(0.375000, 0.000000),
        v2f_new(0.625000, 0.250000),
        v2f_new(0.375000, 0.500000),
        v2f_new(0.625000, 0.500000),
        v2f_new(0.375000, 0.750000),
        v2f_new(0.625000, 0.750000),
        v2f_new(0.375000, 1.000000),
        v2f_new(0.125000, 0.750000),
        v2f_new(0.125000, 0.500000),
        v2f_new(0.875000, 0.500000),
        v2f_new(0.625000, 1.000000),
        v2f_new(0.875000, 0.750000)
    };
    
    v3f defaultCubeNormals[] = {
        v3f_new(-1.0000, 0.0000, 0.0000),
        v3f_new(0.0000, 0.0000, -1.0000),
        v3f_new(1.0000, 0.0000, 0.0000),
        v3f_new(0.0000, 0.0000, 1.0000),
        v3f_new(0.0000, -1.0000, 0.0000),
        v3f_new(0.0000, 1.0000, 0.0000)
    };
    
    u32 defaultCubeVertexDefs[] = {
        1, 0, 0,   2, 1, 0,   0, 2, 0,   
        3, 3, 1,   6, 4, 1,   2, 1, 1,
        7, 5, 2,   4, 6, 2,   6, 4, 2,
        5, 7, 3,   0, 8, 3,   4, 6, 3,
        6, 4, 4,   0, 9, 4,   2, 10, 4,
        3, 11, 5,  5, 7, 5,   7, 5, 5,
        1, 0, 0,   3, 3, 0,   2, 1, 0,
        3, 3, 1,   7, 5, 1,   6, 4, 1,
        7, 5, 2,   5, 7, 2,   4, 6, 2,
        5, 7, 3,   1, 12, 3,  0, 8, 3,
        6, 4, 4,   4, 6, 4,   0, 9, 4,
        3, 11, 5,  1, 13, 5,  5, 7, 5
    };
    
    rendererPtr->world->defaultModel = cdl_assets_model_raw_create_from_vertex_defs("world default model", ARRAY_COUNT(defaultCubeVertexDefs), defaultCubeVertexDefs, defaultCubePositions, defaultCubeUVs, defaultCubeNormals);
    cdl_renderer_create_model_instance_list(rendererPtr->world, rendererPtr->world->defaultModel, 0, 1);
    
    //- FSQ : Full Screen Quad
    u32 VBO;
    
    float fsqVertices[] = {
        // first triangle
        1.f,  1.f, 0.0f,  1.f, 1.f,  // top right
        1.f, -1.f, 0.0f,  1.f, 0.f,  // bottom right
        -1.f,  1.f, 0.0f, 0.f, 1.f,  // top left 
        // second triangle
        1.f, -1.f, 0.0f,  1.f, 0.f,  // bottom right
        -1.f, -1.f, 0.0f, 0.f, 0.f,  // bottom left
        -1.f,  1.f, 0.0f, 0.f, 1.f  // top left
    }; 
    
    check_gl_proc(glGenVertexArrays(1, &rendererPtr->fsq));
    check_gl_proc(glBindVertexArray(rendererPtr->fsq));
    check_gl_proc(glGenBuffers(1, &VBO));
    check_gl_proc(glBindBuffer(GL_ARRAY_BUFFER, VBO));
    check_gl_proc(glBufferData(GL_ARRAY_BUFFER, sizeof(fsqVertices), fsqVertices, GL_STATIC_DRAW));
    check_gl_proc(glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)0));
    check_gl_proc(glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float))));
    check_gl_proc(glEnableVertexAttribArray(0));
    check_gl_proc(glEnableVertexAttribArray(1));
    
    //- GL config
    check_gl_proc(glEnable( GL_BLEND ));
    check_gl_proc(glBlendFuncSeparate(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA, GL_ONE, GL_ONE_MINUS_SRC_ALPHA));
    
    check_gl_proc(glEnable(GL_CULL_FACE));
    check_gl_proc(glCullFace(GL_FRONT));
    
    check_gl_proc(glEnable(GL_MULTISAMPLE));
    check_gl_proc(glEnable(GL_LINE_SMOOTH));
    
    rendererPtr->isValid = 1;
    
    bool8 errors = cdl_renderer_api_flush_errors(__FILE__, __LINE__);
    if ( errors ) {
        rendererPtr->isValid = 0;
    }
}

inline void cdl_renderer_set(cdl_renderer* rendererPtr)
{
    renderer = rendererPtr;
}

internal void cdl_renderer_frame_begin()
{
    PROFILED_FUNC;
    
    check_gl_proc(glEnable( GL_BLEND ));
    check_gl_proc(glBlendFuncSeparate(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA, GL_ONE, GL_ONE_MINUS_SRC_ALPHA));
    
    check_gl_proc(glEnable(GL_CULL_FACE));
    check_gl_proc(glCullFace(GL_FRONT));
    
    check_gl_proc(glEnable(GL_MULTISAMPLE));
    check_gl_proc(glEnable(GL_LINE_SMOOTH));
    
    //- Scene Views
    for (cdl_renderer_scene_view* v = renderer->storage.views; v < renderer->storage.nextView; v++) {
        cdl_render_scene_frame_begin(v->scene);
    }
    
    //- Post Processing
    for (u32 i = 0; i < ARRAY_COUNT(renderer->post.processed); i++) {
        renderer->post.processed[i] = 0;
    }
    
    //- Start Render Queue Work
    cdl_renderer_start_render_queue_work();
}

internal void cdl_renderer_start_render_queue_work()
{
    
}

internal void cdl_renderer_internal_render_scene_instances_and_static(cdl_renderer_scene_view* view)
{
    PROFILED_FUNC;
    
    u32 shaderCount = (u32)(renderer->storage.nextShader - renderer->storage.shaders);
    u32 materialCount = (u32)(renderer->storage.nextMaterial - renderer->storage.materials);
    cdl_render_scene* s = view->scene;
    
    //- Opaque Instances
    for (u32 shaderIndex = 0; shaderIndex < shaderCount; shaderIndex++) {
        cdl_renderer_shader* shader = renderer->storage.shaders + shaderIndex;
        
        // Check if that particular shader is used once
        bool8 skip = 1;
        for (u32 listIndex = 0; listIndex < s->instanceListsCount; listIndex++) {
            cdl_renderer_model_instance_list* list = s->instanceLists + listIndex;
            if ( list->material && list->material->shader == shader && list->count ) {
                skip = 0;
                break;
            }
        }
        
        if ( skip ) { continue; }
        
        cdl_renderer_shader_use(shader, view);
        
        // Loop over materials that uses the current shader
        for (u32 materialIndex = 0; materialIndex < materialCount; materialIndex++) {
            cdl_renderer_material* material = renderer->storage.materials + materialIndex;
            
            if ( material->shader != shader ) { continue; }
            if ( material->hasTransparency ) { continue; }
            cdl_renderer_material_use(s, material);
            
            // Render the instances that uses the current material
            for (u32 listIndex = 0; listIndex < s->instanceListsCount; listIndex++) {
                cdl_renderer_model_instance_list* list = s->instanceLists + listIndex;
                cdl_renderer_material* listMaterial = list->material == 0 ? renderer->defaultMaterial : list->material;
                
                if ( listMaterial != material ) { continue; }
                if ( list->count == 0 ) { continue; }
                
                if ( !list->modelLoaded ) {
                    if ( list->asset && list->asset->head.state == AssetState_Loaded ) {
                        cdl_renderer_create_model_instance_list(s, list->asset, listMaterial, list->clearEveryFrame);
                    }
                }
                
                if ( list->modelLoaded ) {
                    check_gl_proc(glBindVertexArray(list->vao));
                    
                    if ( list->dirty ) {
                        check_gl_proc(glBindBuffer(GL_ARRAY_BUFFER, list->dataVBO));
                        {
                            PROFILED_BLOCK("glBufferSubData::instances");
                            check_gl_proc(glBufferSubData(GL_ARRAY_BUFFER, 0, list->count * sizeof(cdl_renderer_model_instance_data), list->instanceData));
                        }
                        list->dirty = 0;
                    }
                    
                    PROFILED_BLOCK("glDrawElementsInstanced::instances");
                    check_gl_proc(glDrawElementsInstanced(GL_TRIANGLES, list->asset->indexCount, GL_UNSIGNED_INT, 0, list->count));
                }
            }
        }
    }
    
    
    //- Transparent Instances
    for (u32 shaderIndex = 0; shaderIndex < shaderCount; shaderIndex++) {
        cdl_renderer_shader* shader = renderer->storage.shaders + shaderIndex;
        
        // Check if that particular shader is used once
        bool8 skip = 1;
        for (u32 listIndex = 0; listIndex < s->instanceListsCount; listIndex++) {
            cdl_renderer_model_instance_list* list = s->instanceLists + listIndex;
            if ( list->material && list->material->hasTransparency && list->material->shader == shader && list->count ) {
                skip = 0;
                break;
            }
        }
        
        if ( skip ) { continue; }
        
        cdl_renderer_shader_use(shader, view);
        
        // Loop over materials that uses the current shader
        for (u32 materialIndex = 0; materialIndex < materialCount; materialIndex++) {
            cdl_renderer_material* material = renderer->storage.materials + materialIndex;
            
            if ( material->shader != shader ) { continue; }
            if ( !material->hasTransparency ) { continue; }
            cdl_renderer_material_use(s, material);
            
            // Render the instances that uses the current material
            for (u32 listIndex = 0; listIndex < s->instanceListsCount; listIndex++) {
                cdl_renderer_model_instance_list* list = s->instanceLists + listIndex;
                cdl_renderer_material* listMaterial = list->material == 0 ? renderer->defaultMaterial : list->material;
                
                if ( listMaterial != material ) { continue; }
                if ( list->count == 0 ) { continue; }
                
                if ( !list->modelLoaded ) {
                    if ( list->asset && list->asset->head.state == AssetState_Loaded ) {
                        cdl_renderer_create_model_instance_list(s, list->asset, listMaterial, list->clearEveryFrame);
                    }
                }
                
                if ( list->modelLoaded ) {
                    check_gl_proc(glBindVertexArray(list->vao));
                    
                    if ( list->dirty ) {
                        check_gl_proc(glBindBuffer(GL_ARRAY_BUFFER, list->dataVBO));
                        {
                            PROFILED_BLOCK("glBufferSubData::instances");
                            check_gl_proc(glBufferSubData(GL_ARRAY_BUFFER, 0, list->count * sizeof(cdl_renderer_model_instance_data), list->instanceData));
                        }
                        list->dirty = 0;
                    }
                    
                    PROFILED_BLOCK("glDrawElementsInstanced::instances");
                    check_gl_proc(glDrawElementsInstanced(GL_TRIANGLES, list->asset->indexCount, GL_UNSIGNED_INT, 0, list->count));
                }
            }
        }
    }
    
    
    //- Static Triangles
    {
        cdl_renderer_shader_use(renderer->defaultWorldShader, view);
        cdl_renderer_material_use(s, renderer->defaultMaterial);
        
        if ( s->staticTriangles.vertexCount > 0 ) {
            u32 vertexDataSize = (u32)((char*)s->staticTriangles.nextVertex - (char*)s->staticTriangles.vertexBuffer);
            u32 indexDataSize = (u32)((char*)s->staticTriangles.nextIndex - (char*)s->staticTriangles.indexBuffer);
            
            check_gl_proc(glBindVertexArray(s->staticTriangles.VAO));
            
            if ( s->staticTriangles.dirty ) {
                check_gl_proc(glBindBuffer(GL_ARRAY_BUFFER, s->staticTriangles.VBO));
                {
                    PROFILED_BLOCK("glBufferSubData::static_vertices");
                    check_gl_proc(glBufferSubData(GL_ARRAY_BUFFER, 0, vertexDataSize, s->staticTriangles.vertexBuffer));
                }
                
                check_gl_proc(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, s->staticTriangles.EBO));
                {
                    PROFILED_BLOCK("glBufferSubData::static_indices");
                    check_gl_proc(glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, 0, indexDataSize, s->staticTriangles.indexBuffer));
                }
                
                s->staticTriangles.dirty = 0;
            }
            
            {
                PROFILED_BLOCK("glDrawElements::static_triangles");
                check_gl_proc(glDrawElements(GL_TRIANGLES, indexDataSize / sizeof(u32), GL_UNSIGNED_INT, 0));
            }
        }
    }
}

internal void cdl_renderer_internal_render_scene(cdl_render_scene* s)
{
    PROFILED_FUNC;
    
    check_gl_proc(glBindVertexArray(s->triangles.VAO));
    check_gl_proc(glBindBuffer(GL_ARRAY_BUFFER, s->triangles.VBO));
    {
        PROFILED_BLOCK("glBufferSubData::triangles");
        check_gl_proc(glBufferSubData(GL_ARRAY_BUFFER, 0, s->triangles.vertexCount * sizeof(cdl_renderer_batch_vertex), s->triangles.vertexBuffer));
    }
    
    check_gl_proc(glBindVertexArray(s->lines.VAO));
    check_gl_proc(glBindBuffer(GL_ARRAY_BUFFER, s->lines.VBO));
    {
        PROFILED_BLOCK("glBufferSubData::lines");
        check_gl_proc(glBufferSubData(GL_ARRAY_BUFFER, 0, s->lines.vertexCount * sizeof(cdl_renderer_batch_vertex), s->lines.vertexBuffer));
    }
    
    for (u32 renderStateIndex = 0; renderStateIndex < s->statesCount; renderStateIndex++) {
        
        cdl_render_state* state = s->states + renderStateIndex;
        
        u32 triangleVertexOffset = state->vertexStart - s->triangles.vertexBuffer;
        u32 triangleVertexDataSize = state->vertexCount * sizeof(cdl_renderer_batch_vertex);
        
        u32 lineVertexOffset = state->lineVertexStart - s->lines.vertexBuffer;
        u32 lineVertexDataSize = state->lineVertexCount * sizeof(cdl_renderer_batch_vertex);
        
        if ( !triangleVertexDataSize && !lineVertexDataSize ) { continue; }
        
        cdl_renderer_state_process(state);
        
        //- Rendering Triangles
        if ( triangleVertexDataSize > 0 ) {
            PROFILED_BLOCK("cdl_renderer_internal_render_scene::triangles");
            u32 indexOffset = state->indexStart - s->triangles.indexBuffer;
            u32 indexDataSize = state->indexCount * sizeof(u32);
            
            check_gl_proc(glBindVertexArray(s->triangles.VAO));
            check_gl_proc(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, s->triangles.EBO));
            check_gl_proc(glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, 0, indexDataSize, state->indexStart));
            
            {
                PROFILED_BLOCK("glDrawRangeElements::triangles");
                check_gl_proc(glDrawRangeElements(GL_TRIANGLES, indexOffset, indexOffset + state->indexCount, state->indexCount, GL_UNSIGNED_INT, 0));
            }
        }
        
        //- Rendering Lines
        if ( lineVertexDataSize > 0 ) {
            PROFILED_BLOCK("cdl_renderer_internal_render_scene::lines");
            u32 indexOffset = state->lineIndexStart - s->lines.indexBuffer;
            u32 indexDataSize = state->lineIndexCount * sizeof(u32);
            
            check_gl_proc(glBindVertexArray(s->lines.VAO));
            check_gl_proc(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, s->lines.EBO));
            check_gl_proc(glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, 0, indexDataSize, state->lineIndexStart));
            
            {
                PROFILED_BLOCK("glDrawRangeElements::lines");
                check_gl_proc(glDrawRangeElements(GL_LINES, indexOffset, indexOffset + state->lineIndexCount, state->lineIndexCount, GL_UNSIGNED_INT, 0));
            }
        }
    }
}

internal void cdl_renderer_frame_end()
{
    PROFILED_FUNC;
    
    //- Resize framebuffers
    {
        local bool8 alreadyResizedThisFrame = 0;
        if ( alreadyResizedThisFrame ) {
            alreadyResizedThisFrame = 0;
        } else {
            
            if ( cdl_engine_platform_event_happened(PlatformEventType_application_resized) ) {
                PROFILED_BLOCK("cdl_renderer_frame_end::resize_framebuffers");
                
                alreadyResizedThisFrame = 1;
                
                for (cdl_renderer_camera* c = renderer->storage.cameras; c < renderer->storage.nextCamera; c++) {
                    cdl_renderer_camera_update_projection(c);
                }
                
                for (cdl_renderer_scene_view* v = renderer->storage.views; v < renderer->storage.nextView; v++) {
                    cdl_renderer_framebuffer* fb = v->framebuffer;
                    cdl_renderer_framebuffer_clear(fb);
                    fb = cdl_renderer_framebuffer_new(fb->samples);
                }
                
                for (u32 i = 0; i < renderer->post.effectCount; i++) {
                    cdl_renderer_effect* e = renderer->post.effects + i;
                    cdl_renderer_framebuffer_clear(e->framebuffer);
                    e->framebuffer = cdl_renderer_framebuffer_new(1);
                }
            }
            
        }
    }
    
    
    //- Geometry
    cdl_renderer_scene_view* firstView = renderer->storage.views;
    cdl_renderer_scene_view* lastView = renderer->storage.nextView;
    
    {
        PROFILED_BLOCK("cdl_renderer_frame_end::geometry");
        
        check_gl_proc(glEnable(GL_DEPTH_TEST));
        
        for (cdl_renderer_scene_view* v = firstView; v < lastView; v++) {
            if ( v->scene->type == RenderSceneType_3D ) {
                cdl_renderer_scene_view_render(v);
            }
        }
        
        check_gl_proc(glDisable(GL_DEPTH_TEST));
    }
    
    //- UI Rendering
    {
        PROFILED_BLOCK("cdl_renderer_frame_end::uis");
        
        for (cdl_renderer_scene_view* v = firstView; v < lastView; v++) {
            if ( v->scene->type == RenderSceneType_2D ) {
                cdl_renderer_scene_view_render(v);
            }
        }
    }
    
    cdl_renderer_state_reset();
    
    //- Draw Framebuffers Textures to the screen
    {
        PROFILED_BLOCK("cdl_renderer_frame_end::fbos");
        
        if ( renderer->world->dirty ) {
            
            bool8 hasFinal = ( renderer->finalWorldEffect != 0 );
            if ( hasFinal ) {
                cdl_renderer_framebuffer* framebuffer = renderer->worldView->framebuffer;
                check_gl_proc(glBindFramebuffer(GL_READ_FRAMEBUFFER, framebuffer->multisampled->id));
                check_gl_proc(glBindFramebuffer(GL_DRAW_FRAMEBUFFER, framebuffer->transient->id));
                
                {
                    PROFILED_BLOCK("glBlitFramebuffer");
                    check_gl_proc(glBlitFramebuffer(0, 0, framebuffer->multisampled->dim.width, framebuffer->multisampled->dim.height,
                                                    0, 0, framebuffer->transient->dim.width, framebuffer->transient->dim.height,
                                                    GL_COLOR_BUFFER_BIT, GL_NEAREST));
                }
                
                cdl_renderer_effect_process(renderer->worldView, renderer->finalWorldEffect, 1);
            }
            
            cdl_renderer_framebuffer* toRender = hasFinal ?
                renderer->finalWorldEffect->framebuffer : renderer->worldView->framebuffer;
            
            cdl_renderer_shader_use(renderer->fsQuadShader, 0);
            cdl_renderer_shader_uniform_set_float(renderer->fsQuadShader, "u_GammaCorrect", 1);
            
            cdl_renderer_framebuffer_render(toRender);
        }
        
        cdl_renderer_shader_use(renderer->fsQuadShader, 0);
        cdl_renderer_shader_uniform_set_float(renderer->fsQuadShader, "u_GammaCorrect", 1);
        cdl_renderer_framebuffer_render(renderer->uiView->framebuffer);
        
        cdl_renderer_shader_uniform_set_float(renderer->fsQuadShader, "u_GammaCorrect", 0);
    }
    
    //- Flush OpenGL Errors
    cdl_renderer_api_flush_errors(__FILE__, __LINE__);
}