internal void print_meta_struct(cdl_meta_members members[], u32 memberCount, void* metaStruct, u32 indent = 0) 
{
    char tabs[16] = { };
    for (u32 i = 0; i < indent; i+=2) {
        tabs[i] = ' ';
        tabs[i + 1] = ' ';
    }
    tabs[indent] = 0;
    
    log_msg("%s%s:\n", tabs, members[0].structName);
    
    for (u32 i = 0; i < memberCount; i++) {
        
        u8* s = ((u8*)(metaStruct) + members[i].offset);
        
        switch (members[i].type) {
            
            case MetaType_u32:
            {
                log_msg("%s%s: %u\n", tabs, members[i].name, *(u32*)s);
            } break;
            
            case MetaType_i32:
            {
                log_msg("%s%s: %d\n", tabs, members[i].name, *(i32*)s);
            } break;
            
            case MetaType_f32:
            {
                log_msg("%s%s: %d\n", tabs, members[i].name, *(f32*)s);
            } break;
            
            META_HANDLE_TYPE_DUMP(&members[i])

            default:
            {

            } break;
        }
    }
}
