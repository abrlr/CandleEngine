//- [SECTION] IDs
internal inline u32 im_basic_string_hash(const char* string)
{
    PROFILED_FUNC;
    
    u32 hash = 0;
    
    for (u32 i = 0; string[i] != 0; i++) {
        hash += string[i] * ( i + 1 );
    }
    
    return hash;
}

internal inline im_id im_id_invalid()
{
    return { 0 };
}

internal inline im_id im_id_new(const char* name)
{
    im_id result = { im_basic_string_hash(name) };
    result.primary += g_imctx->groupID.primary;
    
    im_window* current = im_current_window();
    if ( current ) {
        result.secondary = current->id.primary;
    }
    
    im_current_window()->lastWidgetID = result;
    return result;
}

internal inline im_id im_id_window(const char* name)
{
    im_id result = { im_basic_string_hash(name) };
    result.primary += g_imctx->groupID.primary;
    
    return result;
}

internal inline bool8 im_id_equal(im_id a, im_id b)
{
    return ( (a.primary == b.primary) && (a.secondary == b.secondary) );
}

internal inline bool8 im_id_is_invalid(im_id id)
{
    return ( id.primary == 0 );
}

internal inline void im_id_group_begin(const char* groupName)
{
    im_id groupID = im_id_new(groupName);
    g_imctx->groupID = groupID;
}

internal inline void im_id_group_end()
{
    g_imctx->groupID = im_id_invalid();
}

//- [SECTION] Draw Commands
inline internal void im_cdl_render_command(im_draw_command* command)
{
    switch (command->type) {
        case IMDrawCommand_Line: ui_line(command->p1, command->p2, command->lineWidth, command->color); break;
        
        case IMDrawCommand_Triangle: 
        {
            color8 colors[3] = { command->color, command->color, command->color };
            ui_triangles(1, command->tri_positions, colors);
        } break;
        
        case IMDrawCommand_Rect: ui_rect_outline(command->rect, command->color, command->lineWidth); break;
        case IMDrawCommand_RectFilled: ui_rect(command->rect, command->color); break;
        
        case IMDrawCommand_RectTextured: 
        {
            ui_quad_textured(rect2_mid(command->rect), rect2_dim(command->rect), command->uvs, command->textureID);
        } break;
        
        case IMDrawCommand_Circle: ui_circle(command->center, command->radius, command->lineWidth, command->color); break;
        case IMDrawCommand_CircleFilled: ui_circle(command->center, command->radius, 0, command->color); break;
        case IMDrawCommand_Text: ui_text(command->text, command->position, renderer->currentFont, command->textAttr); break;
        case IMDrawCommand_PushClipRect: ui_push_clip_rect(command->rect); break;
        case IMDrawCommand_PopClipRect: ui_pop_clip_rect(); break;
        
        default:
        {
            
        } break;
    }
}

internal void im_cdl_render_window(im_window* w)
{
    PROFILED_FUNC;
    
    //- Immediate commands
    im_draw_command* bufferStart = w->immediateBuffer;
    im_draw_command* bufferEnd = w->currentImmediateCommand;
    for (im_draw_command* dc = bufferStart; dc < bufferEnd; dc++) {
        im_cdl_render_command(dc);
    }
    
    //- Defered commands
    bufferStart = w->deferBuffer;
    bufferEnd = w->currentDeferCommand;
    for (im_draw_command* dc = bufferStart; dc < bufferEnd; dc++) {
        im_cdl_render_command(dc);
    }
}

internal inline im_draw_command* im_internal_get_next_draw_command()
{
    PROFILED_FUNC;
    
    im_window* w = im_current_window();
    return w->defer ? w->currentDeferCommand++ : w->currentImmediateCommand++;
}

internal void im_draw_line(v2f start, v2f end, u32 width, color8 color)
{
    PROFILED_FUNC;
    
    im_draw_command* dc = im_internal_get_next_draw_command();
    
    dc->type = IMDrawCommand_Line;
    dc->lineWidth = width;
    dc->color = color;
    
    dc->p1 = start;
    dc->p2 = end;
}

internal void im_draw_triangle(v2f positions[3], color8 color)
{
    PROFILED_FUNC;
    
    im_draw_command* dc = im_internal_get_next_draw_command();
    
    dc->type = IMDrawCommand_Triangle;
    MemoryCopy(dc->tri_positions, positions, sizeof(v2f) * 3);
    dc->color = color;
}

internal void im_draw_rect(rect2 rect, u32 width, color8 color)
{
    PROFILED_FUNC;
    
    im_draw_command* dc = im_internal_get_next_draw_command();
    
    dc->type = IMDrawCommand_Rect;
    dc->lineWidth = width;
    dc->color = color;
    
    dc->rect = rect;
}

internal void im_draw_rect_filled(rect2 rect, color8 color)
{
    PROFILED_FUNC;
    
    im_draw_command* dc = im_internal_get_next_draw_command();
    
    dc->type = IMDrawCommand_RectFilled;
    dc->color = color;
    
    dc->rect = rect;
}

internal void im_draw_rect_textured(rect2 rect, v4f uvs, u32 texture)
{
    PROFILED_FUNC;
    
    im_draw_command* dc = im_internal_get_next_draw_command();
    
    dc->type = IMDrawCommand_RectTextured;
    
    dc->rect = rect;
    dc->uvs = uvs;
    dc->textureID = texture;
}

internal void im_draw_circle(v2f center, f32 radius, u32 width, color8 color)
{
    PROFILED_FUNC;
    
    im_draw_command* dc = im_internal_get_next_draw_command();
    
    dc->type = IMDrawCommand_Circle;
    dc->lineWidth = width;
    dc->color = color;
    
    dc->radius = radius;
    dc->center = center;
}

internal void im_draw_circle_filled(v2f center, f32 radius, color8 color)
{
    PROFILED_FUNC;
    
    im_draw_command* dc = im_internal_get_next_draw_command();
    
    dc->type = IMDrawCommand_CircleFilled;
    dc->color = color;
    
    dc->radius = radius;
    dc->center = center;
}

internal void im_draw_text(const char* text, v2f pos, cdl_text_atb attr)
{
    PROFILED_FUNC;
    
    im_draw_command* dc = im_internal_get_next_draw_command();
    
    dc->type = IMDrawCommand_Text;
    
    MemoryCopy(dc->text, text, ARRAY_COUNT(dc->text));
    dc->position = pos;
    dc->textAttr = attr;
}

internal void im_push_clip_rect(rect2 rect)
{
    PROFILED_FUNC;
    
    im_draw_command* dc = im_internal_get_next_draw_command();
    
    dc->type = IMDrawCommand_PushClipRect;
    dc->rect = rect;
}

internal void im_pop_clip_rect()
{
    PROFILED_FUNC;
    
    im_draw_command* dc = im_internal_get_next_draw_command();
    
    dc->type = IMDrawCommand_PopClipRect;
}

//- [SECTION] Color Theme
global im_rect_theme g_im_theme_window = {
    cdl_rgb(1, 1, 2), // background
    cdl_rgb(2, 6, 12), // border
    color_pink, // selected
    color_pink, // hovered
    color_pink, // active
    color_pink, // special
};

global im_rect_theme g_im_theme_window_header = {
    cdl_rgb(2, 6, 12), // background
    cdl_rgb(5, 15, 20), // border
    color_pink, // selected
    cdl_rgb(2, 5, 8), // hovered
	cdl_rgb(5, 15, 20), // active
    color_pink, // special
};

global im_rect_theme g_im_theme_widget = {
    cdl_rgba(2, 3, 5, 150), // background
    cdl_rgb(10, 20, 25), // border
    color_pink, // selected
    cdl_rgb(2, 6, 12), // hovered
    cdl_rgb(5, 15, 20), // active
    cdl_rgb(100, 100, 125), // icon, handle
};

global im_rect_theme g_im_theme_widget_danger = {
    cdl_rgb(255, 30, 30), // background
    cdl_rgba(255, 0, 0, 175), // border
    color_pink, // selected
    cdl_rgb(220, 12, 12), // hovered
    cdl_rgb(250, 50, 50), // active
    color_pink, // icon, handle
};

global im_rect_theme g_im_theme_checkboxes = {
    color_white, // background
    color_white, // border
    color_pink, // selected
    cdl_rgba(255, 255, 255, 10), // hovered
    color_pink, // active
    color_pink, // toggled
};

global im_rect_theme g_im_theme_radio = {
    color_none, // background
    color_white, // border
    color_white, // selected
    cdl_rgba(255, 255, 255, 50), // hovered
    color_pink, // active
    color_pink, // icon, handle
};

global im_color_theme im_colors = {
    // Fonts
    color_white, // font_header
    color_white, // font_default
    cdl_rgb(175, 175, 175), // font_hint
    
    g_im_theme_window, // Windows
    g_im_theme_window_header, // Headers
    
    g_im_theme_widget, // Generic Widgets
    g_im_theme_widget_danger, // Danger Widgets
    
    g_im_theme_checkboxes, // Checkboxes
    g_im_theme_radio, // Radios
    
    // Charts
    {
        cdl_hex(0x003f5c),
        cdl_hex(0x2f4b7c),
        cdl_hex(0x665191),
        cdl_hex(0xa05195),
        cdl_hex(0xd45087),
        cdl_hex(0xf95d6a),
        cdl_hex(0xff7c43),
        cdl_hex(0xffa600),
    }, // chart_colors[8]
};

//- [SECTION] Layouts
internal void im_layout_begin(IMLayout type)
{
    PROFILED_FUNC;
    
    im_layout* next = im_current_layout() + 1;
    *next = ZeroStruct;
    next->type = type;
    next->cursor = im_current_layout()->cursor;
    next->rect.min = im_current_layout()->cursor;
    next->rect.max = im_current_layout()->cursor;
    next->margins = im_current_layout()->margins;
    next->widthRule = IMDimensionRule_Fixed;
    next->width = DEF_WIDTH;
    next->minWidth = MIN_WIDTH;
    g_imctx->currentLayout++;
}

internal void im_layout_end()
{
    PROFILED_FUNC;
    
    im_layout* current = g_imctx->currentLayout--;
    
    switch (current->type) {
        case IMLayout_Vertical:
        case IMLayout_Horizontal:
        {
            im_layout_add_rect(current->rect);
        } break;
        
        // NOTE(abe): IMLayout_Detached is not added to the previous layout rect
        default:
        {
        } break;
    }
}

internal rect2 im_layout_new_rect()
{
    PROFILED_FUNC;
    
    im_layout* l = im_current_layout();
    im_window* w = im_current_window();
    
    v2f cursor = v2f_add(l->cursor, l->margins);
    
    v2f dim = v2f_new(0);
    dim.height = DEF_HEIGHT;
    
    switch (l->widthRule) {
        case IMDimensionRule_Fixed:
        {
            dim.width = l->width;
        } break;
        
        case IMDimensionRule_Relative:
        {
            dim.width = l->width * rect2_dim(w->rect).width - l->margins.width;
        } break;
    }
    
    dim.width = f32_clamp(dim.width, l->minWidth, F32Inf);
    
    v2f pos = v2f_add(cursor, v2f_times(dim, 0.5));
    rect2 result = rect2_from_size(pos, dim);
    
    return result;
}

internal rect2 im_layout_new_chart_rect(im_chart_atb atb)
{
    PROFILED_FUNC;
    
    rect2 rect = im_layout_new_rect();
    rect.max.x = f32_max(rect.min.x + rect2_dim(rect).width, rect.min.x + atb.dim.width);
    rect.max.y = f32_max(rect.min.y + 20, rect.min.y + atb.dim.height);
    return rect;
}

internal bool8 im_layout_add_rect(rect2 rect)
{
    PROFILED_FUNC;
    
    im_layout* l = im_current_layout();
    im_window* w = im_current_window();
    
    // Discard elements if they are not visible in the window rectangle
    bool8 isOverflowingX = l->cursor.x > w->rect.max.x;
    bool8 isOverflowingY = l->cursor.y > w->rect.max.y || l->cursor.y < w->rect.min.y;
    bool8 discard = isOverflowingX || isOverflowingY;
    w->allowYScroll |= isOverflowingY;
    
    v2f dim = rect2_dim(rect);
    
    switch (l->type) {
        case IMLayout_Vertical:
        case IMLayout_Detached:
        {
            l->cursor.y += dim.height + l->margins.y;
            l->rect.max.y += dim.height + l->margins.y;
            
            if ( l->rect.max.x < l->rect.min.x + dim.width ) {
                l->rect.max.x = l->rect.min.x + dim.width;
            }
        } break;
        
        case IMLayout_Horizontal:
        {
            l->cursor.x += dim.width + l->margins.x;
            l->rect.max.x += dim.width + l->margins.x;
            
            if ( l->rect.max.y < l->rect.min.y + dim.height ) {
                l->rect.max.y = l->rect.min.y + dim.height;
            }
        } break;
        
        default:
        {
            m_assert(0, "Unknown Layout Type");
        } break;
    }
    
    if ( discard ) return 0;
    
    w->lastRect = rect;
    return 1;
}

internal void im_layout_set_widget_width(IMDimensionRule rule, f32 width)
{
    PROFILED_FUNC;
    
    im_current_layout()->widthRule = rule;
    im_current_layout()->width = width;
}

internal void im_layout_indent(f32 indent = DEF_TREE_INDENT)
{
    im_layout* l = im_current_layout();
    switch (l->type) {
        case IMLayout_Vertical:
        {
            l->cursor.x += indent;
        } break;
        
        case IMLayout_Horizontal:
        {
            l->cursor.y += indent;
        } break;
    }
}

internal void im_layout_unindent(f32 indent = DEF_TREE_INDENT)
{
    im_layout_indent(-indent);
}

//- [SECTION] Widget Data
internal im_item_data* im_internal_get_item_data(im_id id)
{
    im_item_data* result = 0;
    im_window* w = im_current_window();
    
    for (u32 i = 0; i < ARRAY_COUNT(w->itemData); i++) {
        if ( im_id_is_invalid(w->itemData[i].id) || im_id_equal(w->itemData[i].id, id)) {
            result = w->itemData + i;
            result->id = id;
            break;
        }
    }
    
    return result;
}

//- [SECTION] Windows
internal void im_internal_window_init(const char* name, im_window* w)
{
    PROFILED_FUNC;
    FLAG_SET(w->flags, IMWindowFlag_Initialized);
    FLAG_SET(w->flags, IMWindowFlag_Visible);
    
    // Window Rect
    v2f wCursor = v2f_new(10);
    v2f wDim = v2f_new(200, 200);
    v2f wPos = v2f_add(wCursor, v2f_times(wDim, 0.5));
    rect2 wRect = rect2_from_size(wPos, wDim);
    w->rect = wRect;
    
    // Window internal widgets
    char buffer[64];
    sprintf(buffer, "@__%s_header__@", name);
    w->headerID = im_id_new(buffer);
    
    MemoryZero(buffer, ARRAY_COUNT(buffer));
    sprintf(buffer, "@__%s_reduceButton__@", name);
    w->minimizeButtonID = im_id_new(buffer);
    
    
    //- Size Handles
    MemoryZero(buffer, ARRAY_COUNT(buffer));
    sprintf(buffer, "@__%s_sizeHandle__@", name);
    w->sizeHandleID = im_id_new(buffer);
    
    MemoryZero(buffer, ARRAY_COUNT(buffer));
    sprintf(buffer, "@__%s_sizeXHandle__@", name);
    w->sizeXHandleID = im_id_new(buffer);
    
    MemoryZero(buffer, ARRAY_COUNT(buffer));
    sprintf(buffer, "@__%s_sizeYHandle__@", name);
    w->sizeYHandleID = im_id_new(buffer);
    
    //- Position Handles
    MemoryZero(buffer, ARRAY_COUNT(buffer));
    sprintf(buffer, "@__%s_posYHande__@", name);
    w->yPosHandleID = im_id_new(buffer);
    
    //- Transition Factors
    for (u32 i = 0; i < ARRAY_COUNT(w->itemData); i++) {
        w->itemData[i].transitionFactor = 1;
        w->itemData[i].id = im_id_invalid();
    }
}

internal bool8 im_window_begin(const char* name)
{
    PROFILED_FUNC;
    
    im_id id = im_id_window(name);
    im_window* w = im_internal_context_find_window(id);
    g_imctx->currentWindow = w;
    w->currentImmediateCommand = w->immediateBuffer;
    w->currentDeferCommand = w->deferBuffer;
    
    if ( !FLAG_HAS(w->flags, IMWindowFlag_Initialized) ) {
        im_internal_window_init(name, w);
        im_internal_context_set_window_focused(w);
    }
    
    FLAG_SET(w->flags, IMWindowFlag_VisibleThisFrame);
    
    // Window Rect
    if ( w->dockspace ) {
        
        // We first check that the sibling window is visible
        im_dockspace* siblingDock = im_internal_dockspace_get_sibling(w->dockspace);
        if ( siblingDock ) {
            im_window* sibling = siblingDock->child;
            
            // If not visible, then remove it from the dockspaces
            // otherwise an empty space will be created for seamingly no reason
            if ( sibling && !FLAG_HAS(sibling->flags, IMWindowFlag_Visible) ) {
                im_internal_dockspace_remove_window(sibling);
                im_internal_window_resize_around_header(sibling);
            }
        }
        
        w->rect = im_internal_dockspace_get_screen_rect(w->dockspace);
    }
    
    // Header Rect
    rect2 hRect = w->rect;
    hRect.max.y = hRect.min.y + 20;
    
    // Reduce button
    rect2 minimizeRect = ZeroStruct;
    f32 minimizeMargin = 2;
    f32 minimizeRectSize = rect2_dim(hRect).height - 2 * minimizeMargin;
    minimizeRect.max = v2f_sub(hRect.max, v2f_new(minimizeMargin));
    minimizeRect.min = v2f_sub(minimizeRect.max, v2f_new(minimizeRectSize));
    
    //- Header Interactions
    if ( im_context_is_current_window_hovered() && collisions_v2f_in_rect(g_imctx->mpos, hRect) ) {
        // Minimize button
        if ( collisions_v2f_in_rect(g_imctx->mpos, minimizeRect) ) {
            g_imctx->nextHot = w->minimizeButtonID;
            
            if ( im_id_equal(g_imctx->hot, w->minimizeButtonID) && g_imctx->mbLeftOnDown ) {
                g_imctx->active = w->minimizeButtonID;
            }
        }
        
        else {
            g_imctx->nextHot = w->headerID;
            
            if ( im_id_equal(g_imctx->hot, w->headerID) && g_imctx->mbLeftOnDown ) {
                g_imctx->active = w->headerID;
                g_imctx->mouseStartActivePos = g_imctx->mpos;
            }
        }
    }
    
    // Header is active + right click ( effectively = left maintained + right click )
    if ( im_id_equal(g_imctx->active, w->headerID) && g_imctx->mbRightOnDown ) {
        
        // If window has no parent dockspace
        if ( !w->dockspace ) {
            // Add window to hovered
            im_internal_dockspace_insert_window_in_hovered(w);
            g_imctx->active = im_id_invalid();
        }
    }
    
    //- Process Header Interactions
    // Move
    if ( im_id_equal(g_imctx->active, w->headerID) ) {
        v2f deltaSinceActiveStarted = v2f_sub(g_imctx->mouseStartActivePos, g_imctx->mpos);
        f32 popWindowDistance = 150;
        
        // If window is "free"
        if ( !w->dockspace ) {
            
            // Process movement
            w->rect = rect2_translate(w->rect, g_imctx->mDelta);
            hRect = rect2_translate(hRect, g_imctx->mDelta);
            minimizeRect = rect2_translate(minimizeRect, g_imctx->mDelta);
        } 
        
        // If window in dockspace and cursor is moved away from the start_interaction_pos
        else if ( v2f_length(deltaSinceActiveStarted) > popWindowDistance ) {
            
            // Remove window from its dockspace
            im_internal_dockspace_remove_window(w);
            
            // Resize the window and set its position under the mouse
            im_internal_window_resize_around(w, g_imctx->mpos);
        }
    }
    
    // Minimize
    else if ( g_imctx->mbLeftOnUp && im_id_equal(g_imctx->nextHot, w->minimizeButtonID) && im_id_equal(g_imctx->active, w->minimizeButtonID) ) {
        FLAG_TOGGLE(w->flags, IMWindowFlag_Minimized);
    }
    
    //- Clamp window to the viewport
    im_internal_window_clamp_rect(w);
    
    w->headerRect = hRect;
    
    //- Window
    if ( !FLAG_HAS(w->flags, IMWindowFlag_Minimized) ) {
        // Drawing Window
        im_draw_rect_filled(w->rect, g_imctx->theme.window.background);
    }
    
    //- Drawing Header
    {
        // Header Rect
        if ( im_id_equal(g_imctx->hot, w->headerID) ) {
            if ( im_id_equal(g_imctx->active, w->headerID) ) {
                im_draw_rect_filled(hRect, g_imctx->theme.window_header.active);
            } else {
                im_draw_rect_filled(hRect, g_imctx->theme.window_header.hovered);
            }
        } else {
            im_draw_rect_filled(hRect, g_imctx->theme.window_header.background);
        }
        
        // Window Title
        cdl_text_atb attr = cdl_text_atb_new();
        attr.anchor = TextAnchor_TopLeft;
        attr.style |= TextStyle_AlignLeft;
        attr.color = g_imctx->theme.font_default;
        v2f textTopLeft = v2f_add(hRect.min, v2f_new(5, 2.5));
        im_draw_text((char*)name, textTopLeft, attr);
        im_draw_line(v2f_new(hRect.min.x, hRect.max.y), hRect.max, 1, g_imctx->theme.window_header.border);
        
        // Drawing Reduce Button
        if ( im_id_equal(g_imctx->hot, w->minimizeButtonID) ) {
            if ( im_id_equal(g_imctx->active, w->minimizeButtonID) ) {
                im_draw_rect_filled(minimizeRect, g_imctx->theme.widget.active);
            } else {
                im_draw_rect_filled(minimizeRect, g_imctx->theme.widget.hovered);
            }
        } else {
            im_draw_rect_filled(minimizeRect, g_imctx->theme.widget.background);
        }
        
        im_draw_rect(minimizeRect, 1, g_imctx->theme.widget.border);
        
        f32 minimizeRectYMid = minimizeRect.min.y + rect2_dim(minimizeRect).height / 2;
        im_draw_line(v2f_new(minimizeRect.min.x + 2, minimizeRectYMid), v2f_new(minimizeRect.max.x - 2, minimizeRectYMid), 2, color_white);
    }
    
    //- Start Window Internal Computations
    if ( !FLAG_HAS(w->flags, IMWindowFlag_Minimized) ) {
        im_layout_begin(IMLayout_Vertical);
        im_current_layout()->cursor = w->rect.min;
        im_current_layout()->cursor = v2f_add(im_current_layout()->cursor, v2f_new(5, 25));
        im_current_layout()->cursor.y -= w->offset.y;
        w->allowYScroll = 0;
        
        rect2 clipWRect = w->rect;
        clipWRect.min.y = hRect.max.y;
        im_push_clip_rect(clipWRect);
    }
    
    im_current_layout()->margins = v2f_new(5, 5);
    
    return !FLAG_HAS(w->flags, IMWindowFlag_Minimized);
}

internal void im_window_end()
{
    PROFILED_FUNC;
    
    im_layout* l = im_current_layout();
    im_window* w = im_current_window();
    
    //- Computations
    rect2 handleResizeXRect = w->rect;
    handleResizeXRect.min.x = handleResizeXRect.max.x - 2;
    handleResizeXRect.max.x += 1;
    
    rect2 handleResizeYRect = w->rect;
    handleResizeYRect.min.y = handleResizeYRect.max.y - 2;
    handleResizeYRect.max.y += 1;
    
    v2f handleDim = v2f_new(10, 10);
    v2f handlePos = v2f_sub(w->rect.max, v2f_times(handleDim, 0.5));
    rect2 handleRect = rect2_from_size(handlePos, handleDim);
    
    // Window Area
    rect2 wAreaRect = w->rect;
    wAreaRect.min.y = w->headerRect.max.y;
    
    // Layout Rect
    rect2 lRect = l->rect;
    
    im_layout_end();
    
    //- Interactions
    if ( im_context_is_current_window_hovered() ) {
        // Resize
        if ( collisions_v2f_in_rect(g_imctx->mpos, handleRect) ) {
            g_imctx->nextHot = w->sizeHandleID;
            
            if ( im_id_equal(g_imctx->hot, w->sizeHandleID) && g_imctx->mbLeftOnDown ) {
                g_imctx->active = w->sizeHandleID;
            }
        }
        
        // Resize X
        else if ( collisions_v2f_in_rect(g_imctx->mpos, handleResizeXRect) ) {
            g_imctx->nextHot = w->sizeXHandleID;
            
            if ( im_id_equal(g_imctx->hot, w->sizeXHandleID) && g_imctx->mbLeftOnDown ) {
                g_imctx->active = w->sizeXHandleID;
            }
        }
        
        // Resize Y
        else if ( collisions_v2f_in_rect(g_imctx->mpos, handleResizeYRect) ) {
            g_imctx->nextHot = w->sizeYHandleID;
            
            if ( im_id_equal(g_imctx->hot, w->sizeYHandleID) && g_imctx->mbLeftOnDown ) {
                g_imctx->active = w->sizeYHandleID;
            }
        }
        
        else if ( im_id_is_invalid(g_imctx->active) && g_imctx->mbLeftOnDown ) {
            g_imctx->lastActive.secondary = w->id.primary;
        }
    }
    
    // Drawing
    im_pop_clip_rect(); // Pop Window Area Rect (Area + Header)
    
    if ( !FLAG_HAS(w->flags, IMWindowFlag_Minimized) ) {
        // XY Size Handle
        if ( im_id_equal(g_imctx->hot, w->sizeHandleID) ) {
            im_draw_rect_filled(handleRect, g_imctx->theme.widget.active);
        } else {
            im_draw_rect_filled(handleRect, g_imctx->theme.widget.background);
        }
        im_draw_rect(handleRect, 1, g_imctx->theme.widget.active);
        
        // Window border
        im_draw_rect(w->rect, 1, g_imctx->theme.window.border);
        
        // X Size Handle if hovered
        if ( im_id_equal(g_imctx->hot, w->sizeXHandleID) ) {
            im_draw_rect_filled(handleResizeXRect, g_imctx->theme.widget.active);
        }
        
        // Y Size Handle if hovered
        if ( im_id_equal(g_imctx->hot, w->sizeYHandleID) ) {
            im_draw_rect_filled(handleResizeYRect, g_imctx->theme.widget.active);
        }
    } else {
        im_draw_rect(w->headerRect, 1, g_imctx->theme.widget.border);
    }
    
    //- Logic
    
    // Resize
    bool8 resizeX = im_id_equal(g_imctx->active, w->sizeHandleID) || im_id_equal(g_imctx->active, w->sizeXHandleID);
    bool8 resizeY = im_id_equal(g_imctx->active, w->sizeHandleID) || im_id_equal(g_imctx->active, w->sizeYHandleID);
    bool8 wasResized = resizeX || resizeY;
    
    if ( wasResized ) {
        if ( w->dockspace ) {
            im_internal_dockspace_resize_mouse(w->dockspace, resizeX, resizeY);
        } else {
            w->rect.max.x = w->rect.max.x * ( 1 - resizeX ) + g_imctx->mpos.x * resizeX;
            w->rect.max.y = w->rect.max.y * ( 1 - resizeY ) + g_imctx->mpos.y * resizeY;
        }
    }
    
    // Scroll
    if ( (!FLAG_HAS(w->flags, IMWindowFlag_Minimized) && w->allowYScroll && collisions_v2f_in_rect(g_imctx->mpos, wAreaRect))
        || wasResized ) {
        f32 maxScroll = f32_max(0, rect2_dim(lRect).height - rect2_dim(w->rect).height);
        w->offset.y = f32_clamp(w->offset.y - g_imctx->mScroll.y * 15.f, 0, maxScroll);
    }
}

internal void im_internal_window_resize_around(im_window* w, v2f point)
{
    w->rect.min = v2f_sub(point, v2f_new(150, 20));
    w->rect.max = v2f_add(w->rect.min, v2f_new(300));
}

internal void im_internal_window_resize_around_header(im_window* w)
{
    v2f headerMid = v2f_new(( w->rect.min.x + w->rect.max.x ) / 2.f, w->rect.min.y);
    im_internal_window_resize_around(w, headerMid);
}

internal void im_internal_window_clamp_rect(im_window* w)
{
    rect2 wRect = w->rect;
    v2f dim = rect2_dim(wRect);
    wRect.min.x = f32_clamp(wRect.min.x, 0, g_imctx->wDimensions.width - 20);
    wRect.min.y = f32_clamp(wRect.min.y, 0, g_imctx->wDimensions.height - 20);
    
    wRect.max = v2f_add(wRect.min, dim);
    wRect.max.x = f32_clamp(wRect.max.x, wRect.min.x + 20, wRect.min.x + g_imctx->wDimensions.width - 2);
    wRect.max.y = f32_clamp(wRect.max.y, wRect.min.y + 20, wRect.min.y + g_imctx->wDimensions.height - 2);
    
    w->rect = wRect;
}

//- [SECTION] Dockspaces
internal void im_internal_dockspace_init_root(im_dockspace* dock)
{
    *dock = ZeroStruct;
    
    dock->rect.min = v2f_new(0);
    dock->rect.max = v2f_new(1);
    
    dock->nextRect.min = v2f_new(0);
    dock->nextRect.max = v2f_new(1);
}

internal rect2 im_internal_dockspace_get_screen_rect(im_dockspace* dock)
{
    PROFILED_FUNC;
    
    v2f frameDim = v2f_new(g_imctx->wDimensions.width, g_imctx->wDimensions.height);
    rect2 result = ZeroStruct;
    result.min = v2f_mult(dock->rect.min, frameDim);
    result.max = v2f_mult(dock->rect.max, frameDim);
    return result;
}

internal im_dockspace* im_internal_dockspace_get_hovered()
{
    PROFILED_FUNC;
    
    im_dockspace* d = g_imctx->dockspaces;
    v2f mpos = g_imctx->mpos;
    u32 it = 50;
    while ( d->left && d->right && it-- > 0 ) {
        if ( collisions_v2f_in_rect(mpos, im_internal_dockspace_get_screen_rect(d->left)) ) {
            d = d->left;
        } else if ( collisions_v2f_in_rect(mpos, im_internal_dockspace_get_screen_rect(d->right)) ) {
            d = d->right;
        }
    }
    
    return d;
}

internal im_dockspace* im_internal_dockspace_get_next_available()
{
    PROFILED_FUNC;
    
    im_dockspace* result = 0;
    im_dockspace* d = g_imctx->dockspaces;
    
    while ( d->type && (d < g_imctx->dockspaces + ARRAY_COUNT(g_imctx->dockspaces)) ) {
        d++;
    }
    
    if ( d < g_imctx->dockspaces + ARRAY_COUNT(g_imctx->dockspaces) ) {
        result = d;
    } else {
        m_assert(0, "No available dockspace");
    }
    
    result->type = IMDockType_Horizontal;
    
    return result;
}

internal im_dockspace* im_internal_dockspace_get_sibling(im_dockspace* dock)
{
    PROFILED_FUNC;
    
    im_dockspace* sibling = 0;
    if ( dock->parent ) {
        if ( dock->parent->left == dock ) {
            sibling = dock->parent->right;
        } else {
            sibling = dock->parent->left;
        }
    }
    
    return sibling;
}

internal void im_internal_dockspace_resize_propagate_down(im_dockspace* dock, rect2 prevRect, rect2 newRect)
{
    PROFILED_FUNC;
    
    dock->nextRect = newRect;
    
    if ( dock->left && dock->right ) {
        rect2 previousLeft = dock->left->rect;
        rect2 newLeft = ZeroStruct;
        
        rect2 previousRight = dock->right->rect;
        rect2 newRight = ZeroStruct;
        
        v2f prevDim = rect2_dim(prevRect);
        v2f newDim = rect2_dim(newRect);
        
        switch (dock->type) {
            case IMDockType_Horizontal:
            {
                f32 leftProportion = rect2_dim(previousLeft).width / prevDim.width;
                newLeft.min = newRect.min;
                newLeft.max = v2f_new(newRect.min.x + leftProportion * newDim.width, newRect.max.y);
                
                newRight.min = v2f_new(newRect.min.x + leftProportion * newDim.width, newRect.min.y);
                newRight.max = newRect.max;
            } break;
            
            case IMDockType_Vertical:
            {
                f32 topProportion = rect2_dim(previousLeft).height / prevDim.height;
                newLeft.min = newRect.min;
                newLeft.max = v2f_new(newRect.max.x, newRect.min.y + topProportion * newDim.height);
                
                newRight.min = v2f_new(newRect.min.x, newRect.min.y + topProportion * newDim.height);;
                newRight.max = newRect.max;
            } break;
            
            default:
            {
            } break;
        }
        
        dock->left->nextRect = newLeft;
        dock->right->nextRect = newRight;
        im_internal_dockspace_resize_propagate_down(dock->left, previousLeft, newLeft);
        im_internal_dockspace_resize_propagate_down(dock->right, previousRight, newRight);
    }
}

internal void im_internal_dockspace_resize_mouse(im_dockspace* dock, bool8 resizeX, bool8 resizeY)
{
    PROFILED_FUNC;
    
    im_window* w = dock->child;
    im_dockspace* toResize = dock;
    im_dockspace* relative = 0;
    
    im_dockspace* parent = dock->parent;
    if ( parent ) {
        
        // If we are changing a left/top, we only need to change the sibbling dock rect
        if ( parent->left == dock ) {
            relative = parent->right;
        }
        
        // If we resize a right/bottom, we need to go up the dockspace tree to find the 
        // last dockspace that shares the same layout as the current in order to resize it
        else {
            im_dockspace* grandParent = parent->parent;
            while ( grandParent && grandParent->type == dock->type) {
                grandParent = grandParent->parent;
            }
            
            if ( grandParent ) {
                // TODO(abe): refactor this
                relative = im_internal_dockspace_get_sibling(parent);
            }
        }
    }
    
    if ( relative ) {
        rect2 oldRect = toResize->rect;
        rect2 newRect = oldRect;
        
        rect2 oldRelativeRect = relative->rect;
        rect2 newRelativeRect = oldRelativeRect;
        
        switch ( parent->type ) {
            case IMDockType_Horizontal:
            {
                if ( resizeX && dock != parent->right ) {
                    f32 delta = ( g_imctx->mpos.x - w->rect.max.x ) / g_imctx->wDimensions.width;
                    
                    newRect.max.x += delta;
                    newRelativeRect.min.x += delta;
                }
            } break;
            
            case IMDockType_Vertical:
            {
                if ( resizeY && dock != parent->bottom ) {
                    f32 delta = ( g_imctx->mpos.y - w->rect.max.y ) / g_imctx->wDimensions.height;
                    
                    newRect.max.y += delta;
                    newRelativeRect.min.y += delta;
                    
                    // TODO(abe): don't allow bottom window y to go over other header
                }
            } break;
            
            default:
            {
            } break;
        }
        
        im_internal_dockspace_resize_propagate_down(dock, oldRect, newRect);
        im_internal_dockspace_resize_propagate_down(relative, oldRelativeRect, newRelativeRect);
    }
}

internal void im_internal_dockspace_insert_window_in_hovered(im_window* window)
{
    PROFILED_FUNC;
    
    im_dockspace* dock = im_internal_dockspace_get_hovered();
    
    IMDockType type = IMDockType_Horizontal;
    if ( keyboard_key_get(KEY_UP) ) {
        type = IMDockType_Vertical;
    }
    
    // If there is currently no window that is part of a dockspace
    if ( !dock->child && !dock->left && !dock->right ) {
        window->dockspace = dock;
        dock->child = window;
    } 
    
    // There is at least one window in the dockspace tree
    else {
        im_window* dockChild = dock->child;
        im_dockspace* left = im_internal_dockspace_get_next_available();
        im_dockspace* right = im_internal_dockspace_get_next_available();
        v2f dockDimensions = rect2_dim(dock->rect);
        
        // Left / Top Dockspace
        dock->left = left;
        dockChild->dockspace = left;
        left->parent = dock;
        left->child = dockChild;
        
        // Right / Bottom Dockspace
        dock->right = right;
        window->dockspace = right;
        right->parent = dock;
        right->child = window;
        
        // TODO(abe): change behaviour depending on the hovered area
        switch (type) {
            case IMDockType_Horizontal:
            {
                left->nextRect = dock->rect;
                right->nextRect = rect2_cut_right(&left->nextRect, dockDimensions.width / 2);
            } break;
            
            case IMDockType_Vertical:
            {
                im_dockspace* top = left;
                im_dockspace* bottom = right;
                
                top->nextRect = dock->rect;
                bottom->nextRect = rect2_cut_bottom(&top->nextRect, dockDimensions.height / 2);
            } break;
        }
        
        dock->child = 0;
    }
    
    dock->type = type;
}

internal void im_internal_dockspace_copy_and_delete(im_dockspace* source, im_dockspace* target)
{
    PROFILED_FUNC;
    
    rect2 targetRect = target->rect;
    rect2 sourceRect = source->rect;
    
    target->type = source->type;
    
    target->left = source->left;
    if ( target->left ) {
        target->left->parent = target;
    }
    
    target->right = source->right;
    if ( target->right ) {
        target->right->parent = target;
    }
    
    target->child = source->child;
    if ( target->child ) {
        target->child->dockspace = target;
    }
    
    im_internal_dockspace_resize_propagate_down(target, sourceRect, targetRect);
    *source = ZeroStruct;
}

internal void im_internal_dockspace_remove_window(im_window* window)
{
    PROFILED_FUNC;
    
    im_dockspace* dock = window->dockspace;
    if ( dock ) {
        
        im_dockspace* parent = dock->parent;
        if ( parent ) {
            
            im_dockspace* sibling = im_internal_dockspace_get_sibling(dock);
            if ( parent->parent ) {
                
                // Move the sibling up to the parent
                im_internal_dockspace_copy_and_delete(sibling, parent);
            } else {
                
                // The sibling is now the new root
                im_internal_dockspace_copy_and_delete(sibling, g_imctx->dockspaces);
            }
            
            // Resets parent dockspace
            *dock = ZeroStruct;
        } else {
            
            // If there is no parent, then we are at the root
            im_internal_dockspace_init_root(dock);
        }
        
    }
    
    window->dockspace = 0;
}

//- [SECTION] Context
#ifndef IM_OWN_CONTEXT
im_context g_im_context;
#endif

internal void im_initialise()
{
#ifndef IM_OWN_CONTEXT
    g_imctx = &g_im_context;
#endif
    
    // Themes
    g_imctx->theme = im_colors;
    
    // Dockspaces
    im_internal_dockspace_init_root(g_imctx->dockspaces);
    
    // Config File
    cdl_file configFile = platform->readfile("im.conf");
    if ( configFile.size > 0 ) {
        MemoryCopy(&g_imctx->theme, configFile.content, sizeof(im_color_theme));
    }
}

internal void im_clean()
{
    // Save config file
    char configBuffer[2048] = { };
    cdl_file config = { 2048, &configBuffer };
    u32 writeCursor = 0;
    
    // Write color theme
    MemoryCopy((configBuffer + writeCursor), &g_imctx->theme, sizeof(im_color_theme));
    writeCursor += sizeof(im_color_theme);
    
    config.size = writeCursor;
    
    if ( platform->writefile("im.conf", &config) ) {
        log_info("Wrote config file.\n");
    } else {
        log_err("Couldn't write config file.\n");
    }
}

internal im_window* im_internal_context_find_window(im_id id)
{
    PROFILED_FUNC;
    
    im_window* result = 0;
    
    // Find window if it exists
    for (u32 i = 0; i < ARRAY_COUNT(g_imctx->windows); i++) {
        if ( im_id_equal(id, g_imctx->windows[i].id) ) {
            result = g_imctx->windows + i;
            break;
        }
    }
    
    // If not, create one
    if ( !result ) {
        for (u32 i = 0; i < ARRAY_COUNT(g_imctx->windows); i++) {
            if ( im_id_is_invalid(g_imctx->windows[i].id) ) {
                result = g_imctx->windows + i;
                break;
            }
        }
    }
    
    result->id = id;
    return result;
}

internal im_window* im_context_find_hovered_window()
{
    PROFILED_FUNC;
    
    im_window* result = 0;
    
    for (im_window* w = g_imctx->lastWindow; w && VALUE_IN_ARRAY_BOUNDS(w, g_imctx->windows); w = w->top) {
        bool8 visible = FLAG_HAS(w->flags, IMWindowFlag_Visible);
        bool8 minimized = FLAG_HAS(w->flags, IMWindowFlag_Minimized);
        
        bool8 isInHeaderRectAndMinimized = visible && minimized && collisions_v2f_in_rect(g_imctx->mpos, w->headerRect);
        bool8 isInRectAndNotMinimized = visible && !minimized && collisions_v2f_in_rect(g_imctx->mpos, w->rect);
        if ( isInHeaderRectAndMinimized || isInRectAndNotMinimized ) {
            result = w;
        }
    }
    
    return result;
}

internal void im_internal_context_set_window_focused(im_window* w)
{
    PROFILED_FUNC;
    
    im_window* previousFocused = g_imctx->focusedWindow;
    if ( previousFocused == w ) { return; }
    
    // Remove window in the chained list
    if ( w->top ) {
        w->top->below = w->below;
    }
    
    if ( w->below ) {
        w->below->top = w->top;
    }
    
    // Insert window on top of the first focused
    if ( previousFocused ) {
        previousFocused->top = w;
    }
    
    w->below = previousFocused;
    w->top = 0;
    
    g_imctx->hoveredWindow = w;
    g_imctx->focusedWindow = w;
    g_imctx->lastActive = w->id;
    
    for (im_window* win = g_imctx->focusedWindow; win; win = win->below) {
        g_imctx->lastWindow = win;
    }
}

internal bool8 im_context_is_current_window_hovered()
{
    im_window* current = g_imctx->currentWindow;
    im_window* hovered = g_imctx->hoveredWindow;
    
    return ( current == hovered );
}

internal void im_frame_begin()
{
    PROFILED_FUNC;
    
    m_assert(g_imctx, "im_frame_begin: No IM context provided");
    
    //- Window Interactions
    g_imctx->hoveredWindow = im_context_find_hovered_window();
    
    //- Mouse Interactions
    {
        g_imctx->mpos = v2itof(mouse_get_position());
        g_imctx->mDelta = mouse_get_delta();
        g_imctx->mScroll = mouse_get_scroll();
        
        g_imctx->wDimensions = v2f_new(core->frameWidth, core->frameHeight);
        
        g_imctx->mbLeftOnDown = mouse_button_on_down(mouse_LEFT);
        g_imctx->mbLeftOnUp = mouse_button_on_up(mouse_LEFT);
        g_imctx->mbLeft = mouse_button_get(mouse_LEFT);
        
        g_imctx->mbRightOnDown = mouse_button_on_down(mouse_RIGHT);
    }
    
    //- Reset Layouts
    {
        im_layout* windowLayout = g_imctx->layoutStack;
        windowLayout->type = IMLayout_Vertical;
        windowLayout->cursor = v2f_new(12, 12);
        windowLayout->rect = ZeroStruct;
        windowLayout->rect.min = v2f_new(10, 10);
        MemoryZero(g_imctx->layoutStack, ARRAY_COUNT(g_imctx->layoutStack) * sizeof(im_layout));
        g_imctx->currentLayout = windowLayout;
    }
    
    for (im_dockspace* d = g_imctx->dockspaces; d < g_imctx->dockspaces + ARRAY_COUNT(g_imctx->dockspaces); d++) {
        d->rect = d->nextRect;
    }
}

internal void im_frame_end()
{
    PROFILED_FUNC;
    
    //- Windows Z Sorting
    im_window* focusedWindow = g_imctx->focusedWindow;
    im_window* lastActiveWindow = 0;
    
    for (im_window* w = g_imctx->windows; FLAG_HAS(w->flags, IMWindowFlag_Initialized); w++) {
        if ( (w->id.primary == g_imctx->lastActive.secondary) && (w != focusedWindow) ) {
            lastActiveWindow = w;
        }
    };
    
    if ( lastActiveWindow ) {
        im_internal_context_set_window_focused(lastActiveWindow);
        focusedWindow = lastActiveWindow;
    }
    
    //- Compute visibility for each window
    for (im_window* w = g_imctx->windows; FLAG_HAS(w->flags, IMWindowFlag_Initialized); w++) {
        // Check if window_begin was called on this window at this frame
        if ( FLAG_HAS(w->flags, IMWindowFlag_VisibleThisFrame) ) {
            
            // If a window transitionned from non visible to visible > bring it in focus
            if ( !FLAG_HAS(w->flags, IMWindowFlag_Visible) ) {
                im_internal_context_set_window_focused(w);
            }
            
            FLAG_SET(w->flags, IMWindowFlag_Visible);
        } 
        
        // If not clear window visibility
        else {
            FLAG_CLEAR(w->flags, IMWindowFlag_Visible);
        }
        
        FLAG_CLEAR(w->flags, IMWindowFlag_VisibleThisFrame);
    }
    
    //- Process Render Commands
    for (im_window* w = g_imctx->lastWindow; w; w = w->top) {
        if ( FLAG_HAS(w->flags, IMWindowFlag_Visible) ) {
            im_cdl_render_window(w);
        }
    }
    
    //- Interactions
    g_imctx->hot = g_imctx->nextHot;
    g_imctx->nextHot = im_id_invalid();
    
    if ( g_imctx->mbLeftOnUp ) {
        g_imctx->active = im_id_invalid();
    }
    
    if ( !im_id_is_invalid(g_imctx->active) ) {
        g_imctx->hot = g_imctx->active;
        g_imctx->lastActive = g_imctx->active;
    }
}

//- [SECTION] Widgets
internal color8 im_internal_get_widget_background_color(im_id id, IMWidgetFlag wflags)
{
    PROFILED_FUNC;
    
    // Initial color theme
    im_item_data* data = im_internal_get_item_data(id);
    color8 background = g_imctx->theme.widget.background;
    color8 hovered = g_imctx->theme.widget.hovered;
    color8 active = g_imctx->theme.widget.active;
    
    // Change colors depending on the widget flags
    if ( FLAG_HAS(wflags, IMWidgetFlag_Danger) ) {
        background = g_imctx->theme.widget_danger.background;
        hovered = g_imctx->theme.widget_danger.hovered;
        active = g_imctx->theme.widget_danger.active;
    }
    
    if ( FLAG_HAS(wflags, IMWidgetFlag_ReadOnly) ) {
        f32 desatFactor = 0.75;
        background = cdl_rgb_desaturate(background, desatFactor);
        hovered = cdl_rgb_desaturate(hovered, desatFactor);
        active = cdl_rgb_desaturate(active, desatFactor);
    }
    
    // Get the final background color depending on the ui state
    color8 result = background;
    
    // If mouse cursor is over the widget
    if ( im_id_equal(g_imctx->hot, id) ) {
        if ( im_id_equal(g_imctx->active, id) ) {
            result = active;
        } else {
            result = hovered;
            data->initialColor = hovered;
        }
        
        data->transitionFactor = 0;
    } 
    
    // If widget not hovered
    else {
        if ( FLAG_HAS(wflags, IMWidgetFlag_UseActive) ) {
            result = active;
        } else {
            // Transition from initialColor to background color
            color8 bgColor = cdl_rgba_lerp(background, data->initialColor, data->transitionFactor);
            data->transitionFactor = f32_clamp(data->transitionFactor + core->deltaTime * 10, 0, 1);
            result = bgColor;
        }
    }
    
    return result;
}

internal color8 im_internal_get_widget_border_color(im_id id, IMWidgetFlag wflags)
{
    color8 result = g_imctx->theme.widget.border;
    
    // Change colors depending on the widget flags
    if ( FLAG_HAS(wflags, IMWidgetFlag_Danger) ) {
        result = g_imctx->theme.widget_danger.border;
    }
    
    if ( FLAG_HAS(wflags, IMWidgetFlag_ReadOnly) ) {
        f32 desatFactor = 0.75;
        result = cdl_rgb_desaturate(result, desatFactor);
    }
    
    return result;
}

internal void im_internal_draw_widget_background(im_id id, rect2 rect, IMWidgetFlag wflags)
{
    PROFILED_FUNC;
    
    color8 color = im_internal_get_widget_background_color(id, wflags);
    color8 border = im_internal_get_widget_border_color(id, wflags);
    
    // Draw the widget rect
    im_draw_rect_filled(rect, color);
    im_draw_rect(rect, 1, border);
}

internal bool8 im_button_behaviour(im_id id, rect2 rect)
{
    PROFILED_FUNC;
    bool8 pressed = 0;
    
    if ( im_context_is_current_window_hovered() ) {
        if ( collisions_v2f_in_rect(g_imctx->mpos, rect) ) {
            g_imctx->nextHot = id;
            
            if ( im_id_equal(g_imctx->hot, id) && g_imctx->mbLeftOnDown ) {
                g_imctx->active = id;
            }
        }
        
        if ( g_imctx->mbLeftOnUp && im_id_equal(g_imctx->nextHot, id) && im_id_equal(g_imctx->active, id) ) {
            pressed = 1;
        }
    }
    
    return pressed;
}

//- Standard
internal bool8 im_button(const char* name, IMWidgetFlag wflags = IMWidgetFlag_None)
{
    PROFILED_FUNC;
    
    // Computations
    im_id id = im_id_new(name);
    rect2 btnRect = im_layout_new_rect();
    
    if ( !im_layout_add_rect(btnRect) ) { return 0; }
    
    // Logic
    bool8 pressed = false;
    if ( !FLAG_HAS(wflags, IMWidgetFlag_ReadOnly) ) {
        pressed = im_button_behaviour(id, btnRect);
    }
    
    // Drawing
    im_internal_draw_widget_background(id, btnRect, wflags);
    
    cdl_text_atb attr = cdl_text_atb_new();
    attr.lineWidth = rect2_dim(btnRect).width - 2;
    attr.color = g_imctx->theme.font_default;
    im_draw_text((char*)name, rect2_mid(btnRect), attr);
    
    return pressed;
}

internal void im_checkbox(const char* name, bool8* toggled, IMWidgetFlag wflags = IMWidgetFlag_None)
{
    PROFILED_FUNC;
    
    // Computations
    im_id id = im_id_new(name);
    rect2 rect = im_layout_new_rect();
    
    if ( !im_layout_add_rect(rect) ) { return; }
    
    // Icon Rect
    v2f chkboxCursor = rect.min;
    f32 iconHeight = 12;
    v2f iconDim = v2f_new(iconHeight);
    f32 iconMargin = 20 - iconHeight;
    rect2 iconRect = ZeroStruct;
    iconRect.min = v2f_add(chkboxCursor, v2f_new(iconMargin / 2));
    iconRect.max = v2f_add(iconRect.min, iconDim);
    chkboxCursor.x += 20;
    
    // Text Rect
    cdl_text_atb attr = cdl_text_atb_new();
    v2f textDim = cdl_text_get_dimensions((char*)name, renderer->currentFont->asset, attr);
    attr.anchor = TextAnchor_TopLeft;
    v2f textCursor = chkboxCursor;
    textCursor.y += (20 - attr.size) / 2;
    chkboxCursor.x += textDim.width;
    
    // Logic
    if ( !FLAG_HAS(wflags, IMWidgetFlag_ReadOnly) ) {
        bool8 pressed = im_button_behaviour(id, rect);
        if ( pressed ) {
            *toggled = !*toggled;
        }
    }
    
    // Drawing
    if ( im_id_equal(g_imctx->nextHot, id) ) {
        im_draw_rect_filled(iconRect, g_imctx->theme.checkbox_icon.hovered);
    }
    
    im_draw_rect(iconRect, 1, g_imctx->theme.checkbox_icon.border);
    
    // Draw Check Icon
    if ( *toggled ) {
        v2f p1 = v2f_add(iconRect.min, v2f_new(iconHeight / 5, 4 * iconHeight / 9));
        v2f p2 = v2f_add(rect2_mid(iconRect), v2f_new(-iconHeight / 5, iconHeight / 4));
        v2f p3 = v2f_new(iconRect.max.x, iconRect.min.y);
        
        im_draw_line(p1, p2, 2, g_imctx->theme.checkbox_icon.background);
        im_draw_line(p2, p3, 2, g_imctx->theme.checkbox_icon.background);
    }
    
    attr.color = g_imctx->theme.font_default;
    im_draw_text((char*)name, textCursor, attr);
}

internal bool8 im_radio(const char* name, bool8 selected, IMWidgetFlag wflags = IMWidgetFlag_None)
{
    PROFILED_FUNC;
    
    // Computations
    im_id id = im_id_new(name);
    rect2 radioRect = im_layout_new_rect();
    
    // Icon Rect
    rect2 iconRect = radioRect;
    f32 iconRadius = 6;
    v2f iconDim = v2f_new(iconRadius);
    f32 iconMargin = rect2_dim(radioRect).height - iconRadius;
    iconRect.min = v2f_add(iconRect.min, v2f_new(iconMargin / 2));
    iconRect.max = v2f_add(iconRect.min, iconDim);
    v2f iconPos = rect2_mid(iconRect);
    
    if ( !im_layout_add_rect(radioRect) ) { return 0; }
    
    // Logic 
    bool8 pressed = false;
    if ( !FLAG_HAS(wflags, IMWidgetFlag_ReadOnly) ) {
        pressed = im_button_behaviour(id, radioRect);
    }
    
    // Drawing
    im_internal_draw_widget_background(id, radioRect, wflags);
    
    // Icon
    im_draw_circle_filled(iconPos, iconRadius, g_imctx->theme.radio_icon.background);
    if ( selected ) {
        im_draw_circle_filled(iconPos, iconRadius, g_imctx->theme.radio_icon.selected);
    } else {
        im_draw_circle(iconPos, iconRadius, 1, g_imctx->theme.radio_icon.border);
        if ( im_id_equal(g_imctx->nextHot, id) ) {
            im_draw_circle_filled(iconPos, iconRadius, g_imctx->theme.radio_icon.hovered);
        }
    }
    
    cdl_text_atb attr = cdl_text_atb_new();
    attr.color = g_imctx->theme.font_default;
    attr.lineWidth = rect2_dim(radioRect).width - 2;
    im_draw_text((char*)name, rect2_mid(radioRect), attr);
    
    return pressed;
}

//- Sliders
internal void im_internal_slider(const char* name, const char* idString, IMVarType type, f32 min, f32 max, f32* value, IMWidgetFlag wflags)
{
    assert( !f32_equals(min, max) );
    PROFILED_FUNC;
    
    // Computations
    im_id id = im_id_new(idString);
    rect2 rect = im_layout_new_rect();
    
    // Get Text Width
    cdl_text_atb attr = cdl_text_atb_new();
    attr.lineWidth = g_imctx->wDimensions.width - 2;
    char buffer[256] = { };
    
    switch (type) {
        case IMVarType_u32:
        {
            u32 u32Val = (u32)*value;
            sprintf(buffer, "%s: %u", name, u32Val);
        } break;
        
        case IMVarType_i32:
        {
            i32 i32Val = (i32)*value;
            sprintf(buffer, "%s: %d", name, i32Val);
        } break;
        
        case IMVarType_f32:
        {
            sprintf(buffer, "%s: %.3f", name, *value);
        } break;
        
        default:
        {
            sprintf(buffer, "Unknown Type");
        } break;
    }
    
    v2f textDim = cdl_text_get_dimensions(buffer, renderer->currentFont->asset, attr);
    rect.max.x = rect.min.x + f32_max(textDim.width, im_current_layout()->width);
    
    if ( !im_layout_add_rect(rect) ) { return; }
    
    f32 handleWidth = 5;
    f32 rangePadding = handleWidth / 2;
    f32 rangeWidth = rect2_dim(rect).width - 2 * rangePadding;
    f32 valueToOffset = rangeWidth / ( max - min );
    
    v2f sliderHandleCursor = v2f_add(rect.min, v2f_new((*value - min) * valueToOffset, 0));
    v2f sliderHandleDim = v2f_new(handleWidth, 20);
    v2f sliderHandlePos = v2f_add(sliderHandleCursor, v2f_times(sliderHandleDim, 0.5));
    rect2 sliderHandleRect = rect2_from_size(sliderHandlePos, sliderHandleDim);
    
    // Logic
    if ( !FLAG_HAS(wflags, IMWidgetFlag_ReadOnly) ) {
        im_button_behaviour(id, rect);
        
        if ( im_id_equal(g_imctx->active, id) ) {
            f32 mouseXClamped = f32_clamp(g_imctx->mpos.x, rect.min.x + rangePadding, rect.max.x - rangePadding);
            f32 xoffset = mouseXClamped - rect.min.x - rangePadding;
            *value = min + xoffset / valueToOffset;
        }
    }
    
    // Drawing
    im_internal_draw_widget_background(id, rect, wflags);
    
    // Handle
    im_draw_rect_filled(sliderHandleRect, g_imctx->theme.widget.handle);
    im_draw_text(buffer, rect2_mid(rect), attr);
}

internal void im_slider1u(const char* name, u32 min, u32 max, u32* value, IMWidgetFlag wflags = IMWidgetFlag_None)
{
    PROFILED_FUNC;
    
    f32 f32Val = (f32)*value;
    im_internal_slider(name, name, IMVarType_u32, (f32)min, (f32)max, &f32Val, wflags);
    *value = (u32)f32Val;
}

internal void im_slider1i(const char* name, i32 min, i32 max, i32* value, IMWidgetFlag wflags = IMWidgetFlag_None)
{
    PROFILED_FUNC;
    
    f32 f32Val = (f32)*value;
    im_internal_slider(name, name, IMVarType_i32, (f32)min, (f32)max, &f32Val, wflags);
    *value = (i32)f32Val;
}

internal void im_slider2i(const char* name, i32 min, i32 max, v2i* value, IMWidgetFlag wflags = IMWidgetFlag_None)
{
    PROFILED_FUNC;
    
    im_layout_begin(IMLayout_Horizontal);
    
    //- Text
    cdl_text_atb attr = cdl_text_atb_new();
    attr.color = g_imctx->theme.font_default;
    im_text_with_attributes(attr, name);
    
    //- Individual Sliders
    char buffer[256] = { };
    
    f32 x = (f32)value->x;
    sprintf(buffer, "@__v2i_x_component_%s__@", name);
    im_internal_slider("x", buffer, IMVarType_i32, min, max, &x, wflags);
    value->x = (i32)x;
    
    f32 y = (f32)value->y;
    sprintf(buffer, "@__v2i_y_component_%s__@", name);
    im_internal_slider("y", buffer, IMVarType_i32, min, max, &y, wflags);
    value->y = (i32)y;
    
    im_layout_end();
}

internal void im_slider1f(const char* name, f32 min, f32 max, f32* value, IMWidgetFlag wflags = IMWidgetFlag_None)
{
    PROFILED_FUNC;
    im_internal_slider(name, name, IMVarType_f32, min, max, value, wflags);
}

internal void im_slider2f(const char* name, f32 min, f32 max, v2f* value, IMWidgetFlag wflags = IMWidgetFlag_None)
{
    PROFILED_FUNC;
    
    im_layout_begin(IMLayout_Horizontal);
    
    //- Text
    cdl_text_atb attr = cdl_text_atb_new();
    attr.color = g_imctx->theme.font_default;
    im_text_with_attributes(attr, name);
    
    //- Individual Sliders
    char buffer[256] = { };
    
    sprintf(buffer, "@__v2f_x_component_%s__@", name);
    im_internal_slider("x", buffer, IMVarType_f32, min, max, &value->x, wflags);
    
    sprintf(buffer, "@__v2f_y_component_%s__@", name);
    im_internal_slider("y", buffer, IMVarType_f32, min, max, &value->y, wflags);
    
    im_layout_end();
}

internal void im_slider3f(const char* name, f32 min, f32 max, v3f* value, IMWidgetFlag wflags = IMWidgetFlag_None)
{
    PROFILED_FUNC;
    
    im_layout_begin(IMLayout_Horizontal);
    
    //- Text
    cdl_text_atb attr = cdl_text_atb_new();
    attr.color = g_imctx->theme.font_default;
    im_text_with_attributes(attr, name);
    
    //- Individual Sliders
    char buffer[256] = { };
    
    sprintf(buffer, "@__v3f_x_component_%s__@", name);
    im_internal_slider("x", buffer, IMVarType_f32, min, max, &value->x, wflags);
    
    sprintf(buffer, "@__v3f_y_component_%s__@", name);
    im_internal_slider("y", buffer, IMVarType_f32, min, max, &value->y, wflags);
    
    sprintf(buffer, "@__v3f_z_component_%s__@", name);
    im_internal_slider("z", buffer, IMVarType_f32, min, max, &value->z, wflags);
    
    im_layout_end();
}

internal void im_slider4f(const char* name, f32 min, f32 max, v4f* value, IMWidgetFlag wflags = IMWidgetFlag_None)
{
    PROFILED_FUNC;
    
    im_layout_begin(IMLayout_Horizontal);
    
    //- Text
    cdl_text_atb attr = cdl_text_atb_new();
    attr.color = g_imctx->theme.font_default;
    im_text_with_attributes(attr, name);
    
    //- Individual Sliders
    char buffer[256] = { };
    
    sprintf(buffer, "@__v4f_x_component_%s__@", name);
    im_internal_slider("x", buffer, IMVarType_f32, min, max, &value->x, wflags);
    
    sprintf(buffer, "@__v4f_y_component_%s__@", name);
    im_internal_slider("y", buffer, IMVarType_f32, min, max, &value->y, wflags);
    
    sprintf(buffer, "@__v4f_z_component_%s__@", name);
    im_internal_slider("z", buffer, IMVarType_f32, min, max, &value->z, wflags);
    
    sprintf(buffer, "@__v4f_w_component_%s__@", name);
    im_internal_slider("w", buffer, IMVarType_f32, min, max, &value->w, wflags);
    
    im_layout_end();
}

//- Colors
internal void im_internal_color_popup(const char* name, rect2 rect, color8* color)
{
    if ( g_imctx->mbLeftOnDown && collisions_v2f_in_rect(g_imctx->mpos, rect) ) {
        im_popup_open(name);
    }
    
    if ( im_popup_begin(name) ) {
        im_text("(%d, %d, %d, %d)", color->r, color->g, color->b, color->a);
        im_popup_end();
    }
}

internal void im_rgb(const char* name, color8* color, IMWidgetFlag wflags = IMWidgetFlag_None)
{
    PROFILED_FUNC;
    
    im_layout_begin(IMLayout_Horizontal);
    
    cdl_text_atb attr = cdl_text_atb_new();
    attr.color = g_imctx->theme.font_default;
    im_text_with_attributes(attr, name);
    
    f32 r = color->r;
    f32 g = color->g;
    f32 b = color->b;
    
    char buffer[256] = { };
    
    sprintf(buffer, "@__rgb_r_component_%s__@", name);
    im_internal_slider("r", buffer, IMVarType_u32, 0, 255, &r, wflags);
    
    sprintf(buffer, "@__rgb_g_component_%s__@", name);
    im_internal_slider("g", buffer, IMVarType_u32, 0, 255, &g, wflags);
    
    sprintf(buffer, "@__rgb_b_component_%s__@", name);
    im_internal_slider("b", buffer, IMVarType_u32, 0, 255, &b, wflags);
    
    color->r = (u8)r;
    color->g = (u8)g;
    color->b = (u8)b;
    color->a = 255;
    
    rect2 rect = im_layout_new_rect();
    rect.max.x = rect.min.x + rect2_dim(rect).height;
    
    if ( im_layout_add_rect(rect) ) {
        im_draw_rect_filled(rect, *color);
        im_draw_rect(rect, 1, g_imctx->theme.widget.border);
    }
    
    im_layout_end();
}

internal void im_rgba(const char* name, color8* color, IMWidgetFlag wflags = IMWidgetFlag_None)
{
    PROFILED_FUNC;
    
    im_layout_begin(IMLayout_Horizontal);
    
    cdl_text_atb attr = cdl_text_atb_new();
    attr.color = g_imctx->theme.font_default;
    im_text_with_attributes(attr, name);
    
    f32 r = color->r;
    f32 g = color->g;
    f32 b = color->b;
    f32 a = color->a;
    
    char buffer[256] = { };
    
    sprintf(buffer, "@__rgba_r_component_%s__@", name);
    im_internal_slider("r", buffer, IMVarType_u32, 0, 255, &r, wflags);
    
    sprintf(buffer, "@__rgba_g_component_%s__@", name);
    im_internal_slider("g", buffer, IMVarType_u32, 0, 255, &g, wflags);
    
    sprintf(buffer, "@__rgba_b_component_%s__@", name);
    im_internal_slider("b", buffer, IMVarType_u32, 0, 255, &b, wflags);
    
    sprintf(buffer, "@__rgba_a_component_%s__@", name);
    im_internal_slider("a", buffer, IMVarType_u32, 0, 255, &a, wflags);
    
    color->r = (u8)r;
    color->g = (u8)g;
    color->b = (u8)b;
    color->a = (u8)a;
    
    rect2 rect = im_layout_new_rect();
    rect.max.x = rect.min.x + rect2_dim(rect).height;
    
    if ( im_layout_add_rect(rect) ) {
        im_draw_rect_filled(rect, *color);
        im_draw_rect(rect, 1, g_imctx->theme.widget.border);
        sprintf(buffer, "@__rgba_popup_%s__@", name);
        im_internal_color_popup(buffer, rect, color);
    }
    
    im_layout_end();
}

//- Charts
internal im_chart_atb im_chart_atb_new()
{
    PROFILED_FUNC;
    
    im_chart_atb result = ZeroStruct;
    
    result.min = F32Inf;
    result.max = -F32Inf;
    
    result.color = color_white;
    result.lineWeight = 1;
    
    result.markerColor = color_white;
    
    return result;
}

internal bool8 im_chart_begin(const char* name, im_chart_atb atb = im_chart_atb_new())
{
    PROFILED_FUNC;
    
    bool8 show = 1;
    im_window* w = im_current_window();
    
    // Computations
    im_id id = im_id_new(name);
    rect2 rect = im_layout_new_chart_rect(atb);
    
    if ( !im_layout_add_rect(rect) ) { return 0; }
    
    im_button_behaviour(id, rect);
    
    g_imctx->currentChartID = id;
    g_imctx->currentChartRect = rect;
    MemoryCopy(g_imctx->currentChartName, name, ARRAY_COUNT(g_imctx->currentChartName));
    
    im_draw_rect_filled(rect, g_imctx->theme.widget.background);
    
    rect2 clipRect = rect;
    clipRect.min.x = f32_max(w->rect.min.x, clipRect.min.x);
    clipRect.min.y = f32_max(w->rect.min.y, clipRect.min.y);
    clipRect.max.x = f32_min(w->rect.max.x, clipRect.max.x);
    clipRect.max.y = f32_min(w->rect.max.y, clipRect.max.y);
    im_push_clip_rect(clipRect);
    
    return show;
}

internal void im_chart_end()
{
    PROFILED_FUNC;
    
    im_id id = g_imctx->currentChartID;
    rect2 rect = g_imctx->currentChartRect;
    
    im_pop_clip_rect();
    
    cdl_text_atb atb = cdl_text_atb_new();
    atb.anchor = TextAnchor_TopLeft;
    atb.style |= TextStyle_BackgroundFill;
    
    im_draw_text(g_imctx->currentChartName, rect.min, atb);
    im_draw_rect(rect, 1, cdl_rgba(255, 255, 255, 25));
    
    g_imctx->currentChartID = im_id_invalid();
}

internal void im_chart_plot_marker(v2f pos, im_chart_atb atb)
{
    u32 radius = 5;
    
    switch (atb.marker) {
        case IMChartMarker_Square:
        {
            rect2 rect = rect2_from_size(pos, v2f_new(radius));
            im_draw_rect_filled(rect, atb.markerColor);
        } break;
        
        case IMChartMarker_Circle:
        {
            im_draw_circle_filled(pos, radius, atb.markerColor);
        } break;
        
        default:
        {
        } break;
    }
}

internal void im_chart_plot_func(const char* legend, f32 xMin, f32 xMax, u32 steps, chartFunc f, im_chart_atb atb = im_chart_atb_new())
{
    PROFILED_FUNC;
    
    rect2 rect = g_imctx->currentChartRect;
    
    // Chart computations
    f32 yMin = F32Inf;
    f32 yMax = -F32Inf;
    
    f32 step = (xMax - xMin) / steps;
    for (f32 x = xMin; x < xMax; x += step) {
        f32 y = f(x);
        
        yMin = f32_min(y, yMin);
        yMax = f32_max(y, yMax);
    }
    
    if ( !f32_equals(atb.min, F32Inf) ) {
        yMin = atb.min;
    }
    
    if ( !f32_equals(atb.max, -F32Inf) ) {
        yMax = atb.max;
    }
    
    // Chart Draw
    v2f minCorner = v2f_new(rect.min.x, rect.max.y);
    v2f maxCorner = v2f_new(rect.max.x, rect.min.y);
    
    f32 xMult = ( maxCorner.x - minCorner.x ) / ( xMax - xMin );
    f32 yMult = ( maxCorner.y - minCorner.y ) / ( yMax - yMin );
    
    v2f firstPos = v2f_new( ( xMin - xMin ) * xMult + minCorner.x, ( f(xMin) - yMin ) * yMult + minCorner.y);
    
    for (f32 x = xMin; x < xMax; x += step) {
        v2f nextPos = v2f_new( ( x - xMin ) * xMult + minCorner.x, ( f(x) - yMin ) * yMult + minCorner.y);
        im_draw_line(firstPos, nextPos, atb.lineWeight, atb.color);
        im_chart_plot_marker(firstPos, atb);
        firstPos = nextPos;
    }
    
    im_chart_plot_marker(firstPos, atb);
}

internal void im_chart_plot_lines(const char* legend, u32 valueCount, f32* values, im_chart_atb atb = im_chart_atb_new())
{
    PROFILED_FUNC;
    
    rect2 rect = g_imctx->currentChartRect;
    
    // Chart computations
    f32 yMin = F32Inf;
    f32 yMax = -F32Inf;
    
    for (u32 i = 0; i < valueCount; i++) {
        f32 y = values[i];
        
        yMin = f32_min(y, yMin);
        yMax = f32_max(y, yMax);
    }
    
    if ( !f32_equals(atb.min, F32Inf) ) {
        yMin = atb.min;
    }
    
    if ( !f32_equals(atb.max, -F32Inf) ) {
        yMax = atb.max;
    }
    
    // Chart Draw
    v2f minCorner = v2f_new(rect.min.x, rect.max.y);
    v2f maxCorner = v2f_new(rect.max.x, rect.min.y);
    
    f32 xMult = ( maxCorner.x - minCorner.x );
    f32 yMult = ( maxCorner.y - minCorner.y ) / ( yMax - yMin );
    
    v2f firstPos = v2f_new( minCorner.x, ( values[0] - yMin ) * yMult + minCorner.y);
    
    for (u32 i = 1; i < valueCount; i++) {
        v2f nextPos = v2f_new( i / (f32)(valueCount - 1) * xMult + minCorner.x, ( values[i] - yMin ) * yMult + minCorner.y);
        im_draw_line(firstPos, nextPos, atb.lineWeight, atb.color);
        im_chart_plot_marker(firstPos, atb);
        firstPos = nextPos;
    }
    
    im_chart_plot_marker(firstPos, atb);
}

//- Images & Images
internal void im_image(const char* name, u32 texture, v2f dim, v4f uvs = v4f_new(0, 1, 0, 1))
{
    PROFILED_FUNC;
    
    // Computations
    im_id id = im_id_new(name);
    rect2 rect = im_layout_new_rect();
    rect.max.x = rect.min.x + dim.width;
    rect.max.y = rect.min.y + dim.height;
    
    if ( !im_layout_add_rect(rect) ) { return; }
    
    im_draw_rect_textured(rect, uvs, texture);
}

internal void im_image_inspect(const char* name, u32 texture, v2f dim, v4f uvs = v4f_new(0, 1, 0, 1))
{
    PROFILED_FUNC;
    
    // Computations
    im_id id = im_id_new(name);
    rect2 rect = im_layout_new_rect();
    rect.max.x = rect.min.x + dim.width;
    rect.max.y = rect.min.y + dim.height;
    
    if ( !im_layout_add_rect(rect) ) { return; }
    
    im_draw_rect_textured(rect, uvs, texture);
    
    if ( collisions_v2f_in_rect(g_imctx->mpos, rect) ) {
        // reference point is bottom left, not top left ( Y is flipped )
        v2f relativeMouseOffset = v2f_new((g_imctx->mpos.x - rect.min.x) / dim.width,
                                          (rect.max.y - g_imctx->mpos.y) / dim.height);
        
        f32 relativeZoom = 0.1;
        f32 aspectRatio = dim.width / dim.height;
        v4f inspectorUVs = v4f_new(relativeMouseOffset.x - relativeZoom, // u min
                                   relativeMouseOffset.x + relativeZoom, // u max
                                   relativeMouseOffset.y - relativeZoom * aspectRatio, // v min
                                   relativeMouseOffset.y + relativeZoom * aspectRatio); // v max
        
        // Draw inspector rect on previous im_image around the cursor
        v2f inspectorRectDim = v2f_times(dim, relativeZoom * 6);
        rect2 inspectorRect = rect2_from_size_and_anchor(g_imctx->mpos, inspectorRectDim, RectAnchor_TopLeft);
        
        // Inspector draw commands are sent to the defered command buffer
        im_window* w = im_current_window();
        w->defer = 1;
        im_draw_rect_filled(inspectorRect, color_black);
        im_draw_rect_textured(inspectorRect, inspectorUVs, texture);
        im_draw_rect(inspectorRect, 1, color_white);
        w->defer = 0;
    }
}

//- Tooltips / Popups
internal rect2 im_internal_floating_fit_in_window(rect2 rect)
{
    PROFILED_FUNC;
    
    rect2 result = rect;
    
    if ( rect.max.x > g_imctx->wDimensions.width ) {
        v2f dim = rect2_dim(result);
        v2f topLeft = result.min;
        rect2 withAnchorTopRight = rect2_from_size_and_anchor(topLeft, dim, RectAnchor_XRight | RectAnchor_YTop);
        if ( withAnchorTopRight.min.x > 0 ) {
            result = withAnchorTopRight;
        }
    }
    
    return result;
}

internal void im_internal_floating_begin(im_id id, v2f topLeft)
{
    PROFILED_FUNC;
    
    im_window* w = im_current_window();
    im_item_data* data = im_internal_get_item_data(id);
    
    // TODO(abe): events happening in the popup area are still processed by widgets below it
    //            add a "clip rect" where events are allowed to be processed by other widgets ?
    
    im_layout_begin(IMLayout_Detached);
    w->popupID = id;
    w->defer = 1;
    
    v2f rectDim = rect2_dim(data->rect);
    rect2 rect = rect2_from_size_and_anchor(topLeft, rectDim, RectAnchor_TopLeft);
    rect = im_internal_floating_fit_in_window(rect);
    im_draw_rect_filled(rect, g_imctx->theme.window.background);
    im_draw_rect(rect, 1, g_imctx->theme.window.border);
    
    // Set the layout cursor manually
    im_layout* tooltipLayout = im_current_layout();
    tooltipLayout->cursor = rect.min;
    tooltipLayout->rect.min = rect.min;
    tooltipLayout->rect.max = rect.min;
}

internal void im_internal_floating_end()
{
    PROFILED_FUNC;
    
    im_window* w = im_current_window();
    im_layout* tooltipLayout = im_current_layout();
    im_item_data* data = im_internal_get_item_data(w->popupID);
    
    data->rect = tooltipLayout->rect;
    
    w->defer = 0;
    w->popupID = im_id_invalid();
    im_layout_end();
}

internal inline void im_tooltip_begin(const char* name)
{
    PROFILED_FUNC;
    
    im_id id = im_id_new(name);
    im_internal_floating_begin(id, g_imctx->mpos);
}

internal inline void im_tooltip_end()
{
    PROFILED_FUNC;
    im_internal_floating_end();
}

internal inline void im_popup_open(const char* name)
{
    PROFILED_FUNC;
    
    im_id id = im_id_new(name);
    im_item_data* data = im_internal_get_item_data(id);
    
    if ( !data->open ) {
        data->rect.min = g_imctx->mpos;
        data->rect.max = g_imctx->mpos;
        data->openedThisFrame = 1;
    }
    
    data->open = 1;
}

internal inline void im_popup_close(const char* name)
{
    PROFILED_FUNC;
    
    im_id id = im_id_new(name);
    im_item_data* data = im_internal_get_item_data(id);
    
    data->open = 0;
}

internal inline bool8 im_popup_begin(const char* name)
{
    PROFILED_FUNC;
    
    bool8 result = 0;
    im_id id = im_id_new(name);
    im_item_data* data = im_internal_get_item_data(id);
    
    if ( data->openedThisFrame ) {
        data->openedThisFrame = 0;
    } else if ( g_imctx->mbLeftOnDown && !collisions_v2f_in_rect(g_imctx->mpos, data->rect) ) {
        data->open = 0;
    }
    
    if ( data->open ) {
        result = 1;
        im_internal_floating_begin(id, data->rect.min);
    }
    
    return result;
}

internal inline void im_popup_end()
{
    PROFILED_FUNC;
    im_internal_floating_end();
}

//- Text
internal void im_internal_text_with_attributes(cdl_text_atb attr, const char* text, va_list args)
{
    PROFILED_FUNC;
    
    // Computations
    rect2 rect = im_layout_new_rect();
    rect.max.y += 20;
    
    char buffer[256] = { };
    vsprintf(buffer, text, args);
    
    v2f textDim = cdl_text_get_dimensions(buffer, renderer->currentFont->asset, attr);
    textDim.height = f32_max(textDim.height, DEF_HEIGHT);
    textDim.width = f32_max(textDim.width, MIN_WIDTH);
    
    v2f textPos = v2f_add(rect.min, v2f_times(textDim, 0.5));
    rect2 textRect = rect2_from_size(textPos, textDim);
    
    if ( !im_layout_add_rect(textRect) ) { return; }
    
    im_draw_text(buffer, textPos, attr);
}

internal void im_header(const char* text, ...)
{
    PROFILED_FUNC;
    
    va_list argptr;
    va_start(argptr, text);
    
    cdl_text_atb attr = cdl_text_atb_new();
    attr.size = 20;
    attr.color = g_imctx->theme.font_header;
    im_internal_text_with_attributes(attr, text, argptr);
    
    va_end(argptr);
}

internal void im_hint(const char* text, ...)
{
    PROFILED_FUNC;
    
    va_list argptr;
    va_start(argptr, text);
    
    cdl_text_atb attr = cdl_text_atb_new();
    attr.size = 10;
    attr.color = g_imctx->theme.font_hint;
    im_internal_text_with_attributes(attr, text, argptr);
    
    va_end(argptr);
}

internal void im_text(const char* text, ...)
{
    PROFILED_FUNC;
    
    va_list argptr;
    va_start(argptr, text);
    
    cdl_text_atb attr = cdl_text_atb_new();
    attr.color = g_imctx->theme.font_default;
    im_internal_text_with_attributes(cdl_text_atb_new(), text, argptr);
    
    va_end(argptr);
}

internal void im_text_with_attributes(cdl_text_atb attr, const char* text, ...)
{
    PROFILED_FUNC;
    
    va_list argptr;
    va_start(argptr, text);
    
    im_internal_text_with_attributes(attr, text, argptr);
    
    va_end(argptr);
}

//- Menus
internal bool8 im_menu_bar_begin()
{
    im_layout* windowLayout = im_current_layout();
    im_layout_begin(IMLayout_Horizontal);
    
    im_layout* menuLayout = im_current_layout();
    menuLayout->cursor = v2f_sub(menuLayout->cursor, windowLayout->margins);
    menuLayout->margins = v2f_new(0);
    
    return 1;
}

internal void im_menu_bar_end()
{
    im_layout_end();
}

internal bool8 im_menu_begin(const char* name, IMWidgetFlag wflags = IMWidgetFlag_None)
{
    PROFILED_FUNC;
    
    // Computations
    im_id id = im_id_new(name);
    im_item_data* data = im_internal_get_item_data(id);
    rect2 rect = im_layout_new_rect();
    
    if ( !im_layout_add_rect(rect) ) { return 0; }
    
    // Logic
    bool8 pressed = false;
    if ( !FLAG_HAS(wflags, IMWidgetFlag_ReadOnly) ) {
        pressed = im_button_behaviour(id, rect);
        
        if ( pressed ) {
            data->open = !data->open;
            
            if ( data->open ) {
                data->rect.min = v2f_new(rect.min.x, rect.max.y);
                data->rect.max = data->rect.min;
            }
        }
    }
    
    // Drawing
    im_internal_draw_widget_background(id, rect, wflags);
    
    cdl_text_atb attr = cdl_text_atb_new();
    attr.lineWidth = rect2_dim(rect).width - 2;
    attr.color = g_imctx->theme.font_default;
    im_draw_text((char*)name, rect2_mid(rect), attr);
    
    // Menu Popup
    if ( data->open ) {
        im_popup_begin(name);
    }
    
    return data->open;
}

internal void im_menu_end()
{
    im_popup_end();
}

//- Layout
internal bool8 im_group_begin(const char* name, IMWidgetFlag wflags = IMWidgetFlag_None)
{
    PROFILED_FUNC;
    
    im_window* w = im_current_window();
    im_layout* l = im_current_layout();
    
    // Computations
    IMWidgetFlag drawFlags = wflags;
    im_id id = im_id_new(name);
    im_item_data* data = im_internal_get_item_data(id);
    rect2 rect = im_layout_new_rect();
    
    // Fill the entire width of the window if window is large enough
    if ( rect2_dim(w->rect).width > MIN_WIDTH ) {
        rect.max.x = w->rect.max.x - l->margins.x;
    }
    
    if ( !im_layout_add_rect(rect) ) { return 0; }
    
    // Logic
    bool8 pressed = false;
    if ( !FLAG_HAS(wflags, IMWidgetFlag_ReadOnly) ) {
        pressed = im_button_behaviour(id, rect);
        
        if ( pressed ) {
            data->toggled = !data->toggled;
        }
    }
    
    // Set UseActive flag if the group is toggled (i.e. open)
    // This is used in draw_widget_background to force the 
    // background color to theme.widget.active
    if ( data->toggled ) {
        FLAG_SET(drawFlags, IMWidgetFlag_UseActive);
    }
    
    // Drawing
    im_internal_draw_widget_background(id, rect, drawFlags);
    
    // Draw group triangle icon
    {
        v2f iconDim = v2f_new(rect2_dim(rect).y / 2);
        rect2 iconRect = rect;
        iconRect.min = v2f_add(iconRect.min, v2f_new(2));
        iconRect.max = v2f_add(iconRect.min, iconDim);
        
        v2f positions[3] = { };
        if ( data->toggled ) {
            positions[0] = iconRect.min;
            positions[1] = v2f_add(iconRect.min, v2f_new(iconDim.x, 0));
            positions[2] = v2f_add(iconRect.min, v2f_new(iconDim.x / 2, iconDim.y / 2));
        } else {
            positions[0] = iconRect.min;
            positions[1] = v2f_add(iconRect.min, v2f_new(iconDim.x / 2, iconDim.y / 2));
            positions[2] = v2f_add(iconRect.min, v2f_new(0, iconDim.y));
        }
        
        im_draw_triangle(positions, color_white);
    }
    
    cdl_text_atb attr = cdl_text_atb_new();
    attr.lineWidth = rect2_dim(rect).width - 2;
    attr.color = g_imctx->theme.font_default;
    im_draw_text((char*)name, rect2_mid(rect), attr);
    
    // If group is open
    if ( data->toggled ) {
        
        // If there is already a groupID we "save" it
        if ( !im_id_is_invalid(w->groupID) ) {
            im_id parentGroupID = w->groupID;
            im_item_data* parentData = im_internal_get_item_data(parentGroupID);
            data->parent = parentData;
        }
        
        w->groupID = id;
        
        // Group line
        data->groupLineBegin = l->cursor;
        data->groupLineBegin.x += l->margins.x;
        
        // Indent the cursor
        im_layout_indent();
        
        im_id_group_begin(name);
    }
    
    return data->toggled;
}

internal void im_group_end()
{
    im_window* w = im_current_window();
    im_layout* l = im_current_layout();
    im_item_data* data = im_internal_get_item_data(w->groupID);
    
    // Put cursor back on previous indentation level
    im_layout_unindent();
    
    // Get end of the group line
    v2f groupLineEnd = l->cursor;
    groupLineEnd.x += l->margins.x;
    im_draw_line(data->groupLineBegin, groupLineEnd, 1, g_imctx->theme.widget.border);
    
    // Set group id depending on wether or not we are in a nested group
    if ( data->parent ) {
        w->groupID = data->parent->id;
    } else {
        w->groupID = im_id_invalid();
    }
    
    im_id_group_end();
}

internal void im_separator()
{
    PROFILED_FUNC;
    
    im_window* w = im_current_window();
    
    // Computations
    rect2 rect = im_layout_new_rect();
    v2f cursor = rect.min;
    cursor.x = w->rect.min.x;
    v2f dim = v2f_new(rect2_dim(w->rect).width, 1);
    v2f pos = v2f_add(cursor, v2f_times(dim, 0.5));
    rect = rect2_from_size(pos, dim);
    
    if ( !im_layout_add_rect(rect) ) { return; }
    
    im_draw_rect_filled(rect, g_imctx->theme.widget.border);
}

//- [SECTION] Helpers
internal inline im_window* im_current_window()
{
    return g_imctx->currentWindow;
}

internal inline im_layout* im_current_layout()
{
    return g_imctx->currentLayout;
}

internal inline rect2 im_get_last_rect()
{
    return im_current_window()->lastRect;
}

internal inline bool8 im_last_is_hovered()
{
    return im_id_equal(g_imctx->nextHot, im_current_window()->lastWidgetID);
}