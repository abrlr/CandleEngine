internal cdl_text_atb cdl_text_atb_new()
{
    PROFILED_FUNC;
    
    cdl_text_atb result = ZeroStruct;
    
    // Set default values
    result.anchor = TextAnchor_Centered;
    result.style = TextStyle_None;
    
    result.size = 12.f;
    result.lineWidth = F32Inf;
    result.color = color_white;
    result.background = color_black;
    
    return result;
}

internal inline cdl_text_cursor cdl_text_cursor_new(cdl_text_atb attributes)
{
    cdl_text_cursor result = ZeroStruct;
    result.current = v2f_new(attributes.hMargin, 0);
    result.max = v2f_new(result.current.x, attributes.size);
    
    result.attributes = attributes;
    result.lineCount = 1;
    
    return result;
}

internal inline f32 cdl_text_internal_line_width(cdl_text_cursor* cursor)
{
    return cursor->current.x;
}

internal inline f32 cdl_text_internal_max_line_width(cdl_text_cursor* cursor)
{
    return cursor->max.x;
}

internal void cdl_text_internal_new_line(cdl_text_cursor* cursor)
{
    f32 lineWidth = cdl_text_internal_line_width(cursor);
    f32 maxLineWidth = cdl_text_internal_max_line_width(cursor);
    
    if ( maxLineWidth < lineWidth ) {
        cursor->max.x = cursor->current.x + cursor->attributes.hMargin;
    }
    
    cursor->current.x = cursor->attributes.hMargin;
    cursor->current.y += cursor->attributes.size;
    cursor->lineCount++;
}

internal void cdl_text_internal_advance_cursor(cdl_text_cursor* cursor, f32 advance)
{
    // Add margin at the end
    f32 lineWidthWithNextChar = cdl_text_internal_line_width(cursor) + advance + cursor->attributes.hMargin;
    
    // If the width is over the max line width
    if ( lineWidthWithNextChar > cursor->attributes.lineWidth ) {
        // TODO(abe): this should backtrack to previous whitespace character
        // if it exists and add the newline at this place in order to avoid cutting words
        cdl_text_internal_new_line(cursor);
    }
    
    cursor->current.x += advance;
}

internal bool8 cdl_text_internal_process_char(char* c, cdl_assets_font* font, cdl_text_cursor* cursor)
{
    bool8 continueProcessing = 1;
    cdl_assets_glyph* charMap = font->glyphs;
    
    switch (*c) {
        case '\r':
        {
        } break;
        
        case '\n':
        {
            if ( *(c + 1) == '\0' ) {
                continueProcessing = 0;
            } else {
                cdl_text_internal_new_line(cursor);
            }
        } break;
        
        case '\t':
        {
            i32 asciiId = i32_clamp(' ', 0, 125);
            f32 advance = charMap[asciiId].xadvance * cursor->attributes.size / font->glyphSize;
            cdl_text_internal_advance_cursor(cursor, advance * 4);
        } break;
        
        case '\0':
        {
            continueProcessing = 0;
        } break;
        
        default:
        {
            // Character width
            i32 asciiId = i32_clamp(*c, 0, 125);
            f32 advance = charMap[asciiId].xadvance * cursor->attributes.size / font->glyphSize;
            cdl_text_internal_advance_cursor(cursor, advance);
        } break;
    };
    
    return continueProcessing;
}

internal f32 cdl_text_get_line_width(char* text, cdl_assets_font* font, cdl_text_atb attributes)
{
    PROFILED_FUNC;
    
    f32 result = 0;
    if ( *text != '\n' ) {
        cdl_text_cursor cursor = cdl_text_cursor_new(attributes);
        
        // Loop over each character
        char* c = text;
        while ( cdl_text_internal_process_char(c, font, &cursor) && ( *c != '\n') ) {
            c++;
        }
        
        // If the text fit on one line, cursor.max.x is never updated
        if ( cursor.max.x < cursor.current.x ) {
            cursor.max.x = cursor.current.x + attributes.hMargin;
        }
        
        result = cursor.max.x;
    }
    
    return result;
}

internal v2f cdl_text_get_dimensions(char* text, cdl_assets_font* font, cdl_text_atb attributes)
{
    // NOTE(abe): this function doesn't take into account the TextStyle flags
    // because they do not matter when computing the text boundary box dimensions
    
    PROFILED_FUNC;
    
    u32 length = StringLength(text);
    v2f result = ZeroStruct;
    
    if ( length > 0 ) {
        
        cdl_text_cursor cursor = cdl_text_cursor_new(attributes);
        
        // Loop over each character
        char* c = text;
        while ( cdl_text_internal_process_char(c, font, &cursor) ) {
            c++;
        }
        
        // If the text fit on one line, cursor.max.x is never updated
        if ( cursor.max.x < cursor.current.x ) {
            cursor.max.x = cursor.current.x + attributes.hMargin;
        }
        cursor.max.y = cursor.lineCount * attributes.size;
        
        result = cursor.max;
    }
    
    return result;
}