internal str8 GetStringFromArray(int id, char** array, int arraySize)
{
    PROFILED_FUNC;
    
    str8 result;
    if ( id >= 0 && id < arraySize ) {
        result.string = array[id];
    } else {
        result.string = "Undefined";
    }
    result.size = StringLength(result.string);
    return result;
}

internal str8 cdl_engine_controller_button_get_name(ControllerButton button)
{
    PROFILED_FUNC;
    
    local char* buttonNames[ControllerButton_Count] = {
#define controllerbutton(key, str) str,
#include "controller_buttons.def"
#undef controllerbutton
    };
    
    str8 result = GetStringFromArray(button, buttonNames, ControllerButton_Count);
    return result;
}

internal bool8 cdl_engine_controller_button_get(int controllerID, ControllerButton button)
{
    PROFILED_FUNC;
    
    return core->events.currentControllers[controllerID].buttons[button];
}

internal bool8 cdl_engine_controller_button_on_down(int controllerID, ControllerButton button)
{
    PROFILED_FUNC;
    
    return core->events.currentControllers[controllerID].buttons[button] 
        && !core->events.previousControllers[controllerID].buttons[button];
}

internal bool8 cdl_engine_controller_button_on_up(int controllerID, ControllerButton button)
{
    PROFILED_FUNC;
    
    return !core->events.currentControllers[controllerID].buttons[button] 
        && core->events.previousControllers[controllerID].buttons[button];
}

internal str8 cdl_engine_controller_axis_get_name(ControllerAxes axes)
{
    PROFILED_FUNC;
    
    local char* axesNames[ControllerAxes_Count] = {
#define controlleraxes(axes, str) str,
#include "controller_axes.def"
#undef controlleraxes
    };
    
    str8 result = GetStringFromArray( (i32)axes, axesNames, ControllerAxes_Count);
    return result;
}

internal f32 cdl_engine_controller_axis_get(int controllerID, ControllerAxes axes)
{
    PROFILED_FUNC;
    
    // TODO(abe): threshold should be a user defined value
    f32 axisvalue = core->events.currentControllers[controllerID].axes[axes];
    if ( axisvalue < 0.2f && axisvalue > -0.2f ) {
        axisvalue = 0.f;
    }
    return axisvalue;
}

internal str8 cdl_engine_keyboard_key_get_name(KeyboardKey key)
{
    PROFILED_FUNC;
    
    local char* keyNames[KeyboardKey_Count] = {
#define kkey(key, str) str,
#include "keyboard_keys.def"
#undef kkey
    };
    
    str8 result = GetStringFromArray(key, keyNames, KeyboardKey_Count);
    return result;
}

internal bool8 cdl_engine_keyboard_key_get(KeyboardKey key)
{
    PROFILED_FUNC;
    return core->events.currentKeyboard.keys[key];
}

internal bool8 cdl_engine_keyboard_key_on_down(KeyboardKey key)
{
    PROFILED_FUNC;
    return core->events.currentKeyboard.keys[key] 
        && !core->events.previousKeyboard.keys[key];
}

internal bool8 cdl_engine_keyboard_key_on_up(KeyboardKey key)
{
    PROFILED_FUNC;
    return !core->events.currentKeyboard.keys[key] 
        && core->events.previousKeyboard.keys[key];
}

internal bool8 cdl_engine_keyboard_key_any()
{
    PROFILED_FUNC;
    bool8 mc = MemoryCompare(core->events.currentKeyboard.keys, core->events.previousKeyboard.keys, sizeof(cdl_engine_keyboard));
    return ( !mc );
}

internal u32 cdl_engine_process_text_input(str8* text, u32 capacity)
{
    PROFILED_FUNC;
    
    // NOTE(abe): this function process the keyboard character registered for a given
    // frame and add them to the text buffer, increasing its size, as well as the special cases
    // such as BACKSPACE, ENTER
    
    // this function also take as input the text.string buffer capacity (!= length of the string)
    // and returns the new capacity of the input buffer (changed only if needed)
    
    // Check whether or not some characters have been added this frame
    u32 resultCapacity = capacity;
    u32 charEnteredThisFrame = 0;
    for (i32 i = 0; i < core->events.currentKeyboard.numberOfChar; i++) {
        charEnteredThisFrame++;
    }
    
    if ( keyboard_key_get(KEY_ENTER) ) {
        charEnteredThisFrame++;
    }
    
    // If chars have been added
    if ( charEnteredThisFrame > 0 ) {
        // If the buffer has not enough capacity, double it
        if ( !(text->size + charEnteredThisFrame < capacity ) ) {
            char* oldBuffer = text->string;
            TranAllocArray(&text->string, "Text Input Processing", char, 2 * capacity);
            MemoryCopy(text->string, oldBuffer, capacity);
            text->string[capacity] = '\0';
            resultCapacity = 2 * capacity;
            TranRelease(oldBuffer);
        }
        
        // Add the new chars to the buffer
        for (i32 i = 0; i < core->events.currentKeyboard.numberOfChar; i++) {
            text->string[text->size++] = core->events.currentKeyboard.text[i];
        }
        text->string[text->size] = '\0';
    }
    
    // If backspace was presed
    if ( keyboard_key_get(KEY_BACKSPACE) && text->size > 0 ) {
        // TODO(abe): this is not very responsive, use a better technique instead
        local u32 initialFrameDelay = 2;
        local u32 frameDelay = initialFrameDelay;
        frameDelay--;
        if ( frameDelay == 0 ) {
            text->string[text->size - 1] = '\0';
            text->size--;
            frameDelay = initialFrameDelay;
        }
    }
    
    // If enter was pressed and there are already chars in the buffer
    if ( keyboard_key_get(KEY_ENTER) && text->size > 0 ) {
        local u32 initialFrameDelay = 5;
        local u32 frameDelay = initialFrameDelay;
        frameDelay--;
        if ( frameDelay == 0 ) {
            text->string[text->size++] = '\n';
            text->string[text->size] = '\0';
            frameDelay = initialFrameDelay;
        }
    }
    
    return resultCapacity;
}

internal str8 cdl_engine_mouse_button_get_name(MouseButton button)
{
    PROFILED_FUNC;
    
    local char* mouseButtonNames[MouseButton_Count] = {
#define mousebutton(button, str) str,
#include "mouse_buttons.def"
#undef mousebutton
    };
    
    str8 result = GetStringFromArray(button, mouseButtonNames, MouseButton_Count);
    return result;
}

internal bool8 cdl_engine_mouse_button_get(MouseButton button)
{
    PROFILED_FUNC;
    return core->events.currentMouse.buttons[button];
}

internal bool8 cdl_engine_mouse_button_on_down(MouseButton button)
{
    PROFILED_FUNC;
    return core->events.currentMouse.buttons[button]
        && !core->events.previousMouse.buttons[button];
}

internal bool8 cdl_engine_mouse_button_on_up(MouseButton button)
{
    PROFILED_FUNC;
    return !core->events.currentMouse.buttons[button]
        && core->events.previousMouse.buttons[button];
}

internal v2i cdl_engine_mouse_get_raw_position()
{
    PROFILED_FUNC;
    return core->events.currentMouse.position;
}

/*
internal v2i __cdl_engine_internal_mouse_position_in_viewport()
{
    v2i framePadding = { core->events.frameBorderLeft, core->events.frameBorderTop };
    v2i mousePosPadded = v2i_sub(core->events.currentMouse.position, framePadding);
    
    v2i frameOffset = cdl_v2ftoi(cdl_renderer_get_view()->viewport.min);
    v2i mousePosWithOffset = v2i_sub(mousePosPadded, frameOffset);
    
    v2i frameDim = cdl_v2ftoi(cdl_rect2_dim(cdl_renderer_get_view()->viewport));
    v2i mousePosInViewport = cdl_v2ftoi(cdl_v2f_new(core->events.frameWidth * mousePosWithOffset.x / (f32)frameDim.x, core->events.frameHeight * mousePosWithOffset.y / (f32)frameDim.y));
    
    return mousePosInViewport;
}
*/

internal v2i cdl_engine_mouse_get_position()
{
    PROFILED_FUNC;
    
    v2i framePadding = { core->frameBorderLeft, core->frameBorderTop };
    v2i mousePosPadded = v2i_sub(core->events.currentMouse.position, framePadding);
    
    return mousePosPadded;
}

internal v2i cdl_engine_mouse_get_window_position()
{
    PROFILED_FUNC;
    
    v2i framePadding = { core->frameBorderLeft, core->frameBorderTop };
    return v2i_sub(core->events.currentMouse.position, framePadding);
}

internal v2f cdl_engine_mouse_get_gl_position()
{
    PROFILED_FUNC;
    
    v2i position = core->events.currentMouse.position;
    v2f result = {
        ( position.x * 2 - core->frameWidth - core->frameBorderLeft * 2 ) / (f32)core->frameWidth,
        ( core->frameHeight - position.y * 2 + core->frameBorderTop * 2 ) / (f32)core->frameHeight
    };
    return result;
}

internal v2f cdl_engine_mouse_get_scroll()
{
    PROFILED_FUNC;
    return core->events.currentMouse.scroll;
}

internal v2f cdl_engine_mouse_get_delta()
{
    PROFILED_FUNC;
    return core->events.currentMouse.delta;
}

internal bool8 cdl_engine_platform_event_happened(PlatformEventType eventType)
{
    PROFILED_FUNC;
    
    for (u32 i = 0; i < core->events.count; i++) {
        cdl_engine_platform_event* pe = core->events.queue + i;
        if ( pe->type == eventType && !pe->handled ) return 1;
    }
    return 0;
}