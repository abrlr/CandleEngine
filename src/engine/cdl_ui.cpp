//- [SECTION] UI Themes

global cdl_ui_theme __ui_themes[UITheme_Count] = {
    _ui_theme,
    _ui_theme_danger
};

global cdl_ui_theme __previousTheme;
global cdl_ui_theme g_uiTheme = __ui_themes[UITheme_Base];

internal void cdl_ui_theme_set(UITheme theme)
{
    PROFILED_FUNC;
    __previousTheme = g_uiTheme;
    g_uiTheme = __ui_themes[theme];
}

internal void cdl_ui_theme_reset()
{
    PROFILED_FUNC;
    g_uiTheme = __previousTheme;
}


//- [SECTION] UI Widget Attributes
internal cdl_ui_widget_attributes cdl_ui_widget_attributes_new()
{
    PROFILED_FUNC;
    
    cdl_ui_widget_attributes result = ZeroStruct;
    result.flags = UIWidgetFlags_None;
    result.layout = UIWidgetLayout_None;
    result.theme = UITheme_Base;
    result.constraints = ZeroStruct;
    return result;
}

//- [SECTION] UI Widget Behaviour Functions

internal void cdl_ui_set_textarea_text(cdl_ui_widget* w, char* newText)
{
    PROFILED_FUNC;
    
    u32 textSize = StringLength(newText);
    w->text.size = textSize;
    if ( textSize > w->capacity ) {
        TranRelease(w->text.string);
        w->capacity *= 2;
        TranAllocArray(&w->text.string, "Text Area Alloc", char, w->capacity);
    }
    MemoryZero(w->text.string, w->capacity * sizeof(char));
    MemoryCopy(w->text.string, newText, textSize);
}

internal void cdl_ui_radiobutton_group(cdl_ui_widget* first, cdl_ui_widget* last)
{
    PROFILED_FUNC;
    
    cdl_ui_widget* w = first;
    while (w && w <= last) {
        w->firstOfGroup = first;
        w->lastOfGroup = last;
        w = w->next;
    }
    
    last->firstOfGroup = first;
    last->lastOfGroup = last;
}

internal void cdl_ui_group(cdl_ui_widget* groupWidget, cdl_ui_widget* first, cdl_ui_widget* last)
{
    PROFILED_FUNC;
    
    groupWidget->firstOfGroup = first;
    groupWidget->lastOfGroup = last;
    
    for ( cdl_ui_widget* w = first; w; w = w->next) {
        w->hide = 1;
        if ( w == last ) {
            break;
        }
    }
}

internal void __cdl_ui_internal_toggle_group(cdl_ui_widget* groupWidget)
{
    PROFILED_FUNC;
    
    bool8 hide = !groupWidget->toggled;
    
    for ( cdl_ui_widget* w = groupWidget->firstOfGroup; w; w = w->next) {
        w->hide = hide;
        
        if ( groupWidget->toggled && w->type == UIWidgetType_Group ) {
            __cdl_ui_internal_toggle_group(w);
            w = w->lastOfGroup;
        }
        
        if ( w == groupWidget->lastOfGroup ) {
            break;
        }
    }
    
}

//- [SECTION] UI Widgets

internal void __cdl_ui_internal_widget_init_dimensions(cdl_ui_widget* w)
{
    PROFILED_FUNC;
    
    rect2 parentRect = ZeroStruct;
    v2f parentDim = ZeroStruct;
    
    rect2 rect = ZeroStruct;
    v2f midpoint = ZeroStruct;
    v2f size = rect2_dim(w->rect);
    
    cdl_ui_widget_attributes attributes = w->attributes;
    cdl_ui_constraints constraints = attributes.constraints;
    
    //- Compute the offset in the parent widget if needed 
    if ( w->parent ) {
        parentRect = w->parent->rect;
    } else {
        parentRect = rect2_new(v2f_new(0, 0), v2f_new(core->frameWidth, core->frameHeight));
    }
    parentDim = rect2_dim(parentRect);
    
    if ( w->parent && (w->parent->attributes.layout != UIWidgetLayout_None) && w->parent->child ) {
        cdl_ui_widget* c = w->parent->child;
        
        while ( c && c != w ) {
            v2f childSize = v2f_add(rect2_dim(c->rect), c->marginsDim);
            
            switch (w->parent->attributes.layout) {
                case UIWidgetLayout_Horizontal:
                {
                    parentDim.width -= childSize.width;
                } break;
                
                case UIWidgetLayout_Vertical:
                default:
                {
                    parentDim.height -= childSize.height;
                } break;
            }
            
            c = c->next;
        }
    }
    
    //- Size constraints
    if ( !w->sizeOverride ) {
        // X scale
        if ( constraints.type & UIWidgetConstraint_SizeFixedX ) {
            size.x = constraints.size.x;
        }
        
        else if ( constraints.type & UIWidgetConstraint_SizeRelativeX ) {
            size.x = constraints.size.x * rect2_dim(parentRect).x;
        }
        
        else {
            size.x = parentDim.x;
        }
        
        // Y scale
        if ( constraints.type & UIWidgetConstraint_SizeFixedY ) {
            size.y = constraints.size.y;
        }
        
        else if ( constraints.type & UIWidgetConstraint_SizeRelativeY ) {
            size.y = constraints.size.y * rect2_dim(parentRect).y;
        }
        
        else {
            size.y = parentDim.y;
        }
        
        // Hard Constraints
        if ( constraints.type & UIWidgetConstraint_SizeByAspect_HardX ) {
            size.x = constraints.size.x * parentDim.x;
            size.y = constraints.aspect * size.x;
        }
    }
    
    rect = rect2_from_size(midpoint, size);
    rect = rect2_translate(rect, parentRect.min);
    w->rect = rect;
    w->marginsDim = v2f_new(0);
}

internal void __cdl_ui_internal_widget_update_position(cdl_ui_widget* w)
{
    PROFILED_FUNC;
    
    rect2 parentRect = ZeroStruct;
    v2f parentDim = ZeroStruct;
    
    rect2 rect = w->rect;
    v2f midpoint = ZeroStruct;
    v2f size = rect2_dim(w->rect);
    v2f offsetInParent = ZeroStruct;
    
    cdl_ui_widget_attributes attributes = w->attributes;
    cdl_ui_constraints constraints = attributes.constraints;
    
    //- Compute the offset in the parent widget if needed 
    if ( w->parent ) {
        parentRect = w->parent->rect;
    } else {
        parentRect = rect2_new(v2f_new(0, 0), v2f_new(core->frameWidth, core->frameHeight));
    }
    
    parentDim = rect2_dim(parentRect);
    
    //- Position constraints
    
    // X position
    if ( constraints.type & UIWidgetConstraint_PosFixedLeft ) {
        rect.min.x = parentRect.min.x + constraints.positions.left;
        rect.max.x = rect.min.x + size.x;
    }
    
    else if ( constraints.type & UIWidgetConstraint_PosFixedRight ) {
        offsetInParent.x = parentDim.x - constraints.positions.right - size.x * 0.5f;
    }
    
    else if ( constraints.type & UIWidgetConstraint_PosRelativeLeft ) {
        offsetInParent.x = constraints.positions.left * parentDim.x;
    }  
    // TODO(abe): test relative right & bottom constraints, not sure if they work
    else if ( constraints.type & UIWidgetConstraint_PosRelativeRight ) {
        offsetInParent.x = parentDim.x * ( 1.f - constraints.positions.right ) - size.x * 0.5f;
    } 
    
    else {
        // Default X position: PosFixedLeft = 0
        rect.min.x = parentRect.min.x;
        rect.max.x = rect.min.x + size.x;
    }
    
    // Y position
    if ( constraints.type & UIWidgetConstraint_PosFixedTop ) {
        rect.min.y = parentRect.min.y + constraints.positions.top;
        rect.max.y = rect.min.y + size.y;
    }
    
    else if ( constraints.type & UIWidgetConstraint_PosFixedBottom ) {
        offsetInParent.y = parentDim.y - constraints.positions.bottom - size.y * 0.5f;
    }
    
    else if ( constraints.type & UIWidgetConstraint_PosRelativeTop ) {
        offsetInParent.y = constraints.positions.top * parentDim.y;
    }
    
    else if ( constraints.type & UIWidgetConstraint_PosRelativeBottom ) {
        offsetInParent.y = parentDim.y * ( 1.f - constraints.positions.bottom ) - size.y * 0.5f;
    }
    
    else {
        // Default Y Position: PosFixedTop = 0
        rect.min.y = parentRect.min.y;
        rect.max.y = rect.min.y + size.y;
    }
    
    //- Margin constraints
    {
        // X axis
        if ( constraints.type & UIWidgetConstraint_MarginFixedLeft ) {
            rect.min.x += constraints.margins.left;
            rect.max.x += constraints.margins.left;
            w->marginsDim.x += constraints.margins.left;
        } 
        
        else if ( constraints.type & UIWidgetConstraint_MarginFixedRight ) {
            rect.min.x -= constraints.margins.right;
            rect.max.x -= constraints.margins.right;
            w->marginsDim.x += constraints.margins.right;
        }
        
        else if ( constraints.type & UIWidgetConstraint_MarginRelativeLeft ) {
            rect.min.x += parentDim.x * constraints.margins.left;
            rect.max.x += parentDim.x * constraints.margins.left;
            w->marginsDim.x += parentDim.x * constraints.margins.left;;
        }
        
        else if ( constraints.type & UIWidgetConstraint_MarginRelativeRight ) {
            rect.min.x -= parentDim.x * constraints.margins.right;
            rect.max.x -= parentDim.x * constraints.margins.right;
            w->marginsDim.x += parentDim.x * constraints.margins.right;;
        }
        
        // Y axis
        if ( constraints.type & UIWidgetConstraint_MarginFixedTop ) {
            rect.min.y += constraints.margins.top;
            rect.max.y += constraints.margins.top;
            w->marginsDim.y += constraints.margins.top;
        }
        
        else if ( constraints.type & UIWidgetConstraint_MarginFixedBottom ) {
            rect.min.y -= constraints.margins.bottom;
            rect.max.y -= constraints.margins.bottom;
            w->marginsDim.y += constraints.margins.bottom;
        }
        
        else if ( constraints.type & UIWidgetConstraint_MarginRelativeTop ) {
            rect.min.y += parentDim.y * constraints.margins.top;
            rect.max.y += parentDim.y * constraints.margins.top;
            w->marginsDim.y += parentDim.y * constraints.margins.top;;
        }
        
        else if ( constraints.type & UIWidgetConstraint_MarginRelativeBottom ) {
            rect.min.y -= parentDim.y * constraints.margins.bottom;
            rect.max.y -= parentDim.y * constraints.margins.bottom;
            w->marginsDim.y += parentDim.y * constraints.margins.bottom;;
        }
    }
    
    
    //- Layout
    UIWidgetFlags flags = w->attributes.flags;
    if ( !(flags & UIWidgetFlags_AbsolutePosition) ) {
        cdl_ui_widget* previous = w->previous;
        while ( previous ) {
            if ( previous->hide ) {
                previous = previous->previous;
                continue;
            }
            
            UIWidgetFlags previousFlags = previous->attributes.flags;
            UIWidgetLayout parentLayout = w->parent->attributes.layout;
            if ( !(previousFlags & UIWidgetFlags_AbsolutePosition) ) {
                switch (parentLayout) {
                    case UIWidgetLayout_Vertical:
                    {
                        offsetInParent.y = previous->rect.max.y - w->parent->rect.min.y;
                    } break;
                    
                    case UIWidgetLayout_Horizontal:
                    {
                        offsetInParent.x = previous->rect.max.x - w->parent->rect.min.x;
                    } break;
                    
                    default:
                    {
                        //offsetInParent = v2f_add(offsetInParent, v2f_new(50, 50));
                        //assert(0); // Layout is not supported
                    } break;
                }
                
            }
            
            break;
        }
    }
    
    //- Offset by parent type
    if ( w->parent
        && !w->parent->hide
        && w->parent->child == w ) {
        switch ( w->parent->type ) {
            
            case UIWidgetType_Dropdown:
            {
                offsetInParent.y += rect2_dim(w->parent->rect).y;
            } break;
            
            default:
            {
                
            } break;
        }
    }
    
    rect = rect2_translate(rect, offsetInParent);
    w->rect = rect;
}

internal void __cdl_ui_internal_update_rect(cdl_ui_widget* w)
{
    PROFILED_FUNC;
    
    //- Compute initial dimensions
    __cdl_ui_internal_widget_init_dimensions(w);
    
    //- Update position
    __cdl_ui_internal_widget_update_position(w);
    
    //- Update the rect for child & next elements
    if ( w->child ) {
        __cdl_ui_internal_update_rect(w->child);
    }
    
    if ( w->next ) {
        __cdl_ui_internal_update_rect(w->next);
    }
    
    w->dirty = 0;
}

internal void __cdl_ui_internal_begin_interaction(cdl_ui* ui)
{
    PROFILED_FUNC;
    
    cdl_ui_interaction hotInteraction = ui->hotInteraction;
    cdl_ui_widget* hot = hotInteraction.w;
    
    if ( !hot ) {
        return;
    }
    
    UIWidgetFlags hotFlags = hot->attributes.flags;
    if ( hotFlags & UIWidgetFlags_Disabled ) {
        return;
    }
    
    ui->interaction.w = hot;
    
    switch (hot->type) {
        
        // Panel
        case UIWidgetType_Panel:
        {
            rect2 handleRect = hot->rect;
            handleRect.min = v2f_sub(hot->rect.max, panelResizeHandleDim);
            
            if ( hotFlags & UIWidgetFlags_Draggable ) {
                ui->interaction.type = UIInteractionType_Drag;
            }
            
            if ( hotFlags & UIWidgetFlags_Resizable ) {
                if ( collisions_v2i_in_rect(mouse_get_position(), handleRect) ) {
                    ui->interaction.type = UIInteractionType_DragSize;
                    hot->sizeOverride = 1;
                }
            }
        } break;
        
        case UIWidgetType_MenuPanel:
        {
            ui->interaction.type = UIInteractionType_Drag;
        } break;
        
        case UIWidgetType_Group:
        case UIWidgetType_Dropdown:
        case UIWidgetType_RadioButton:
        case UIWidgetType_Boolean:
        {
            ui->interaction.type = UIInteractionType_Click;
        } break;
        
        // Button
        case UIWidgetType_Button:
        {
            ui->interaction.type = UIInteractionType_Click;
            hot->toggled = 1;
        } break;
        
        case UIWidgetType_ColorPicker:
        case UIWidgetType_FloatSlider:
        {
            ui->interaction.type = UIInteractionType_Drag;
        } break;
        
        // Text Area
        case UIWidgetType_TextArea:
        {
            ui->interaction.type = UIInteractionType_Click;
            // TODO(abe): text selection via Drag ?
        } break;
        
        default:
        {
            ui->interaction.type = UIInteractionType_None;
            ui->interaction.w = 0;
        } break;
    }
    
    if ( hot->beginInteract ) {
        hot->beginInteract(hot);
    }
}

internal void __cdl_ui_internal_end_interaction(cdl_ui* ui)
{
    PROFILED_FUNC;
    
    if ( ui->interaction.type == UIInteractionType_None ) { return; }
    
    bool8 clearInteractions = 1;
    cdl_ui_widget* hot = ui->hotInteraction.w;
    
    switch(ui->interaction.type) {
        
        // Click interaction
        case UIInteractionType_Click:
        {
            switch (hot->type) {
                
                case UIWidgetType_Group:
                {
                    hot->toggled = !hot->toggled;
                    __cdl_ui_internal_toggle_group(hot);
                    hot->dirty = 1;
                    
                    if ( hot->child ) {
                        __cdl_ui_internal_update_rect(hot->child);
                    }
                    
                    if ( hot->next ) {
                        __cdl_ui_internal_update_rect(hot->next);
                    }
                } break;
                
                case UIWidgetType_Dropdown:
                {
                    hot->toggled = !hot->toggled;
                } break;
                
                case UIWidgetType_Button:
                {
                    // Button press not taken into account if the user move the mouse out of the button
                    if ( collisions_v2i_in_rect(mouse_get_position(), hot->rect) ) {
                        hot->pressed = 1;
                        if ( hot->onPressed ) {
                            hot->onPressed(hot);
                        }
                    }
                    
                    hot->toggled = 0;
                } break;
                
                case UIWidgetType_RadioButton:
                {
                    cdl_ui_widget* groupButton = hot->firstOfGroup;
                    while ( groupButton && groupButton <= hot->lastOfGroup ) {
                        groupButton->toggled = 0;
                        groupButton++;
                    }
                    hot->pressed = 1;
                    hot->toggled = 1;
                } break;
                
                case UIWidgetType_Boolean:
                {
                    hot->pressed = 1;
                    hot->toggled = !hot->toggled;
                    
                    if ( hot->onToggled ) {
                        hot->onToggled(hot);
                    }
                } break;
                
                case UIWidgetType_TextArea:
                {
                    clearInteractions = 0;
                } break;
                
                default:
                {
                    
                } break;
            } break;
        };
        
        // Drag interaction
        case UIInteractionType_Drag:
        {
        } break;
        
        default:
        {
            
        } break;
    };
    
    if ( hot->endInteract ) {
        hot->endInteract(hot);
    }
    
    if ( clearInteractions ) {
        ui->interaction.w = 0;
        ui->interaction.type = UIInteractionType_None;
    } else {
        ui->interaction.w = hot;
        ui->interaction.type = UIInteractionType_InputText;
    }
}

internal void __cdl_ui_internal_interaction_process(cdl_ui* ui)
{
    PROFILED_FUNC;
    
    // ----------- Interaction Handling -------------
    bool8 mouseLeftOnDown = mouse_button_on_down(mouse_LEFT);
    bool8 mouseLeftOnUp = mouse_button_on_up(mouse_LEFT);
    v2i mousePosition = mouse_get_position();
    v2f mouseDelta = mouse_get_delta();
    
    cdl_ui_widget* w = ui->interaction.w;
    UIInteractionType type = ui->interaction.type;
    
    if ( w && type ) {
        
        // NOTE(abe): mouse mouvement interaction
        switch(ui->interaction.type) {
            
            //- Drag interaction
            case UIInteractionType_Drag:
            {
                switch (w->type) {
                    
                    // Panel
                    case UIWidgetType_Panel: 
                    {
                        w->rect = rect2_translate(w->rect, mouseDelta);
                        
                        if ( w->child ) {
                            w->child->dirty = 1;
                        }
                        
                        if ( w->next ) {
                            w->next->dirty = 1;
                        }
                    } break;
                    
                    // Menu Panel
                    case UIWidgetType_MenuPanel:
                    {
                        cdl_ui_widget* p = w->parent;
                        p->rect = rect2_translate(p->rect, mouseDelta);
                        
                        if ( p->child ) {
                            p->child->dirty = 1;
                        }
                        
                        if ( p->next ) {
                            p->next->dirty = 1;
                        }
                        
                    } break;
                    
                    // Color Picker
                    case UIWidgetType_ColorPicker:
                    {
                        v2f position = rect2_mid(w->rect);
                        v2f dim = rect2_dim(w->rect);
                        w->picker.x = f32_clamp(mousePosition.x, position.x - dim.x / 2, position.x + dim.x / 2);
                        w->picker.y = f32_clamp(mousePosition.y, position.y - dim.y / 2, position.y + dim.y / 2);
                        
                        v2f localCoord = v2f_sub(position, w->picker);
                        f32 grayValue = ( localCoord.x + dim.x / 2.f ) / dim.x;
                        f32 redValue = ( localCoord.y + dim.y / 2.f ) / dim.y;
                        w->colorValue = v4f_new(redValue, redValue * grayValue, redValue * grayValue, 1.0f);
                        w->colorValue = cdl_rgba_adjust_hue(cdl_rgba_from_v4f_new(w->colorValue), w->fValue);
                    } break;
                    
                    // Float Slider
                    case UIWidgetType_FloatSlider:
                    {
                        f32 minX = w->rect.min.x;
                        f32 dimX = rect2_dim(w->rect).x;
                        w->fValue = ( mousePosition.x - minX ) * (w->fMax - w->fMin) / dimX;
                        w->fValue = f32_clamp(w->fValue, w->fMin, w->fMax);
                        
                        if ( w->onFloat ) {
                            w->onFloat(w);
                        }
                    } break;
                    
                    // Default
                    default: {
                    } break;
                }
            } break;
            
            //- Drag Size interaction
            case UIInteractionType_DragSize:
            {
                w->rect.max = v2f_add(w->rect.max, mouseDelta);
                w->rect.max.x = f32_clamp(w->rect.max.x, w->rect.min.x + 2 * panelResizeHandleDim.x, F32Inf);
                w->rect.max.y = f32_clamp(w->rect.max.y, w->rect.min.y + 2 * panelResizeHandleDim.y, F32Inf);
                w->sizeOverride = 1;
                
                if ( w->child ) {
                    w->child->dirty = 1;
                }
                
                if ( w->next ) {
                    w->next->dirty = 1;
                }
            } break;
            
            //- Input text interaction
            case UIInteractionType_InputText:
            {
                w->capacity = cdl_engine_process_text_input(&w->text, w->capacity);
            } break;
            
            default:
            {
                
            } break;
        }
        
        // NOTE(abe): mouse click interaction
        if ( mouseLeftOnUp ) {
            __cdl_ui_internal_end_interaction(ui);
        } else {
            // NOTE(abe): usefull to keep the event_used boolean set even if the user is dragging
            // a widget and the mouse cursor leaves its' bounds
            //core->event_used = 1;
        }
        
        // NOTE(abe): used when w has a "persistent" behavior (stay the interacting widget
        // even if the cursor is not on it (TextAreas for example) and the user wants to interact
        // with another widget without escaping the first
        if ( mouseLeftOnDown ) {
            if ( ui->nextHotInteraction.w != ui->interaction.w ) {
                ui->hotInteraction = ui->nextHotInteraction;
                __cdl_ui_internal_begin_interaction(ui);
            }
        }
        
    }
    
    // If the user is not interacting with a widget
    else {
        ui->hotInteraction = ui->nextHotInteraction;
        
        // Used when the user left click on a widget
        if ( mouseLeftOnDown ) {
            __cdl_ui_internal_begin_interaction(ui);
        }
    }
    
    if ( ui->nextHotInteraction.w && (ui->nextHotInteraction.w->attributes.flags &  UIWidgetFlags_EatInputs) ) {
        //core->event_used = 1;
    }
    
    ui->nextHotInteraction = ZeroStruct;
}

internal color8 __cdl_ui_internal_widget_color_by_state(cdl_ui_widget* w, bool8 isHotWidget)
{
    PROFILED_FUNC;
    
    cdl_ui_theme widgetTheme = __ui_themes[w->attributes.theme];
    color8 result = widgetTheme.def;
    
    if ( w->attributes.flags & UIWidgetFlags_Disabled ) {
        result = color_gray;
    } 
    
    else if ( w->toggled ) {
        result = widgetTheme.interact;
    } 
    
    else if ( isHotWidget ) {
        result.r += 20;
        result.g += 20; 
        result.b += 20;
    }
    
    return result;
}

internal void __cdl_ui_internal_draw_widget(cdl_ui* ui, cdl_ui_widget* w)
{
    assert(ui && w);
    PROFILED_FUNC;
    
    //- Update the draw rect
    if ( w->dirty ) {
        __cdl_ui_internal_update_rect(w);
    }
    
    //- Drawing next widgets
    if ( w->next ) {
        __cdl_ui_internal_draw_widget(ui, w->next);
    }
    
    if ( w->hide ) { return; }
    
    cdl_ui_theme widgetTheme = __ui_themes[w->attributes.theme];
    rect2 drawRect = w->rect;
    bool8 drawChildsIfAny = 1;
    bool8 isHotWidget = ( ui->hotInteraction.w == w );
    
    char* buffer = 0;
    FrameAllocArray(&buffer, "ui_draw_widget_text", char, 256);
    
    str8 label = ZeroStruct;
    label.string = w->label;
    label.size = StringLength(w->label);
    str8 textToDraw = label;
    
    cdl_text_atb attributes = cdl_text_atb_new();
    attributes.anchor = w->anchor;
    attributes.size = w->fontSize;
    
    //- Testing if the mouse is currently hovering the widget
    {
        bool8 mouseInDrawRect = collisions_v2i_in_rect(mouse_get_position(), drawRect);
        
        // TODO(abe):
        //- Process animations
        UIWidgetFlags wFlags = w->attributes.flags;
        
        if ( isHotWidget && (wFlags & UIWidgetFlags_Animated) ) {
            if ( w->parent ) {
                v2f animOffset = ZeroStruct;
                UIWidgetLayout parentLayout = w->parent->attributes.layout;
                
                switch ( parentLayout ) {
                    
                    case UIWidgetLayout_Horizontal:
                    {
                        animOffset.y = 5;
                    } break;
                    
                    case UIWidgetLayout_Vertical:
                    {
                        animOffset.x = 5;
                    } break;
                    
                    default:
                    {
                        
                    } break;
                    
                }
                drawRect = rect2_translate(drawRect, animOffset);
            }
        }
        
        //- Test for hovering animated widget
        bool8 mouseInAnimatedRect = collisions_v2i_in_rect(mouse_get_position(), drawRect);
        if ( mouseInDrawRect || mouseInAnimatedRect ) {
            ui->nextHotInteraction.w = w;
            ui->nextHotInteraction.type = UIInteractionType_None;
        }
    }
    
    // Widget Drawing
    {
        v2f drawRectMid = rect2_mid(drawRect);
        v2f drawRectDim = rect2_dim(drawRect);
        
        color8 color = __cdl_ui_internal_widget_color_by_state(w, isHotWidget);
        cdl_renderer_font* currentFont = renderer->currentFont;
        
        //- First Widget Draw (background)
        switch (w->type) {
            case UIWidgetType_Canvas:
            {
                cdl_render_scene_ui_push_rect(ui->scene, drawRect, cdl_rgba(0, 0, 0, 0));
            } break;
            
            case UIWidgetType_Panel:
            case UIWidgetType_MenuPanel:
            {
                cdl_render_scene_ui_push_rect(ui->scene, drawRect, widgetTheme.window_bg);
                cdl_render_scene_ui_push_rect_outline(ui->scene, drawRect, widgetTheme.hovered, 2);
            } break;
            
            case UIWidgetType_Group:
            {
                w->pressed = 0;
                
                attributes.lineWidth = f32_floor(drawRectDim.width);
                v2f textDim = cdl_text_get_dimensions(textToDraw.string, currentFont->asset, attributes);
                
                v2f iconDim = v2f_new(textDim.y - 4);
                rect2 iconRect = drawRect;
                iconRect.min = v2f_add(iconRect.min, v2f_new(2, ( drawRectDim.y - textDim.y ) / 2 + 2));
                iconRect.max = v2f_add(iconRect.min, iconDim);
                
                v2f positions[3] = { };
                if ( w->toggled ) {
                    positions[0] = iconRect.min;
                    positions[1] = v2f_add(iconRect.min, v2f_new(iconDim.x, 0));
                    positions[2] = v2f_add(iconRect.min, v2f_new(iconDim.x / 2, iconDim.y / 2));
                } else {
                    positions[0] = iconRect.min;
                    positions[1] = v2f_add(iconRect.min, v2f_new(iconDim.x / 2, iconDim.y / 2));
                    positions[2] = v2f_add(iconRect.min, v2f_new(0, iconDim.y));
                }
                
                color8 colors[] = {
                    color_white,
                    color_white,
                    color_white
                };
                
                cdl_render_scene_ui_push_rect(ui->scene, drawRect, color);
                cdl_render_scene_ui_push_triangles(ui->scene, 1, positions, colors);
                cdl_render_scene_ui_push_rect_outline(ui->scene, drawRect, widgetTheme.hovered, 2);
            } break;
            
            case UIWidgetType_Dropdown:
            {
                drawChildsIfAny = w->toggled;
                
                rect2 listBgRect = ZeroStruct;
                listBgRect.min = v2f_new(F32Inf);
                listBgRect.max = v2f_new(0);
                
                cdl_ui_widget* child = w->child;
                bool8 traverse = (child != 0);
                
                while ( traverse ) {
                    rect2 childRect = child->rect;
                    if ( listBgRect.min.x >= childRect.min.x && listBgRect.min.y >= childRect.min.y ) {
                        listBgRect.min = childRect.min;
                    }
                    
                    if ( listBgRect.max.x <= childRect.max.x && listBgRect.max.y <= childRect.max.y ) {
                        listBgRect.max = childRect.max;
                    }
                    
                    if ( child->next ) {
                        child = child->next;
                    } else {
                        traverse = 0;
                    }
                }
                
                listBgRect.min.x -= 3;
                listBgRect.max.x += 3;
                listBgRect.min.y -= 3;
                listBgRect.max.y += 3;
                
                cdl_render_scene_ui_push_rect(ui->scene, drawRect, color);
                cdl_render_scene_ui_push_rect_outline(ui->scene, drawRect, widgetTheme.hovered, 2);
                
                if ( w->toggled ) {
                    cdl_render_scene_ui_push_rect(ui->scene, listBgRect, widgetTheme.def);
                }
            } break;
            
            case UIWidgetType_Button:
            {
                w->pressed = 0;
                
                cdl_render_scene_ui_push_rect(ui->scene, drawRect, color);
                cdl_render_scene_ui_push_rect_outline(ui->scene, drawRect, widgetTheme.hovered, 2);
            } break;
            
            case UIWidgetType_RadioButton:
            {
                color8 bColor = __cdl_ui_internal_widget_color_by_state(w, isHotWidget);
                w->pressed = 0;
                
                rect2 iconRect = drawRect;
                iconRect.min = v2f_add(iconRect.min, v2f_new(4.f, ( drawRectDim.y - attributes.size ) / 2.f + 1));
                iconRect.max = v2f_add(iconRect.min, v2f_new(attributes.size, attributes.size));
                
                v2f iconCenter = rect2_mid(iconRect);
                v2f iconDim = rect2_dim(iconRect);
                
                cdl_render_scene_ui_push_quad(ui->scene, drawRectMid, drawRectDim, bColor);
                
                if ( wp_toggled(w) ) {
                    cdl_render_scene_ui_push_circle(ui->scene, iconCenter, iconDim.x / 2.f - 1.f, 0, color_white);
                } else {
                    cdl_render_scene_ui_push_circle(ui->scene, iconCenter, iconDim.x / 2.f - 1.f, 1.5f, color_white);
                }
                
                cdl_render_scene_ui_push_rect_outline(ui->scene, drawRect, widgetTheme.hovered, 2);
            } break;
            
            case UIWidgetType_Boolean:
            {
                color8 bColor = __cdl_ui_internal_widget_color_by_state(w, isHotWidget);
                w->pressed = 0;
                
                rect2 iconRect = drawRect;
                iconRect.min = v2f_add(iconRect.min, v2f_new(2, ( drawRectDim.y - attributes.size ) / 2));
                iconRect.max = v2f_add(iconRect.min, v2f_new(attributes.size, attributes.size));
                
                v2f iconCenter = rect2_mid(iconRect);
                v2f iconDim = rect2_dim(iconRect);
                
                cdl_assets_texture* squareIcon = cdl_assets_texture_get(ASSET_ICON(square));
                cdl_renderer_texture* squareTexture = cdl_renderer_texture_get(squareIcon);
                i32 texture = cdl_renderer_texture_push(ui->scene, squareTexture);
                
                cdl_render_scene_ui_push_rect(ui->scene, drawRect, bColor);
                cdl_render_scene_ui_push_quad_sdf(ui->scene, iconCenter, iconDim, v4f_new(0, 1, 0, 1), texture);
                
                if ( wp_toggled(w) ) {
                    cdl_assets_texture* checkIcon = cdl_assets_texture_get(ASSET_ICON(check));
                    cdl_renderer_texture* checkTexture = cdl_renderer_texture_get(checkIcon);
                    texture = cdl_renderer_texture_push(ui->scene, checkTexture);
                    
                    iconDim = v2f_times(iconDim, 0.7);
                    cdl_render_scene_ui_push_quad_sdf(ui->scene, iconCenter, iconDim, v4f_new(0, 1, 0, 1), texture);
                }
                
                cdl_render_scene_ui_push_rect_outline(ui->scene, drawRect, widgetTheme.hovered, 2);
            } break;
            
            case UIWidgetType_ColorPicker:
            {
                cdl_render_scene_ui_push_quad_color_picker(ui->scene, drawRectMid, drawRectDim, w->fValue);
                cdl_render_scene_ui_push_rect_outline(ui->scene, drawRect, widgetTheme.hovered, 2);
                
                if ( !v2f_is_zero(w->picker) ) {
                    u32 pointSize = 5;
                    if ( isHotWidget ) {
                        pointSize = 25;
                    }
                    
                    cdl_render_scene_ui_push_point(ui->scene, w->picker, pointSize, cdl_rgba_from_v4f_new(w->colorValue));
                    cdl_render_scene_ui_push_rect_outline(ui->scene, rect2_from_size(w->picker, v2f_new(pointSize + 1, pointSize + 1)), color_white, 2);
                }
            } break;
            
            case UIWidgetType_FloatSlider:
            {
                rect2 subRect = drawRect;
                
                v2f subRectDim = rect2_dim(subRect);
                subRect.max.x = subRect.min.x + subRectDim.width * ( w->fValue - w->fMin ) / ( w->fMax - w->fMin );
                
                rect2 handleRect = subRect;
                handleRect.min.x = handleRect.max.x - 3;
                handleRect.max.x += 3;
                
                textToDraw.string = buffer;
                str8 fValueString = ZeroStruct;
                
                char fValueBuffer[16] = { };
                sprintf(fValueBuffer, " %.2f", w->fValue);
                fValueString.string = fValueBuffer;
                fValueString.size = StringLength(fValueBuffer);
                ConcatStr8(label, fValueString, &textToDraw);
                
                cdl_render_scene_ui_push_rect(ui->scene, drawRect, color);
                cdl_render_scene_ui_push_rect_outline(ui->scene, drawRect, widgetTheme.hovered, 2);
                cdl_render_scene_ui_push_rect(ui->scene, subRect, widgetTheme.def);
                cdl_render_scene_ui_push_rect(ui->scene, handleRect, color_gray);
            } break;
            
            case UIWidgetType_Label:
            {
                cdl_render_scene_ui_push_rect_outline(ui->scene, drawRect, widgetTheme.hovered, 2);
            } break;
            
            case UIWidgetType_TextArea:
            {
                textToDraw = w->text;
                
                color = widgetTheme.def;
                if ( w->attributes.flags & UIWidgetFlags_Disabled ) {
                    color = color_gray;
                } else if ( isHotWidget ) {
                    color.r += 2;
                    color.g += 2; 
                    color.b += 2;
                }
                
                
                cdl_render_scene_ui_push_rect(ui->scene, drawRect, color);
                cdl_render_scene_ui_push_rect_outline(ui->scene, drawRect, widgetTheme.hovered, 2);
            } break;
            
            case UIWidgetType_TextureTarget:
            {
                if ( w->texture ) {
                    cdl_render_scene_ui_push_rect(ui->scene, drawRect, color_black);
                    
                    u32 texture = cdl_renderer_texture_push(ui->scene, w->texture);
                    f32 targetAspect = 0;
                    
                    if ( w->texture->asset ) {
                        targetAspect = w->texture->asset->width / (f32)w->texture->asset->height;
                    } else {
                        targetAspect = cdl_renderer_aspect_ratio();
                    }
                    
                    v2f targetDim = ZeroStruct;
                    
                    if ( targetAspect > 1 ) {
                        // predominant dimension: width
                        targetDim.width = drawRectDim.width;
                        targetDim.height = targetDim.width / targetAspect;
                    } else {
                        // predominant dimension: height
                        targetDim.height = drawRectDim.height;
                        targetDim.width = targetDim.height * targetAspect;
                    }
                    
                    cdl_render_scene_ui_push_quad_textured(ui->scene, drawRectMid, targetDim, v4f_new(0, 1, 0, 1), texture);
                    
                    rect2 targetRect = rect2_from_size(drawRectMid, targetDim);
                    cdl_render_scene_ui_push_rect_outline(ui->scene, targetRect, widgetTheme.hovered, 2);
                } else {
                    cdl_render_scene_ui_push_rect(ui->scene, drawRect, color);
                }
                
                cdl_render_scene_ui_push_rect_outline(ui->scene, drawRect, widgetTheme.hovered, 2);
            } break;
            
            case UIWidgetType_DrawArea:
            {
                cdl_render_scene_ui_push_rect_outline(ui->scene, drawRect, widgetTheme.hovered, 2);
            } break;
            
            default:
            {
                // Awful colors to clearly see widgets were not implemented
                cdl_render_scene_ui_push_rect(ui->scene, drawRect, color_red);
                cdl_render_scene_ui_push_rect_outline(ui->scene, drawRect, color_yellow, 2);
            } break;
        }
        
        //- Push clip rectangle if it exists
        bool8 clipRect = (w->attributes.flags & UIWidgetFlags_HasClipRect) || (w->sizeOverride);
        if ( clipRect ) {
            cdl_render_scene_push_clip_rect(ui->scene, drawRect);
        }
        
        //- On Draw Func
        if ( w->onDraw ) {
            w->onDraw(w);
        }
        
        //- Drawing the text in the widget
        //attributes.anchor = w->anchor ? w->anchor : TextAnchor_Centered;
        
        switch (w->type) {
            case UIWidgetType_Canvas:
            case UIWidgetType_Panel:
            case UIWidgetType_MenuPanel:
            case UIWidgetType_TextureTarget:
            case UIWidgetType_DrawArea:
            {
            } break;
            
            case UIWidgetType_TextArea:
            {
                attributes.lineWidth = drawRectDim.x;
                v2f cursorPos = cdl_render_scene_ui_text(ui->scene, textToDraw.string, drawRectMid, currentFont, attributes);
                
                if ( ui->interaction.type == UIInteractionType_InputText ) {
                    color8 cursorColor = color_white;
                    cursorColor.a = ( sin(core->totalTime * 5) > 0 ? 1 : 0 ) * 255;
                    cdl_render_scene_ui_push_line(ui->scene, cursorPos, v2f_add(cursorPos, v2f_new(0, attributes.size)), 3, cursorColor);
                }
            } break;
            
            default:
            {
                if ( textToDraw.string[0] != 0 ) {
                    attributes.lineWidth = drawRectDim.x;
                    cdl_render_scene_ui_text(ui->scene, textToDraw.string, drawRectMid, currentFont, attributes);
                }
            } break;
        }
        
        //- Drawing child widgets 
        if ( drawChildsIfAny && w->child ) {
            __cdl_ui_internal_draw_widget(ui, w->child);
        }
        
        /*
        if ( isHotWidget ) {
            cdl_render_scene_ui_push_rect_outline(ui->scene, drawRect, color_red, 2);
        }
        */
        
        //- Pop clip rectangle
        if ( clipRect ) {
            cdl_render_scene_pop_clip_rect(ui->scene);
        }
        
        //- Last Draw (foreground)
        switch (w->type) {
            case UIWidgetType_Panel:
            {
                if ( w->attributes.flags & UIWidgetFlags_Resizable ) {
                    rect2 handleRect = w->rect;
                    color8 handleColor = widgetTheme.def;
                    handleRect.min = v2f_sub(w->rect.max, panelResizeHandleDim);
                    
                    if ( collisions_v2i_in_rect(mouse_get_position(), handleRect) ) {
                        ui->nextHotInteraction.w = w;
                        ui->nextHotInteraction.type = UIInteractionType_Drag;
                        handleColor = widgetTheme.interact;
                    }
                    
                    cdl_render_scene_ui_push_rect(ui->scene, handleRect, handleColor);
                    cdl_render_scene_ui_push_rect_outline(ui->scene, handleRect, widgetTheme.interact, 2);
                }
            } break;
            
            default:
            {
            } break;
        }
        
    }
    
}

internal void cdl_ui_widget_new(cdl_ui_widget* widget, cdl_ui_widget* parent, const char* label, UIWidgetType type, cdl_ui_widget_attributes attributes)
{
    PROFILED_FUNC;
    
    cdl_ui_widget* w = widget;
    *w = ZeroStruct;
    
    w->type = type;
    w->attributes = attributes;
    w->anchor = TextAnchor_Centered;
    
    if ( parent ) {
        w->parent = parent;
        
        if ( !parent->child ) {
            parent->child = w;
        }
        
        else {
            cdl_ui_widget* place = parent->child;
            cdl_ui_widget* previous = 0;
            
            while ( place != 0 ) {
                previous = place;
                place = place->next;
            }
            
            if ( previous ) {
                previous->next = w;
                w->previous = previous;
            }
            
        }
    }
    
    w->dirty = 1;
    w->colorValue = v4f_new(1);
    w->fMax = 1;
    
    switch (w->type) {
        case UIWidgetType_Group:
        case UIWidgetType_Dropdown:
        case UIWidgetType_Button:
        case UIWidgetType_RadioButton:
        case UIWidgetType_Boolean:
        case UIWidgetType_ColorPicker:
        case UIWidgetType_FloatSlider:
        case UIWidgetType_TextArea:
        {
            w->attributes.flags |= UIWidgetFlags_HighlightOnHover;
        }
        
        case UIWidgetType_Panel:
        case UIWidgetType_MenuPanel:
        case UIWidgetType_Label:
        case UIWidgetType_TextureTarget:
        case UIWidgetType_DrawArea:
        {
            w->attributes.flags |= UIWidgetFlags_EatInputs;
        } break;
        
        default:
        {
        } break;
    }
    
    MemoryZero(w->label, ARRAY_COUNT(w->label));
    w->fontSize = 12.f;
    if ( label ) {
        MemoryCopy(w->label, label, StringLength(label));
    }
    
    if ( type == UIWidgetType_TextArea ) {
        local i32 baseTextAreaBufferSize = 16;
        TranAllocArray(&w->text.string, "Text Area Base Alloc", char, baseTextAreaBufferSize);
        MemoryZero(w->text.string, baseTextAreaBufferSize * sizeof(char));
        w->text.size = 0;
        w->capacity = baseTextAreaBufferSize;
    }
    
    __cdl_ui_internal_update_rect(w);
}

internal void cdl_ui_update(cdl_ui* ui) 
{
    assert(ui);
    PROFILED_FUNC;
    
    if ( !ui->scene ) {
        ui->scene = renderer->ui;
    }
    
    if ( !ui->rootWidget ) {
        return;
    }
    
    cdl_ui_widget* root = ui->rootWidget;
    if ( cdl_engine_platform_event_happened(PlatformEventType_application_resized) 
        || !f32_equals(rect2_dim(root->rect).width, core->frameWidth) ) {
        __cdl_ui_internal_update_rect(root);
    }
    
    //- UI Rendering
    __cdl_ui_internal_draw_widget(ui, root);
    
    //- UI Interactions
    __cdl_ui_internal_interaction_process(ui);
}