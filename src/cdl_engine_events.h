#ifndef __CDL_INCLUDE_CDL_ENGINE_EVENTS_H__
#define __CDL_INCLUDE_CDL_ENGINE_EVENTS_H__

//- Forward Declarations

// Structs
struct cdl_engine_controller;
struct cdl_engine_keyboard;
struct cdl_engine_mouse;
struct cdl_engine_platform_event;
struct cdl_engine_events;

// Typedefs
typedef u32 ControllerButton;
typedef u32 ControllerAxes;
typedef u32 KeyboardKey;
typedef u32 MouseButton;
typedef u32 PlatformEventType;

//-

//- Controllers
enum ControllerButton_ {
#define controllerbutton(key, str) controller_##key,
#include "engine/controller_buttons.def"
#undef controllerbutton
    ControllerButton_Count
};

enum ControllerAxes_ {
#define controlleraxes(axes, str) controller_##axes,
#include "engine/controller_axes.def"
#undef controlleraxes
    ControllerAxes_Count
};

struct cdl_engine_controller {
    bool8 buttons[ControllerButton_Count];
    f32 axes[ControllerAxes_Count];
};

internal str8 cdl_engine_controller_button_get_name(ControllerButton button);
internal bool8 cdl_engine_controller_button_get(int controllerID, ControllerButton button);
internal bool8 cdl_engine_controller_button_on_down(int controllerID, ControllerButton button);
internal bool8 cdl_engine_controller_button_on_up(int controllerID, ControllerButton button);

#define controller_button_get(controllerID, button)       cdl_engine_controller_button_get(controllerID, button)
#define controller_button_on_down(controllerID, button)   cdl_engine_controller_button_on_down(controllerID, button)
#define controller_button_on_up(controllerID, button)     cdl_engine_controller_button_on_up(controllerID, button)

internal str8 cdl_engine_controller_axis_get_name(ControllerAxes axes);
internal f32 cdl_engine_controller_axis_get(int controllerID, ControllerAxes axes);

#define controller_axis_get(controller, axes) cdl_engine_controller_axis_get(controller, axes)


//- Keyboard
// NOTE(abe): keyboard default layout is QWERTY
enum KeyboardKey_ {
#define kkey(key, str) key,
#include "engine/keyboard_keys.def"
#undef kkey
    KeyboardKey_Count
};

#define CDL_KEYBOARD_MAX_CHAR_PER_FRAME (32)
struct cdl_engine_keyboard {
    bool8 keys[KeyboardKey_Count];
    
    u8 numberOfChar;
    char text[CDL_KEYBOARD_MAX_CHAR_PER_FRAME];
};

internal str8 cdl_engine_keyboard_key_get_name(KeyboardKey key);
internal bool8 cdl_engine_keyboard_key_get(KeyboardKey key);
internal bool8 cdl_engine_keyboard_key_on_down(KeyboardKey key);
internal bool8 cdl_engine_keyboard_key_on_up(KeyboardKey key);
internal bool8 cdl_engine_keyboard_key_any();
internal u32 cdl_engine_process_text_input(str8* text, u32 capacity);

#define keyboard_key_get(key)        cdl_engine_keyboard_key_get(key)
#define keyboard_key_on_down(key)    cdl_engine_keyboard_key_on_down(key)
#define keyboard_key_on_up(key)      cdl_engine_keyboard_key_on_up(key)


//- Mouse
enum MouseButton_ {
#define mousebutton(button, str) mouse_##button,
#include "engine/mouse_buttons.def"
#undef mousebutton
    MouseButton_Count
};

struct cdl_engine_mouse
{
    bool8 buttons[MouseButton_Count];
    v2i position;
    v2f delta;
    v2f scroll;
};

internal str8 cdl_engine_mouse_button_get_name(MouseButton button);
internal bool8 cdl_engine_mouse_button_get(MouseButton button);
internal bool8 cdl_engine_mouse_button_on_down(MouseButton button);
internal bool8 cdl_engine_mouse_button_on_up(MouseButton button);
internal v2i cdl_engine_mouse_get_raw_position();
internal v2i cdl_engine_mouse_get_position(); // Return the mouse position in the current viewport
internal v2i cdl_engine_mouse_get_window_position(); // Return the mouse position in the current frame
internal v2f cdl_engine_mouse_get_gl_position();
internal v2f cdl_engine_mouse_get_scroll();
internal v2f cdl_engine_mouse_get_delta();

#define mouse_button_get(button)        cdl_engine_mouse_button_get(button)
#define mouse_button_on_down(button)    cdl_engine_mouse_button_on_down(button)
#define mouse_button_on_up(button)      cdl_engine_mouse_button_on_up(button)
#define mouse_get_position()            cdl_engine_mouse_get_position()
#define mouse_get_scroll()              cdl_engine_mouse_get_scroll()
#define mouse_get_delta()               cdl_engine_mouse_get_delta()

//- [SECTION] Platform Event Types
enum PlatformEventType_ {
    PlatformEventType_Invalid,
    
    PlatformEventType_application_start,
    PlatformEventType_application_close,
    PlatformEventType_application_resized,
    PlatformEventType_application_moved,
    PlatformEventType_application_minimise,
    
    PlatformEventType_file_open,
    PlatformEventType_file_read,
    PlatformEventType_file_write,
    PlatformEventType_file_close,
    
    PlatformEventType_key_pressed,
    PlatformEventType_key_repeat,
    PlatformEventType_key_released,
    
    PlatformEventType_text,
    
    PlatformEventType_mouse_enter,
    PlatformEventType_mouse_leave,
    PlatformEventType_mouse_moved,
    PlatformEventType_mouse_pressed,
    PlatformEventType_mouse_released,
    PlatformEventType_mouse_scrolled,
    
    PlatformEventType_controller_connect,
    PlatformEventType_controller_disconnect,
    PlatformEventType_controller_button_pressed,
    PlatformEventType_controller_button_released,
    PlatformEventType_controller_axes_moved,
    
    PlatformEventType_Count
};

struct cdl_engine_platform_event {
    PlatformEventType type;
    bool8 handled;
    
    union {
        str8 file;
        i32 key;
        char text;
        struct {
            i8 ID;
            ControllerButton button;
            ControllerAxes axes;
            f32 axesAmount;
        } controller;
        struct {
            v2i position;
            v2f delta;
            v2f scroll;
            MouseButton button;
        } mouse;
        struct { // window size, frame dimensions and borders
            v2i frame;
            v2i border;
            v2f size;
            v2f position;
        } window;
    };
};


//- [SECTION] Events Structure
#define MAX_EVENT_PER_FRAME (256)
struct cdl_engine_events {
    cdl_engine_platform_event queue[MAX_EVENT_PER_FRAME];
    u32 count;
    
    cdl_engine_keyboard previousKeyboard;
    cdl_engine_keyboard currentKeyboard;
    
    i16 connectedControllers;
    cdl_engine_controller previousControllers[4];
    cdl_engine_controller currentControllers[4];
    
    bool8 mouseInWindow;
    cdl_engine_mouse currentMouse;
    cdl_engine_mouse previousMouse;
};

internal bool8 cdl_engine_platform_event_happened(PlatformEventType eventType);

#endif // __CDL_INCLUDE_CDL_ENGINE_EVENTS_H__