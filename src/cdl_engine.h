#ifndef __CDL_INCLUDE_CDL_ENGINE_H__
#define __CDL_INCLUDE_CDL_ENGINE_H__

#define CDL_ENGINE_APP_HEIGHT 720
#define CDL_ENGINE_APP_WIDTH 1280
#define CDL_ENGINE_APP_NAME "Engine"

//- Forward Declarations

// Structs
struct cdl_engine_memory;
struct cdl_engine_runtime;

//-

//- Memory
struct cdl_engine_memory {
    bool8 isInitialized;
    
    memory_block permanent;
    memory_block frame;
    memory_block transient;
};

//- Runtime
struct cdl_engine_runtime {
    i8 run;
    f32 processingTime;
    f32 deltaTime;
    f32 totalTime;
    
    random_series rng;
    
    cdl_engine_memory memory;
    cdl_assets* assets;
    cdl_audio_mixer mixer;
    
    thread_work_queue highQueue;
    thread_work_queue lowQueue;
    
    bool8 hasFixedAspectRatio;
    f32 aspectRatio;
    f32 windowWidth;
    f32 windowHeight;
    
    i32 frameWidth;
    i32 frameHeight;
    i32 frameBorderLeft;
    i32 frameBorderTop;
    
    // Input recording for debug builds
    struct {
        bool8 recordInputs;
        int recordingSlot;
        bool8 playbackInputs;
        int playbackSlot;
    };
    
    cdl_engine_events events;
};
extern cdl_engine_runtime* core;

//- Functions Used by the Platform Layer and Exported at Compile Time
#define ENGINE_INIT(name) void name(cdl_engine_memory* engineMemory)
typedef ENGINE_INIT(EngineInitFunc);

#define ENGINE_HOTRELOAD(name) void name(cdl_engine_memory* engineMemory)
typedef ENGINE_HOTRELOAD(EngineHotReloadFunc);

#define ENGINE_UPDATE(name) void name(cdl_engine_memory* engineMemory)
typedef ENGINE_UPDATE(EngineUpdateFunc);

struct cdl_platform;
#define ENGINE_SETPLATFORM(name) void name(cdl_platform* platformPtr, cdl_engine_runtime* enginePtr)
typedef ENGINE_SETPLATFORM(EngineSetPlatformFunc);

#define ENGINE_CLEAN(name) void name(cdl_engine_memory* engineMemory)
typedef ENGINE_CLEAN(EngineCleanFunc);

//- Memory Allocation Helper Macros
#define PermAllocStruct(ptrAddr, name, st) (MemoryAllocStruct(ptrAddr, &core->memory.permanent, st, name))
#define PermAllocArray(ptrAddr, name, st, count) (MemoryAllocArray(ptrAddr, &core->memory.permanent, st, count, name))
#define PermRelease(chunk) (MemoryRelease(&core->memory.permanent, chunk))

#define FrameAllocStruct(ptrAddr, name, st) (MemoryAllocStruct(ptrAddr, &core->memory.frame, st, name))
#define FrameAllocArray(ptrAddr, name, st, count) (MemoryAllocArray(ptrAddr, &core->memory.frame, st, count, name))

#define TranAllocStruct(ptrAddr, name, st) (MemoryAllocStruct(ptrAddr, &core->memory.transient, st, name))
#define TranAllocArray(ptrAddr, name, st, count) (MemoryAllocArray(ptrAddr, &core->memory.transient, st, count, name))
#define TranRelease(chunk) (MemoryRelease(&core->memory.transient, chunk))

#endif // __CDL_INCLUDE_CDL_ENGINE_H__