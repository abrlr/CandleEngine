#ifndef __CDL_INCLUDE_CDL_ENGINE_DEBUG_H__
#define __CDL_INCLUDE_CDL_ENGINE_DEBUG_H__

#ifdef CDL_DEBUG

struct debug_memory {
    bool8 display;
    bool8 initialised;
    i64 frameCount;
    
    // Console
#ifdef ABE_CONSOLE_ENABLE
    log_console devConsole;
#endif
    
    // Frame Profiler
    struct {
        bool8 pAllocated;
        u32 nextProfilerIndex;
        i32 inspectedDepth;
        profiler* pastProfilers;
        color8* profilerColors;
        
        bool8 hasFrozenFrame;
        profiler* frozenProfiler;
    };
    
    //- Debug Display
    struct {
        cdl_ui UI;
        cdl_ui_widget windowCanvas;
        
        // Info bar
        cdl_ui_widget bottomBar,
        bottomBarDeltaTimeDisplay,
        bottomBarElapsedTimeDisplay,
        bottomBarToggleEngineManagement;
        
        //- Engine Management Window
        cdl_ui_widget emWindow;
        
        // Window Menu Bar
        cdl_ui_widget emWMenuBar,
        emWMBAssetInspectorRadio,
        emWMBProfilerRadio,
        emWMBMemoryRadio,
        emWMBConsoleRadio;
        
        // Window Panel
        cdl_ui_widget emWPanel;
        cdl_ui_widget emWPTextureTarget;
        cdl_ui_widget emWPWidgets[128];
    };
};

internal void debug_init(debug_memory* debug);
internal void debug_update(debug_memory* debug);

#else
#define PROFILED_BLOCK(name)
#endif

#endif // __CDL_INCLUDE_CDL_ENGINE_DEBUG_H__
