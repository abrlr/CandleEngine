#ifndef __CDL_INCLUDE_CDL_UI_H__
#define __CDL_INCLUDE_CDL_UI_H__

//- Forward Definitions

// Structs
struct cdl_ui_theme;
struct cdl_ui_constraints;
struct cdl_ui_widget_attributes;
struct cdl_ui_widget;
struct cdl_ui_interaction;
struct cdl_ui;

// Typedefs
typedef u32 UITheme;
typedef u32 UIWidgetType;
typedef u32 UIWidgetFlags;
typedef u32 UIInteractionType;
typedef u32 UIWidgetConstraint;
typedef u32 UIWidgetLayout;

//-

//- [SECTION] UI Themes

struct cdl_ui_theme {
	color8 window_bg;
	color8 bg;
	color8 def;
	color8 hovered;
	color8 interact;
};

// Normal
global cdl_ui_theme _ui_theme = {
    cdl_rgba( 1,  2,  4, 150 ),	// window_bg
    cdl_rgba( 2,  3,  5, 150 ),	// background
	cdl_rgb(  2,  5,  8 ),	// default
	cdl_rgb(  2,  6, 12 ),	// hovered
	cdl_rgb(  5, 15, 20 ),    // interact
};

// Danger
global cdl_ui_theme _ui_theme_danger = {
    cdl_rgba( 58,  29,  26, 150 ),    // window_bg
    cdl_rgba( 117, 18,  18, 150 ),    // background
    cdl_rgb( 217, 65, 65 ),    // default
    cdl_rgb( 191, 40,  40 ),    // hovered
    cdl_rgb( 166, 30,  30 ),    // interact
};

enum UITheme_ {
    UITheme_Base,
    UITheme_Danger,
    
    UITheme_Count
};

internal void cdl_ui_theme_set(UITheme theme);
internal void cdl_ui_theme_reset();

global v2f g_ui_panelHandleDim = v2f_new(10,10);

//- Widgets

enum UIWidgetType_ {
    UIWidgetType_Invalid,
    
    UIWidgetType_Canvas,
    UIWidgetType_Panel,
    UIWidgetType_MenuPanel,
    
    UIWidgetType_Group,
    UIWidgetType_Dropdown,
    UIWidgetType_Button,
    UIWidgetType_RadioButton,
    UIWidgetType_Boolean,
    
    UIWidgetType_ColorPicker,
    UIWidgetType_FloatSlider,
    
    UIWidgetType_Label,
    UIWidgetType_TextArea,
    UIWidgetType_TextureTarget,
    
    UIWidgetType_DrawArea,
    
    UIWidgetType_Count
};

enum UIWidgetFlags_ {
    UIWidgetFlags_None                  = BIT(0),
    
    UIWidgetFlags_Disabled              = BIT(1),
    UIWidgetFlags_HighlightOnHover      = BIT(2),
    UIWidgetFlags_Draggable             = BIT(3),
    UIWidgetFlags_Resizable             = BIT(4),
    UIWidgetFlags_HasClipRect           = BIT(5),
    UIWidgetFlags_Animated              = BIT(6),
    UIWidgetFlags_EatInputs             = BIT(7),
    UIWidgetFlags_AbsolutePosition      = BIT(8),
    
};

enum UIInteractionType_ {
    UIInteractionType_None,
    
    UIInteractionType_Drag,
    UIInteractionType_DragSize,
    UIInteractionType_Click,
    UIInteractionType_InputText,
    
    UIInteractionType_Count
};

enum UIWidgetConstraint_ {
    UIWidgetConstraint_None                      = BIT(0),
    
    // Size constraints
    UIWidgetConstraint_SizeRelativeX             = BIT(1),
    UIWidgetConstraint_SizeRelativeY             = BIT(2),
    UIWidgetConstraint_SizeFixedX                = BIT(3),
    UIWidgetConstraint_SizeFixedY                = BIT(4),
    UIWidgetConstraint_SizeMaxX                  = BIT(5),
    UIWidgetConstraint_SizeMaxY                  = BIT(6),
    
    // Margin constraints
    UIWidgetConstraint_MarginFixedLeft           = BIT(7),
    UIWidgetConstraint_MarginFixedRight          = BIT(8),
    UIWidgetConstraint_MarginFixedTop            = BIT(9),
    UIWidgetConstraint_MarginFixedBottom         = BIT(10),
    
    UIWidgetConstraint_MarginRelativeLeft        = BIT(11),
    UIWidgetConstraint_MarginRelativeRight       = BIT(12),
    UIWidgetConstraint_MarginRelativeTop         = BIT(13),
    UIWidgetConstraint_MarginRelativeBottom      = BIT(14),
    
    // Position constraints
    UIWidgetConstraint_PosFixedLeft              = BIT(15),
    UIWidgetConstraint_PosFixedRight             = BIT(16),
    UIWidgetConstraint_PosFixedTop               = BIT(17),
    UIWidgetConstraint_PosFixedBottom            = BIT(18),
    
    UIWidgetConstraint_PosRelativeLeft           = BIT(19),
    UIWidgetConstraint_PosRelativeRight          = BIT(20),
    UIWidgetConstraint_PosRelativeTop            = BIT(21),
    UIWidgetConstraint_PosRelativeBottom         = BIT(22),
    
    // Hard Size constraints
    UIWidgetConstraint_SizeByAspect_HardX        = BIT(23),
    UIWidgetConstraint_SizeByAspect_HardY        = BIT(24),
};

enum UIWidgetLayout_ {
    UIWidgetLayout_None,
    
    UIWidgetLayout_Vertical,
    UIWidgetLayout_Horizontal,
    
    UIWidgetLayout_Count
};

introspect("UI")
struct cdl_ui_constraints {
    u32 type;
    
    f32 aspect;
    v2f size;
    v4f margins;
    v4f positions;
};


//- [SECTION] UI Widget Attributes
introspect("UI")
struct cdl_ui_widget_attributes {
    u32 flags;
    UIWidgetLayout layout;
    UITheme theme;
    cdl_ui_constraints constraints;
};

internal cdl_ui_widget_attributes cdl_ui_widget_attributes_new();

#define cdl_ui_widget_callback(name) void name(cdl_ui_widget* w)
typedef cdl_ui_widget_callback(cdl_ui_widget_callback_func);

global v2f panelResizeHandleDim = v2f_new(12, 12);


//- [SECTION] UI Widget Behaviour Functions
internal void cdl_ui_set_textarea_text(cdl_ui_widget* w, char* newText);
internal void cdl_ui_radiobutton_group(cdl_ui_widget* first, cdl_ui_widget* last);
internal void cdl_ui_group(cdl_ui_widget* groupWidget, cdl_ui_widget* first, cdl_ui_widget* last);


//- [SECTION] UI Widgets
introspect("UI")
struct cdl_ui_widget {
    UIWidgetType type;
    cdl_ui_widget_attributes attributes;
    
    // Widget Behaviour
    struct {
        cdl_ui_widget_callback_func* beginInteract;
        cdl_ui_widget_callback_func* endInteract;
        
        cdl_ui_widget_callback_func* onToggled;
        cdl_ui_widget_callback_func* onPressed;
        cdl_ui_widget_callback_func* onFloat;
        
        cdl_ui_widget_callback_func* onDraw;
    };
    
    // Drawing
    bool8 dirty;
    bool8 hide;
    bool8 sizeOverride;
    rect2 rect; // Widget Rectangle
    v2f marginsDim; // Total margins around the widget rect
    
    // Inner Text
    f32 fontSize;
    char label[128];
    TextAnchor anchor;
    
    // Dropdown
    bool8 displayChilds;
    
    // Toggle
    bool8 toggled;
    
    // Button
    bool8 pressed;
    
    // RadioButton, Region
    cdl_ui_widget* firstOfGroup;
    cdl_ui_widget* lastOfGroup;
    
    // Color Picker
    v2f picker;
    v4f colorValue;
    
    // Float Slider
    f32 fValue;
    f32 fMin;
    f32 fMax;
    
    // Text areas
    str8 text;
    u32 capacity;
    
    // Texture Target
    cdl_renderer_texture* texture;
    
    // Widget Tree
    struct {
        cdl_ui_widget* parent;
        cdl_ui_widget* previous;
        cdl_ui_widget* next;
        cdl_ui_widget* child;
    };
    
    struct {
        void* userData1;
        void* userData2;
        void* userData3;
        void* userData4;
    };
};

internal void cdl_ui_widget_new(cdl_ui_widget* w, cdl_ui_widget* parent, const char* label, UIWidgetType type, cdl_ui_widget_attributes attributes);

#define w_pressed(widget)         (widget.pressed)
#define w_toggled(widget)         (widget.toggled)

//- NOTE(abe): temporary solution to the following issue:
// On a given frame, hiding a widget that is "pressed" (when hiding/showing menus for example),
// will prevent the widget update during the next frame
// thus leaving it in a constant pressed state
// This behaviour is not normal and will be changed to not have to deal with this mess of an api
#define w_pressed_eat(widget)     (widget.pressed = 0)
//-

#define wp_pressed(widget_ptr)    (widget_ptr->pressed)
#define wp_toggled(widget_ptr)    (widget_ptr->toggled)

introspect("UI")
struct cdl_ui_interaction {
    cdl_ui_widget* w;
    UIInteractionType type;
};

introspect("UI")
struct cdl_ui {
    cdl_render_scene* scene;
    cdl_ui_widget* rootWidget;
    
    cdl_ui_interaction nextHotInteraction;
    cdl_ui_interaction hotInteraction;
    cdl_ui_interaction interaction;
};
internal void cdl_ui_update(cdl_ui* ui);


/* IMGUI functions */
global v2i _ui_window_base_size = v2i_new(175, 0);
global v2i _ui_window_head_size = v2i_new(175, 14);
global v2i _ui_button_size      = v2i_new(125, 30);
global v2i _ui_checkbox_size    = v2i_new(20, 20);
global v2i _ui_slider_base_size = v2i_new(_ui_button_size.width, 20);
global v2i _ui_slider_head_size = v2i_new(9, 18);

#endif // __CDL_INCLUDE_CDL_UI_H__