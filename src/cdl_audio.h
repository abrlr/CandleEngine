#ifndef __CDL_INCLUDE_CDL_AUDIO_H__
#define __CDL_INCLUDE_CDL_AUDIO_H__

//- Forward Definitions

// Structs
struct cdl_audio_track;
struct cdl_audio_native_buffer;
struct cdl_audio_mixer;

//-

//- [SECTION] Audio Tracks 
introspect("Audio")
struct cdl_audio_track {
    cdl_assets_audio* asset;
    bool8 loop;
    f32 volume;
    u32 currentSample;
};

internal cdl_audio_track* cdl_audio_track_queue(cdl_assets_audio* trackAsset);

//- [SECTION] Audio Mixer
introspect("Audio")
struct cdl_audio_native_buffer {
    i16* samples;
    u32 samplesPerSecond;
    u32 sampleCount;
};

introspect("Audio")
struct cdl_audio_mixer {
    cdl_audio_track playing[32];
    cdl_audio_native_buffer native;
    f32 volume;
};

internal void cdl_audio_engine_play();

#endif // __CDL_INCLUDE_CDL_AUDIO_H__