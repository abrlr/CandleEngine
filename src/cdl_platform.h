#ifndef __CDL_INCLUDE_CDL_PLATFORM_H__
#define __CDL_INCLUDE_CDL_PLATFORM_H__

#ifdef ABE_WIN
#elif defined(ABE_APPLE)
#error "Missing include in cdl_platform.h"
#elif defined (ABE_LINUX)
#include <X11/Xlib.h>
#endif

//- Forward Declarations

// Structs
struct GLFWwindow;
struct cdl_filesystem_iterator;
struct cdl_file;
struct cdl_platform_audio;
struct cdl_platform;

//-

//- Input 
#define PLATFORM_INPUT_SET_CONTROLLER_VIBRATION(name) void name(i32 controller, f32 lowFreqValue, f32 highFreqValue)
typedef PLATFORM_INPUT_SET_CONTROLLER_VIBRATION(PlatformInputSetControllerVibrationFunc);

//- Window Utility 
#define PLATFORM_WINDOW_SETTITLE(name) void name(char* windowTitle)
typedef PLATFORM_WINDOW_SETTITLE(PlatformWindowSetTitleFunc);

#define PLATFORM_WINDOW_SETFULLSCREEN(name) void name(bool8 toggle)
typedef PLATFORM_WINDOW_SETFULLSCREEN(PlatformWindowSetFullScreenFunc);

#define PLATFORM_WINDOW_ISACTIVE(name) bool8 name()
typedef PLATFORM_WINDOW_ISACTIVE(PlatformWindowIsActive);

#define PLATFORM_WINDOW_REQUESTATTENTION(name) void name()
typedef PLATFORM_WINDOW_REQUESTATTENTION(PlatformWindowRequestAttention);

//- File System 
struct cdl_filesystem_iterator {
    bool8 valid;
    bool8 isRecursive;
    char path[CDL_MAX_PATH];
    char nativeData[1024];
    
    bool8 currentIsDir;
    char currentPath[CDL_MAX_PATH];
    u32 currentSize;
};

#define PLATFORM_FS_ITERATOR_NEW(name) cdl_filesystem_iterator name(char* directory, bool8 recursive)
typedef PLATFORM_FS_ITERATOR_NEW(PlatformFSIteratorNew);

#define PLATFORM_FS_ITERATE(name) void name(cdl_filesystem_iterator* it)
typedef PLATFORM_FS_ITERATE(PlatformFSIterate);


//- File IO 
struct cdl_file {
    u32 size;
    void* content;
};

#define PLATFORM_FREEFILEMEMORY(name) void name(void* fileMemory)
typedef PLATFORM_FREEFILEMEMORY(PlatformFreeFileMemoryFunc);

#define PLATFORM_READENTIREFILE(name) cdl_file name(char* filepath)
typedef PLATFORM_READENTIREFILE(PlatformReadEntireFileFunc);

#define PLATFORM_WRITEENTIREFILE(name) bool8 name(char* filepath, cdl_file* file)
typedef PLATFORM_WRITEENTIREFILE(PlatformWriteEntireFileFunc);

#define PLATFORM_OPENFILEEXPLORER(name) bool8 name(char* openFilepath, char* fileExtension, char* filter)
typedef PLATFORM_OPENFILEEXPLORER(PlatformOpenFileExplorerFunc);

#define PLATFORM_SAVEFILEEXPLORER(name) bool8 name(char* saveFilepath, char* fileExtension)
typedef PLATFORM_SAVEFILEEXPLORER(PlatformSaveFileExplorerFunc);

//- Audio
struct cdl_platform_audio {
    bool8 valid;
    
    u32 runningSampleIndex;
    i32 samplesPerSecond;
    i32 latencySampleCount;
    i32 bytesPerSample;
    i32 audioBufferSize;
    
    i16* soundSamples;
    void* nativeSoundBuffer;
    
#ifdef ABE_WIN
    DWORD lastPlayCursor;
    DWORD byteToLock;
    DWORD targetCursor;
    DWORD bytesToWrite;
#elif defined(ABE_APPLE)
#elif defined(ABE_LINUX)
#endif
};

//- Platform 
#define THREAD_COUNT (4)
#define PERMANENT_MEMORY_SIZE ((u64)MegaBytes(128))
#define FRAME_MEMORY_SIZE ((u64)MegaBytes(64))
#define TRANSIENT_MEMORY_SIZE ((u64)GigaBytes(1))

struct cdl_platform {
    void* state;
    u8 targetFramesPerSecond;
    
    f32 frameTime;
    u32 frameCount;
    
    // Threads
    thread threads[THREAD_COUNT];
    
    // Audio
    cdl_platform_audio audio;
    
#ifdef ABE_WIN
    // App Code
    struct {
        bool8 appCodeIsValid;
        
        HMODULE appHandle;
        char workingDirectory[CDL_MAX_PATH];
        FILETIME appLastUpdateTime;
    };
    
    // State
    struct {
        GLFWwindow* glfwWindow;
        HWND nativeWindow;
        
        HANDLE recordingFile;
        HANDLE playbackFile;
        
        void* engineMemoryBlock;
        u64 engineMemoryBlockSize;
    };
    
    // Time
    struct {
        bool8 sleepIsGranular;
        LARGE_INTEGER pcFrequency;
        
        LARGE_INTEGER appStart;
        LARGE_INTEGER frameStart;
        
        LARGE_INTEGER appUpdateStart;
        LARGE_INTEGER appUpdateEnd;
    };
    
#elif defined(ABE_APPLE)
#error "Missing definitions in platform.h"
    
#elif defined(ABE_LINUX)
    // App Code
    struct {
        bool8 appCodeIsValid;
        
        void* appHandle;
        char workingDirectory[CDL_MAX_PATH];
        time_t appLastUpdateTime;
    };
    
    // State
    struct {
        GLFWwindow* glfwWindow;
        Window nativeWindow;
        
        void* recordingFile;
        void* playbackFile;
        
        void* engineMemoryBlock;
        u64 engineMemoryBlockSize;
    };
    
    // Time
    struct {
        bool8 sleepIsGranular;
        u64 pcFrequency;
        
        u64 appStart;
        u64 frameStart;
        
        u64 appUpdateStart;
        u64 appUpdateEnd;
    };
    
#else
#error "Unsupported Platform"
#endif
    
    // Application
    EngineInitFunc* AppInit;
    EngineHotReloadFunc* AppReload;
    EngineUpdateFunc* AppUpdate;
    EngineSetPlatformFunc* AppSetPlatform;
    EngineCleanFunc* AppClean;
    
    // Input
    PlatformInputSetControllerVibrationFunc* setControllerVibration;
    
    // Window Utility
    PlatformWindowSetTitleFunc* setWindowTitle;
    PlatformWindowSetFullScreenFunc* setFullScreen;
    PlatformWindowIsActive* isActive;
    PlatformWindowRequestAttention* requestAttention;
    
    // File System
    PlatformFSIteratorNew* fsIterator;
    PlatformFSIterate* fsIterate;
    
    // File IO
    PlatformFreeFileMemoryFunc* freefile;
    PlatformReadEntireFileFunc* readfile;
    PlatformWriteEntireFileFunc* writefile;
    PlatformOpenFileExplorerFunc* openfileexplorer;
    PlatformSaveFileExplorerFunc* savefileexplorer;
};
extern cdl_platform* platform;

internal bool8 cdl_platform_initialize();
internal void cdl_platform_update();
internal void cdl_platform_clean();

#endif // __CDL_INCLUDE_CDL_PLATFORM_H__