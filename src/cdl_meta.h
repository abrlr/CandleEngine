#ifndef __CDL_INCLUDE_CDL_META_H__
#define __CDL_INCLUDE_CDL_META_H__

enum MetaType_ {
    MetaType_None,
    
    //- Base types
    MetaType_void,
    MetaType_voidPtr,
    
    MetaType_char,
    MetaType_charPtr,
    
    MetaType_i8,
    MetaType_i8Ptr,
    MetaType_i16,
    MetaType_i16Ptr,
    MetaType_i32,
    MetaType_i32Ptr,
    MetaType_i64,
    MetaType_i64Ptr,
    
    MetaType_u8,
    MetaType_u8Ptr,
    MetaType_u16,
    MetaType_u16Ptr,
    MetaType_u32,
    MetaType_u32Ptr,
    MetaType_u64,
    
    MetaType_f32,
    MetaType_f32Ptr,
    MetaType_f64,
    MetaType_f64Ptr,
    
    MetaType_bool8,
    MetaType_bool8Ptr,
    
    MetaType_str8,
    
    //- Maths types
    MetaType_v2i,
    MetaType_v2iPtr,
    MetaType_v2f,
    MetaType_v2fPtr,
    MetaType_line2,
    MetaType_line2Ptr,
    MetaType_rect2,
    MetaType_rect2Ptr,
    
    MetaType_v3f,
    MetaType_v3fPtr,
    MetaType_line3,
    MetaType_line3Ptr,
    MetaType_v4f,
    MetaType_v4fPtr,
    
    MetaType_m3f,
    MetaType_m3fPtr,
    MetaType_m4f,
    MetaType_m4fPtr,
    
    //- Random Types
    MetaType_random_series,
    MetaType_guid,
    
    //- Asset types
    MetaType_AssetType,
    MetaType_AssetState,
    
    MetaType_cdl_assets_header,
    
    MetaType_ShaderType,
    MetaType_cdl_assets_shader,
    MetaType_cdl_assets_shaderPtr,
    
    MetaType_TextureType,
    MetaType_cdl_assets_texture,
    MetaType_cdl_assets_texturePtr,
    
    MetaType_cdl_assets_material,
    
    MetaType_cdl_assets_glyph,
    MetaType_cdl_assets_glyphPtr,
    MetaType_cdl_assets_font,
    MetaType_cdl_assets_fontPtr,
    
    MetaType_cdl_assets_model_vertex,
    MetaType_cdl_assets_model_vertexPtr,
    MetaType_cdl_assets_model_raw,
    MetaType_cdl_assets_model_rawPtr,
    MetaType_cdl_assets_model_instance,
    MetaType_cdl_assets_model_instancePtr,
    
    MetaType_AudioType,
    MetaType_cdl_assets_audio,
    MetaType_cdl_assets_audioPtr,
    
    MetaType_cdl_assets,
    
    //- Renderer types
    MetaType_color8,
    
    MetaType_cdl_renderer_texture,
    MetaType_cdl_renderer_texturePtr,
    
    MetaType_RendererShaderType,
    MetaType_cdl_renderer_shader,
    MetaType_cdl_renderer_shaderPtr,
    
    MetaType_cdl_renderer_light,
    MetaType_cdl_renderer_lightPtr,
    
    MetaType_cdl_renderer_material,
    MetaType_cdl_renderer_materialPtr,
    
    MetaType_cdl_renderer_environment,
    MetaType_cdl_renderer_environmentPtr,
    
    MetaType_CameraType,
    MetaType_cdl_renderer_camera,
    MetaType_cdl_renderer_cameraPtr,
    
    MetaType_cdl_renderer_scene_view,
    MetaType_cdl_renderer_scene_viewPtr,
    
    MetaType_cdl_renderer_api_framebuffer,
    MetaType_cdl_renderer_api_framebufferPtr,
    MetaType_cdl_renderer_framebuffer,
    MetaType_cdl_renderer_framebufferPtr,
    
    MetaType_TextAnchor,
    MetaType_TextStyle,
    MetaType_cdl_text_atb,
    
    MetaType_cdl_renderer_font,
    MetaType_cdl_renderer_fontPtr,
    
    MetaType_cdl_renderer_model_transform,
    MetaType_cdl_renderer_model_transformPtr,
    MetaType_cdl_renderer_model_instance_data,
    MetaType_cdl_renderer_model_instance_dataPtr,
    MetaType_cdl_renderer_model_instance_list,
    MetaType_cdl_renderer_model_instance_listPtr,
    
    MetaType_BatchVertexFlags,
    MetaType_BatchUIVertexFlags,
    MetaType_cdl_renderer_batch_vertex,
    MetaType_cdl_renderer_batch_vertexPtr,
    MetaType_cdl_renderer_batch_entry,
    MetaType_cdl_renderer_batch_entryPtr,
    
    MetaType_cdl_renderer_triangle_batch,
    MetaType_cdl_renderer_triangle_batchPtr,
    MetaType_cdl_renderer_line_batch,
    MetaType_cdl_renderer_line_batchPtr,
    MetaType_cdl_renderer_point_batch,
    MetaType_cdl_renderer_point_batchPtr,
    
    MetaType_RenderSceneType,
    MetaType_cdl_render_scene,
    MetaType_cdl_render_scenePtr,
    
    MetaType_RenderStateVarType,
    MetaType_cdl_render_state_clip_rect,
    MetaType_cdl_render_state_clip_rectPtr,
    MetaType_cdl_render_state,
    MetaType_cdl_render_statePtr,
    MetaType_cdl_render_state_vars,
    
    MetaType_cdl_renderer_effect,
    MetaType_cdl_renderer_effectPtr,
    
    MetaType_cdl_renderer_post_processing,
    
    MetaType_cdl_renderer_storage,
    
    MetaType_cdl_renderer,
    
    //- Audio Types
    MetaType_cdl_audio_track,
    MetaType_cdl_audio_trackPtr,
    
    MetaType_cdl_audio_native_buffer,
    MetaType_cdl_audio_native_bufferPtr,
    
    MetaType_cdl_audio_mixer,
    MetaType_cdl_audio_mixerPtr,
    
    //- UI Types
    MetaType_UITheme,
    MetaType_UIWidgetConstraint,
    MetaType_UIWidgetType,
    MetaType_UIWidgetFlags,
    MetaType_UIWidgetLayout,
    MetaType_UIInteractionType,
    
    MetaType_cdl_ui_constraints,
    MetaType_cdl_ui_widget_attributes,
    MetaType_cdl_ui_widget_callback_funcPtr,
    MetaType_cdl_ui_widget,
    MetaType_cdl_ui_widgetPtr,
    MetaType_cdl_ui_interaction,
    MetaType_cdl_ui,
    MetaType_cdl_uiPtr,
    
    //- IMGUI Types
    MetaType_IMDrawCommand,
    MetaType_IMLayout,
    MetaType_IMDimensionRule,
    MetaType_IMWindowFlag,
    MetaType_IMDockType,
    MetaType_IMChartMarker,
    
    MetaType_im_id,
    MetaType_im_draw_command,
    MetaType_im_draw_commandPtr,
    MetaType_im_rect_theme,
    MetaType_im_color_theme,
    MetaType_im_color_themePtr,
    MetaType_im_layout,
    MetaType_im_layoutPtr,
    MetaType_im_item_data,
    MetaType_im_window,
    MetaType_im_windowPtr,
    MetaType_im_dockspace,
    MetaType_im_dockspacePtr,
    MetaType_im_context,
    MetaType_im_chart_atb,
    
    //- Count
    MetaType_Count
};

struct cdl_meta_members {
    char* structName;
    char* name;
    MetaType_ type;
    u32 offset;
    bool8 isPtr;
};

#endif // __CDL_INCLUDE_CDL_META_H__