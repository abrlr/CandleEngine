#ifndef __ABE_THREADS_H__
#define __ABE_THREADS_H__

#include "abe_base.h"

//- Forward Declarations

// Structs
struct thread_work_queue;
struct thread_work_entry;
struct thread;

// Typedefs
typedef u32 ThreadState;

//-

enum ThreadState_ {
    ThreadState_Invalid,
    ThreadState_Run,
    ThreadState_Hold,
    ThreadState_Close
};

#ifdef ABE_WIN

#include <windows.h>
#include <winnt.h>
#include <intrin.h>
#include <process.h>

//- [SECTION] Semaphores
typedef HANDLE thread_semaphore;
internal thread_semaphore thread_semaphore_create(i32 initialCount, i32 maxCount);
internal i32 thread_semaphore_wait(thread_semaphore* semaphore);
internal i32 thread_semaphore_release(thread_semaphore* semaphore, i32 amount);

//- [SECTION] Threads
struct thread {
    HANDLE handle;
    thread_work_queue* queue;
    
    i32 platform_id;
    i32 logical_id;
    
    void* startFunction;
    ThreadState state;
};

internal thread thread_create(void* startFunction);
internal void thread_run(thread* thread);

#define COMPLETE_PAST_BEFORE_FUTURE_WRITES _WriteBarrier(); _mm_sfence()
#define COMPLETE_PAST_BEFORE_FUTURE_READS _ReadBarrier()

#define InterlockedIncrement32(ptr) (InterlockedIncrement((LONG volatile*)ptr) - 1)
#define InterlockedCompareExchange32(valueAddr, exchange, comparand) (InterlockedCompareExchange((LONG volatile*)valueAddr, (LONG)exchange, (LONG)comparand))
#define InterlockedExchangePtr(ptrAddr, newPtr) (InterlockedExchangePointer((PVOID*)ptrAddr, (PVOID)newPtr))
#define InterlockedCompareExchangePtr(ptrAddr, exchange, comparand) (InterlockedCompareExchangePointer((PVOID*)ptrAddr, (VOID)exchange, (VOID)comparand))

#elif (defined(ABE_APPLE) || defined(ABE_LINUX))

#include <semaphore.h>
#include <pthread.h>

#define CDLTHREADRUNFUNC(name) void* name(void* userData)
typedef CDLTHREADRUNFUNC(thread_run_func);

// --- Semaphores ---
typedef sem_t thread_semaphore;
static thread_semaphore thread_semaphore_create(i32 initialCount, i32 maxCount)
{
    thread_semaphore result;
    sem_init(&result, 0, initialCount);
    return result;
}

static i32 thread_semaphore_wait(thread_semaphore* semaphore)
{
    sem_wait(semaphore);
    return 1;
}

static i32 thread_semaphore_release(thread_semaphore* semaphore, i32 amount)
{
    sem_post(semaphore);
    return 1;
}

// --- Threads ---
struct thread {
    pthread_t handle;
    thread_work_queue* queue;
    
    i32 platform_id;
    i32 logical_id;
    
    thread_run_func* startFunction;
    ThreadState state;
};

static thread thread_create(void* startFunction) {
    thread result = { };
    result.startFunction = (thread_run_func*)startFunction;
    result.state = ThreadState_Invalid;
    return result;
}

static void thread_run(thread* thread) {
    thread->state = ThreadState_Run;
    int valid = pthread_create(&thread->handle, NULL, thread->startFunction, (void*)thread);
    if ( valid != 0 ) {
        thread->state = ThreadState_Invalid;
    }
}

#define COMPLETE_PAST_BEFORE_FUTURE_WRITES //_WriteBarrier(); _mm_sfence()
#define COMPLETE_PAST_BEFORE_FUTURE_READS //_ReadBarrier()

#define InterlockedIncrement32(ptr) __sync_fetch_and_add(ptr, 1)
#define InterlockedCompareExchange32(ptr, exchange, comparand) __sync_val_compare_and_swap(ptr, comparand, exchange)
#define InterlockedExchangePtr(ptr, exchange) __sync_lock_test_and_set(ptr, exchange)

#elif CDL_GCC


#include <semaphore.h>
#include <pthread.h>

#define CDLTHREADRUNFUNC(name) void* name(void* userData)
typedef CDLTHREADRUNFUNC(thread_run_func);

//- [SECTION] Semaphores
typedef sem_t thread_semaphore;
internal thread_semaphore thread_semaphore_create(i32 initialCount, i32 maxCount)
internal i32 thread_semaphore_wait(thread_semaphore* semaphore)
internal i32 thread_semaphore_release(thread_semaphore* semaphore, i32 amount)

// --- Threads ---
struct thread {
    pthread_t handle;
    thread_work_queue* queue;
    
    i32 platform_id;
    i32 logical_id;
    
    thread_run_func* startFunction;
    ThreadState state;
};

internal thread thread_create(void* startFunction);
internal void thread_run(thread* thread);

#define COMPLETE_PAST_BEFORE_FUTURE_WRITES //_WriteBarrier(); _mm_sfence()
#define COMPLETE_PAST_BEFORE_FUTURE_READS //_ReadBarrier()

#define InterlockedIncrement32(ptr) __sync_fetch_and_add(ptr, 1)
#define InterlockedCompareExchange32(ptr, exchange, comparand) __sync_val_compare_and_swap(ptr, comparand, exchange)
#define InterlockedExchangePtr(ptr, exchange) __sync_lock_test_and_set(ptr, exchange)

#endif

//- [SECTION] Work Entry
#define thread_work_entry_func(name) void name(thread* thread, void* userData)
typedef thread_work_entry_func(thread_work_entry_function);

struct thread_work_entry {
    thread_work_entry_function* userFunction;
    void* userData;
};

//- [SECTION] Work Queue
struct thread_work_queue {
    thread_semaphore semaphore;
    
    u32 volatile nextEntryToWrite;
    u32 volatile nextEntryToRead;
    
    u32 volatile completionGoal;
    u32 volatile completionCount;
    
    thread_work_entry entries[128];
};

internal thread_work_queue thread_work_queue_create(u32 threadCount);
internal void thread_work_queue_add_entry(thread_work_queue* wq, thread_work_entry_function* userFunction, void* userData);
internal bool8 thread_work_queue_work_exists(thread_work_queue* wq);

internal void thread_spawn(u32 threadCount, thread* threadArray, thread_work_queue* wq);
internal void thread_work_queue_complete(thread_work_queue* wq);

internal bool8 thread_internal_complete_next_entry(thread_work_queue* wq, thread* thread);
internal void thread_internal_proc(void* threadInfo);

#endif // __ABE_THREADS_H__
