#ifndef __ABE_ALGORITHMS_H__
#define __ABE_ALGORITHMS_H__

#include "abe_base.h"

//- [SECTION] Sorting Algorithms 

// i32 arrays
internal void algorithms_i32_swap(i32* a, i32* b);
internal i32 algorithms_i32_quicksort_partition(i32* arr, i32 low, i32 high);
internal void algorithms_i32_quicksort(i32* arr, i32 low, i32 high);

// generic arrays using f32 keys
internal void algorithms_swap(void* a, void* b, u32 size);
internal i32 algorithms_quicksort_partition(f32* keys, void* values, u32 valueSize, i32 low, i32 high);
internal void algorithms_quicksort(f32* keys, void* values, u32 valueSize, i32 low, i32 high);

#endif // __ABE_ALGORITHMS_H__