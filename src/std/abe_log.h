#ifndef __ABE_LOG_H__
#define __ABE_LOG_H__

#include "abe_base.h"

#ifndef ABE_CONSOLE_ENABLE

internal void log_msg_internal(char* file, u32 line, char* msg, ...);
internal void log_info_internal(char* file, u32 line, char* msg, ...);
internal void log_warn_internal(char* file, u32 line, char* msg, ...);
internal void log_err_internal(char* file, u32 line, char* msg, ...);

#else

#define ABE_LOG_LINE_HEADER_COUNT (32)
#define ABE_LOG_LINE_BODY_COUNT (256)
#define ABE_LOG_CONSOLE_LINE_COUNT (128)

struct log_console_line {
    char lineStr[ABE_LOG_LINE_HEADER_COUNT + ABE_LOG_LINE_BODY_COUNT];
    color8 color;
};

struct log_console {
    i32 lineCount;
    log_console_line lines[ABE_LOG_LINE_HEADER_COUNT + ABE_LOG_LINE_BODY_COUNT];
};
log_console* _log_console_ptr;

internal void log_set_console(log_console* console);
internal void log_move_buffer_up();
internal void log_msg_internal(char* file, u32 line, char* msg,...);
internal void log_info_internal(char* file, u32 line, char* msg,...);
internal void log_err_internal(char* file, u32 line, char* msg,...);
internal void log_warn_internal(char* file, u32 line, char* msg,...);

#endif

#define log_msg(msg, ...) log_msg_internal(__FILE__, __LINE__, msg, ##__VA_ARGS__)
#define log_info(msg, ...) log_info_internal(__FILE__, __LINE__, msg, ##__VA_ARGS__)
#define log_warn(msg, ...) log_warn_internal(__FILE__, __LINE__, msg, ##__VA_ARGS__)
#define log_err(msg, ...) log_err_internal(__FILE__, __LINE__, msg, ##__VA_ARGS__)

#endif // __ABE_LOG_H__