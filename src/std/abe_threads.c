//- [SECTION] Semaphores
#ifdef _WIN64
internal thread_semaphore thread_semaphore_create(i32 initialCount, i32 maxCount)
{
    thread_semaphore result = CreateSemaphoreEx(0, initialCount, maxCount, 0, 0, SEMAPHORE_ALL_ACCESS);
    return result;
}

internal i32 thread_semaphore_wait(thread_semaphore* semaphore)
{
    DWORD result = WaitForSingleObject(*semaphore, INFINITE);
    return (i32)( result == 0 );
}

internal i32 thread_semaphore_release(thread_semaphore* semaphore, i32 amount)
{
    DWORD result = ReleaseSemaphore(*semaphore, amount, 0);
    return (i32)( result == 0 );
}

#elif __APPLE__
internal thread_semaphore thread_semaphore_create(i32 initialCount, i32 maxCount)
{
    thread_semaphore result;
    sem_init(&result, 0, initialCount);
    return result;
}

internal i32 thread_semaphore_wait(thread_semaphore* semaphore)
{
    sem_wait(semaphore);
    return 1;
}

internal i32 thread_semaphore_release(thread_semaphore* semaphore, i32 amount)
{
    sem_post(semaphore);
    return 1;
}

#endif

//- [SECTION] Threads
#if _WIN64
internal thread thread_create(void* startFunction)
{
    thread result = ZeroStruct;
    result.startFunction = startFunction;
    result.state = ThreadState_Invalid;
    return result;
}

internal void thread_run(thread* thread)
{
    thread->state = ThreadState_Run;
    thread->handle = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)thread->startFunction, (void*)thread, 0,  (LPDWORD)&thread->platform_id);
    if ( thread->handle == NULL ) {
        thread->state = ThreadState_Invalid;
    }
}

#elif __APPLE__

internal thread thread_create(void* startFunction) {
    thread result = ZeroStruct;
    result.startFunction = (thread_run_func*)startFunction;
    result.state = ThreadState_Invalid;
    return result;
}

internal void thread_run(thread* thread) {
    thread->state = ThreadState_Run;
    int valid = pthread_create(&thread->handle, NULL, thread->startFunction, (void*)thread);
    if ( valid != 0 ) {
        thread->state = ThreadState_Invalid;
    }
}

#endif

//- [SECTION] Work Queue
internal thread_work_queue thread_work_queue_create(u32 threadCount)
{
    thread_work_queue result = ZeroStruct;
    result.semaphore = thread_semaphore_create(threadCount, threadCount);
    return result;
}

internal void thread_work_queue_add_entry(thread_work_queue* wq, thread_work_entry_function* userFunction, void* userData)
{
    // NOTE(abe): only one thread can write to the queue 
    // switch to InterlockedCompareExchange so that any thread can add
    
    u32 newNextEntryToWrite = ((wq->nextEntryToWrite + 1) % ARRAY_COUNT(wq->entries));
    assert( newNextEntryToWrite != wq->nextEntryToRead);
    thread_work_entry* entry = wq->entries + wq->nextEntryToWrite;
    entry->userData = userData;
    entry->userFunction = userFunction;
    wq->completionGoal++;
    
    COMPLETE_PAST_BEFORE_FUTURE_WRITES;
    
    wq->nextEntryToWrite = newNextEntryToWrite;
    thread_semaphore_release(&wq->semaphore, 1);
}

internal bool8 thread_work_queue_work_exists(thread_work_queue* wq)
{
    return ( wq->nextEntryToRead != wq->nextEntryToWrite );
}

internal void thread_spawn(u32 threadCount, thread* threadArray, thread_work_queue* wq)
{
    for (u32 i = 0; i < threadCount; i++) {
        thread* thread = threadArray + i;
        *thread = thread_create((void*)thread_internal_proc);
        thread->queue = wq;
        thread->logical_id = i;
        thread_run(thread);
    }
}

internal void thread_work_queue_complete(thread_work_queue* wq)
{
    thread dummyThread = ZeroStruct;
    dummyThread.logical_id = U32Max;
    while ( wq->completionCount != wq->completionGoal ) {
        thread_internal_complete_next_entry(wq, &dummyThread);
    }
    
    wq->completionGoal = 0;
    wq->completionCount = 0;
}

internal bool8 thread_internal_complete_next_entry(thread_work_queue* wq, thread* thread)
{
    bool8 shouldSleep = 0;
    
    u32 originalNextEntryToRead = wq->nextEntryToRead;
    u32 newNextEntryToRead = ( originalNextEntryToRead + 1 ) % ARRAY_COUNT(wq->entries);
    
    if ( originalNextEntryToRead != wq->nextEntryToWrite ) {
        u32 index = InterlockedCompareExchange32(&wq->nextEntryToRead, newNextEntryToRead, originalNextEntryToRead);
        
        if ( index == originalNextEntryToRead ) {
            thread_work_entry* entry = wq->entries + index;
            entry->userFunction(thread, entry->userData);
            InterlockedIncrement32(&wq->completionCount);
        }
        
    }
    
    else {
        shouldSleep = 1;
    }
    
    return shouldSleep;
}

internal void thread_internal_proc(void* threadInfo)
{
    thread* info = (thread*)threadInfo;
    thread_work_queue* queue = info->queue;
    
    for (;;) {
        
        bool8 shouldSleep = thread_internal_complete_next_entry(queue, info);
        if (shouldSleep ) {
            thread_semaphore_wait(&queue->semaphore);
        }
    }
}