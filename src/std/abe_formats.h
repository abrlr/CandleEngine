#ifndef __ABE_FORMATS_H__
#define __ABE_FORMATS_H__

#include "abe_base.h"

#pragma pack(push, 1)

//- [SEcTION] RIFF File

#define RIFFCHUNKID(a, b, c, d) ( (u32)(a << 0) | (u32)(b << 8) | (u32)(c << 16) | (u32)(d << 24) )

enum riff_chunk_id_ {
    riff_chunk_id_data = RIFFCHUNKID('d', 'a', 't', 'a'),
    riff_chunk_id_fmt = RIFFCHUNKID('f', 'm', 't', ' '),
    riff_chunk_id_riff = RIFFCHUNKID('R', 'I', 'F', 'F'),
    riff_chunk_id_wave = RIFFCHUNKID('W', 'A', 'V', 'E'),
};

struct riff_chunk_header {
    u32 id;
    u32 size;
};

struct wave_header {
    u32 riffID;
    u32 fileSize;
    u32 waveID;
};

struct wave_chunk_fmt {
    riff_chunk_header header;
    u16 formatTag;
    
    u16 channels;
    u32 sampleRate;
    u32 byteRate;
    u16 blockAlign;
    u16 bitsPerSample;
};

struct riff_file_iterator {
    void* at;
    void* end;
};

inline u32 riff_file_iterator_get_chunk_type(riff_file_iterator it);
inline riff_file_iterator riff_file_iterator_new(void* content, u32 size);
inline bool8 riff_file_iterator_valid(riff_file_iterator* it);
inline void riff_file_iterate(riff_file_iterator* it);

//- [SECTION] WAV File
struct wave_file {
    // RIFF chunk
    struct {
        char chunkID[4];
        u32 chunkSize;
        char typeID[4];
    };
    
    // WAV Data Description
    struct {
        char dataChunkID[4];
        u32 dataSize;
    };
};
#pragma pack(pop)

#endif // __ABE_FORMATS_H__