
internal void profiler_set(profiler* profiler)
{
    profiler_ptr = profiler;
    profiler->next = profiler->blocks;
    profiler->count = 0;
    profiler->current = 0;
    profiler->disabled = 0;
}

internal void profiler_begin_frame(profiler* profiler)
{
    profiler->next = profiler->blocks;
    profiler->count = 0;
    profiler->current = 0;
    profiler->disabled = 0;
}

internal void profiler_end_frame(profiler* profiler)
{
    profiler->disabled = 1;
}

internal profiler_block* profiler_block_new(profiler* profiler)
{
    profiler_block* result = 0;
    
    if ( profiler->count + 1 < ARRAY_COUNT(profiler->blocks) && !profiler->disabled ) {
        result = profiler->next++;
        *result = ZeroStruct;
        
        if ( profiler->current ) {
            result->parent = profiler->current;
            profiler_block* child = profiler->current->child;
            
            if ( child ) {
                while ( child->next ) {
                    child = child->next;
                }
                
                child->next = result;
            }
            
            else {
                profiler->current->child = result;
            }
        }
        
        profiler->current = result;
        profiler->count++;
    }
    
    return result;
}