#ifndef __ABE_MATHS_H__
#define __ABE_MATHS_H__

#include "abe_base.h"

// TODO(abe): SIMD

//- [SECTION] i32
internal i32 i32_sign(i32 value);
internal i32 i32_max(i32 a, i32 b);
internal i32 i32_min(i32 a, i32 b);
internal i32 i32_abs(i32 value);
internal i32 i32_clamp(i32 value, i32 min, i32 max);


//- [SECTION] f32
internal i32 f32_sign(f32 value);
internal i32 f32_floor(f32 value);
internal i32 f32_ceil(f32 value);
internal bool8 f32_equals(f32 a, f32 b);
internal bool8 f32_is_zero(f32 value);
internal bool8 f32_is_nan(f32 value);
internal f32 f32_max(f32 a, f32 b);
internal f32 f32_min(f32 a, f32 b);
internal f32 f32_abs(f32 value);
internal f32 f32_clamp(f32 value, f32 min, f32 max);
internal f32 f32_square_root_approx(f32 value);
internal f32 f32_rad_to_deg(f32 rad);
internal f32 f32_deg_to_rad(f32 deg);

// Easing Functions
internal f32 f32_lerp(f32 target, f32 source, f32 t);
internal f32 f32_ease_in_expo(f32 x, f32 offset);
internal f32 f32_ease_out_expo(f32 x, f32 offset);


//- [SECTION] v2i
introspect("maths") 
struct v2i {
    union {
        i32 x;
        i32 width;
    };
    union {
        i32 y;
        i32 height;
    };
};

inline v2i v2i_new();
inline v2i v2i_new(i32 value);
inline v2i v2i_new(i32 x, i32 y);

internal bool8 v2i_equals(v2i a, v2i b);
internal v2i v2i_add(v2i a, v2i b); // return (a + b)
internal v2i v2i_sub(v2i a, v2i b); // return (a - b)
internal v2i v2i_mult(v2i a, v2i b); // return { a.x  b.x ; a.y  b.y }
internal v2i v2i_times_i32(v2i v, i32 factor);
internal v2i v2i_times_f32(v2i v, f32 factor);
internal i32 v2i_dot(v2i a, v2i b); // return a . b


//- [SECTION] v2f
introspect("maths") 
struct v2f {
    union {
        f32 x;
        f32 width;
    };
    union {
        f32 y;
        f32 height;
    };
};

inline v2f v2f_new();
inline v2f v2f_new(f32 value);
inline v2f v2f_new(f32 x, f32 y);

internal v2i v2ftoi(v2f v);
internal bool8 v2f_is_zero(v2f v);
internal bool8 v2f_equals(v2f a, v2f b);
internal v2f v2f_add(v2f a, v2f b); // return (a + b)
internal v2f v2f_sub(v2f a, v2f b); // return (a - b)
internal v2f v2f_mult(v2f a, v2f b); // return { a.x  b.x ; a.y  b.y }
internal v2f v2f_times(v2f v, f32 factor);
internal f32 v2f_dot(v2f a, v2f b); // return a . b
internal f32 v2f_length(v2f v);
internal v2f v2f_normalize(v2f v);


//- [SECTION] line 2D
introspect("maths")
struct line2 {
    v2f p1;
    v2f p2;
};


//- [SECTION] rect2
introspect("maths") 
struct rect2 {
    union {
        v2f min;
        v2f p1;
    };
    union {
        v2f max;
        v2f p2;
    };
};


typedef u32 RectAnchor;
enum RectAnchor_ {
    RectAnchor_Default    = 0,
    RectAnchor_YTop       = 1 << 0,
    RectAnchor_YCenter    = 1 << 1,
    RectAnchor_YBottom    = 1 << 2,
    
    RectAnchor_XLeft      = 1 << 3,
    RectAnchor_XCenter    = 1 << 4,
    RectAnchor_XRight     = 1 << 5,
    
    RectAnchor_TopLeft    = RectAnchor_XLeft | RectAnchor_YTop,
};

inline rect2 rect2_new();
inline rect2 rect2_new(v2f p1, v2f p2);

internal rect2 rect2_from_size(v2f position, v2f size);
internal rect2 rect2_from_size_and_anchor(v2f anchorPosition, v2f size, RectAnchor anchor = RectAnchor_Default);
internal v2f rect2_mid(rect2 rect); // return midle of a rect2
internal v2f rect2_dim(rect2 rect); // return dimensions of the rect2
internal rect2 rect2_translate(rect2 rect, v2f v); // translates rect by v
internal rect2 rect2_resize(rect2 rect, f32 factor); // size around the midpoint

internal rect2 rect2_cut_left(rect2* rect, f32 amount);
internal rect2 rect2_cut_right(rect2* rect, f32 amount);
internal rect2 rect2_cut_top(rect2* rect, f32 amount);
internal rect2 rect2_cut_bottom(rect2* rect, f32 amount);

//- [SECTION] v3f
introspect("maths") 
struct v3f {
    union {
        f32 x;
        f32 r;
    };
    
    union {
        f32 y;
        f32 g;
    };
    
    union {
        f32 z;
        f32 b;
    };
};

inline v3f v3f_new();
inline v3f v3f_new(f32 value);
inline v3f v3f_new(f32 x, f32 y, f32 z);
inline v3f v3f_new(v2f v);

internal bool8 v3f_equals(v3f a, v3f b);
internal v3f v3f_add(v3f a, v3f b); // return (a + b)
internal v3f v3f_sub(v3f a, v3f b); // return (a - b)
internal v3f v3f_mult(v3f a, v3f b); // return { a.x  b.x ; a.y  b.y ; a.z  b.z }
internal v3f v3f_cross(v3f a, v3f b); // return a ^ b
internal f32 v3f_magnitude(v3f v);
internal v3f v3f_normalize(v3f v);
internal v3f v3f_lerp(v3f target, v3f base, f32 t);
internal v3f v3f_times(v3f v, f32 factor);
internal f32 v3f_dot(v3f a, v3f b);


//- [SECTION] line 3D
introspect("maths")
struct line3 {
    v3f p1;
    v3f p2;
};


//- [SECTION] v4f
introspect("maths") 
struct v4f {
    union {
        f32 x;
        f32 r;
        f32 left;
    };
    
    union {
        f32 y;
        f32 g;
        f32 top;
    };
    
    union {
        f32 z;
        f32 b;
        f32 right;
    };
    
    union {
        f32 w;
        f32 a;
        f32 bottom;
    };
};

inline v4f v4f_new();
inline v4f v4f_new(f32 value);
inline v4f v4f_new(f32 x_r, f32 y_g, f32 z_b, f32 w_a);
inline v4f v4f_new(v3f v);

internal v4f v4f_add(v4f a, v4f b); // return (a + b)
internal v4f v4f_sub(v4f a, v4f b); // return (a - b)
internal v4f v4f_mult(v4f a, v4f b); // return { a.x  b.x ; a.y  b.y ; a.z  b.z ; a.w  b.w }
internal v4f v4f_times(v4f v, f32 factor);
internal f32 v4f_magnitude(v4f v);
internal v4f v4f_normalize(v4f v);
internal v4f v4f_lerp(v4f target, v4f base, f32 t);
internal f32 v4f_dot(v4f a, v4f b);


//- [SECTION] m3f
introspect("maths") 
struct m3f {
    f32 x0; f32 x1; f32 x2;
    f32 y0; f32 y1; f32 y2;
    f32 z0; f32 z1; f32 z2;
};

internal m3f m3f_add(m3f* a, m3f* b);
internal m3f m3f_sub(m3f* a, m3f* b);
internal m3f m3f_mult(m3f* a, m3f* b); // matrix multiplication
internal v3f m3f_multv3f(m3f* m, v3f v);


//- [SECTION] m4f
introspect("maths") 
struct m4f {
    f32 x0; f32 x1; f32 x2; f32 x3;
    f32 y0; f32 y1; f32 y2; f32 y3;
    f32 z0; f32 z1; f32 z2; f32 z3;
    f32 w0; f32 w1; f32 w2; f32 w3;
};

internal m4f m4f_zero();
internal m4f m4f_identity();
internal m4f m4f_add(m4f* a, m4f* b);
internal m4f m4f_sub(m4f* a, m4f* b);
internal m4f m4f_mult(m4f* a, m4f* b); // matrix multiplication
internal v4f m4f_multv4f(m4f* m, v4f v);
internal m4f m4f_times(m4f* m, f32 factor);

internal m4f m4f_orthographic(f32 left, f32 right, f32 top, f32 bottom, f32 near, f32 far);
internal m4f m4f_orthographic_inverse(f32 left, f32 right, f32 top, f32 bottom, f32 near, f32 far);
internal m4f m4f_perspective(f32 fov, f32 aspect, f32 far, f32 near);
internal m4f m4f_lookat();

internal m4f m4f_transpose(m4f* m);
internal m4f m4f_inverse(m4f* m);

internal m4f m4f_translation_matrix(v3f v);
internal m4f m4f_scale_matrix(m4f* m, f32 factor);
internal m4f m4f_rotation_matrix(v3f axis, f32 angle);


//- [SECTION] Collision Detection
internal bool8 collisions_v2i_in_bounds(v2i point, v2i min, v2i max);
internal bool8 collisions_v2i_in_quad(v2i point, v2i quadPos, v2i quadSize);
internal bool8 collisions_v2f_in_bounds(v2f point, v2f min, v2f max);
internal bool8 collisions_v2f_in_quad(v2f point, v2f topLeft, v2f bottomRight);

internal bool8 collisions_v2i_in_rect(v2i point, rect2 rect);
internal bool8 collisions_v2f_in_rect(v2f point, rect2 rect);


//- [SECTION] Curves
internal v2f bezier_point_on_linear(v2f p1, v2f p2, f32 t);
internal v2f bezier_point_on_cubic(v2f p1, v2f p2, v2f p3, f32 t);
internal v2f bezier_point_on_quadratic(v2f p1, v2f p2, v2f p3, v2f p4, f32 t);


//- [SECTION] 3D Curves
internal v2f bezier3_point_on_linear(v2f p1, v2f p2, f32 t);
internal v2f bezier3_point_on_cubic(v2f p1, v2f p2, v2f p3, f32 t);
internal v2f bezier3_point_on_quadratic(v2f p1, v2f p2, v2f p3, v2f p4, f32 t);

#endif // __ABE_MATHS_H__