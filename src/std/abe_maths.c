#include "stdlib.h"
#include "math.h"
#include "abe_maths.h"

//- [SECTION] i32
internal i32 i32_sign(i32 value)
{
    i32 result = value >= 0 ? 1 : -1;
    return result;
}

internal i32 i32_max(i32 a, i32 b)
{
    i32 result = a >= b ? a : b;
    return result;
}

internal i32 i32_min(i32 a, i32 b)
{
    i32 result = a <= b ? a : b;
    return result;
}

internal i32 i32_abs(i32 value)
{
    if ( value < 0 ) return -value;
    return value;
}

internal i32 i32_clamp(i32 value, i32 min, i32 max)
{
    if ( value < min ) return min;
    if ( value > max ) return max;
    return value;
}


//- [SECTION] f32
inline i32 f32_sign(f32 value)
{
    if ( value < 0.f ) return -1;
    if ( value > 0.f ) return 1;
    return 0;
}

inline i32 f32_floor(f32 value)
{
    if ( value < 0.f ) return (i32)(value - 1);
    return (i32)value;
}

inline i32 f32_ceil(f32 value)
{
    if ( value < 0.f ) return (i32)(value - 0.5f);
    return (i32)(value + 0.5f);
}

inline bool8 f32_equals(f32 a, f32 b)
{
    if ( a == b ) return 1;
    
    bool8 result = ( a * (1.f + F32Epsilon) > b )
        && ( a* ( 1.f - F32Epsilon ) < b );
    return result;
}

inline bool8 f32_is_zero(f32 value)
{
    bool8 result = ( value > -F32Epsilon && value < F32Epsilon );;
    return result;
}

inline bool8 f32_is_nan(f32 value)
{
    bool8 result = (value != value);
    return result;
}

inline f32 f32_max(f32 a, f32 b)
{
    if (a < b) return b;
    return a;
}

inline f32 f32_min(f32 a, f32 b)
{
    if (a < b) return a;
    return b;
}

inline f32 f32_abs(f32 value)
{
    if ( value < 0.f ) return -value;
    return value;
}

inline f32 f32_clamp(f32 value, f32 min, f32 max)
{
    if ( value < min ) return min;
    if ( value > max ) return max;
    return value;
}

internal f32 f32_square_root_approx(f32 value)
{
    i32 i;
    f32 x, y;
    x = value * 0.5f;
    y = value;
    i = *(i32*) &y;
    i = 0x5f3759df - (i >> 1);
    y = *(f32*) &i;
    y = y * (1.5f - (x * y * y) );
    y = y * (1.5f - (x * y * y) );
    f32 result = value * y;
    return result;
}

inline f32 f32_rad_to_deg(f32 rad)
{
    f32 result = rad * 180.f / F32Pi;
    return result;
}

inline f32 f32_deg_to_rad(f32 deg)
{
    f32 result = fmod(deg, 360) * F32Pi / 180.f;
    return result;
}

//- Easing Functions
inline f32 f32_lerp(f32 target, f32 source, f32 t)
{
    f32 result = t * target + (1.f - t) * source;
    return result;
}

internal f32 f32_ease_in_expo(f32 x, f32 offset)
{
    f32 result = 0;
    f32 xoff = x - offset;
    if ( xoff <= 0 ) {
        result = 0;
    } else if ( xoff > 1 ) {
        result = 1;
    } else {
        result = pow(2, 10 * xoff - 10);
    }
    
    return result;
}

internal f32 f32_ease_out_expo(f32 x, f32 offset)
{
    f32 result = 0;
    f32 xoff = x - offset;
    if ( xoff > 1 ) {
        result = 1;
    } else if ( xoff < 0 ) {
        result = 0;
    } else {
        result = 1 - pow(2, -10 * xoff);
    }
    
    return result;
}

//- [SECTION] v2i
inline v2i v2i_new()
{
    return ZeroStruct;
}

inline v2i v2i_new(i32 value)
{
    v2i result;
    result.x = value;
    result.y = value;
    return result;
}

inline v2i v2i_new(i32 a, i32 b)
{
    v2i result;
    result.x = a;
    result.y = b;
    return result;
}

internal bool8 v2i_equals(v2i a, v2i b)
{
    bool8 result = ( a.x == b.x ) && ( a.y == b.y );
    return result;
}

internal v2i v2i_add(v2i a, v2i b)
{
    v2i result = ZeroStruct;
    result.x = a.x + b.x;
    result.y = a.y + b.y;
    return result;
}

internal v2i v2i_sub(v2i a, v2i b)
{
    v2i result = ZeroStruct;
    result.x = a.x - b.x;
    result.y = a.y - b.y;
    return result;
}

internal v2i v2i_mult(v2i a, v2i b)
{
    v2i result = ZeroStruct;
    result.x = a.x * b.x;
    result.y = a.y * b.y;
    return result;
}

internal v2i v2i_times_i32(v2i v, i32 factor)
{
    v2i result = ZeroStruct;
    result.x = v.x * factor;
    result.y = v.y * factor;
    return result;
}

internal v2i v2i_times_f32(v2i v, f32 factor)
{
    v2i result = ZeroStruct;    
    result.x = f32_floor(v.x * factor);
    result.y = f32_floor(v.y * factor);
    return result;
}

internal i32 v2i_dot(v2i a, v2i b)
{
    i32 result = a.x * b.x + a.y * b.y;
    return result;
}


//- [SECTION] v2f
inline v2f v2f_new(f32 value)
{
    v2f result = { value, value };
    return result;
}

inline v2f v2f_new(f32 a, f32 b)
{
    v2f result = { a, b };
    return result;
}

internal v2f v2itof(v2i v)
{
    return v2f_new(v.x, v.y);
}

internal v2i v2ftoi(v2f v)
{
    return v2i_new(f32_floor(v.x),
                   f32_floor(v.y));
}

internal bool8 v2f_is_zero(v2f v)
{
    return f32_is_zero(v.x) && f32_is_zero(v.y);
}

internal bool8 v2f_equals(v2f a, v2f b)
{
    bool8 xEquals = f32_equals(a.x, b.x);
    bool8 yEquals = f32_equals(a.y, b.y);
    return (xEquals && yEquals);
}

internal v2f v2f_add(v2f a, v2f b)
{
    v2f result = ZeroStruct;
    result.x = a.x + b.x;
    result.y = a.y + b.y;
    return result;
}

internal v2f v2f_sub(v2f a, v2f b)
{
    v2f result = ZeroStruct;
    result.x = a.x - b.x;
    result.y = a.y - b.y;
    return result;
}

internal v2f v2f_mult(v2f a, v2f b)
{
    v2f result = ZeroStruct;
    result.x = a.x * b.x;
    result.y = a.y * b.y;
    return result;
}

internal v2f v2f_times(v2f v, f32 factor)
{
    v2f result = ZeroStruct;
    result.x = v.x * factor;
    result.y = v.y * factor;
    return result;
}

internal f32 v2f_dot(v2f a, v2f b)
{
    f32 result = a.x * b.x + a.y * b.y;
    return result;
}

internal f32 v2f_length(v2f v)
{
    f32 lengthSquared = v.x * v.x + v.y * v.y;
    f32 length = f32_square_root_approx(lengthSquared);
    if ( length < 0.0005f && length > -0.005f )
        return 0.f;
    return length;
}

internal v2f v2f_normalize(v2f v)
{
    v2f result = ZeroStruct;
    f32 length = v2f_length(v);
    result.x = v.x / length;
    result.y = v.y / length;
    return result;
}


//- [SECTION] rect2
inline rect2 rect2_new(v2f p1, v2f p2)
{
    rect2 result = { p1, p2 };
    return result;
}

internal rect2 rect2_from_size(v2f position, v2f size)
{
    rect2 result = ZeroStruct;
    result.min = v2f_sub(position, v2f_times(size, 0.5f));
    result.max = v2f_add(position, v2f_times(size, 0.5f));
    return result;
}

internal rect2 rect2_from_size_and_anchor(v2f anchorPosition, v2f size, RectAnchor anchor)
{
    rect2 result = rect2_from_size(anchorPosition, size);
    v2f halfSize = v2f_times(size, 0.5);
    
    if ( anchor & RectAnchor_YTop ) {
        result = rect2_translate(result, v2f_new(0, halfSize.y));
    } else if ( anchor & RectAnchor_YBottom ) {
        result = rect2_translate(result, v2f_new(0, -halfSize.y));
    }
    
    if ( anchor & RectAnchor_XLeft ) {
        result = rect2_translate(result, v2f_new(halfSize.x, 0));
    } else if ( anchor & RectAnchor_XRight ) {
        result = rect2_translate(result, v2f_new(-halfSize.x, 0));
    }
    
    return result;
}

internal v2f rect2_mid(rect2 rect)
{
    v2f midpoint = ZeroStruct;
    midpoint.x = (rect.min.x + rect.max.x ) / 2;
    midpoint.y = (rect.min.y + rect.max.y ) / 2;
    return midpoint;
}

internal v2f rect2_dim(rect2 rect)
{
    v2f result = v2f_new((rect.max.x - rect.min.x),
                         (rect.max.y - rect.min.y));
    return result;
}

internal rect2 rect2_translate(rect2 rect, v2f v)
{
    rect2 result = rect2_new(v2f_add(rect.p1, v),
                             v2f_add(rect.p2, v));
    return result;
}

internal rect2 rect2_resize(rect2 rect, f32 factor)
{
    v2f midpoint = rect2_mid(rect);
    v2f midToMax = v2f_sub(rect.max, midpoint);
    v2f newMidToMax = v2f_times(midToMax, factor);
    
    rect2 result = ZeroStruct;
    result.min = v2f_sub(midpoint, newMidToMax);
    result.max = v2f_add(midpoint, newMidToMax);
    return result;
}

// The input rect is modified and the output rect is the left cutout of the input rectangle
internal rect2 rect2_cut_left(rect2* rect, f32 amount)
{
    f32 minX = rect->min.x;
    rect->min.x = f32_min(rect->max.x, rect->min.x + amount);
    
    rect2 result = rect2_new( v2f_new(minX, rect->min.y), v2f_new(rect->min.x, rect->max.y) );
    return result;
}

// The input rect is modified and the output rect is the right cutout of the input rectangle
internal rect2 rect2_cut_right(rect2* rect, f32 amount)
{
    f32 maxX = rect->max.x;
    rect->max.x = f32_max(rect->min.x, rect->max.x - amount);
    
    rect2 result = rect2_new( v2f_new(rect->max.x, rect->min.y), v2f_new(maxX, rect->max.y));
    return result;
}

internal rect2 rect2_cut_top(rect2* rect, f32 amount)
{
    f32 minY = rect->min.y;
    rect->min.y = f32_min(rect->max.y, rect->min.y + amount);
    
    rect2 result = rect2_new( v2f_new(rect->min.x, minY), v2f_new(rect->max.x, rect->min.y) );
    return result;
}

internal rect2 rect2_cut_bottom(rect2* rect, f32 amount)
{
    f32 maxY = rect->max.y;
    rect->max.y = f32_max(rect->min.y, rect->max.y - amount);
    
    rect2 result = rect2_new( v2f_new(rect->min.x, rect->max.y), v2f_new(rect->max.x, maxY));
    return result;
}

//- [SECTION] v3f
inline v3f v3f_new(f32 value)
{
    v3f result = { value, value, value };
    return result;
}

inline v3f v3f_new(f32 x, f32 y, f32 z)
{
    v3f result = { x, y, z };
    return result;
}

inline v3f v3f_new(v2f v)
{
    v3f result = { v.x, v.y, 0 };
    return result;
}

internal bool8 v3f_equals(v3f a, v3f b)
{
    bool8 xEquals = f32_equals(a.x, b.x);
    bool8 yEquals = f32_equals(a.y, b.y);
    bool8 zEquals = f32_equals(a.z, b.z);
    return ( xEquals && yEquals && zEquals );
}

internal v3f v3f_add(v3f a, v3f b)
{
    v3f result = {
        a.x + b.x,
        a.y + b.y,
        a.z + b.z
    };
    return result;
}

internal v3f v3f_sub(v3f a, v3f b)
{
    v3f result = {
        a.x - b.x,
        a.y - b.y,
        a.z - b.z
    };
    return result;
}

internal v3f v3f_mult(v3f a, v3f b)
{
    assert(0); // Not implemented
    v3f result = ZeroStruct;
    return result;
}

internal v3f v3f_cross(v3f a, v3f b)
{
    assert(0); // Not implemented
    v3f result = ZeroStruct;
    return result;
}

internal f32 v3f_magnitude(v3f v)
{
    f32 magnitude = f32_square_root_approx(v.x * v.x + v.y * v.y + v.z * v.z);
    if (  f32_equals(magnitude, 0) ) {
        magnitude = 0;
    }
    return magnitude;
}

internal v3f v3f_normalize(v3f v)
{
    f32 magnitude = v3f_magnitude(v);
    if ( f32_equals(magnitude, 0) ) {
        return { 0.f, 0.f, 0.f };
    }
    
    v3f result = v;
    result.x /= magnitude;
    result.y /= magnitude;
    result.z /= magnitude;
    return result; 
}

internal v3f v3f_lerp(v3f target, v3f base, f32 t)
{
    v3f result = ZeroStruct;
    result.x = f32_lerp(target.x, base.x, t);
    result.y = f32_lerp(target.y, base.y, t);
    result.z = f32_lerp(target.z, base.z, t);
    return result;
}

internal v3f v3f_times(v3f v, f32 factor)
{
    v3f result = {
        v.x * factor,
        v.y * factor,
        v.z * factor
    };
    
    return result;
}

internal f32 v3f_dot(v3f a, v3f b)
{
    return a.x * b.x + a.y * b.y + a.z * b.z;
}


//- [SECTION] v4f
inline v4f v4f_new(f32 value)
{
    v4f result = { value, value, value, value };
    return result;
}


inline v4f v4f_new(f32 x, f32 y, f32 z, f32 w)
{
    v4f result = { x, y, z, w };
    return result;
}

inline v4f v4f_new(v3f v)
{
    v4f result = { v.x, v.y, v.z, 0 };
    return result;
}

internal v4f v4f_add(v4f a, v4f b)
{
    v4f result = {
        a.x + b.x,
        a.y + b.y,
        a.z + b.z,
        a.w + b.w
    };
    
    return result;
}

internal v4f v4f_sub(v4f a, v4f b)
{
    v4f result = {
        a.x - b.x,
        a.y - b.y,
        a.z - b.z,
        a.w - b.w
    };
    
    return result;
}

internal v4f v4f_mult(v4f a, v4f b)
{
    assert(0); // Not implemented
    v4f result = ZeroStruct;
    return result;
}

internal v4f v4f_times(v4f v, f32 factor)
{
    v4f result = {
        v.x * factor,
        v.y * factor,
        v.z * factor,
        v.w * factor
    };
    
    return result;
}

internal f32 v4f_magnitude(v4f v)
{
    f32 magnitude = f32_square_root_approx(v.x * v.x + v.y * v.y + v.z * v.z + v.w * v.w);
    return magnitude;
}

internal v4f v4f_normalize(v4f v)
{
    f32 magnitude = v4f_magnitude(v);
    if ( f32_equals(magnitude, 0) ) {
        return { 0.f, 0.f, 0.f, 0.f };
    }
    
    v4f result = v;
    result.x /= magnitude;
    result.y /= magnitude;
    result.z /= magnitude;
    result.w /= magnitude;
    return result; 
}

// NOTE(abe): linear interpolation between target & base by an amount t
//            if t = 0: return base
//            if t = 1: return target
internal v4f v4f_lerp(v4f target, v4f base, f32 t)
{
    v4f result = ZeroStruct;
    result.x = t * target.x + (1.f - t) * base.x;
    result.y = t * target.y + (1.f - t) * base.y;
    result.z = t * target.z + (1.f - t) * base.z;
    result.w = t * target.w + (1.f - t) * base.w;
    return result;
}

internal f32 v4f_dot(v4f a, v4f b)
{
    return a.x * b.x + a.y * b.y + a.z * b.z + a.w * b.w;
}


//- [SECTION] m3f
internal m3f m3f_add(m3f* a, m3f* b)
{
    assert(0); // Not implemented
    m3f result = ZeroStruct;
    return result;
}

internal m3f m3f_sub(m3f* a, m3f* b)
{
    assert(0); // Not implemented
    m3f result = ZeroStruct;
    return result;
}

internal m3f m3f_mult(m3f* a, m3f* b)
{
    assert(0); // Not implemented
    m3f result = ZeroStruct;
    return result;
}

internal v3f m3f_multv3f_new(m3f* m, v3f v)
{
    assert(0); // Not implemented
    v3f result = ZeroStruct;
    return result;   
}


//- [SECTION] m4f
inline internal m4f m4f_zero()
{
    m4f result = ZeroStruct;
    return result;
}

internal m4f m4f_identity()
{
    m4f result = ZeroStruct;
    result.x0 = 1.f;
    result.y1 = 1.f;
    result.z2 = 1.f;
    result.w3 = 1.f;
    return result;
}

internal m4f m4f_mult(m4f* a, m4f* b)
{
    m4f result = ZeroStruct;
    result.x0 = a->x0 * b->x0 + a->x1 * b->y0 + a->x2 * b->z0 + a->x3 * b->w0; 
    result.x1 = a->x0 * b->x1 + a->x1 * b->y1 + a->x2 * b->z1 + a->x3 * b->w1; 
    result.x2 = a->x0 * b->x2 + a->x1 * b->y2 + a->x2 * b->z2 + a->x3 * b->w2; 
    result.x3 = a->x0 * b->x3 + a->x1 * b->y3 + a->x2 * b->z3 + a->x3 * b->w3;
    
    result.y0 = a->y0 * b->x0 + a->y1 * b->y0 + a->y2 * b->z0 + a->y3 * b->w0; 
    result.y1 = a->y0 * b->x1 + a->y1 * b->y1 + a->y2 * b->z1 + a->y3 * b->w1; 
    result.y2 = a->y0 * b->x2 + a->y1 * b->y2 + a->y2 * b->z2 + a->y3 * b->w2; 
    result.y3 = a->y0 * b->x3 + a->y1 * b->y3 + a->y2 * b->z3 + a->y3 * b->w3;
    
    result.z0 = a->z0 * b->x0 + a->z1 * b->y0 + a->z2 * b->z0 + a->z3 * b->w0; 
    result.z1 = a->z0 * b->x1 + a->z1 * b->y1 + a->z2 * b->z1 + a->z3 * b->w1; 
    result.z2 = a->z0 * b->x2 + a->z1 * b->y2 + a->z2 * b->z2 + a->z3 * b->w2; 
    result.z3 = a->z0 * b->x3 + a->z1 * b->y3 + a->z2 * b->z3 + a->z3 * b->w3;
    
    result.w0 = a->w0 * b->x0 + a->w1 * b->y0 + a->w2 * b->z0 + a->w3 * b->w0; 
    result.w1 = a->w0 * b->x1 + a->w1 * b->y1 + a->w2 * b->z1 + a->w3 * b->w1; 
    result.w2 = a->w0 * b->x2 + a->w1 * b->y2 + a->w2 * b->z2 + a->w3 * b->w2; 
    result.w3 = a->w0 * b->x3 + a->w1 * b->y3 + a->w2 * b->z3 + a->w3 * b->w3;
    
    return result;
}

internal v4f m4f_multv4f(m4f* m, v4f v)
{
    v4f result = ZeroStruct;
    result.x = m->x0 * v.x + m->x1 * v.y + m->x2 * v.z + m->x3 * v.w;
    result.y = m->y0 * v.x + m->y1 * v.y + m->y2 * v.z + m->y3 * v.w;
    result.z = m->z0 * v.x + m->z1 * v.y + m->z2 * v.z + m->z3 * v.w;
    result.w = m->w0 * v.x + m->w1 * v.y + m->w2 * v.z + m->w3 * v.w;
    return result;
}

internal m4f m4f_times(m4f* m, f32 factor)
{
    m4f result = *m;
    
    result.x0 *= factor;
    result.x1 *= factor;
    result.x2 *= factor;
    result.x3 *= factor;
    
    result.y0 *= factor;
    result.y1 *= factor;
    result.y2 *= factor;
    result.y3 *= factor;
    
    result.z0 *= factor;
    result.z1 *= factor;
    result.z2 *= factor;
    result.z3 *= factor;
    
    result.w0 *= factor;
    result.w1 *= factor;
    result.w2 *= factor;
    result.w3 *= factor;
    
    return result;
}

internal m4f m4f_orthographic(f32 left, f32 right, f32 top, f32 bottom, f32 nearPlane, f32 farPlane)
{
    m4f result = ZeroStruct;
    result.x0 = 2.f / (right - left);
    result.y1 = 2.f / (top - bottom);
    result.z2 = -2.f / (farPlane - nearPlane);
    
    result.x3 = - (right + left) / (right - left);
    result.y3 = - (top + bottom) / (top - bottom);
    result.z3 = - (farPlane + nearPlane) / (farPlane - nearPlane);
    result.w3 = 1.f;
    
    return result;
}

internal m4f m4f_orthographic_inverse(f32 left, f32 right, f32 top, f32 bottom, f32 nearPlane, f32 farPlane)
{
    m4f result = ZeroStruct;
    result.x0 = (right - left) / 2.f;
    result.y1 = (top - bottom) / 2.f;
    result.z2 = - (farPlane - nearPlane) / 2.f;
    
    result.x3 = (right + left) / 2.f;
    result.y3 = (top + bottom) / 2.f;
    result.z3 = - (farPlane + nearPlane) / 2.f;
    result.w3 = 1.f;
    
    return result;
}

internal m4f m4f_perspective(f32 fov, f32 aspect, f32 farPlane, f32 nearPlane)
{
    m4f result = ZeroStruct;
    
    f32 tanHalfFov = ( tan( fov / 2.f ) );
    
    result.x0 = 1.f / ( tanHalfFov * aspect );
    result.y1 = 1.f / tanHalfFov;
    result.z2 = farPlane / ( farPlane - nearPlane );
    result.w2 = 1.f;
    result.z3 = - ( farPlane * nearPlane ) / ( farPlane - nearPlane );
    
    return result;
}

// openGL is column major while my matrices are row major
internal m4f m4f_transpose(m4f* m)
{
    m4f result = *m;
    result.y0 = m->x1; result.x1 = m->y0;
    result.z0 = m->x2; result.x2 = m->z0;
    result.w0 = m->x3; result.x3 = m->w0;
    
    result.z1 = m->y2; result.y2 = m->z1;
    result.w1 = m->y3; result.y3 = m->w1;
    
    result.w2 = m->z3; result.z3 = m->w2;
    
    return result;
}

internal m4f m4f_inverse(m4f* m)
{
    m4f inv = ZeroStruct;
    
    inv.x0 = m->y1  * m->z2 * m->w3 - 
        m->y1  * m->z3 * m->w2 - 
        m->z1  * m->y2  * m->w3 + 
        m->z1  * m->y3  * m->w2 +
        m->w1 * m->y2  * m->z3 - 
        m->w1 * m->y3  * m->z2;
    
    inv.y0 = -m->y0  * m->z2 * m->w3 + 
        m->y0  * m->z3 * m->w2 + 
        m->z0  * m->y2  * m->w3 - 
        m->z0  * m->y3  * m->w2 - 
        m->w0 * m->y2  * m->z3 + 
        m->w0 * m->y3  * m->z2;
    
    inv.z0 = m->y0  * m->z1 * m->w3 - 
        m->y0  * m->z3 * m->w1 - 
        m->z0  * m->y1 * m->w3 + 
        m->z0  * m->y3 * m->w1 + 
        m->w0 * m->y1 * m->z3 - 
        m->w0 * m->y3 * m->z1;
    
    inv.w0 = -m->y0  * m->z1 * m->w2 + 
        m->y0  * m->z2 * m->w1 +
        m->z0  * m->y1 * m->w2 - 
        m->z0  * m->y2 * m->w1 - 
        m->w0 * m->y1 * m->z2 + 
        m->w0 * m->y2 * m->z1;
    
    inv.x1 = -m->x1  * m->z2 * m->w3 + 
        m->x1  * m->z3 * m->w2 + 
        m->z1  * m->x2 * m->w3 - 
        m->z1  * m->x3 * m->w2 - 
        m->w1 * m->x2 * m->z3 + 
        m->w1 * m->x3 * m->z2;
    
    inv.y1 = m->x0  * m->z2 * m->w3 - 
        m->x0  * m->z3 * m->w2 - 
        m->z0  * m->x2 * m->w3 + 
        m->z0  * m->x3 * m->w2 + 
        m->w0 * m->x2 * m->z3 - 
        m->w0 * m->x3 * m->z2;
    
    inv.z1 = -m->x0  * m->z1 * m->w3 + 
        m->x0  * m->z3 * m->w1 + 
        m->z0  * m->x1 * m->w3 - 
        m->z0  * m->x3 * m->w1 - 
        m->w0 * m->x1 * m->z3 + 
        m->w0 * m->x3 * m->z1;
    
    inv.w1 = m->x0  * m->z1 * m->w2 - 
        m->x0  * m->z2 * m->w1 - 
        m->z0  * m->x1 * m->w2 + 
        m->z0  * m->x2 * m->w1 + 
        m->w0 * m->x1 * m->z2 - 
        m->w0 * m->x2 * m->z1;
    
    inv.x2 = m->x1  * m->y2 * m->w3 - 
        m->x1  * m->y3 * m->w2 - 
        m->y1  * m->x2 * m->w3 + 
        m->y1  * m->x3 * m->w2 + 
        m->w1 * m->x2 * m->y3 - 
        m->w1 * m->x3 * m->y2;
    
    inv.y2 = -m->x0  * m->y2 * m->w3 + 
        m->x0  * m->y3 * m->w2 + 
        m->y0  * m->x2 * m->w3 - 
        m->y0  * m->x3 * m->w2 - 
        m->w0 * m->x2 * m->y3 + 
        m->w0 * m->x3 * m->y2;
    
    inv.z2 = m->x0  * m->y1 * m->w3 - 
        m->x0  * m->y3 * m->w1 - 
        m->y0  * m->x1 * m->w3 + 
        m->y0  * m->x3 * m->w1 + 
        m->w0 * m->x1 * m->y3 - 
        m->w0 * m->x3 * m->y1;
    
    inv.w2 = -m->x0  * m->y1 * m->w2 + 
        m->x0  * m->y2 * m->w1 + 
        m->y0  * m->x1 * m->w2 - 
        m->y0  * m->x2 * m->w1 - 
        m->w0 * m->x1 * m->y2 + 
        m->w0 * m->x2 * m->y1;
    
    inv.x3 = -m->x1 * m->y2 * m->z3 + 
        m->x1 * m->y3 * m->z2 + 
        m->y1 * m->x2 * m->z3 - 
        m->y1 * m->x3 * m->z2 - 
        m->z1 * m->x2 * m->y3 + 
        m->z1 * m->x3 * m->y2;
    
    inv.y3 = m->x0 * m->y2 * m->z3 - 
        m->x0 * m->y3 * m->z2 - 
        m->y0 * m->x2 * m->z3 + 
        m->y0 * m->x3 * m->z2 + 
        m->z0 * m->x2 * m->y3 - 
        m->z0 * m->x3 * m->y2;
    
    inv.z3 = -m->x0 * m->y1 * m->z3 + 
        m->x0 * m->y3 * m->z1 + 
        
        m->y0 * m->x1 * m->z3 - 
        m->y0 * m->x3 * m->z1 - 
        m->z0 * m->x1 * m->y3 + 
        m->z0 * m->x3 * m->y1;
    
    inv.w3 = m->x0 * m->y1 * m->z2 - 
        m->x0 * m->y2 * m->z1 - 
        m->y0 * m->x1 * m->z2 + 
        m->y0 * m->x2 * m->z1 + 
        m->z0 * m->x1 * m->y2 - 
        m->z0 * m->x2 * m->y1;
    
    f32 det = m->x0 * inv.x0 + m->x1 * inv.y0 + m->x2 * inv.z0 + m->x3 * inv.w0;
    
    assert( !f32_is_zero(det) );
    
    det = 1.0 / det;
    
    m4f result = m4f_times(&inv, det);
    return result;
}

internal m4f m4f_translation_matrix(v3f v)
{
    m4f translationMat = m4f_identity();
    translationMat.x3 = v.x;
    translationMat.y3 = v.y;
    translationMat.z3 = v.z;
    
    return translationMat;
}

internal m4f m4f_scale_matrix(f32 factor)
{
    assert(factor != 0.f);
    m4f scaleMatrix = m4f_identity();
    scaleMatrix.x0 = factor;
    scaleMatrix.y1 = factor;
    scaleMatrix.z2 = factor;
    
    return scaleMatrix;
}

internal m4f m4f_rotation_matrix(v3f axis, f32 angle)
{
    m4f rotationMat = ZeroStruct;
    
    f32 c = cos(angle);
    f32 s = sin(angle);
    v3f u = v3f_normalize(axis);
    
    rotationMat.x0 = c + u.x * u.x * ( 1.f - c );
    rotationMat.x1 = u.x * u.y * ( 1.f - c ) - u.z * s;
    rotationMat.x2 = u.x * u.z * ( 1.f - c ) + u.y * s;
    
    rotationMat.y0 = u.y * u.x * ( 1.f - c ) + u.z * s; 
    rotationMat.y1 = c + u.y * u.y * ( 1.f - c );
    rotationMat.y2 = u.y * u.z * ( 1.f - c ) - u.x * s;
    
    rotationMat.z0 = u.z * u.x * ( 1.f - c ) - u.y * s;
    rotationMat.z1 = u.z * u.y * ( 1.f - c ) + u.x * s;
    rotationMat.z2 = c + u.z * u.z * ( 1.f - c );
    
    rotationMat.w3 = 1.f;
    
    return rotationMat;
}


//- [SECTION] Collision Detection
internal bool8 collisions_v2i_in_bounds(v2i point, v2i min, v2i max)
{
    return (min.x <= point.x && point.x <= max.x &&
            min.y <= point.y && point.y <= max.y);
}

internal bool8 collisions_v2i_in_quad(v2i point, v2i quadPos, v2i quadSize)
{
    v2i halfSize = { quadSize.x / 2, quadSize.y / 2 };
    v2i topLeft = v2i_sub(quadPos, halfSize);
    v2i bottomRight = v2i_add(quadPos, halfSize);
    
    return collisions_v2i_in_bounds(point, topLeft, bottomRight);
}

internal bool8 collisions_v2f_in_bounds(v2f point, v2f min, v2f max)
{
    return (min.x <= point.x && point.x <= max.x &&
            min.y <= point.y && point.y <= max.y);
}

internal bool8 collisions_v2f_in_quad(v2f point, v2f quadPos, v2f quadSize)
{
    v2f halfSize = { quadSize.x / 2, quadSize.y / 2 };
    v2f topLeft = v2f_sub(quadPos, halfSize);
    v2f bottomRight = v2f_add(quadPos, halfSize);
    
    return collisions_v2f_in_bounds(point, topLeft, bottomRight);
}

internal bool8 collisions_v2i_in_rect(v2i point, rect2 rect)
{
    v2f f32Point = v2f_new(point.x, point.y);
    return collisions_v2f_in_bounds(f32Point, rect.min, rect.max);
}

internal bool8 collisions_v2f_in_rect(v2f point, rect2 rect)
{
    return collisions_v2f_in_bounds(point, rect.min, rect.max);
}


//- [SECTION] Curves
internal v2f bezier_point_on_linear(v2f p1, v2f p2, f32 t)
{
    v2f result = ZeroStruct;
    
    v2f p1p2 = v2f_sub(p2, p1);
    result.x = p1.x + p1p2.x * t;
    result.y = p1.y + p1p2.y * t;
    
    return result;
}

internal v2f bezier_point_on_quadratic(v2f p1, v2f p2, v2f p3, f32 t)
{
    v2f result = ZeroStruct;
    
    v2f l1 = bezier_point_on_linear(p1, p2, t);
    v2f l2 = bezier_point_on_linear(p2, p3, t);
    
    v2f l1l2 = v2f_sub(l2, l1);
    result.x = l1.x + l1l2.x * t;
    result.y = l1.y + l1l2.y * t;
    
    return result;
}

internal v2f bezier_point_on_cubic(v2f p1, v2f p2, v2f p3, v2f p4, f32 t)
{
    v2f result = ZeroStruct;
    
    v2f c1 = bezier_point_on_quadratic(p1, p2, p3, t);
    v2f c2 = bezier_point_on_quadratic(p2, p3, p4, t);
    
    v2f c1c2 = v2f_sub(c2, c1);
    result.x = c1.x + c1c2.x * t;
    result.y = c1.y + c1c2.y * t;
    
    return result;
}


//- [SECTION] 3D Curves
internal v3f bezier3_point_on_linear(v3f p1, v3f p2, f32 t)
{
    v3f result = ZeroStruct;
    
    v3f p1p2 = v3f_sub(p2, p1);
    result.x = p1.x + p1p2.x * t;
    result.y = p1.y + p1p2.y * t;
    result.z = p1.z + p1p2.z * t;
    
    return result;
}

internal v3f bezier3_point_on_quadratic(v3f p1, v3f p2, v3f p3, f32 t)
{
    v3f result = ZeroStruct;
    
    v3f l1 = bezier3_point_on_linear(p1, p2, t);
    v3f l2 = bezier3_point_on_linear(p2, p3, t);
    
    v3f l1l2 = v3f_sub(l2, l1);
    result.x = l1.x + l1l2.x * t;
    result.y = l1.y + l1l2.y * t;
    result.z = l1.z + l1l2.z * t;
    
    return result;
}

internal v3f bezier3_point_on_cubic(v3f p1, v3f p2, v3f p3, v3f p4, f32 t)
{
    v3f result = ZeroStruct;
    
    v3f c1 = bezier3_point_on_quadratic(p1, p2, p3, t);
    v3f c2 = bezier3_point_on_quadratic(p2, p3, p4, t);
    
    v3f c1c2 = v3f_sub(c2, c1);
    result.x = c1.x + c1c2.x * t;
    result.y = c1.y + c1c2.y * t;
    result.z = c1.z + c1c2.z * t;
    
    return result;
}