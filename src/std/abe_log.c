#include <stdio.h>
#include <stdarg.h>

#ifndef ABE_CONSOLE_ENABLE

internal void log_msg_internal(char* file, u32 line, char* msg, ...)
{
    va_list argptr;
    va_start(argptr, msg);
    
    char* lastSlashInFileStr = (char*)file + strlen(file);
    while ( *lastSlashInFileStr != '/' && *lastSlashInFileStr != '\\' ) {
        lastSlashInFileStr--;
        if ( lastSlashInFileStr == file ) break;
    }

    if ( lastSlashInFileStr != file ) {
        lastSlashInFileStr++;
    }
    
    printf("[Message] %s (%d): ", lastSlashInFileStr, line);
    vfprintf(stdout, msg, argptr);
    
    va_end(argptr);
}

internal void log_info_internal(char* file, u32 line, char* msg, ...)
{
    va_list argptr;
    va_start(argptr, msg);
    
    char* lastSlashInFileStr = (char*)file + strlen(file);
    while ( *lastSlashInFileStr != '/' && *lastSlashInFileStr != '\\' ) {
        lastSlashInFileStr--;
        if ( lastSlashInFileStr == file ) break;
    }

    if ( lastSlashInFileStr != file ) {
        lastSlashInFileStr++;
    }
    
    printf(C_Gre "[Information] %s (%d): " C_RCol, lastSlashInFileStr, line);
    vfprintf(stdout, msg, argptr);
    
    va_end(argptr);
}

internal void log_warn_internal(char* file, u32 line, char* msg, ...)
{
    va_list argptr;
    va_start(argptr, msg);
    
    char* lastSlashInFileStr = (char*)file + strlen(file);
    while ( *lastSlashInFileStr != '/' && *lastSlashInFileStr != '\\' ) {
        lastSlashInFileStr--;
        if ( lastSlashInFileStr == file ) break;
    }

    if ( lastSlashInFileStr != file ) {
        lastSlashInFileStr++;
    }
    
    printf(C_Yel "[Warning] %s (%d): " C_RCol, lastSlashInFileStr, line);
    vfprintf(stdout, msg, argptr);
    
    va_end(argptr);
}

internal void log_err_internal(char* file, u32 line, char* msg, ...)
{
    va_list argptr;
    va_start(argptr, msg);
    
    char* lastSlashInFileStr = (char*)file + strlen(file);
    while ( *lastSlashInFileStr != '/' && *lastSlashInFileStr != '\\' ) {
        lastSlashInFileStr--;
        if ( lastSlashInFileStr == file ) break;
    }

    if ( lastSlashInFileStr != file ) {
        lastSlashInFileStr++;
    }
    
    printf(C_Red "[Error] %s (%d): " C_RCol, lastSlashInFileStr, line);
    vfprintf(stdout, msg, argptr);
    
    va_end(argptr);
}

#else

internal void log_set_console(log_console* console)
{
    _log_console_ptr = console;
}

internal void log_move_buffer_up()
{
    if ( !_log_console_ptr ) { return; }
    
    for (i32 i = ABE_LOG_CONSOLE_LINE_COUNT - 2; i > -1; i--) {
        _log_console_ptr->lines[i+1].color = _log_console_ptr->lines[i].color;
        memcpy(_log_console_ptr->lines[i+1].lineStr, _log_console_ptr->lines[i].lineStr, ABE_LOG_LINE_HEADER_COUNT + ABE_LOG_LINE_BODY_COUNT);
    }
    
    for (i32 i = 0; i < ABE_LOG_LINE_BODY_COUNT; i++) _log_console_ptr->lines[0].lineStr[i] = ' ';
    _log_console_ptr->lineCount++;
}

internal void log_msg_internal(char* file, u32 line, char* msg,...)
{
    if ( !_log_console_ptr ) { return; }
    
    va_list argptr;
    va_start(argptr, msg);
    
    log_move_buffer_up();
    
    char* lastSlashInFileStr = (char*)file + strlen(file);
    while ( *lastSlashInFileStr != '/' && *lastSlashInFileStr != '\\' ) {
        lastSlashInFileStr--;
        if ( lastSlashInFileStr == file ) break;
    }
    
    char* headerStr = _log_console_ptr->lines[0].lineStr;
    sprintf(headerStr, "%s (%d):", lastSlashInFileStr + 1, line);
    
    _log_console_ptr->lines[0].lineStr[strlen(headerStr)] = ' ';
    
    char* consoleLine = _log_console_ptr->lines[0].lineStr + ABE_LOG_LINE_HEADER_COUNT;
    vsnprintf(consoleLine, ABE_LOG_LINE_BODY_COUNT, msg, argptr);
    
    _log_console_ptr->lines[0].color = color_white;
    
    va_end(argptr);
}

internal void log_info_internal(char* file, u32 line, char* msg,...)
{
    if ( !_log_console_ptr ) { return; }
    
    va_list argptr;
    va_start(argptr, msg);
    
    log_move_buffer_up();
    
    char* lastSlashInFileStr = (char*)file + strlen(file);
    while ( *lastSlashInFileStr != '/' && *lastSlashInFileStr != '\\' ) {
        lastSlashInFileStr--;
        if ( lastSlashInFileStr == file ) break;
    }
    
    char* headerStr = _log_console_ptr->lines[0].lineStr;
    sprintf(headerStr, "%s (%d):", lastSlashInFileStr + 1, line);
    
    _log_console_ptr->lines[0].lineStr[strlen(headerStr)] = ' ';
    
    char* consoleLine = _log_console_ptr->lines[0].lineStr + ABE_LOG_LINE_HEADER_COUNT;
    vsnprintf(consoleLine, ABE_LOG_LINE_BODY_COUNT, msg, argptr);
    
    _log_console_ptr->lines[0].color = color_green;
    
    va_end(argptr);
}

internal void log_err_internal(char* file, u32 line, char* msg,...)
{
    if ( !_log_console_ptr ) { return; }
    
    va_list argptr;
    va_start(argptr, msg);
    
    log_move_buffer_up();
    
    char* lastSlashInFileStr = (char*)file + strlen(file);
    while ( *lastSlashInFileStr != '/' && *lastSlashInFileStr != '\\' ) {
        lastSlashInFileStr--;
        if ( lastSlashInFileStr == file ) break;
    }
    
    char* headerStr = _log_console_ptr->lines[0].lineStr;
    sprintf(headerStr, "%s (%d):", lastSlashInFileStr + 1, line);
    
    _log_console_ptr->lines[0].lineStr[strlen(headerStr)] = ' ';
    
    char* consoleLine = _log_console_ptr->lines[0].lineStr + ABE_LOG_LINE_HEADER_COUNT;
    vsnprintf(consoleLine, ABE_LOG_LINE_BODY_COUNT, msg, argptr);
    
    _log_console_ptr->lines[0].color = color_red;
    
    va_end(argptr);
}

internal void log_warn_internal(char* file, u32 line, char* msg,...)
{
    if ( !_log_console_ptr ) { return; }
    
    va_list argptr;
    va_start(argptr, msg);
    
    log_move_buffer_up();
    
    char* lastSlashInFileStr = (char*)file + strlen(file);
    while ( *lastSlashInFileStr != '/' && *lastSlashInFileStr != '\\' ) {
        lastSlashInFileStr--;
        if ( lastSlashInFileStr == file ) break;
    }
    
    char* headerStr = _log_console_ptr->lines[0].lineStr;
    sprintf(headerStr, "%s (%d):", lastSlashInFileStr + 1, line);
    
    _log_console_ptr->lines[0].lineStr[strlen(headerStr)] = ' ';
    
    char* consoleLine = _log_console_ptr->lines[0].lineStr + ABE_LOG_LINE_HEADER_COUNT;
    vsnprintf(consoleLine, ABE_LOG_LINE_BODY_COUNT, msg, argptr);
    
    _log_console_ptr->lines[0].color = color_yellow;
    
    va_end(argptr);
}

#endif