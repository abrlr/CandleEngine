#ifndef __ABE_BASE_H__
#define __ABE_BASE_H__

#define global static
#define internal static
#define local static

#define introspect(args)

#define ZeroStruct { }

#define KiloBytes(x) ( (x) * 1024)
#define MegaBytes(x) ( KiloBytes(x) * 1024 )
#define GigaBytes(x) ( MegaBytes(x) * 1024 )

#define STRINGIFY__(x) #x
#define STRINGIFY(x) STRINGIFY__(x)

#include <stdlib.h>
#include <string.h>

#ifdef _WIN64
    #define ABE_WIN
#elif __APPLE__
    #define ABE_APPLE
#elif (defined(__GNUC__) && !defined(__clang__))
    #define ABE_LINUX
#else
    #error "Unsupported platform"
#endif

#ifdef assert
#undef assert
#endif

#ifdef ABE_WIN
#include <cstdint>
#define struct_offset(st, mbr) ( (u32)(&((st*)0)->mbr) )
#define assert(expression) { if ( !(expression) ) { *(int*)0 = 0; } }
#define s_assert(expression, msg) static_assert(expression, msg)
#define m_assert(expression, msg) \
{ \
if ( !(expression) ) { \
char buffer[1024] = { }; \
sprintf(buffer, "%s l.%d: %s", __FILE__, __LINE__, msg); \
MessageBox(NULL, buffer,  "Assert", MB_OK | MB_ICONERROR); \
exit(1); \
} \
}
#define CDL_MAX_PATH (MAX_PATH)

#elif defined(ABE_APPLE)
#include <stdint.h>
#define struct_offset(st, mbr) ( (u64)(&((st*)0)->mbr) )
#define assert(expression) { if ( !(expression) ) { __builtin_trap(); } }
#define s_assert(expression, msg)
#define m_assert(expression, msg) (assert(expression))
#define CDL_MAX_PATH (1024 * 4)
#pragma GCC diagnostic ignored "-Wwritable-strings"

#elif defined(ABE_LINUX)
#include <stdint.h>
#define struct_offset(st, mbr) ( (u64)(&((st*)0)->mbr) )
#define assert(expression) { if ( !(expression) ) { __builtin_trap(); } }
#define s_assert(expression, msg)
#define m_assert(expression, msg) (assert(expression))
#define CDL_MAX_PATH (1024 * 4)
#pragma GCC diagnostic ignored "-Wwrite-strings"

#endif

typedef int8_t i8;
typedef int16_t i16;
typedef int32_t i32;
typedef int64_t i64;

#define I8Max INT8_MAX
#define I16Max INT16_MAX
#define I32Max INT32_MAX
#define I64Max INT64_MAX

typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

#define U8Max UINT8_MAX
#define U16Max UINT16_MAX
#define U32Max UINT32_MAX
#define U64Max UINT64_MAX

typedef float f32;
typedef double f64;

#define F32Inf INFINITY
#define F32Pi 3.14159265358979323846f
#define F32e  2.71828182845904523536f
#define F32Epsilon 0.000001

typedef int8_t bool8;

struct color8 {
    u8 r;
    u8 g;
    u8 b;
    u8 a;
};

global color8 color_none = { 0, 0, 0, 0 };
global color8 color_white = { 255, 255, 255, 255 };
global color8 color_black = { 0, 0, 0, 255 };
global color8 color_gray = { 122, 122, 122, 255 };

global color8 color_red = { 255, 0, 0, 255 };
global color8 color_green = { 0, 255, 0, 255 };
global color8 color_blue = { 0, 0, 255, 255 };

global color8 color_yellow = { 255, 255, 0, 255 };
global color8 color_pink = { 255, 0, 255, 255 };
global color8 color_cyan = { 0, 255, 255, 255 };

inline u32 SafeTruncateU64toU32(u64 size)
{
    assert(size <= 0xFFFFFFFF);
    return (u32)size;
}

//- Console colors
// Text Reset
#define C_RCol "\033[0m"

// Blacks
#define C_Bla "\033[0;30m" 
#define C_BBla "\033[1;30m" 
#define C_UBla "\033[4;30m" 
#define C_IBla "\033[0;90m" 
#define C_BIBla "\033[1;90m" 
#define C_On_Bla "\033[40m" 
#define C_On_IBla "\033[0;100m"

// Reds
#define C_Red "\033[0;31m"
#define C_BRed "\033[1;31m"
#define C_URed "\033[4;31m"
#define C_IRed "\033[0;91m"
#define C_BIRed "\033[1;91m"
#define C_On_Red "\033[41m"
#define C_On_IRed "\033[0;101m"

// Greens
#define C_Gre "\033[0;32m"
#define C_BGre "\033[1;32m"
#define C_UGre "\033[4;32m"
#define C_IGre "\033[0;92m"
#define C_BIGre "\033[1;92m"
#define C_On_Gre "\033[42m"
#define C_On_IGre "\033[0;102m"

// Yellows
#define C_Yel "\033[0;33m"
#define C_BYel "\033[1;33m"
#define C_UYel "\033[4;33m"
#define C_IYel "\033[0;93m"
#define C_BIYel "\033[1;93m"
#define C_On_Yel "\033[43m"
#define C_On_IYel "\033[0;103m"

// Blues
#define C_Blu "\033[0;34m" 
#define C_BBlu "\033[1;34m" 
#define C_UBlu "\033[4;34m" 
#define C_IBlu "\033[0;94m" 
#define C_BIBlu "\033[1;94m" 
#define C_On_Blu "\033[44m"
#define C_On_IBlu "\033[0;104m"

//- Flag Utilities
#define BIT(x) ( 1 << x )

#define FLAG_HAS(var, flag) (var & flag)
#define FLAG_SET(var, flag) (var |= flag)
#define FLAG_CLEAR(var, flag) (var &= ~flag)
#define FLAG_TOGGLE(var, flag) (var ^= flag)

//- [SECTION] Array Utilities
#define ARRAY_COUNT(array) ( sizeof(array) / sizeof((array)[0]) )
#define ARRAY_COPY(source, target) for (int i = 0; i < ARRAY_COUNT(source); i++) { target[i] = source[i]; }
#define ARRAY_SET(array, value) for (int i = 0; i < ARRAY_COUNT(array); i++) { array[i] = (value); }
#define VALUE_IN_ARRAY_BOUNDS(value, array) ( value >= array && value < array + ARRAY_COUNT(array) )

#define MemoryCompare(p1, p2, size)  (memcmp((void*)p1, (void*)p2, size) == 0)
#define MemoryCopy(dest, from, size) (memcpy((void*)dest, (void*)from, size))
#define MemorySet(dest, value, size) (memset((void*)dest, value, size)
#define MemoryZero(dest, size)       (memset((void*)dest, 0, size))

//- [SECTION] String Utilities
struct str8 {
    u32 size;
    char* string;
};

internal bool8 IsNumber(char c);
internal bool8 IsAlpha(char c);
internal bool8 IsWhitespace(char c);

internal void ConcatString(i64 firstCount, char* firstStr,
                           i64 secCount, char* secStr,
                           i64 destCount, char* destStr);
internal void ConcatStr8(str8 first, str8 second, str8* dest);

internal u32 StringLength(const char* str);
internal u32 GetLineLength(char* line);
internal u32 GetLineCount(char* text);

internal char* GetNextLine(char* text);
internal u32 GetLastSlashLoc(char* text);

internal void SplitOnChar(str8 text, char token, str8* splits, u32* splitCount);
internal void SplitOnWhitespace(str8 text, str8* splits, u32* splitCount);

internal u32 GetFirstTokenLocation(char token, str8 text);
internal u32 GetFirstTokenLocation(char* token, str8 text);
internal u32 GetLastTokenLocation(char token, str8 text);

//- [SECTION] Number parsing
enum __NumberReadFlags_ {
    __NumberReadFlags_None      = BIT(0),
    __NumberReadFlags_Decimal   = BIT(1),
    __NumberReadFlags_Sign      = BIT(2),
    __NumberReadFlags_Float     = __NumberReadFlags_Sign | __NumberReadFlags_Decimal
};

internal u32 __GetSizeOfDesiredNumberRead(str8 text, __NumberReadFlags_ flags, u32 maxSize);
internal u32 ReadU32(str8 text);
internal i32 ReadI32(str8 text);
internal f32 ReadF32(str8 text);
internal i32 ReadI32AfterTokenAndAdvance(char* token, str8* text);

//- [SECTION] Memory Utilities
// TODO(abe): will be moved into cdl_memory.h
struct memory_block_entry {
    u32 id;
    bool8 used;
#ifdef CDL_DEBUG
    char file[32];
    char name[32];
    u32 line;
#endif
    
    u32 size;
    u8* location;
    u8** ptrAddr;
};

#define MEMORY_POOL_MAX_ENTRIES 2048
struct memory_block {
    memory_block_entry entries[MEMORY_POOL_MAX_ENTRIES];
    memory_block_entry* nextEntry;
    u32 nextEntryID;
    
    u64 size;
    u64 occupied;
    u8* location;
    u8* next;
};

internal void memory_block_new(memory_block* block, void* location, u64 size);
internal void memory_block_alloc_array(u8** ptrAddr, memory_block* block, u64 structSize, u32 count, char* name, char* file, i32 line);
internal void memory_block_layout_collapse(memory_block* block);
internal void memory_block_release_chunk(memory_block* block, void* chunk);
internal void memory_block_clear(memory_block* block);

#define MemoryAllocArray(ptrAddr, block, st, count, name) (memory_block_alloc_array((u8**)ptrAddr, block, sizeof(st), count, name, __FILE__, __LINE__))
#define MemoryAllocStruct(ptrAddr, block, st, name) (memory_block_alloc_array((u8**)ptrAddr, block, sizeof(st), 1, name, __FILE__, __LINE__))
#define MemoryRelease(block, chunk) (memory_block_release_chunk(block, chunk))
#define MemoryClear(block) (memory_block_clear(block))

#endif // __ABE_BASE_H__