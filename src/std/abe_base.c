#include "abe_base.h"

//- [SECTION] String Utilities
internal bool8 IsNumber(char c)
{
    return ( (c >= '0') && (c <= '9') );
}

internal bool8 IsAlpha(char c)
{
    bool8 isLowerCase = (c >= 'a') && (c <= 'z');
    bool8 isUpperCase = (c >= 'A') && (c <= 'Z');
    return ( isLowerCase || isUpperCase );
}

internal bool8 IsWhitespace(char c)
{
    return ( (c == ' ') || 
            (c == '\n') ||
            (c == '\t') ||
            (c == '\r'));
}

internal void ConcatString(i64 firstCount, char* firstStr,
                           i64 secCount, char* secStr,
                           i64 destCount, char* destStr)
{
    for(int i = 0; i < firstCount; i++) {
        *destStr++ = *firstStr++;
    }
    
    for(int i = 0; i < secCount; i++) {
        *destStr++ = *secStr++;
    }
    
    *destStr++ = 0;
}

internal void ConcatStr8(str8 first, str8 second, str8* dest)
{
    char* destStr = dest->string;
    for(u32 i = 0; i < first.size; i++) {
        *destStr++ = *first.string++;
    }
    
    for(u32 i = 0; i < second.size; i++) {
        *destStr++ = *second.string++;
    }
    
    dest->size = destStr - dest->string;
    *destStr++ = 0;
}

internal u32 StringLength(const char* str)
{
    u32 size = 0;
    
    while (str[size] != '\0') {
        size++;
    }
    
    return size;
}

internal u32 GetLineLength(char* line)
{
    char* ptr = (char*)line;
    
    while ( *ptr != '\n' && *ptr != '\0' ) {
        ptr++;
    }
    
    return (u32)(ptr - line);
}

internal u32 GetLineCount(char* text)
{
    char* ptr = (char*)text;
    u32 result = 0;
    
    while (*ptr != '\0' ) {
        if ( *ptr == '\n' )
            result++;
        ptr++;
    }
    
    return result;
}

internal char* GetNextLine(char* text)
{
    char* ptr = (char*)text;
    
    while (*ptr != '\n' && *ptr != '\0' ) {
        ptr++;
        
        if ( *ptr == '\0' ) return ptr;
    }
    ptr++;
    
    return ptr;
}

internal u32 GetLastSlashLoc(char* text)
{
    // Goes from the end of the file string to its beginning to find a '/' or '\' char
    char* lastSlashInFileStr = text + StringLength(text);
    while ( *lastSlashInFileStr != '/' && *lastSlashInFileStr != '\\' ) {
        lastSlashInFileStr--;
        if ( lastSlashInFileStr == text ) break;
    }
    
    return (u32)(lastSlashInFileStr - text);
}

// NOTE(abe): this assumes that the output array of pointers is big enough
internal void SplitOnChar(str8 text, char token, str8* splits, u32* splitCount)
{
    u32 splitIndex = 0;
    
    splits[splitIndex].string = text.string;
    u32 previousEnd = 0;
    
    for (u32 i = 0; i < text.size; i++) {
        
        if ( *(text.string + i) == token ) {
            splits[splitIndex].size = i - previousEnd;
            
            // Skip whitespace char for length calculation
            previousEnd = i + 1;
            splitIndex += 1;
            splits[splitIndex].string = (char*)text.string + i + 1;
        }
    }
    
    splits[splitIndex].size = text.size - previousEnd;
    *splitCount = splitIndex + 1;
}

internal void SplitOnWhitespace(str8 text, str8* splits, u32* splitCount)
{
    if ( !text.string || text.size <= 0 ) { return; }
    
    u32 splitIndex = 0;
    
    splits[splitIndex].string = text.string;
    u32 previousEnd = 0;
    
    for (u32 i = 0; i < text.size; i++) {
        
        if ( IsWhitespace(*(text.string + i)) ) {
            splits[splitIndex].size = i - previousEnd;
            
            // Skip whitespace char for length calculation
            previousEnd = i + 1;
            splitIndex += 1;
            splits[splitIndex].string = (char*)text.string + i + 1;
        }
    }
    
    splits[splitIndex].size = text.size - previousEnd;
    *splitCount = splitIndex + 1;
}

internal u32 GetFirstTokenLocation(char token, str8 text)
{
    if ( !text.string || text.size <= 0 ) { return U32Max; }
    
    for (char* linePtr = text.string; (u32)(linePtr - text.string) < text.size; linePtr++) {
        if ( token == *linePtr ) {
            return (u32)(linePtr - text.string);
        }
    }
    
    return U32Max;
}

internal u32 GetFirstTokenLocation(char* token, str8 text)
{
    if ( !text.string || text.size <= 0 ) { return U32Max; }
    
    u32 tokenSize = StringLength(token);
    
    for (char* linePtr = text.string; (u32)(linePtr - text.string) < text.size - tokenSize + 1; linePtr++) {
        if ( memcmp(token, linePtr, tokenSize) == 0 ) {
            return (u32)(linePtr - text.string);;
        }
    }
    
    return U32Max;
}

internal u32 GetLastTokenLocation(char token, str8 text)
{
    if ( !text.string || text.size <= 0 ) { return U32Max; }
    
    for (char* ptr = text.string + text.size - 1; ptr != text.string; ptr--) {
        if ( token == *ptr ) {
            return (u32)(ptr - text.string);
        }
    }
    
    return 0;
}

//- [SECTION] Number Parsing
internal u32 __GetSizeOfDesiredNumberRead(str8 text, __NumberReadFlags_ flags, u32 maxSize)
{
    assert(text.string != 0);
    
    // Initialise
    bool8 foundDecimal = 0;
    u32 numberReadStringSize = 0;
    u32 startAt = 0;
    
    // Test for a negative number
    if ( (flags & __NumberReadFlags_Sign) && (text.string[0] == '-') ) {
        numberReadStringSize++;
        startAt++;
    }
    
    // Loop over the characters of the text
    for (u32 charIndex = startAt; charIndex < text.size; charIndex++) {
        
        // Account for the decimal point
        if ( (flags & __NumberReadFlags_Decimal) && (text.string[charIndex] == '.') ) {
            if ( !foundDecimal ) {
                foundDecimal = 1;
                charIndex++; // Skip the decimal
                numberReadStringSize++;
            }
            
            else break;
        }
        
        // Check if we are still getting numbers
        if ( IsNumber(text.string[charIndex]) ) {
            numberReadStringSize++;
        } else {
            break;
        }
        
        assert(numberReadStringSize < maxSize);
    }
    
    return numberReadStringSize;
}

internal u32 ReadU32(str8 text)
{
    u32 u32StringSize = __GetSizeOfDesiredNumberRead(text, __NumberReadFlags_None, 32);
    
    char charResult[32] = { };
    MemoryCopy(charResult, text.string, u32StringSize);
    
    u64 u64result = strtoul(charResult, 0, 10);
    u32 result = SafeTruncateU64toU32(u64result);
    
    return result;
}

internal i32 ReadI32(str8 text)
{
    u32 i32StringSize = __GetSizeOfDesiredNumberRead(text, __NumberReadFlags_Sign, 32);
    
    char charResult[32] = { };
    MemoryCopy(charResult, text.string, i32StringSize);
    
    i32 result = atoi(charResult);
    return result;
}

internal f32 ReadF32(str8 text)
{
    u32 f32StringSize = __GetSizeOfDesiredNumberRead(text, __NumberReadFlags_Float, 32);
    
    char charResult[32] = { };
    MemoryCopy(charResult, text.string, f32StringSize);
    
    f32 result = atof(charResult);
    return result;
}

// TODO(abe): rewrite this using ReadI32
internal i32 ReadI32AfterTokenAndAdvance(char* token, str8* text)
{
    assert(token != 0);
    assert(text->string != 0);
    
    // Initialise
    u32 tokenSize = StringLength(token);
    u32 tokenLoc = GetFirstTokenLocation(token, *text);
    if ( tokenLoc == U32Max ) return I32Max;
    u32 i32StringSize = 0;
    
    // Test for a negative number
    if ( text->string[tokenLoc + tokenSize + i32StringSize] == '-' ) {
        i32StringSize++;
    }
    
    while ( IsNumber(text->string[tokenLoc + tokenSize + i32StringSize]) && i32StringSize < 32 ) {
        i32StringSize++;
    }
    
    i32 result = I32Max;
    char charResult[32] = { };
    MemoryCopy(charResult, (void*)&text->string[tokenLoc + tokenSize], i32StringSize);
    
    result = atoi(charResult);
    text->string += tokenLoc + tokenSize + i32StringSize;
    text->size -= tokenLoc + tokenSize + i32StringSize;
    
    return result;
}

//- [SECTION] Memory Utilities

internal void memory_block_new(memory_block* block, void* location, u64 size)
{
    block->size = size;
    block->location = (u8*)location;
    block->next = (u8*)location;
    
    block->nextEntry = block->entries;
}

internal void memory_block_alloc_array(u8** ptrAddr, memory_block* block, u64 structSize, u32 count, char* name, char* file, i32 line)
{
    assert(block->occupied + structSize * count < block->size);
    u8* location = block->next;
    block->occupied += structSize * count;
    block->next += structSize * count;
    
    block->nextEntry->id = 1 + block->nextEntryID++;
    block->nextEntry->used = 1;
    
#ifdef CDL_DEBUG
    char* lastSlashInFileStr = file + GetLastSlashLoc(file) + 1;
    MemoryZero(block->nextEntry->file, sizeof(block->nextEntry->file));
    MemoryZero(block->nextEntry->name, sizeof(block->nextEntry->name));
    MemoryCopy(block->nextEntry->file, (lastSlashInFileStr), StringLength(lastSlashInFileStr));
    MemoryCopy(block->nextEntry->name, name, StringLength(name));
    block->nextEntry->line = line;
#endif
    
    block->nextEntry->size = structSize * count;
    block->nextEntry->location = (u8*)location;
    block->nextEntry->ptrAddr = ptrAddr;
    block->nextEntry++;
    
    *ptrAddr = location;
}

internal void memory_block_layout_collapse(memory_block* block)
{
    // Pack memory entries towards the head of the memory block
    for (u32 i = 0; i < MEMORY_POOL_MAX_ENTRIES - 1; i++) {
        if ( !block->entries[i].used && block->entries[i+1].used ) {
            MemoryCopy(block->entries[i].location, block->entries[i+1].location, block->entries[i+1].size);
            
#ifdef CDL_DEBUG
            MemoryZero(block->entries[i].file, sizeof(block->nextEntry->file));
            MemoryZero(block->entries[i].name, sizeof(block->nextEntry->name));
            MemoryCopy(block->entries[i].file, block->entries[i+1].file, StringLength(block->entries[i+1].file));
            MemoryCopy(block->entries[i].name, block->entries[i+1].name, StringLength(block->entries[i+1].name));
            block->entries[i].line = block->entries[i+1].line;
#endif
            
            block->entries[i].size = block->entries[i+1].size;
            block->entries[i+1].size = 10;
            block->entries[i].used = 1;
            block->entries[i+1].used = 0;
            
            block->entries[i].ptrAddr = block->entries[i+1].ptrAddr;
            *block->entries[i].ptrAddr = block->entries[i].location;
            block->entries[i+1].location = block->entries[i].location + block->entries[i].size;
        }
    }
    
    // Backtrack the allocation cursor
    for (u32 i = MEMORY_POOL_MAX_ENTRIES - 1; i > 1; i--) {
        if ( !block->entries[i].used && block->entries[i - 1].used ) {
            block->nextEntry = block->entries + i;
            break;
        }
    }
}

internal void memory_block_release_chunk(memory_block* block, void* chunk)
{
    for (u32 i = 0; i < MEMORY_POOL_MAX_ENTRIES; i++) {
        if ( block->entries[i].location == (u8*)chunk ) {
            memory_block_entry* entry = block->entries + i;
            entry->used = 0;
            block->occupied -= entry->size;
            
            //memory_block_layout_collapse(block);
            break;
        }
    }
}

internal void memory_block_clear(memory_block* block)
{
    block->nextEntry = block->entries;
    block->nextEntryID = 0;
    
    block->next = block->location;
    block->occupied = 0;
}