#ifndef __ABE_PROFILER_H__
#define __ABE_PROFILER_H__

#ifdef ABE_WIN
#include <windows.h>
#elif (defined(ABE_APPLE) || defined(ABE_LINUX))
#include <time.h>
#else
#error "Unsupported Platform"
#endif

struct profiler_block {
    char name[64];
    char file[64];
    i32 line;
    i32 depth;
    
#ifdef ABE_WIN
    LARGE_INTEGER start;
    LARGE_INTEGER end;
#elif (defined(ABE_APPLE) || defined(ABE_LINUX))
    timespec start;
    timespec end;
#endif
    
    i32 time; //ns
    
    profiler_block* parent;
    profiler_block* next;
    profiler_block* child;
};

struct profiler {
    bool8 disabled;
    u32 count;
    profiler_block blocks[4192];
    profiler_block* next;
    profiler_block* current;
};
profiler* profiler_ptr;

internal void profiler_set(profiler* profiler)
{
    profiler_ptr = profiler;
    profiler->next = profiler->blocks;
    profiler->count = 0;
    profiler->current = 0;
    profiler->disabled = 0;
}

internal void profiler_begin_frame(profiler* profiler)
{
    profiler->next = profiler->blocks;
    profiler->count = 0;
    profiler->current = 0;
    profiler->disabled = 0;
}

internal void profiler_end_frame(profiler* profiler)
{
    profiler->disabled = 1;
}

internal profiler_block* profiler_block_new(profiler* profiler)
{
    profiler_block* result = 0;
    
    if ( profiler->count + 1 < ARRAY_COUNT(profiler->blocks) && !profiler->disabled ) {
        result = profiler->next++;
        *result = ZeroStruct;
        
        if ( profiler->current ) {
            result->parent = profiler->current;
            profiler_block* child = profiler->current->child;
            
            if ( child ) {
                while ( child->next ) {
                    child = child->next;
                }
                
                child->next = result;
            }
            
            else {
                profiler->current->child = result;
            }
        }
        
        profiler->current = result;
        profiler->count++;
    }
    
    return result;
}

struct profiler_counter {
    
    profiler_block* block;
    profiler* p;
    
    profiler_counter(profiler* profiler, char* name, char* file, int line) {
        assert(profiler);
        p = profiler;
        
        if ( !p->disabled ) {
            block = profiler_block_new(p);
            if ( block ) {
                
                if ( block->parent ) {
                    block->depth = block->parent->depth + 1;
                }
                
                MemoryCopy(block->name, name, ARRAY_COUNT(block->name));
                MemoryCopy(block->file, file, ARRAY_COUNT(block->file));
                block->line = line;
                
#ifdef ABE_WIN
                QueryPerformanceCounter(&block->start);
#elif (defined(ABE_APPLE) || defined(ABE_LINUX))
                clock_gettime(CLOCK_MONOTONIC, &block->start);
#endif
            }
        }
    };
    
    ~profiler_counter() {
        if ( !p->disabled && block ) {
            
#ifdef ABE_WIN
            QueryPerformanceCounter(&block->end);
            
            LARGE_INTEGER freq;
            QueryPerformanceFrequency(&freq);
            block->time += ( block->end.QuadPart - block->start.QuadPart ) * 1000000000 / freq.QuadPart;
#elif (defined(ABE_APPLE) || defined(ABE_LINUX))
            clock_gettime(CLOCK_MONOTONIC, &block->end);
            
            block->time += ( block->end.tv_nsec - block->start.tv_nsec );
#endif
            p->current = block->parent;
        }
    };
};

#define PROFILED_BLOCK(name) profiler_counter ___COUNTER__(profiler_ptr, (char*)name, __FILE__, __LINE__)
#define PROFILED_FUNC PROFILED_BLOCK(__func__)

#endif // __ABE_PROFILER_H__