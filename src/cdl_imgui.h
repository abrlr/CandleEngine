#ifndef __CDL_INCLUDE_CDL_IMUI_H__
#define __CDL_INCLUDE_CDL_IMUI_H__

//- Forward declarations

// Structs
struct im_id;
struct im_draw_command;
struct im_rect_theme;
struct im_color_theme;
struct im_layout;
struct im_item_data;
struct im_window;
struct im_dockspace;
struct im_context;
struct im_chart_atb;

// Typedefs
typedef u32 IMVarType;
typedef u32 IMDrawCommand;
typedef u32 IMLayout;
typedef u32 IMDimensionRule;
typedef u32 IMWindowFlag;
typedef u32 IMDockType;
typedef u32 IMWidgetType;
typedef u32 IMWidgetFlag;
typedef u32 IMChartMarker;

typedef f32 (*chartFunc)(f32);

//-

//- [SECTION] Generics
enum IMVarType_ {
    IMVarType_Undefined,
    
    IMVarType_u32,
    IMVarType_i32,
    IMVarType_f32,
    
    IMVarType_Count
};

//- [SECTION] IDs
introspect("IMGUI")
struct im_id {
    u32 primary;
    u32 secondary;
};

internal inline im_id im_id_new(const char* name);
internal inline im_id im_id_window(const char* name);

internal inline bool8 im_id_equal(im_id a, im_id b);
internal inline bool8 im_id_is_invalid(im_id id);

internal inline void im_id_group_begin(const char* groupName);
internal inline void im_id_group_end();

//- [SECTION] Draw Commands
enum IMDrawCommand_ {
    IMDrawCommand_Null,
    
    IMDrawCommand_Line,
    IMDrawCommand_Triangle,
    IMDrawCommand_Rect,
    IMDrawCommand_RectFilled,
    IMDrawCommand_RectTextured,
    IMDrawCommand_Circle,
    IMDrawCommand_CircleFilled,
    
    IMDrawCommand_Text,
    
    IMDrawCommand_PushClipRect,
    IMDrawCommand_PopClipRect,
    
    IMDrawCommand_Count
};

introspect("IMGUI")
struct im_draw_command {
    IMDrawCommand type;
    
    color8 color;
    u32 lineWidth;
    union {
        // Line
        struct {
            v2f p1;
            v2f p2;
        };
        
        // Triangle
        struct {
            v2f tri_positions[3];
        };
        
        // Rect, Clip Rect
        struct {
            rect2 rect;
            v4f uvs;
            u32 textureID;
        };
        
        // Circle
        struct {
            f32 radius;
            v2f center;
        };
        
        // Text
        struct {
            char text[256];
            v2f position;
            cdl_text_atb textAttr;
        };
    };
};

internal void im_draw_line(v2f start, v2f end, u32 width, color8 color);
internal void im_draw_triangle(v2f positions[3], color8 color);
internal void im_draw_rect(rect2 rect, u32 width, color8 color);
internal void im_draw_rect_filled(rect2 rect, color8 color);
internal void im_draw_rect_textured(rect2 rect, v4f uvs, u32 texture);
internal void im_draw_circle(v2f center, f32 radius, u32 lineWidth, color8 color);
internal void im_draw_circle_filled(v2f center, f32 radius, color8 color);
internal void im_draw_text(const char* text, v2f pos, cdl_text_atb attr);
internal void im_push_clip_rect(rect2 rect);
internal void im_pop_clip_rect();

//- [SECTION] Color Theme
introspect("IMGUI")
struct im_rect_theme {
    color8 background;
    color8 border;
    color8 selected;
    color8 hovered;
    color8 active;
    
    union {
        color8 handle;
        color8 icon;
        color8 toggled;
    };
};

introspect("IMGUI")
struct im_color_theme {
    // Fonts
    color8 font_header;
    color8 font_default;
    color8 font_hint;
    
    im_rect_theme window;
    im_rect_theme window_header;
    
    im_rect_theme widget;
    im_rect_theme widget_danger;
    im_rect_theme checkbox_icon;
    im_rect_theme radio_icon;
    
    // Charts
    color8 chart_colors[8];
};

//- [SECTION] Layouts
enum IMLayout_ {
    IMLayout_Vertical,
    IMLayout_Horizontal,
    
    IMLayout_Detached,
};

enum IMDimensionRule_ {
    IMDimensionRule_Fixed,
    IMDimensionRule_Relative
};

#define DEF_WIDTH (100)
#define MIN_WIDTH (70)
#define DEF_HEIGHT (20)
#define DEF_TREE_INDENT (10)

introspect("IMGUI")
struct im_layout {
    IMLayout type;
    v2f cursor;
    rect2 rect;
    
    v2f margins;
    
    IMDimensionRule widthRule;
    f32 width;
    f32 minWidth;
};

internal void im_layout_begin(IMLayout type);
internal void im_layout_end();

internal rect2 im_layout_new_rect();
internal bool8 im_layout_add_rect(rect2 rect);

internal void im_layout_set_widget_width(IMDimensionRule rule, f32 width);
internal void im_layout_indent(f32 indent);
internal void im_layout_unindent(f32 indent);

//- [SECTION] Widget Data
struct im_item_data {
    im_id id;
    im_item_data* parent;
    
    // Background color transition
    f32 transitionFactor;
    color8 initialColor;
    color8 targetColor;
    
    union {
        // Group persist
        struct {
            bool8 toggled;
            v2f groupLineBegin;
        };
        
        // Tooltip / Popups
        struct {
            bool8 open;
            bool8 openedThisFrame; // TODO(abe): change these bools to flags
            rect2 rect;
        };
    };
};

internal im_item_data* im_internal_get_item_data(im_id id);

//- [SECTION] Windows
enum IMWindowFlag_ {
    IMWindowFlag_None = 0,
    
    IMWindowFlag_Initialized      = BIT(0),
    IMWindowFlag_Visible          = BIT(1),
    IMWindowFlag_Minimized        = BIT(2),
    
    IMWindowFlag_VisibleThisFrame = BIT(3),
};

introspect("IMGUI")
struct im_window {
    char name[16];
    im_id id;
    im_window* top;
    im_window* below;
    im_dockspace* dockspace;
    
    //- Interactions
    IMWindowFlag flags;
    rect2 rect;
    rect2 headerRect;
    rect2 lastRect; // Rect at previous frame
    
    bool8 allowYScroll;
    v2f offset;
    
    //- Per Item Properties
    im_item_data itemData[128]; // TODO(abe): replace arrays with Hashmap ?
    
    //- Rendering
    im_draw_command immediateBuffer[1024];
    im_draw_command* currentImmediateCommand;
    
    im_draw_command deferBuffer[1024];
    im_draw_command* currentDeferCommand;
    bool8 defer;
    
    //- IDs
    im_id headerID;
    im_id minimizeButtonID;
    
    im_id sizeHandleID;
    im_id sizeXHandleID;
    im_id sizeYHandleID;
    
    im_id yPosHandleID;
    
    //- Additionnal IDs
    im_id groupID; // Create different ids for widgets using the same name
    im_id lastWidgetID; // ID of the last widget
    im_id popupID; // ID of the opened popup/tooltip
};

internal bool8 im_window_begin(const char* name);
internal void im_window_end();

internal void im_internal_window_resize_around(im_window* w, v2f point);
internal void im_internal_window_resize_around_header(im_window* w);

internal void im_internal_window_clamp_rect(im_window* w);

//- [SECTION] Dockspaces
enum IMDockType_ {
    IMDockType_None,
    IMDockType_Vertical,
    IMDockType_Horizontal
};

introspect("IMGUI")
struct im_dockspace {
    IMDockType type;
    
    union {
        im_dockspace* left;
        im_dockspace* top;
    };
    
    union {
        im_dockspace* right;
        im_dockspace* bottom;
    };
    
    im_dockspace* parent;
    im_window* child;
    
    // Relative to frame dimensions
    rect2 rect;
    rect2 nextRect;
};

internal void im_internal_dockspaces_init();
internal rect2 im_internal_dockspace_get_screen_rect(im_dockspace* dock);
internal im_dockspace* im_internal_dockspace_get_next_available();
internal im_dockspace* im_internal_dockspace_get_hovered();
internal im_dockspace* im_internal_dockspace_get_sibling(im_dockspace* dock);

internal void im_internal_dockspace_resize_propagate_down(im_dockspace* dock, rect2 prevRect, rect2 newRect);
internal void im_internal_dockspace_resize_mouse(im_dockspace* dock, bool8 resizeX, bool8 resizeY);

internal void im_internal_dockspace_insert_window_in_hovered(im_window* window);
internal void im_internal_dockspace_remove_window(im_window* window);

//- [SECTION] Context
introspect("IMGUI")
struct im_context {
    
    // Dockspaces
    struct {
        im_dockspace dockspaces[32];
        im_dockspace* nextDockspace;
    };
    
    // Windows
    struct {
        im_window windows[32];
        im_window* currentWindow;
        
        im_window* hoveredWindow;
        im_window* focusedWindow;
        im_window* lastWindow; // Window at the end of the linked list (to ease render back to front)
    };
    
    // Layouts
    struct {
        im_layout windowLayout;
        im_layout layoutStack[64];
        im_layout* currentLayout;
    };
    
    // Charts
    struct {
        im_id currentChartID;
        rect2 currentChartRect;
        char currentChartName[64];
    };
    
    // Mouse Interactions
    struct {
        v2f mpos;
        v2f mDelta;
        v2f mScroll;
        v2f wDimensions;
        
        bool8 mbLeftOnDown;
        bool8 mbLeftOnUp;
        bool8 mbLeft;
        
        bool8 mbRightOnDown;
    };
    
    struct {
        v2f mouseStartActivePos;
    };
    
    // Interactions
    struct {
        im_id lastActive;
        im_id active;
        im_id hot;
        im_id nextHot;
    };
    
    im_id groupID;
    im_color_theme theme;
};
extern im_context* g_imctx;
#ifndef IM_OWN_CONTEXT
extern im_context g_im_context;
#endif
internal void im_initialise();
internal void im_clean();

internal im_window* im_internal_context_find_window(im_id id);
internal im_window* im_context_find_hovered_window();
internal void im_internal_context_set_window_focused(im_window* w);
internal bool8 im_context_is_current_window_hovered();

internal void im_frame_begin();
internal void im_frame_end();

//- [SECTION] Widgets
enum IMWidgetFlag_ {
    IMWidgetFlag_None = 0,
    
    IMWidgetFlag_ReadOnly = BIT(0),
    
    IMWidgetFlag_Danger = BIT(1),
    
    IMWidgetFlag_UseActive = BIT(2),
};

//- Widgets
internal color8 im_internal_get_widget_background_color(im_id id, IMWidgetFlag wflags);
internal color8 im_internal_get_widget_border_color(im_id id, IMWidgetFlag wflags);
internal bool8 im_button_behaviour(im_id id, rect2 rect);

//- Standards
internal bool8 im_button(const char* name, IMWidgetFlag wflags);
internal void im_checkbox(const char* name, bool8* toggled, IMWidgetFlag wflags);
internal bool8 im_radio(const char* name, bool8 selected, IMWidgetFlag wflags);

//- Sliders
internal void im_slider1u(const char* name, u32 min, u32 max, u32* value, IMWidgetFlag wflags);
internal void im_slider1i(const char* name, i32 min, i32 max, i32* value, IMWidgetFlag wflags);
internal void im_slider2i(const char* name, i32 min, i32 max, v2i* value, IMWidgetFlag wflags);

internal void im_slider1f(const char* name, f32 min, f32 max, f32* value, IMWidgetFlag wflags);
internal void im_slider2f(const char* name, f32 min, f32 max, v2f* value, IMWidgetFlag wflags);
internal void im_slider3f(const char* name, f32 min, f32 max, v3f* value, IMWidgetFlag wflags);
internal void im_slider4f(const char* name, f32 min, f32 max, v4f* value, IMWidgetFlag wflags);

//- Colors
internal void im_internal_color_popup(const char* name, rect2 rect, color8* color);
internal void im_rgb(const char* name, color8* color, IMWidgetFlag wflags);
internal void im_rgba(const char* name, color8* color, IMWidgetFlag wflags);

//- Charts
enum IMChartMarker_ {
    IMChartMarker_None,
    
    IMChartMarker_Square,
    IMChartMarker_Circle,
    
    IMChartMarker_Count,
};

introspect("IMGUI")
struct im_chart_atb {
    v2f dim;
    
    f32 min;
    f32 max;
    
    color8 color;
    u32 lineWeight;
    
    IMChartMarker marker;
    color8 markerColor;
};

internal im_chart_atb im_chart_atb_new();

internal bool8 im_chart_begin(const char* name, im_chart_atb atb);
internal void im_chart_end();

internal void im_chart_plot_func(const char* legend, f32 xMin, f32 xMax, u32 steps, chartFunc f, im_chart_atb atb);
internal void im_chart_plot_lines(const char* legend, u32 valueCount, f32* values, im_chart_atb atb);

//- Textures & Images
internal void im_image(const char* name, u32 texture, v2f dim, v4f uvs);
internal void im_image_inspect(const char* name, u32 texture, v2f dim, v4f uvs);

//- Tooltips (only on hover) / Popups (stay open on click)
internal rect2 im_internal_floating_fit_in_window(rect2 rect);
internal void im_internal_floating_begin(im_id id, v2f topLeft);
internal void im_internal_floating_end();

internal inline void im_tooltip_begin(const char* name);
internal inline void im_tooltip_end();

internal inline void im_popup_open(const char* name);
internal inline void im_popup_close(const char* name);
internal inline bool8 im_popup_begin(const char* name);
internal inline void im_popup_end();

//- Text
internal void im_header(const char* text, ...);
internal void im_hint(const char* text, ...);
internal void im_text(const char* text, ...);
internal void im_text_with_attributes(cdl_text_atb attr, const char* text, ...);

//- Menus
internal bool8 im_menu_bar_begin();
internal void im_menu_bar_end();
internal bool8 im_menu_begin(const char* name, IMWidgetFlag wflags);
internal void im_menu_end();

//- Layout
internal bool8 im_group_begin(const char* name, IMWidgetFlag wflags);
internal void im_group_end();
internal void im_separator();

//- [SECTION] Helpers
internal inline im_window* im_current_window();
internal inline im_layout* im_current_layout();

internal inline rect2 im_get_last_rect();
internal inline bool8 im_last_is_hovered();

#endif // __CDL_INCLUDE_CDL_IMUI_H__
