#ifndef __CDL_INCLUDE_CDL_ASSETS_H__
#define __CDL_INCLUDE_CDL_ASSETS_H__

#define ASSET_PATH "assets/engine/"
#define ASSET_SHADER(name) ASSET_PATH "shaders/" #name ".shader"
#define ASSET_TEXTURE(name) ASSET_PATH "textures/" #name ".png"
#define ASSET_ICON(name) ASSET_PATH "icons/" #name ".png"
#define ASSET_FONT_FILE(name) ASSET_PATH "fonts/" #name ".fnt"
#define ASSET_FONT_TEXTURE(name) ASSET_PATH "fonts/" #name ".png"

//- Engine Assets
#define ASSETDEF_SHADER(name) const char* SHADER_##name = ASSET_SHADER(name)
#define ASSETDEF_TEXTURE(name) const char* TEXTURE_##name = ASSET_TEXTURE(name)
#define ASSETDEF_ICON(name) const char* ICON_##name = ASSET_ICON(name)

#define ASSETDEF_FONT_FILE(name) const char* FONT_FILE_##name = ASSET_FONT_FILE(name)
#define ASSETDEF_FONT_TEXTURE(name) const char* FONT_TEXTURE_##name = ASSET_FONT_TEXTURE(name)
#define ASSETDEF_FONT(name) ASSETDEF_FONT_FILE(name); ASSETDEF_FONT_TEXTURE(name)

#include "assets.def"

//- Forward Declarations

// Structs
struct cdl_assets_header;
struct cdl_assets_shader;
struct cdl_assets_texture;
struct cdl_assets_glyph;
struct cdl_assets_font;
struct cdl_assets_model_vertex;
struct cdl_assets_model_raw;
struct cdl_assets_audio;
struct cdl_assets;

// Typedefs
typedef u32 AssetType;
typedef u32 AssetState;
typedef u32 ShaderType;
typedef u32 TextureType;
typedef u32 AudioType;

//-

//- [SECTION] Asset Header 
enum AssetType_ {
    AssetType_Invalid,
    
    AssetType_Shader,
    AssetType_Texture,
    AssetType_Font,
    AssetType_ModelRaw,
    AssetType_Audio,
    
    AssetType_Count
};

enum AssetState_ {
    AssetState_None,
    
    AssetState_Created,
    AssetState_Requested,
    AssetState_Loaded,
    
    AssetState_Count
};

introspect("Assets")
struct cdl_assets_header {
    AssetType type;
    AssetState state;
    char path[256];
    u32 id;
    guid uuid;
};
#define INVALID_ASSET 0xffffffffU

internal u32 cdl_assets_internal_next_id();

//- [SECTION] Shaders 
enum ShaderType_ {
    ShaderType_Invalid,
    
    ShaderType_World,
    ShaderType_UI,
    ShaderType_Text,
    
    ShaderType_FullScreenEffect,
    ShaderType_FullScreenQuad,
    
    ShaderType_Count
};

introspect("Assets")
struct cdl_assets_shader {
    cdl_assets_header head;
    
    ShaderType type;
    str8 vertexCode;
    str8 fragmentCode;
};

internal cdl_assets_shader* cdl_assets_internal_shader_new(char* path);
internal cdl_assets_shader* cdl_assets_shader_create(ShaderType type, char* path);
internal cdl_assets_shader* cdl_assets_shader_create_and_load(ShaderType type, char* path);
internal void cdl_assets_shader_load_code(cdl_assets_shader* asset);
internal cdl_assets_shader* cdl_assets_shader_get(ShaderType type, char* path);


//- [SECTION] Textures 
enum TextureType_ {
    TextureType_Invalid,
    
    TextureType_Diffuse,
    TextureType_NormalMap,
    
    TextureType_Count
};

introspect("Assets")
struct cdl_assets_texture {
    cdl_assets_header head;
    
    TextureType type;
    i32 width;
    i32 height;
    i32 channels;
    void* pixels;
};

internal cdl_assets_texture* cdl_assets_internal_texture_new(char* path);
internal cdl_assets_texture* cdl_assets_texture_create(TextureType type, char* path);
internal cdl_assets_texture* cdl_assets_texture_create_and_load(TextureType type, char* path);
internal void cdl_assets_texture_load_pixels(cdl_assets_texture* asset);
internal cdl_assets_texture* cdl_assets_texture_get(char* path);


//- [SECTION] Fonts 
introspect("Assets")
struct cdl_assets_glyph {
    u32 id;
    i32 xpos, ypos;
    i32 width, height;
    i32 xoffset, yoffset;
    i32 xadvance;
};

introspect("Assets")
struct cdl_assets_font {
    cdl_assets_header head;
    
    cdl_assets_texture* texture;
    cdl_assets_glyph glyphs[200];
    i32 glyphSize;
};

internal cdl_assets_font* cdl_assets_internal_font_new(char* fontFile);
internal cdl_assets_font* cdl_assets_font_create(char* fontFile, char* fontTexture);
internal cdl_assets_font* cdl_assets_font_create_and_load(char* fontFile, char* fontTexture);
internal void cdl_assets_font_load_data(cdl_assets_font* asset);
internal cdl_assets_font* cdl_assets_font_get(char* fontFile);


//- [SECTION] Models 
introspect("Assets")
struct cdl_assets_model_vertex {
    v3f localPosition;
    v2f uv;
    v3f normal;
    color8 color;
};

introspect("Assets")
struct cdl_assets_model_raw {
    cdl_assets_header head;
    
    u32 vertexCount;
    cdl_assets_model_vertex* vertices;
    
    u32 indexCount;
    u32* indices;
};

internal cdl_assets_model_raw* cdl_assets_internal_model_raw_new(char* path);
internal cdl_assets_model_raw* cdl_assets_model_raw_create(char* name, u32 vertexCount, v3f* positions, v2f* uvs, v3f* normals, u32 indexCount, u32* indices);
internal cdl_assets_model_raw* cdl_assets_model_raw_create_from_vertex_defs(char* name, u32 vertexDefCount, u32* vertexDefs, v3f* positions, v2f* uvs, v3f* normals);
internal cdl_assets_model_raw* cdl_assets_model_raw_create_from_obj(char* path);


//- [SECTION] Audio
enum AudioType_ {
    AudioType_Invalid,
    
    AudioType_Music,
    AudioType_SFX,
    
    AudioType_Count
};

introspect("Assets")
struct cdl_assets_audio {
    cdl_assets_header head;
    AudioType type;
    
    u16 channels;
    u32 sampleRate;
    u32 byteRate;
    u16 blockAlign;
    u16 bitsPerSample;
    
    u32 sampleCount;
    u32 dataSize;
    i16* leftSamples;
    i16* rightSamples;
};

internal cdl_assets_audio* cdl_assets_internal_audio_new(char* path);
internal cdl_assets_audio* cdl_assets_audio_create(AudioType type, char* path);
internal cdl_assets_audio* cdl_assets_audio_create_and_load(AudioType type, char* path);
internal void cdl_assets_audio_load(cdl_assets_audio* asset);

//- [SECTION] Assets
#define ASSETS_MAX_KIND_OF 256
introspect("Assets")
struct cdl_assets {
    u32 nextID;
    
    cdl_assets_shader shaders[ASSETS_MAX_KIND_OF];
    u32 nextShader;
    
    cdl_assets_texture textures[ASSETS_MAX_KIND_OF];
    u32 nextTexture;
    
    cdl_assets_font fonts[ASSETS_MAX_KIND_OF];
    u32 nextFont;
    
    cdl_assets_model_raw rawModels[ASSETS_MAX_KIND_OF];
    u32 nextRawModel;
    
    cdl_assets_audio audios[ASSETS_MAX_KIND_OF];
    u32 nextAudio;
};

internal void cdl_engine_assets_init();

#endif // __CDL_INCLUDE_CDL_ASSETS_H__