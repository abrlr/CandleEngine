#ifndef __CDL_INCLUDE_CDL_TEXT_H__
#define __CDL_INCLUDE_CDL_TEXT_H__

//- Forward declarations

// Structs
struct cdl_text_size;
struct cdl_text_atb;

// Typedefs
typedef u32 TextAnchor;
typedef u32 TextStyle;

//- [SECTION] Text Attributes
enum TextAnchor_ {
    TextAnchor_None           = BIT(0),
    
    TextAnchor_x_center       = BIT(1),
    TextAnchor_x_right        = BIT(2),
    TextAnchor_x_left         = BIT(3),
    
    TextAnchor_y_center       = BIT(4),
    TextAnchor_y_top          = BIT(5),
    TextAnchor_y_bottom       = BIT(6),
    
    TextAnchor_Centered       = TextAnchor_x_center | TextAnchor_y_center,
    TextAnchor_TopLeft        = TextAnchor_x_left   | TextAnchor_y_top,
    TextAnchor_BottomRight    = TextAnchor_x_right  | TextAnchor_y_bottom
};

enum TextStyle_ {
    TextStyle_None             = BIT(0),
    TextStyle_Bold             = BIT(1),
    TextStyle_BackgroundFill   = BIT(2),
    
    TextStyle_AlignLeft        = BIT(3),
    TextStyle_AlignRight       = BIT(4),
};

introspect("Text")
struct cdl_text_atb {
    TextAnchor anchor;
    TextStyle style;
    
    // TODO(abe): add another field for vertical margin
    f32 hMargin;
    f32 size;
    f32 lineWidth;
    color8 color;
    color8 background;
    
    f32 weight;
};

internal cdl_text_atb cdl_text_atb_new();

//- [SECTION] Text Cursor
struct cdl_text_cursor {
    v2f current;
    v2f max;
    u32 lineCount;
    cdl_text_atb attributes;
};

internal inline cdl_text_cursor cdl_text_cursor_new(cdl_text_atb attributes);
internal inline f32 cdl_text_internal_line_width(cdl_text_cursor* cursor);
internal inline f32 cdl_text_internal_max_line_width(cdl_text_cursor* cursor);
internal void cdl_text_internal_new_line(cdl_text_cursor* cursor);
internal bool8 cdl_text_internal_process_char(char* c, cdl_text_cursor* cursor);
internal f32 cdl_text_get_line_width(char* text, cdl_assets_font* font, cdl_text_atb attributes);
internal v2f cdl_text_get_dimensions(char* text, cdl_assets_font* font, cdl_text_atb attributes);

#endif // __CDL_INCLUDE_CDL_TEXT_H__