@vertex
#version 330 core
layout (location = 0) in vec3 i_Position;
layout (location = 1) in vec2 i_Uvs;
layout (location = 2) in vec3 i_Normal;
layout (location = 3) in uint i_Color;
layout (location = 4) in uint i_Flags;
layout (location = 5) in vec4 i_Misc;
layout (location = 6) in mat4 i_ModelMat;

out vec3 o_Pos;
out vec2 o_Uvs;
out vec4 o_Color;

flat out float o_UseTexture;
flat out float o_TexIndex;

flat out float o_UseSCSDF; // Single Channel Signed Distance Field

flat out float o_FontSize;
flat out float o_FontWeight;

flat out float o_IsColorPicker;
flat out float o_HueAdjust;

flat out float o_IsRoundedQuad;
flat out float o_PixCornerRadius;
flat out vec2  o_CenterPixPos;
flat out vec2  o_PixSize;

flat out float o_IsCircle;
flat out float o_CirclePixRadius;
flat out float o_CirclePixEdge;

// Global Uniforms
uniform vec2 g_uResolution;
uniform float g_uAspectRatio;
uniform float g_uTime;

// BatchUIVertexFlags
const uint vflags_UseTexture       = uint(1 << 1);
const uint vflags_UseSDF           = uint(1 << 2);
const uint vflags_UseModelMat      = uint(1 << 3);

const uint vflags_ColorPicker      = uint(1 << 4);

const uint vflags_TextBold         = uint(1 << 5);

const uint vflags_RoundCorners     = uint(1 << 6);
const uint vflags_Circle		   = uint(1 << 7);

float has_flag(uint flag)
{
   return step(0.5, float(i_Flags & flag));
}

vec4 color_from_uint(uint color)
{
   vec4 result = vec4(0);
   uint mask = uint(0xff);

   result.r = float( (color >>  0) & mask ) / 255.;
   result.g = float( (color >>  8) & mask ) / 255.;
   result.b = float( (color >> 16) & mask ) / 255.;
   result.a = float( (color >> 24) & mask ) / 255.;
   return result;
}

void main()
{
   gl_Position = vec4(i_Position, 1.0);

   o_Pos = i_Position;
   o_Uvs = i_Uvs;
   o_Color = color_from_uint(i_Color);

   o_UseTexture = has_flag(vflags_UseTexture);
   o_TexIndex = i_Misc.x;

   o_UseSCSDF = has_flag(vflags_UseSDF);

   o_FontSize = i_Misc.y;
   float isFontBold = has_flag(vflags_TextBold);
   o_FontWeight = isFontBold * i_Misc.w + (1. - isFontBold);

   o_IsColorPicker = has_flag(vflags_ColorPicker);
   o_HueAdjust = i_Misc.w;

   o_IsRoundedQuad = has_flag(vflags_RoundCorners);
   o_PixCornerRadius = i_Misc.y;

   o_IsCircle = has_flag(vflags_Circle);
   o_CirclePixRadius = i_Misc.y;
   o_CirclePixEdge = i_Misc.w;

   uint xMask = uint(0xffff);
   o_CenterPixPos = vec2(uint(i_Misc.z) & xMask, uint(i_Misc.z) >> 16);
   o_PixSize = vec2(uint(i_Misc.w) & xMask, uint(i_Misc.w) >> 16);
}


@fragment
#version 330 core

layout(origin_upper_left) in vec4 gl_FragCoord;
in vec3 o_Pos;
in vec2 o_Uvs;
in vec4 o_Color;

flat in float o_UseTexture;
flat in float o_TexIndex;

flat in float o_UseSCSDF;

flat in float o_FontSize;
flat in float o_FontWeight;

flat in float o_IsColorPicker;
flat in float o_HueAdjust;

flat in float o_IsRoundedQuad;
flat in float o_PixCornerRadius;
flat in vec2  o_CenterPixPos;
flat in vec2  o_PixSize;

flat in float o_IsCircle;
flat in float o_CirclePixRadius;
flat in float o_CirclePixEdge;

out vec4 fragmentColor;

// Global Uniforms
uniform vec2 g_uResolution;
uniform float g_uAspectRatio;
uniform float g_uTime;

// Shader specific uniform
uniform sampler2D uTextures[8];

// --
// Adjust hue of an rgb value
// https://stackoverflow.com/questions/9234724/how-to-change-hue-of-a-texture-with-glsl/
// --

const vec4 kRGBToYPrime = vec4(0.299, 0.587, 0.114, 0.0);
const vec4 kRGBToI = vec4(0.596, -0.275, -0.321, 0.0);
const vec4 kRGBToQ = vec4(0.212, -0.523, 0.311, 0.0);

const vec4 kYIQToR = vec4(1.0, 0.956, 0.621, 0.0);
const vec4 kYIQToG = vec4(1.0, -0.272, -0.647, 0.0);
const vec4 kYIQToB = vec4(1.0, -1.107, 1.704, 0.0);

const float PI = 3.14159265358979323846264;

vec4 adjustHue(vec4 color, float hueAdjust) {
   vec4 outColor = color;

   // Convert to YIQ
   float YPrime = dot(color, kRGBToYPrime);
   float I = dot(color, kRGBToI);
   float Q = dot(color, kRGBToQ);

   // Calculate the hue and chroma
   float hue = atan(Q, I);
   float chroma = sqrt(I * I + Q * Q);

   // Make the user's adjustments
   hue += hueAdjust;

   // Convert back to YIQ
   Q = chroma * sin(hue);
   I = chroma * cos(hue);

   // Convert back to RGB
   vec4 yIQ = vec4(YPrime, I, Q, 0.0);
   outColor.r = dot(yIQ, kYIQToR);
   outColor.g = dot(yIQ, kYIQToG);
   outColor.b = dot(yIQ, kYIQToB);

   return outColor;
}


void main()
{
   // --
   // NOTE(abe): Handle texture coordinates
   // --

   vec4 texColor = vec4(1.0);
   float useTexture = step(0.5, o_UseTexture);
   switch(int(o_TexIndex)) {
      case 1: texColor = ( useTexture * texture(uTextures[0], o_Uvs) + (1.0 - useTexture) * vec4(1.0) ); break;
      case 2: texColor = ( useTexture * texture(uTextures[1], o_Uvs) + (1.0 - useTexture) * vec4(1.0) ); break;
      case 3: texColor = ( useTexture * texture(uTextures[2], o_Uvs) + (1.0 - useTexture) * vec4(1.0) ); break;
      case 4: texColor = ( useTexture * texture(uTextures[3], o_Uvs) + (1.0 - useTexture) * vec4(1.0) ); break;
      case 5: texColor = ( useTexture * texture(uTextures[4], o_Uvs) + (1.0 - useTexture) * vec4(1.0) ); break;
      case 6: texColor = ( useTexture * texture(uTextures[5], o_Uvs) + (1.0 - useTexture) * vec4(1.0) ); break;
      case 7: texColor = ( useTexture * texture(uTextures[6], o_Uvs) + (1.0 - useTexture) * vec4(1.0) ); break;
      case 8: texColor = ( useTexture * texture(uTextures[7], o_Uvs) + (1.0 - useTexture) * vec4(1.0) ); break;
   }
   fragmentColor = texColor * o_Color;

   // --
   // NOTE(abe): Handle text rendering
   // --

   // Arbitrary values that SHOULD make the glyphs look nice at pretty much every resolution
   float glyphWidth = 0.45 + 0.035 * o_FontWeight;
   float glyphEdge = 0.03 + step(o_FontSize, 120) * 0.01 + step(o_FontSize, 100) * 0.03 + step(o_FontSize, 50) * 0.05 + step(o_FontSize, 30) * 0.05 + step(o_FontSize, 15) * 0.03;
   float glyphAlpha = 1.0 - smoothstep(glyphWidth, glyphWidth + glyphEdge, 1.0 - texColor.a);
   float isGlyph = step(0.5, o_FontSize);
   fragmentColor.a = isGlyph * glyphAlpha * o_Color.a + (1.0 - isGlyph) * fragmentColor.a;

   // --
   // NOTE(abe): Handle single channel SDFs
   // --

   float sdfWidth = 0.5;
   float sdfEdge = 0.03 + step(o_PixSize.x, 120) * 0.01 + step(o_PixSize.x, 100) * 0.03 + step(o_PixSize.x, 50) * 0.05 + step(o_PixSize.x, 30) * 0.05 + step(o_PixSize.x, 15) * 0.03;
   float sdfAlpha = 1.0 - smoothstep(sdfWidth, sdfWidth + sdfEdge, 1.0 - fragmentColor.r);
   fragmentColor.rgb = o_Color.rgb * o_UseSCSDF + (1. - o_UseSCSDF) * fragmentColor.rgb;
   fragmentColor.a = sdfAlpha * o_UseSCSDF + (1. - o_UseSCSDF) * fragmentColor.a;

   // --
   // NOTE(abe): Handle color picker quad
   // --

   // Create a red based color picker
   float gray = 1.0 - o_Uvs.x;
   float red = o_Uvs.y;
   vec4 lerpedColor = vec4(red, gray * red, gray * red, 1.0);

   // Adjust the hue of the color picker to use different shades
   vec4 hueAdjustedColor = adjustHue(lerpedColor, o_HueAdjust);

   fragmentColor = o_IsColorPicker * hueAdjustedColor + ( 1.0 - o_IsColorPicker ) * fragmentColor;

   // --
   // NOTE(abe): Evaluate the alpha of the current pixel
   //			if it is in a corner
   // --

   // Compute the gl coordinates of the quad's center
   vec2 centerGLPos = vec2(0);
   centerGLPos.x = 2. * o_CenterPixPos.x / g_uResolution.x - 1.;
   centerGLPos.y = 1. - 2. * o_CenterPixPos.y / g_uResolution.y;

   // Compute pixel position of the current fragment
   vec2 glDelta = centerGLPos - vec2( o_Pos.x, o_Pos.y );
   vec2 myPixelPos = o_CenterPixPos - vec2(glDelta.x, glDelta.y) * g_uResolution / 2;

   // Min & max corners of the quad ( m: Min, M: max)
   // x------------------------x
   // |m                       |
   // |                        |
   // |                        |
   // |                        |
   // |                       M|
   // x------------------------x

   // Make sure that the corner radius doesn't allow glitch in the rendering
   float cornerRadius = min(o_PixCornerRadius, min(o_PixSize.x / 2., o_PixSize.y / 2.));
   vec2 minCorner = o_CenterPixPos - o_PixSize / 2. + vec2(cornerRadius);
   vec2 maxCorner = o_CenterPixPos + o_PixSize / 2. - vec2(cornerRadius);

   // Booleans that check if the current fragment is in one corner
   // For example, a fragment in the top left corner will have ppxmin & ppymin set to 1 and the other to 0 (px < min_x & py < min_y)
   // A fragment in the top right corner will have ppxmax & ppymin set to 1, the other to 0, etc...

   float ppxmin = 1. - step(minCorner.x, myPixelPos.x);
   float ppxmax = 1. - step(myPixelPos.x, maxCorner.x);
   float ppymin = 1. - step(minCorner.y, myPixelPos.y);
   float ppymax = 1. - step(myPixelPos.y, maxCorner.y);

   // Compute the corner alpha
   float lowerBound = 0;
   float upperBound = o_PixCornerRadius;
   vec2 corner = vec2(ppxmin * minCorner.x + ppxmax * maxCorner.x, ppymin * minCorner.y + ppymax * maxCorner.y);
   float cornerAlpha = 1. - smoothstep(upperBound, upperBound + 3., distance(myPixelPos, corner));

   // Apply the corner alpha to the fragment
   float useCorners = o_IsRoundedQuad * (ppxmin + ppxmax) * (ppymin + ppymax);
   fragmentColor.a *= useCorners * cornerAlpha + (1. - useCorners);


   // --
   // NOTE(abe): Handle Circle/Disks
   // --

   float distFromCenter = distance(o_CenterPixPos, myPixelPos);
   float circleAlpha = 1. - smoothstep(o_CirclePixRadius - 1.5, o_CirclePixRadius, distFromCenter);
   float interiorAlpha = smoothstep(o_CirclePixRadius - o_CirclePixEdge - 1.5, o_CirclePixRadius - o_CirclePixEdge, distFromCenter); 
   fragmentColor.a *= o_IsCircle * clamp(circleAlpha * interiorAlpha, 0, 1) + (1. - o_IsCircle);
}
