@vertex
#version 330 core
layout (location = 0) in vec3 i_Position;
layout (location = 1) in vec2 i_Uvs;
layout (location = 2) in vec3 i_Normal;
layout (location = 3) in uint i_Color;
layout (location = 4) in uint i_Flags;
layout (location = 5) in vec4 i_Misc;

layout (location = 6) in mat4 i_ModelMat;
layout (location = 10) in uint i_ModelColor;

out vec3 o_Pos;
out vec2 o_UVs;
out vec3 o_Normal;
out vec4 o_Color;
out mat4 o_ModelMat;
flat out float o_TextureIndex;

// Global Uniforms
uniform vec2 g_uResolution;
uniform float g_uAspectRatio;
uniform float g_uTime;

// Shader specific uniform
uniform mat4 u_ProjViewMat;

// Noise function from @patriciogonzalezvivo
float mod289(float x){return x - floor(x * (1.0 / 289.0)) * 289.0;}
vec4 mod289(vec4 x){return x - floor(x * (1.0 / 289.0)) * 289.0;}
vec4 perm(vec4 x){return mod289(((x * 34.0) + 1.0) * x);}

float noise(vec3 p){
    vec3 a = floor(p);
    vec3 d = p - a;
    d = d * d * (3.0 - 2.0 * d);
    
    vec4 b = a.xxyy + vec4(0.0, 1.0, 0.0, 1.0);
    vec4 k1 = perm(b.xyxy);
    vec4 k2 = perm(k1.xyxy + b.zzww);
    
    vec4 c = k2 + a.zzzz;
    vec4 k3 = perm(c);
    vec4 k4 = perm(c + 1.0);
    
    vec4 o1 = fract(k3 * (1.0 / 41.0));
    vec4 o2 = fract(k4 * (1.0 / 41.0));
    
    vec4 o3 = o2 * d.z + o1 * (1.0 - d.z);
    vec2 o4 = o3.yw * d.x + o3.xz * (1.0 - d.x);
    
    return o4.y * d.y + o4.x * (1.0 - d.y);
}

vec4 color_from_uint(uint color)
{
    vec4 result = vec4(0);
    uint mask = uint(0xff);
    
    result.r = float( (color >>  0) & mask ) / 255.;
    result.g = float( (color >>  8) & mask ) / 255.;
    result.b = float( (color >> 16) & mask ) / 255.;
    result.a = float( (color >> 24) & mask ) / 255.;
    return result;
}

void main()
{
    //- Vertex Position
    float useModelMat = float(int(i_Flags) & 0x4) / 4;
    mat4 modelMatrix = ( (1. - useModelMat) * mat4(1) + useModelMat * i_ModelMat );
    vec4 worldPosition =  modelMatrix * vec4(i_Position, 1.0);
    gl_Position = u_ProjViewMat * worldPosition;
    
    //- To the Fragment Shader
    o_Pos = worldPosition.xyz;
    o_UVs = i_Uvs;
    o_Normal = i_Normal;
    o_ModelMat = modelMatrix;
    o_TextureIndex = i_Misc.x * float(int(i_Flags) & 0x2) / 2;
    
    //- Colors
    float useModelTint = step(0.5, float(i_ModelColor));
    float useVertexColor = step(0.5, float(i_Color));
    vec4 tint = useModelTint * color_from_uint(i_ModelColor) + ( 1 - useModelTint ) * vec4(1);
    vec4 vertexColor = useVertexColor * color_from_uint(i_Color) + ( 1 - useVertexColor ) * vec4(1);
    o_Color = vertexColor * tint;
}


@fragment
#version 330 core
layout (location = 0) out vec4 fragmentColor;

in vec3 o_Pos;
in vec2 o_UVs;
in vec3 o_Normal;
in vec4 o_Color;
in mat4 o_ModelMat;
flat in float o_TextureIndex;

// Global Uniforms
uniform vec2 g_uResolution;
uniform float g_uAspectRatio;
uniform float g_uTime;

// Shader specific uniform
uniform vec3 u_CamPosition;
uniform sampler2D uTextures[8];

struct Material {
    vec4 tint;
    uint textures;
};
uniform Material u_Material;

struct WorldLight {
    vec3 position;
    float intensity;
    uint color;
};
uniform WorldLight u_Lights[8];

struct Environment {
    vec3 direction;
    uint color;
    float intensity;
};
uniform Environment u_Environment;

vec4 color_from_uint(uint color)
{
    vec4 result = vec4(0);
    uint mask = uint(0xff);
    
    result.r = float( (color >>  0) & mask ) / 255.;
    result.g = float( (color >>  8) & mask ) / 255.;
    result.b = float( (color >> 16) & mask ) / 255.;
    result.a = float( (color >> 24) & mask ) / 255.;
    return result;
}

// Smooth pixel art
vec2 uv_iq(vec2 uv, ivec2 texture_size) {
    vec2 pixel = uv;
    vec2 seam = floor(pixel + 0.5);
    vec2 dudv = fwidth(pixel);
    pixel = seam + clamp( (pixel - seam) / dudv, -0.5, 0.5);
    pixel = clamp(pixel, vec2(0.0), vec2(texture_size));
    return pixel / texture_size;
}

// Noise function from @patriciogonzalezvivo
float mod289(float x){return x - floor(x * (1.0 / 289.0)) * 289.0;}
vec4 mod289(vec4 x){return x - floor(x * (1.0 / 289.0)) * 289.0;}
vec4 perm(vec4 x){return mod289(((x * 34.0) + 1.0) * x);}

float noise(vec3 p){
    vec3 a = floor(p);
    vec3 d = p - a;
    d = d * d * (3.0 - 2.0 * d);
    
    vec4 b = a.xxyy + vec4(0.0, 1.0, 0.0, 1.0);
    vec4 k1 = perm(b.xyxy);
    vec4 k2 = perm(k1.xyxy + b.zzww);
    
    vec4 c = k2 + a.zzzz;
    vec4 k3 = perm(c);
    vec4 k4 = perm(c + 1.0);
    
    vec4 o1 = fract(k3 * (1.0 / 41.0));
    vec4 o2 = fract(k4 * (1.0 / 41.0));
    
    vec4 o3 = o2 * d.z + o1 * (1.0 - d.z);
    vec2 o4 = o3.yw * d.x + o3.xz * (1.0 - d.x);
    
    return o4.y * d.y + o4.x * (1.0 - d.y);
}

void main()
{
    //- Compute new UVs if the texture used is pixel art
    float isPixelArt = 0;
    ivec2 texture_size = ivec2(256);
    vec2 uv = uv_iq(o_UVs * vec2(texture_size), texture_size) * isPixelArt + o_UVs * (1.0 - isPixelArt);
    
    
    //- Compute surface color and texture
    float diffuseIndex = float(u_Material.textures >> 24 & uint(0xff));
    float diffuseMap = step(0.5, diffuseIndex) * diffuseIndex + step(0.5, o_TextureIndex) * o_TextureIndex;
    vec4 texColor = vec4(1.0);
    float useTexture = step(0.5, diffuseMap);
    
    switch(int(diffuseMap)) {
        case 1: texColor = ( useTexture * texture(uTextures[0], uv) + (1.0 - useTexture) * vec4(1.0) ); break;
        case 2: texColor = ( useTexture * texture(uTextures[1], uv) + (1.0 - useTexture) * vec4(1.0) ); break;
        case 3: texColor = ( useTexture * texture(uTextures[2], uv) + (1.0 - useTexture) * vec4(1.0) ); break;
        case 4: texColor = ( useTexture * texture(uTextures[3], uv) + (1.0 - useTexture) * vec4(1.0) ); break;
        case 5: texColor = ( useTexture * texture(uTextures[4], uv) + (1.0 - useTexture) * vec4(1.0) ); break;
        case 6: texColor = ( useTexture * texture(uTextures[5], uv) + (1.0 - useTexture) * vec4(1.0) ); break;
        case 7: texColor = ( useTexture * texture(uTextures[6], uv) + (1.0 - useTexture) * vec4(1.0) ); break;
        case 8: texColor = ( useTexture * texture(uTextures[7], uv) + (1.0 - useTexture) * vec4(1.0) ); break;
    }
    
    vec4 surfaceColor = o_Color * texColor;
    
    
    //- Compute Normals
    float normalMap = step(0.5, float(u_Material.textures >> 16 & uint(0xff))) * float(u_Material.textures >> 16 & uint(0xff));
    vec3 unitNormal = normalize(o_Normal);
    float useNormalMap = step(0.5, normalMap);
    
    switch(int(normalMap)) {
        case 1: unitNormal = ( useNormalMap * texture(uTextures[0], uv).rgb + (1.0 - useNormalMap) * unitNormal ); break;
        case 2: unitNormal = ( useNormalMap * texture(uTextures[1], uv).rgb + (1.0 - useNormalMap) * unitNormal ); break;
        case 3: unitNormal = ( useNormalMap * texture(uTextures[2], uv).rgb + (1.0 - useNormalMap) * unitNormal ); break;
        case 4: unitNormal = ( useNormalMap * texture(uTextures[3], uv).rgb + (1.0 - useNormalMap) * unitNormal ); break;
        case 5: unitNormal = ( useNormalMap * texture(uTextures[4], uv).rgb + (1.0 - useNormalMap) * unitNormal ); break;
        case 6: unitNormal = ( useNormalMap * texture(uTextures[5], uv).rgb + (1.0 - useNormalMap) * unitNormal ); break;
        case 7: unitNormal = ( useNormalMap * texture(uTextures[6], uv).rgb + (1.0 - useNormalMap) * unitNormal ); break;
        case 8: unitNormal = ( useNormalMap * texture(uTextures[7], uv).rgb + (1.0 - useNormalMap) * unitNormal ); break;
    }
    
    unitNormal = normalize(mat3(transpose(inverse(o_ModelMat))) * unitNormal);
    
    //- Compute Lighting ( !! this is not in tangent space !! )
    float useLights = step(0.5, length(unitNormal));
    
    // Environment Contribution 
    vec3 environmentColor = color_from_uint(u_Environment.color).rgb;
    float nDotNDir = dot(unitNormal, normalize(-u_Environment.direction));
    float environmentIntensity = max(0., min(nDotNDir, 1)) * u_Environment.intensity;
    vec3 ambient = 0.25 * environmentColor * environmentIntensity;
    vec3 lightContribution = ambient;
    
    // Lights Contribution
    for (int i = 0; i < 8; i++) {
        vec3 lightColor = color_from_uint(u_Lights[i].color).rgb * u_Lights[i].intensity;
        
        vec3 toLight = u_Lights[i].position - o_Pos;
        vec3 unitToLight = normalize(toLight);
        float nDotL = dot(unitNormal, unitToLight);
        
        vec3 toCamera = u_CamPosition - o_Pos;
        vec3 unitToCamera = normalize(toCamera);
        
        // Diffuse
        float diffuseIntensity = max(0., min(nDotL, 1));
        vec3 diffuse = diffuseIntensity * lightColor;
        
        // Specular
        float specularStrength = 0.5;
        vec3 reflectDir = reflect(-unitToLight, unitNormal);
        float specularIntensity = pow(max(dot(unitToCamera, reflectDir), 0.), 64);
        vec3 specular = specularStrength * specularIntensity * lightColor;
        
        // Light Total Contribution
        vec3 lightOnFragment = diffuse + specular;
        lightContribution += lightOnFragment / length(toLight);
    }
    
    //- Compute final fragment color
    if ( useLights > 0 ) {
        fragmentColor = surfaceColor * vec4(lightContribution, 1.0);
    } else {
        fragmentColor = surfaceColor * ( 1.0 - useLights );
    }
    
    
}
