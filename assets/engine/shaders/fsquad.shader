@vertex
#version 330 core
layout (location = 0) in vec3 i_Position;
layout (location = 1) in vec2 i_Uvs;

out vec2 o_Uvs;

void main()
{
    gl_Position = vec4(i_Position, 1.0);
    o_Uvs = i_Uvs;
}


@fragment
#version 330 core

in vec2 o_Uvs;

out vec4 fragmentColor;

uniform float u_GammaCorrect;
uniform sampler2D u_ScreenTexture;

void main()
{
    fragmentColor = texture(u_ScreenTexture, o_Uvs);
    
    const float gamma = 2.2;
    
    // Gamma Correction
    vec4 textureSample = texture(u_ScreenTexture, o_Uvs);
    vec3 result = u_GammaCorrect * pow(textureSample.rgb, vec3(1.0 / gamma)) + ( 1. - u_GammaCorrect ) * textureSample.rgb;
    
	fragmentColor = vec4(result, textureSample.a);
}
