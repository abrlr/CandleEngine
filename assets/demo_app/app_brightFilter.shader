@vertex
#version 330 core
layout (location = 0) in vec3 i_Position;
layout (location = 1) in vec2 i_Uvs;

out vec2 o_Uvs;

void main()
{
    gl_Position = vec4(i_Position, 1.0);
    o_Uvs = i_Uvs;
}


@fragment
#version 330 core

in vec2 o_Uvs;

out vec4 fragmentColor;

uniform sampler2D uTextures[3]; // 1 scene texture + 2 parent textures

void main()
{
    vec4 color = vec4(texture(uTextures[0], o_Uvs).rgb, 1.);
    float brightness = length(color.rgb);
    float useColor = step(0.8, brightness);
    fragmentColor = useColor * color;
}
