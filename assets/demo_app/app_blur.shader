@vertex
#version 330 core
layout (location = 0) in vec3 i_Position;
layout (location = 1) in vec2 i_Uvs;

out vec2 o_Uvs;

void main()
{
    gl_Position = vec4(i_Position, 1.0);
    o_Uvs = i_Uvs;
}


@fragment
#version 330 core

in vec2 o_Uvs;

out vec4 fragmentColor;

uniform vec2 g_uResolution;
uniform sampler2D uTextures[3]; // 1 scene texture + 2 parent textures

void main()
{
    float twoPi = 6.28318530718;

    float polarSamples = 16.0;
    float radialSamples = 8.0;

    float radius = 8.0;
    vec2 radiusVec = radius / g_uResolution;
    vec4 color = texture(uTextures[1], o_Uvs);

    for (float angle = 0.0; angle < twoPi; angle += twoPi / polarSamples) {
        for (float i = 1.0 / radialSamples; i <= 1.0; i += 1.0 / radialSamples) {
            vec2 uv = o_Uvs + vec2(cos(angle), sin(angle)) * radiusVec * i;
            color += texture(uTextures[1], uv);
        }
    }

    color /= radialSamples * polarSamples - 15.0;
    fragmentColor = color;
}