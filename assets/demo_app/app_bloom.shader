@vertex
#version 330 core
layout (location = 0) in vec3 i_Position;
layout (location = 1) in vec2 i_Uvs;

out vec2 o_Uvs;

void main()
{
	gl_Position = vec4(i_Position, 1.0);
	o_Uvs = i_Uvs;
}


@fragment
#version 330 core

in vec2 o_Uvs;

out vec4 fragmentColor;

uniform sampler2D uTextures[3]; // 1 scene texture + 2 parent textures

void main()
{
	const float exposure = 2;
    
	vec4 hdrColor = texture(uTextures[0], o_Uvs);
	vec4 bloomColor = texture(uTextures[1], o_Uvs);
    
	// Additive Blending
	vec3 result = hdrColor.rgb + bloomColor.rgb;
    
    // Tone Mapping
    result = vec3(1.0) - exp(-result * exposure);
	
	fragmentColor = vec4(result, 1.0);
}
