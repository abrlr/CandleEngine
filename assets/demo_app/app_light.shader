@vertex
#version 330 core
layout (location = 0) in vec3 i_Position;
layout (location = 6) in mat4 i_ModelMat;
layout (location = 10) in uint i_ModelColor;

flat out uint o_Color;

// Global Uniforms
uniform vec2 g_uResolution;
uniform float g_uAspectRatio;
uniform float g_uTime;

// Shader specific uniform
uniform mat4 u_ProjViewMat;

void main()
{
   vec4 worldPosition =  i_ModelMat * vec4(i_Position, 1.0);
   gl_Position = u_ProjViewMat * worldPosition;

   o_Color = i_ModelColor;
}

@fragment
#version 330 core
layout (location = 0) out vec4 fragmentColor;

flat in uint o_Color;

// Global Uniforms
uniform vec2 g_uResolution;
uniform float g_uAspectRatio;
uniform float g_uTime;

// Shader specific uniform
uniform vec3 u_CamPosition;
uniform sampler2D uTextures[8];

vec4 color_from_uint(uint color)
{
   vec4 result = vec4(0);
   uint mask = uint(0xff);

   result.r = float( (color >>  0) & mask ) / 255.;
   result.g = float( (color >>  8) & mask ) / 255.;
   result.b = float( (color >> 16) & mask ) / 255.;
   result.a = float( (color >> 24) & mask ) / 255.;
   return result;
}

void main()
{
   fragmentColor = vec4(color_from_uint(o_Color).rgb, 1);
}
