enum AppState_ {
    AppState_None,
    AppState_Run,
    AppState_Exit
};

struct application_memory {
    AppState_ state;
    cdl_renderer_camera* camera;
};

internal void application_update(application_memory* app, thread_work_queue* wq)
{
    switch (app->state)
    {
        case AppState_None:
        {
            app->camera = cdl_renderer_camera_new(CameraType_Perspective);
            renderer->worldView->camera = app->camera;
            app->state = AppState_Run;
        } break;
        
        case AppState_Run:
        {
            if ( keyboard_key_on_down(KEY_ESCAPE) ) {
                app->state = AppState_Exit;
            }
        } break;
        
        case AppState_Exit:
        {
            core->run = false;
        } break;
        
        default:
        {
            log_err("Invalid code path in application state: %d", app->state);
        } break;
    }
}