enum AppState_ {
    AppState_None,
    
    AppState_Run,
    
    AppState_Quit
};

struct app_ui {
    cdl_ui master;
    
    cdl_ui_widget canvas;
    
    cdl_ui_widget topBar,
    fileDropdown,
    openButton,
    saveButton,
    saveAsButton,
    closeButton,
    quitButton,
    
    editDropdown,
    undoButton;
    
    cdl_ui_widget appArea,
    areaLeftColumn,
    areaRightColumn;
    
    cdl_ui_widget reloadUIButton,
    testButton2,
    testBoolean,
    testSlider,
    colorPicker;
    
    cdl_ui_widget rightTextArea;
};

struct application_memory {
    AppState_ state;
    
    app_ui ui;
};

internal void application_build_ui(application_memory* app)
{
    app_ui* ui = &app->ui;
    ui->master.rootWidget = &ui->canvas;
    
    //- Window Canvas 
    cdl_ui_widget_attributes canvasAttribs = cdl_ui_widget_attributes_new();
    canvasAttribs.layout = UIWidgetLayout_Vertical;
    cdl_ui_widget_new(&ui->canvas, 0, 0, UIWidgetType_Canvas, canvasAttribs);
    
    //- TopBar
    cdl_ui_widget_attributes topBarAreaAttribs = cdl_ui_widget_attributes_new();
    topBarAreaAttribs.layout = UIWidgetLayout_Horizontal;
    topBarAreaAttribs.constraints.type = UIWidgetConstraint_SizeFixedY
        | UIWidgetConstraint_PosRelativeLeft;
    topBarAreaAttribs.constraints.size.height = 20;
    topBarAreaAttribs.constraints.positions.left = 0.5;
    cdl_ui_widget_new(&ui->topBar, &ui->canvas, 0, UIWidgetType_Panel, topBarAreaAttribs);
    
    //- Top Bar Buttons
    cdl_ui_widget_attributes barButtonAttribs = cdl_ui_widget_attributes_new();
    barButtonAttribs.layout = UIWidgetLayout_Vertical;
    barButtonAttribs.constraints.type = UIWidgetConstraint_SizeFixedX
        | UIWidgetConstraint_SizeFixedY
        | UIWidgetConstraint_MarginFixedLeft;
    barButtonAttribs.constraints.size = v2f_new(75, 20);
    barButtonAttribs.constraints.margins.left = 5;
    
    cdl_ui_widget_attributes dropdownBtnAttr = barButtonAttribs;
    dropdownBtnAttr.constraints.size.width = 200;
    
    cdl_ui_widget_new(&ui->fileDropdown, &ui->topBar, "File...", UIWidgetType_Dropdown, barButtonAttribs);
    cdl_ui_widget_new(&ui->openButton, &ui->fileDropdown, "Open", UIWidgetType_Button, dropdownBtnAttr);
    cdl_ui_widget_new(&ui->saveButton, &ui->fileDropdown, "Save", UIWidgetType_Button, dropdownBtnAttr);
    cdl_ui_widget_new(&ui->saveAsButton, &ui->fileDropdown, "Save As", UIWidgetType_Button, dropdownBtnAttr);
    cdl_ui_widget_new(&ui->closeButton, &ui->fileDropdown, "Close", UIWidgetType_Button, dropdownBtnAttr);
    cdl_ui_widget_new(&ui->quitButton, &ui->fileDropdown, "Quit", UIWidgetType_Button, dropdownBtnAttr);
    ui->quitButton.attributes.theme = UITheme_Danger;
    
    cdl_ui_widget_new(&ui->editDropdown, &ui->topBar, "Edit...", UIWidgetType_Dropdown, barButtonAttribs);
    cdl_ui_widget_new(&ui->undoButton, &ui->editDropdown, "Undo", UIWidgetType_Button, dropdownBtnAttr);
    
    //- App Area
    cdl_ui_widget_attributes appAreaAttribs = cdl_ui_widget_attributes_new();
    cdl_ui_widget_new(&ui->appArea, &ui->canvas, 0, UIWidgetType_Panel, appAreaAttribs);
    
    //- Left Column
    cdl_ui_widget_attributes panelAttrib = cdl_ui_widget_attributes_new();
    panelAttrib.layout = UIWidgetLayout_Vertical;
    panelAttrib.constraints.type = UIWidgetConstraint_SizeRelativeX 
        | UIWidgetConstraint_SizeRelativeY
        | UIWidgetConstraint_PosRelativeLeft
        | UIWidgetConstraint_PosRelativeTop;
    panelAttrib.constraints.size = v2f_new(0.25, 1);
    panelAttrib.constraints.positions.left = 0.125;
    panelAttrib.constraints.positions.top = 0.5;
    
    cdl_ui_widget_new(&ui->areaLeftColumn, &ui->appArea, 0, UIWidgetType_Panel, panelAttrib);
    
    //- 
    cdl_ui_widget_attributes buttonAttrib = cdl_ui_widget_attributes_new();
    buttonAttrib.constraints.type = UIWidgetConstraint_SizeFixedY
        | UIWidgetConstraint_PosRelativeLeft;
    buttonAttrib.constraints.size.y = 20;
    buttonAttrib.constraints.positions.left = 0.5;
    
    cdl_ui_widget_new(&ui->reloadUIButton, &ui->areaLeftColumn, "Reload UI", UIWidgetType_Button, buttonAttrib);
    cdl_ui_widget_new(&ui->testButton2, &ui->areaLeftColumn, "Test button 2", UIWidgetType_Button, buttonAttrib);
    cdl_ui_widget_new(&ui->testBoolean, &ui->areaLeftColumn, "Test Checkbox", UIWidgetType_Boolean, buttonAttrib);
    cdl_ui_widget_new(&ui->testSlider, &ui->areaLeftColumn, "Slider", UIWidgetType_FloatSlider, buttonAttrib);
    
    buttonAttrib.constraints.size.y = 100;
    cdl_ui_widget_new(&ui->colorPicker, &ui->areaLeftColumn, "", UIWidgetType_ColorPicker, buttonAttrib);
    
    //- Right Column
    panelAttrib.flags |= UIWidgetFlags_HasClipRect;
    panelAttrib.constraints.positions.left = 0.625;
    panelAttrib.constraints.size.x = 0.75;
    
    cdl_ui_widget_new(&ui->areaRightColumn, &ui->appArea, 0, UIWidgetType_Panel, panelAttrib);
    cdl_ui_widget_new(&ui->rightTextArea, &ui->areaRightColumn, 0, UIWidgetType_TextArea, {});
}

internal void application_init(application_memory* app)
{
    core->platform.setWindowTitle(&core->platform, "UI Test Application");
    application_build_ui(app);
}

internal void application_update(application_memory* app, thread_work_queue* wq)
{
    switch (app->state) {
        case AppState_None:
        {
            application_init(app);
            app->state = AppState_Run;
        } break;
        
        case AppState_Run:
        {
            cdl_ui_update(&app->ui.master);
            
            if ( w_pressed(app->ui.openButton) ) {
                char filePath[CDL_MAX_PATH] = { };
                core->platform.openfileexplorer(filePath, "*.shader", "*.shader");
                log_msg(filePath);
            }
            
            if ( w_pressed(app->ui.saveButton) ) {
                char filePath[CDL_MAX_PATH] = { };
                core->platform.savefileexplorer(filePath, "*.txt");
                log_msg("Filepath: %s\n", filePath);
                
                cdl_file newfile = ZeroStruct;
                newfile = *(cdl_file*)&app->ui.rightTextArea.text;
                core->platform.writefile(filePath, &newfile);
            }
            
            if ( w_pressed(app->ui.quitButton) ) {
                core->run = 0;
            }
            
            if ( w_pressed(app->ui.reloadUIButton) ) {
                application_build_ui(app);
            }
            
            app->ui.colorPicker.fValue = app->ui.testSlider.fValue * 2 * F32Pi;
        } break;
    }
}