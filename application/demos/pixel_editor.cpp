typedef u32 AppState;
enum AppState_ {
    AppState_None,
    AppState_Run,
    AppState_Exit
};

struct pixel {
    v2i position;
    color8 color;
};

struct application_memory {
    AppState state;
    u32 pixelDim;
    pixel pixels[512];
    u32 pixelCount;
    
    v2f gridOffset;
    v2f lastPixelPosition;
    color8 color;
};

internal bool32 cim_pixel_button(const char* name, color8 color, u32 pixelElementWidth)
{
    PROFILED_FUNC;
    
    // Computations
    im_id id = im_id_new(name);
    rect2 btnRect = im_layout_new_rect();
    
    btnRect.max = v2f_add(btnRect.min, v2f_new(pixelElementWidth, pixelElementWidth));
    
    if ( !im_layout_add_rect(btnRect) ) { return 0; }
    
    // Logic
    bool32 pressed = im_button_behaviour(id, btnRect);
    
    // Drawing
    im_draw_rect_filled(btnRect, color);
    
    if ( collisions_v2f_in_rect(g_imctx->mpos, btnRect) ) {
        im_draw_rect(btnRect, 1, color_white);
    }
    
    return pressed;
}

internal void application_clear_grid(application_memory* app)
{
    for (u32 i = 0; i < app->pixelCount; i++) {
        app->pixels[i] = ZeroStruct;
    }
    app->pixelCount = 0;
}

internal void application_im_frame(application_memory* app)
{
    im_frame_begin();
    
    if ( im_window_begin("Editor Window") ) {
        im_layout_begin(IMLayout_Horizontal);
        im_layout_set_widget_width(IMDimensionRule_Fixed, 15);
        
        if ( im_button("Clear") ) {
            application_clear_grid(app);
        }
        
        if ( im_button("Quit", IMWidgetFlag_Danger) ) {
            core->run = 0;
        }
        im_layout_end();
        
        im_text("Grid Offset: ( %.3f , %.3f )", app->gridOffset.x, app->gridOffset.y);
        im_text("Last Pixel: ( %.0f , %.0f )", app->lastPixelPosition.x, app->lastPixelPosition.y);
        
        f32 winWidth = rect2_dim(im_current_window()->rect).width;
        
        i32 pixelElementCountInWidth = 6;
        i32 pixelElementWidth = f32_floor( ( winWidth - pixelElementCountInWidth * 5 ) / pixelElementCountInWidth );
        pixelElementWidth = i32_clamp(pixelElementWidth, 5, 30);
        
        im_layout_begin(IMLayout_Horizontal);
        
        for (u32 r = 0; r < 255; r += 50) {
            im_layout_begin(IMLayout_Vertical);
            
            for (u32 b = 0; b < 255; b += 50) {
                char name[32] = { };
                sprintf(name, "pixelcol-%d", r*7+b*5);
                color8 color = cdl_rgb(r, 100, b);
                
                if ( cim_pixel_button(name, color, pixelElementWidth) ) {
                    app->color = color;
                }
            }
            
            im_layout_end();
        }
        
        im_layout_end();
        im_window_end();
    }
    
    im_frame_end();
}

internal void application_add_pixel(application_memory* app)
{
    v2i mpos = mouse_get_position();
    v2i pos = v2i_new();
    
    v2f relativeMPos = v2f_sub(v2itof(mpos), app->gridOffset);
    
    pos.x = relativeMPos.x >= 0 ?
    (mpos.x - app->gridOffset.x) / app->pixelDim : (1 + mpos.x - app->gridOffset.x) / app->pixelDim - 1;
    pos.y = relativeMPos.y >= 0 ?
    (mpos.y - app->gridOffset.y) / app->pixelDim : (mpos.y - app->gridOffset.y) / app->pixelDim - 1;
    
    app->pixels[app->pixelCount++] = {
        pos,
        app->color
    };
    
    app->lastPixelPosition = v2itof(pos);
}

internal void application_remove_pixel(application_memory* app)
{
    v2i mpos = mouse_get_position();
    v2i pos = v2i_new();
    
    v2f relativeMPos = v2f_sub(v2itof(mpos), app->gridOffset);
    
    pos.x = relativeMPos.x >= 0 ?
    (mpos.x - app->gridOffset.x) / app->pixelDim : (1 + mpos.x - app->gridOffset.x) / app->pixelDim - 1;
    pos.y = relativeMPos.y >= 0 ?
    (mpos.y - app->gridOffset.y) / app->pixelDim : (mpos.y - app->gridOffset.y) / app->pixelDim - 1;
    
    for (u32 i = 0; i < app->pixelCount; i++) {
        if ( v2i_equals(pos, app->pixels[i].position) ) {
            app->pixels[i].color = ZeroStruct;
        }
    }
}

internal void application_interactions(application_memory* app)
{
    // If the mouse is not hovering the UI
    if ( !g_imctx->hoveredWindow ) {
        if ( mouse_button_on_down(mouse_LEFT) ) {
            application_add_pixel(app);
        }
        
        if ( mouse_button_get(mouse_RIGHT) ) {
            application_remove_pixel(app);
        }
        
        if ( mouse_button_get(mouse_MIDDLE) ) {
            app->gridOffset = v2f_add(app->gridOffset, mouse_get_delta());
        }
        
        app->pixelDim += mouse_get_scroll().y;
        app->pixelDim = app->pixelDim <= 0 ? 1 : app->pixelDim;
    }
}

internal void application_draw_pixel_grid(application_memory* app)
{
    color8 color = cdl_rgba(255, 255, 255, 50);
    
    f32 offsetX = (i32(app->gridOffset.x) % app->pixelDim);
    f32 offsetY = (i32(app->gridOffset.y) % app->pixelDim);
    
    // Horizontal
    for (i32 i = -1; i < f32_floor(core->frameHeight / app->pixelDim) + 1; i++) {
        v2f p1 = v2f_new(0, offsetY + i * app->pixelDim);
        v2f p2 = v2f_new(core->frameWidth, offsetY + i * app->pixelDim);
        ui_line(p1, p2, 1, color);
    }
    
    // Vertical
    for (i32 i = -1; i < f32_floor(core->frameWidth / app->pixelDim) + 1; i++) {
        v2f p1 = v2f_new(offsetX + i * app->pixelDim, 0);
        v2f p2 = v2f_new(offsetX + i * app->pixelDim, core->frameHeight);
        ui_line(p1, p2, 1, color);
    }
}

internal void application_draw_pixels(application_memory* app)
{
    v2i dim = v2i_new(app->pixelDim);
    for (u32 i = 0; i < app->pixelCount; i++) {
        pixel* p = app->pixels + i;
        rect2 pixelRect = ZeroStruct;
        
        v2i rectMin = v2i_times_i32(p->position, app->pixelDim);
        pixelRect.min = v2f_add(app->gridOffset, v2itof(rectMin));
        pixelRect.max = v2f_add(pixelRect.min, v2itof(dim));
        ui_rect(pixelRect, p->color);
    }
}

internal void application_update(application_memory* app, thread_work_queue* wq)
{
    switch (app->state) {
        case AppState_None:
        {
            app->pixelDim = 30;
            app->state = AppState_Run;
        } break;
        
        case AppState_Run:
        {
            application_interactions(app);
            application_draw_pixel_grid(app);
            application_draw_pixels(app);
            
            application_im_frame(app);
        } break;
        
        default:
        {
        } break;
    }
}