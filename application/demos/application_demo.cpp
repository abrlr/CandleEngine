enum AppState_ {
    AppState_None,
    AppState_Run,
    AppState_Exit
};

struct application_memory {
    AppState_ state;
    
    // Camera
    struct {
        cdl_renderer_camera* camera;
        
        bool32 useOrbitCamera;
        f32 transitionFactor;
        f32 transitionSpeed;
        
        v3f physicalPosition;
        v3f physicalRotation;
        v3f initialPosition;
        v3f initialRotation;
        
        v3f freeRotation;
        v3f freePosition;
        
        v3f orbitRotation;
        v3f orbitPosition;
    };
    
    cdl_assets_model_raw* groundQuad;
    
    cdl_renderer_material* lightMaterial;
    cdl_renderer_material* cubeMaterial;
    
    cdl_renderer_effect* brightFilter;
    cdl_renderer_effect* blur;
    cdl_renderer_effect* bloom;
    
    cdl_renderer_light* lights[5];
    
    cdl_render_scene* modelViewerScene;
    cdl_renderer_scene_view* modelViewerView;
    cdl_renderer_camera* modelViewerCamera;
    cdl_renderer_light* modelViewerLight;
    
    // Editor UI
    cdl_ui UI;
    struct {
        cdl_ui_widget windowCanvas,
        topBar,
        bezierStepsSlider;
        
        // Test dropdown
        cdl_ui_widget dropdown,
        button2,
        toggle1,
        colorPicker;
        
        // Demos dropdown
        cdl_ui_widget demosDropdown,
        showAlphabetToggle,
        showIconsToggle,
        bezierToggle,
        circlesToggle;
        
        // Effects dropdown
        cdl_ui_widget effectsDropdown,
        bloomToggle;
    };
};

//- Show the latin alphabet at multiple sizes 
internal void Editor_ShowAlphabet(application_memory* app)
{
    PROFILED_FUNC;
    
    if ( w_toggled(app->showAlphabetToggle) ) {
        v2f position = v2f_new(core->frameWidth / 2, core->frameHeight / 10);
        cdl_renderer_font* font = renderer->currentFont;
        cdl_text_atb attributes = cdl_text_atb_new();
        attributes.lineWidth = 4000;
        attributes.weight = 5;
        attributes.style = TextStyle_Bold;
        
        attributes.size = 10;
        ui_text("ABCDEFGHIJKLMNOPQRSTUVWYZ", position, font, attributes);
        
        attributes.size = 15;
        position.y += attributes.size;
        ui_text("ABCDEFGHIJKLMNOPQRSTUVWYZ", position, font, attributes);
        
        attributes.size = 20;
        position.y += attributes.size;
        ui_text("ABCDEFGHIJKLMNOPQRSTUVWYZ", position, font, attributes);
        
        attributes.size = 40;
        position.y += attributes.size;
        ui_text("ABCDEFGHIJKLMNOPQRSTUVWXYZ", position, font, attributes);
        
        attributes.size = 60;
        position.y += attributes.size;
        ui_text("ABCDEFGHIJKLMNOPQRSTUVWXYZ", position, font, attributes);
        
        attributes.size = 80;
        position.y += attributes.size;
        ui_text("ABCDEFGHIJKLMNOPQRSTUVWXYZ", position, font, attributes);
        
        attributes.size = 100;
        position.y += attributes.size;
        ui_text("ABCDEFGHIJKLMNOPQRSTUVWXYZ", position, font, attributes);
        
        attributes.size = 150;
        position.y += attributes.size;
        ui_text("ABCDEFGHIJKLMNOPQRSTUVWXYZ", position, font, attributes);
        
        attributes.size = 200;
        position.y += attributes.size;
        ui_text("ABCDEFGHIJKLMNOPQRSTUVWXYZ", position, font, attributes);
        
        attributes.size = 250;
        position.y += attributes.size;
        ui_text("ABCDEFGHIJKLMNOPQRSTUVWXYZ", position, font, attributes);
    }
}

//- Render a SDF icon 
internal void Editor_RenderIcon(v2f center, v2f dim)
{
    PROFILED_FUNC;
    
    cdl_assets_texture* squareIcon = cdl_assets_texture_get(ASSET_ICON(square));
    cdl_renderer_texture* squareTexture = cdl_renderer_texture_get(squareIcon);
    i32 texture = cdl_renderer_texture_push(renderer->ui, squareTexture);
    ui_quad_sdf(center, dim, v4f_new(0, 1, 0, 1), texture);
    
    cdl_assets_texture* checkIcon = cdl_assets_texture_get(ASSET_ICON(check));
    cdl_renderer_texture* checkTexture = cdl_renderer_texture_get(checkIcon);
    texture = cdl_renderer_texture_push(renderer->ui, checkTexture);
    
    v2f subIconDim = v2f_times(dim, 0.7);
    ui_quad_sdf(center, subIconDim, v4f_new(0, 1, 0, 1), texture);
}

//- Show Icons at multiple sizes 
internal void Editor_ShowIcons(application_memory* app)
{
    PROFILED_FUNC;
    
    if ( w_toggled(app->showIconsToggle) ) {
        v2f iconCenter = v2f_new(100, core->frameHeight / 2);
        v2f iconDim = v2f_new(15, 15);
        Editor_RenderIcon(iconCenter, iconDim);
        
        iconCenter.x += 50;
        iconDim = v2f_new(30, 30);
        Editor_RenderIcon(iconCenter, iconDim);
        
        iconCenter.x += 75;
        iconDim = v2f_new(60, 60);
        Editor_RenderIcon(iconCenter, iconDim);
        
        iconCenter.x += 100;
        iconDim = v2f_new(100, 100);
        Editor_RenderIcon(iconCenter, iconDim);
        
        iconCenter.x += 200;
        iconDim = v2f_new(200, 200);
        Editor_RenderIcon(iconCenter, iconDim);
        
        iconCenter.x += 300;
        iconDim = v2f_new(300, 300);
        Editor_RenderIcon(iconCenter, iconDim);
    }
}

//- Show a quadratic & a cubic Bézier curves 
internal void Editor_ShowBezierCurves(application_memory* app)
{
    PROFILED_FUNC;
    
    if ( w_toggled(app->bezierToggle) ) {
        
        // UI Bézier Curves
        local v2f points[7] = {
            v2f_new(core->frameWidth / 2, core->frameHeight / 2),
            v2f_new(core->frameWidth / 2 + 100, 200),
            v2f_new(core->frameWidth / 2, core->frameHeight / 2 + 200),
            v2f_new(200, core->frameHeight / 2),
            v2f_new(250, 300),
            v2f_new(400, 200),
            v2f_new(500, core->frameHeight / 2)
        };
        
        local i32 held = -1;
        
        for (u32 i = 0; i < ARRAY_COUNT(points); i++) {
            rect2 pointHitbox = rect2_from_size(points[i], v2f_new(20, 20));
            if ( collisions_v2i_in_rect(mouse_get_position(), pointHitbox) ) {
                ui_point(points[i], 20, cdl_rgba(255, 255, 255, 200));
                
                if ( mouse_button_on_down(mouse_LEFT) ) {
                    held = i;
                    break;
                }
            }
        }
        
        if ( held >= 0 ) {
            v2f delta = v2f_sub(v2itof(mouse_get_position()), points[held]);
            points[held] = v2itof(mouse_get_position());
            
            if ( held == 3 ) {
                points[4] = v2f_add(points[4], delta);
            }
            
            if ( held == 6 ) {
                points[5] = v2f_add(points[5], delta);
            }
        }
        
        if ( mouse_button_on_up(mouse_LEFT) ) {
            held = -1;
        }
        
        u32 steps = app->bezierStepsSlider.fValue;
        
        ui_bezier_quadratic(points[0], points[1], points[2], 2, color_white, steps);
        ui_bezier_cubic(points[3], points[4], points[5], points[6], 2, color_white, steps);
        
        ui_line(points[3], points[4], 1, cdl_rgba(255, 255, 255, 200));
        ui_line(points[5], points[6], 1, cdl_rgba(255, 255, 255, 200));
        
        // Control points
        for (u32 i = 0; i < ARRAY_COUNT(points); i++) {
            ui_point(points[i], 5, color_red);
        }
        
    }
}

internal void Editor_ShowCircles(application_memory* app)
{
    PROFILED_FUNC;
    
    if ( w_toggled(app->circlesToggle) ) {
        v2f position = v2f_new(100, core->frameHeight / 2);
        ui_circle(position, 50, 0, color_white);
        
        position.x += 200;
        ui_circle(position, 100, 0, color_red);
        
        position.x += 200;
        ui_circle(position, 150, 0, color_green);
        
        position.x += 200;
        ui_circle(position, 200, 100, color_yellow);
        
        position.x += 200;
        ui_circle(position, 250, 50, color_blue);
        
        position.x += 200;
        ui_circle(position, 300, 10, color_pink);
    }
}

internal void Editor_Effects(application_memory* app)
{
    PROFILED_FUNC;
    
    if ( w_pressed(app->bloomToggle) ) {
        if ( w_toggled(app->bloomToggle) ) {
            renderer->finalWorldEffect = 0;
        } else {
            renderer->finalWorldEffect = app->bloom;
        }
    }
}

//- Build the default UI 
internal void Editor_CreateWidgets(application_memory* app)
{
    PROFILED_FUNC;
    
    app->UI.rootWidget = &app->windowCanvas;
    
    //- Window Canvas 
    cdl_ui_widget_attributes emptyAttr = cdl_ui_widget_attributes_new();
    cdl_ui_widget_new(&app->windowCanvas, 0, 0, UIWidgetType_Canvas, emptyAttr);
    
    //- Top Bar 
    cdl_ui_widget_attributes topBarAttr = cdl_ui_widget_attributes_new();
    topBarAttr.flags |= UIWidgetFlags_EatInputs;
    topBarAttr.layout = UIWidgetLayout_Horizontal;
    topBarAttr.constraints.type = UIWidgetConstraint_SizeRelativeX 
        | UIWidgetConstraint_SizeFixedY;
    topBarAttr.constraints.size = v2f_new(1, 20);
    
    
    cdl_ui_widget_attributes topBarButtonAttr = cdl_ui_widget_attributes_new();
    topBarButtonAttr.layout = UIWidgetLayout_Vertical;
    topBarButtonAttr.constraints.type = UIWidgetConstraint_SizeFixedX
        | UIWidgetConstraint_MarginFixedLeft
        | UIWidgetConstraint_PosFixedTop;
    topBarButtonAttr.constraints.size = v2f_new(100, 1);
    topBarButtonAttr.constraints.positions.top = 0;
    topBarButtonAttr.constraints.margins.left = 10;
    
    
    cdl_ui_widget_attributes dropdownButtonAttr = cdl_ui_widget_attributes_new();
    dropdownButtonAttr.constraints.type = UIWidgetConstraint_MarginFixedTop
        | UIWidgetConstraint_MarginFixedLeft
        | UIWidgetConstraint_SizeFixedX
        | UIWidgetConstraint_SizeFixedY;
    dropdownButtonAttr.constraints.margins.left = 5;
    dropdownButtonAttr.constraints.margins.top = 5;
    dropdownButtonAttr.constraints.size = v2f_new(150, 20);
    
    //- Panel
    cdl_ui_widget_new(&app->topBar, &app->windowCanvas, 0, UIWidgetType_Panel, topBarAttr);
    
    //- Dropdown
    cdl_ui_widget_new(&app->dropdown, &app->topBar, "Dropdown...", UIWidgetType_Dropdown, topBarButtonAttr);
    
    cdl_ui_widget_new(&app->button2, &app->dropdown, "Button 2", UIWidgetType_Button, dropdownButtonAttr);
    
    cdl_ui_widget_new(&app->toggle1, &app->dropdown, "Toggle 1", UIWidgetType_Boolean, dropdownButtonAttr);
    
    dropdownButtonAttr.constraints.size.y = 150;
    cdl_ui_widget_new(&app->colorPicker, &app->dropdown, "", UIWidgetType_ColorPicker, dropdownButtonAttr);
    dropdownButtonAttr.constraints.size.y = 20;
    
    //- Demos dropdown
    cdl_ui_widget_new(&app->demosDropdown, &app->topBar, "Demos...", UIWidgetType_Dropdown, topBarButtonAttr);
    
    dropdownButtonAttr.flags |= UIWidgetFlags_Animated;
    cdl_ui_widget_new(&app->showAlphabetToggle, &app->demosDropdown, "Alphabet", UIWidgetType_Boolean, dropdownButtonAttr);
    
    cdl_ui_widget_new(&app->showIconsToggle, &app->demosDropdown, "Icons", UIWidgetType_Boolean, dropdownButtonAttr);
    
    cdl_ui_widget_new(&app->bezierToggle, &app->demosDropdown, "Bezier", UIWidgetType_Boolean, dropdownButtonAttr);
    
    cdl_ui_widget_new(&app->circlesToggle, &app->demosDropdown, "Circles", UIWidgetType_Boolean, dropdownButtonAttr);
    
    //- Effects dropdown
    cdl_ui_widget_new(&app->effectsDropdown, &app->topBar, "Effects...", UIWidgetType_Dropdown, topBarButtonAttr);
    
    cdl_ui_widget_new(&app->bloomToggle, &app->effectsDropdown, "Disable Bloom", UIWidgetType_Boolean, dropdownButtonAttr);
    
    //- Bezier Steps Slider 
    cdl_ui_widget_new(&app->bezierStepsSlider, &app->topBar, "Steps", UIWidgetType_FloatSlider, topBarButtonAttr);
    app->bezierStepsSlider.fMin = 1;
    app->bezierStepsSlider.fValue = 50;
    app->bezierStepsSlider.fMax = 100;
}

internal void application_initialise(application_memory* app)
{
    platform->setWindowTitle("Demo Application");
    
    app->transitionSpeed = 1;
    app->camera = cdl_renderer_camera_new(CameraType_Perspective);
    app->camera->fov = 70;
    renderer->worldView->camera = app->camera;
    
    // Ground Quad
    v3f pos[] = {
        v3f_new( 100, -5,  100),
        v3f_new( 100, -5, -100),
        v3f_new(-100, -5, -100),
        v3f_new(-100, -5,  100)
    };
    
    v2f uvs[] = {
        v2f_new(0)
    };
    
    v3f normals[] = {
        v3f_new(0, 1, 0)
    };
    
    u32 vertexDefs[] = {
        0, 0, 0,
        1, 0, 0,
        3, 0, 0,
        
        1, 0, 0,
        2, 0, 0,
        3, 0, 0,
    };
    
    app->groundQuad = cdl_assets_model_raw_create_from_vertex_defs("App: Ground Quad", ARRAY_COUNT(vertexDefs), vertexDefs, pos, uvs, normals);
    
    // Cube Material
    app->cubeMaterial = cdl_renderer_material_new(0);
    
    // Light Material
    cdl_assets_shader* lightShaderAsset = cdl_assets_shader_create(ShaderType_World, "assets/demo_app/app_light.shader");
    cdl_renderer_shader* lightShader = cdl_renderer_shader_new(RendererShaderType_World, lightShaderAsset);
    app->lightMaterial = cdl_renderer_material_new(lightShader);
    
    // Bright Filter
    cdl_assets_shader* brightFilterShaderAsset = cdl_assets_shader_create(ShaderType_FullScreenEffect, "assets/demo_app/app_brightFilter.shader");
    app->brightFilter = cdl_renderer_effect_new(brightFilterShaderAsset, 0);
    
    // Blur
    cdl_assets_shader* blurShaderAsset = cdl_assets_shader_create(ShaderType_FullScreenEffect, "assets/demo_app/app_blur.shader");
    cdl_renderer_effect* parents[4] = { app->brightFilter, 0, 0, 0 };
    app->blur = cdl_renderer_effect_new(blurShaderAsset, parents);
    
    // Bloom
    cdl_assets_shader* bloomShaderAsset = cdl_assets_shader_create(ShaderType_FullScreenEffect, "assets/demo_app/app_bloom.shader");
    parents[0] = app->blur;
    app->bloom = cdl_renderer_effect_new(bloomShaderAsset, parents);
    renderer->finalWorldEffect = app->bloom;
    
    // Lights
    app->lights[0] = cdl_render_scene_push_light(renderer->world, v3f_new(   0, 5,   0), color_white);
    app->lights[1] = cdl_render_scene_push_light(renderer->world, v3f_new(  15, 5,  15), color_blue);
    app->lights[2] = cdl_render_scene_push_light(renderer->world, v3f_new(  15, 5, -15), color_green);
    app->lights[3] = cdl_render_scene_push_light(renderer->world, v3f_new( -15, 5,  15), color_red);
    app->lights[4] = cdl_render_scene_push_light(renderer->world, v3f_new( -15, 5, -15), color_yellow);
    
    // Application Model Viewer
    {
        // Scene
        app->modelViewerScene = cdl_render_scene_new(RenderSceneType_3D);
        app->modelViewerScene->defaultModel = renderer->world->defaultModel;
        
        // Environment
        app->modelViewerScene->environment.ambientDirection = v3f_new(1, -1, 1);
        app->modelViewerScene->environment.ambientColor = color_white;
        app->modelViewerScene->environment.ambientIntensity = 1;
        app->modelViewerLight = cdl_render_scene_push_light(app->modelViewerScene, v3f_new(0, 5, 0), color_white);
        
        // Camera
        app->modelViewerCamera = cdl_renderer_camera_new(CameraType_Perspective);
        app->modelViewerCamera->fov = 70;
        cdl_renderer_camera_update_projection(app->modelViewerCamera);
        
        // View
        app->modelViewerView = cdl_renderer_scene_view_new(-1, app->modelViewerScene, app->modelViewerCamera, renderer->defaultWorldShader, 1);
        
#ifdef CDL_DEBUG
        MemoryCopy(app->modelViewerView->debugName, "Model Viewer View", StringLength("Model Viewer View"));
#endif
    }
    
    Editor_CreateWidgets(app);
}

internal void application_update_positions(application_memory* app)
{
    //- Free Movement 
    if ( !app->useOrbitCamera ) {
        f32 xAxisAmount = 0;
        xAxisAmount+= controller_axis_get(0, controller_LEFT_X) * 0.15f;
        xAxisAmount+= keyboard_key_get(KEY_RIGHT) * 0.15f - keyboard_key_get(KEY_LEFT) * 0.15f;
        app->freePosition.x += xAxisAmount;
        
        f32 yAxisAmount = 0;
        yAxisAmount += controller_button_get(0, controller_A) * 0.15f - controller_button_get(0, controller_B) * 0.15f;
        yAxisAmount += keyboard_key_get(KEY_SPACE) * 0.15f - keyboard_key_get(KEY_LEFT_SHIFT) * 0.15f;
        app->freePosition.y -= yAxisAmount;
        
        f32 zAxisAmount = 0;
        zAxisAmount += controller_axis_get(0, controller_LEFT_Y) * 0.15f;
        zAxisAmount += keyboard_key_get(KEY_DOWN) * 0.15f - keyboard_key_get(KEY_UP) * 0.15f;
        app->freePosition.z -= zAxisAmount;
        
        app->freeRotation.x -= controller_axis_get(0, controller_RIGHT_Y) * 0.05f;
        app->freeRotation.y -= controller_axis_get(0, controller_RIGHT_X) * 0.05f;
    }
    
    //- Orbit Movement
    f32 radTime = f32_deg_to_rad(core->totalTime * 15);
    app->orbitPosition = v3f_new(10 * cos(radTime), 5, 10 * sin(radTime));
    app->orbitRotation.y = radTime + F32Pi / 2;
}

internal void application_toggle_camera_transition(application_memory* app)
{
    app->useOrbitCamera = !app->useOrbitCamera;
    app->transitionFactor = 0;
    
    app->initialPosition = app->physicalPosition;
    app->initialRotation = app->physicalRotation;
}

internal void application_update_camera(application_memory* app)
{
    v3f targetPos = app->useOrbitCamera ? app->orbitPosition : app->freePosition;
    v3f targetRot = app->useOrbitCamera ? app->orbitRotation : app->freeRotation;
    
    if ( app->transitionFactor < 1 ) {
        app->transitionFactor += core->deltaTime * app->transitionSpeed;
        app->physicalPosition = v3f_lerp(targetPos, app->initialPosition, app->transitionFactor);
        app->physicalRotation = v3f_lerp(targetRot, app->initialRotation, app->transitionFactor);
    }
    
    else {
        app->physicalPosition = targetPos;
        app->physicalRotation = targetRot;
        app->transitionFactor = 1;
    }
    
    app->camera->position = app->physicalPosition;
    app->camera->rotation = app->physicalRotation;
}

internal void application_run(application_memory* app)
{
    if ( cdl_engine_keyboard_key_on_down(KEY_ESCAPE) ) {
        app->state = AppState_Exit;
    }
    
    if ( cdl_engine_keyboard_key_on_down(KEY_R) ) {
        for (cdl_filesystem_iterator it = platform->fsIterator(".", 0); it.valid; platform->fsIterate(&it)) {
            log_msg("%s\n", it.currentPath);
        }
    }
    
    //- Camera
    {
        PROFILED_BLOCK("application_update::camera");
        application_update_positions(app);
        application_update_camera(app);
    }
    
    //- Geometry 
    {
        PROFILED_BLOCK("application_update::geometry");
        
        world_line(v3f_new(-100, 0, 0), v3f_new(100, 0, 0), cdl_rgba(255, 0, 0, 200));
        world_line(v3f_new(0, 0, -100), v3f_new(0, 0, 100), cdl_rgba(0, 255, 0, 200));
        
        cdl_renderer_model_transform gQT = cdl_renderer_model_transform_new();
        world_model_instance(app->groundQuad, gQT);
        
        color8 colors[11] = {
            cdl_rgb(10, 255, 10),
            cdl_rgb(25, 225, 10),
            cdl_rgb(50, 200, 10),
            cdl_rgb(75, 175, 10),
            cdl_rgb(100, 150, 10),
            cdl_rgb(125, 125, 10),
            cdl_rgb(150, 100, 10),
            cdl_rgb(175, 75, 10),
            cdl_rgb(200, 50, 10),
            cdl_rgb(225, 25, 10),
            cdl_rgb(255, 10, 10)
        };
        
        // Cubes
        for (i32 i = -5; i < 6; i++) {
            cdl_renderer_model_transform transform = cdl_renderer_model_transform_new();
            transform.position = v3f_new(i * 1.05, 0, 0);
            transform.rotation = v3f_new(core->totalTime / 10, 0, 0);
            
            app->cubeMaterial->color = colors[i + 5];
            world_model_instance_with_mat(0, app->cubeMaterial, transform);
        }
        
        // Light Cubes
        for (u32 i = 0; i < ARRAY_COUNT(app->lights); i++) {
            cdl_renderer_model_transform transform = cdl_renderer_model_transform_new();
            transform.position = app->lights[i]->position;
            app->lightMaterial->color = app->lights[i]->color;
            world_model_instance_with_mat(0, app->lightMaterial, transform);
        }
        
        // Position Markers
        app->cubeMaterial->color = color_white;
        
        cdl_renderer_model_transform transform = cdl_renderer_model_transform_new();
        transform.position = app->freePosition;
        transform.rotation = app->freeRotation;
        transform.scale = 0.2;
        world_model_instance_with_mat(0, app->cubeMaterial, transform);
        
        transform.position = app->orbitPosition;
        transform.rotation = app->orbitRotation;
        world_model_instance_with_mat(0, app->cubeMaterial, transform);
    }
    
    //- Model Viewer
    {
        f32 radTime = f32_deg_to_rad(core->totalTime) * 15;
        app->modelViewerCamera->position = v3f_new(2 * cos(radTime), 2, 2 * sin(radTime));
        app->modelViewerCamera->rotation.x = f32_deg_to_rad(-33.f);
        app->modelViewerCamera->rotation.y = radTime + F32Pi / 2;
        
        cdl_renderer_model_transform transform = cdl_renderer_model_transform_new();
        cdl_render_scene_push_model_instance(app->modelViewerScene, 0, 0, transform);
        
        cdl_render_scene_push_line(app->modelViewerScene, v3f_new(-100, 0, 0), v3f_new(100, 0, 0), color_red);
        cdl_render_scene_push_line(app->modelViewerScene, v3f_new(0, 0, -100), v3f_new(0, 0, 100), color_green);
    }
    
    //- UI
    {
        Editor_ShowAlphabet(app);
        Editor_ShowIcons(app);
        Editor_ShowBezierCurves(app);
        Editor_ShowCircles(app);
        Editor_Effects(app);
    }
}

internal void application_update(application_memory* app, thread_work_queue* wq)
{
    PROFILED_FUNC;
    
    cdl_ui_update(&app->UI);
    
    switch (app->state)
    {
        case AppState_None:
        {
            application_initialise(app);
            app->state = AppState_Run;
        } break;
        
        case AppState_Run:
        {
            application_run(app);
            
            im_frame_begin();
            if ( im_window_begin("Window") ) {
                
                im_layout_begin(IMLayout_Horizontal);
                {
                    im_layout_begin(IMLayout_Vertical);
                    {
                        im_text("Current camera");
                        im_radio("Orbit", app->useOrbitCamera, IMWidgetFlag_ReadOnly);
                        im_radio("Free", !app->useOrbitCamera, IMWidgetFlag_ReadOnly);
                    }
                    im_layout_end();
                    
                    im_layout_begin(IMLayout_Vertical);
                    {
                        if ( im_button("Transition") ) {
                            application_toggle_camera_transition(app);
                        }
                        im_slider1f("Speed", 0.1, 5, &app->transitionSpeed);
                        im_slider1f("Factor", 0, 1, &app->transitionFactor, IMWidgetFlag_ReadOnly);
                    }
                    im_layout_end();
                }
                im_layout_end();
                
                im_window_end();
            }
            im_frame_end();
        } break;
        
        case AppState_Exit:
        {
            log_info("Exit application");
            core->run = false;
        } break;
        
        default:
        {
            log_err("Invalid code path in application state: %d", app->state);
        } break;
    }
}