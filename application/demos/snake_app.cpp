#define GRID_W 32
#define GRID_H 32

struct snake {
    v2i direction;
    
    u32 tailLength;
    v2i tail[GRID_W * GRID_H];
};

struct snakeUI {
    cdl_ui master;
    
    cdl_ui_widget canvas,
    gameoverPanel,
    gameoverLabel,
    gameoverRestartButton,
    gameoverQuitButton;
};

enum AppState_ {
    AppState_None,
    
    AppState_RunTransition,
    AppState_Run,
    
    AppState_GameOverTransition,
    AppState_GameOver,
    
    AppState_Quit
};

struct application_memory {
    AppState_ state;
    
    v2i applePos;
    snake s;
    
    u32 gameTick;
    u32 gameTickFrequency;
    
    snakeUI sUI;
    cdl_assets_audio* startSFX;
    cdl_assets_audio* pickupSFX;
    cdl_assets_audio* gameoverSFX;
};

internal void init_ui(application_memory* app)
{
    snakeUI* ui = &app->sUI;
    ui->master.rootWidget = &ui->canvas;
    
    //- Window Canvas 
    cdl_ui_widget_new(&ui->canvas, 0, 0, UIWidgetType_Canvas, {});
    
    //- GameOver Panel
    cdl_ui_widget_attributes panelAttrib = cdl_ui_widget_attributes_new();
    panelAttrib.layout = UIWidgetLayout_Vertical;
    panelAttrib.constraints.type = UIWidgetConstraint_SizeRelativeX 
        | UIWidgetConstraint_SizeRelativeY
        | UIWidgetConstraint_PosRelativeLeft
        | UIWidgetConstraint_PosRelativeTop;
    panelAttrib.constraints.size = v2f_new(0.5, 0.5);
    panelAttrib.constraints.positions.left = 0.5;
    panelAttrib.constraints.positions.top = 0.5;
    
    
    cdl_ui_widget_attributes panelElementAttrib = cdl_ui_widget_attributes_new();
    panelElementAttrib.layout = UIWidgetLayout_Vertical;
    panelElementAttrib.constraints.type = UIWidgetConstraint_SizeRelativeX
        | UIWidgetConstraint_SizeRelativeY
        | UIWidgetConstraint_PosRelativeLeft
        | UIWidgetConstraint_MarginRelativeTop;
    panelElementAttrib.constraints.size = v2f_new(.9, .3);
    panelElementAttrib.constraints.positions.left = 0.5;
    panelElementAttrib.constraints.margins.top = ( 1. - panelElementAttrib.constraints.size.y * 3 ) / 4; // 3 elements, 4 margins
    
    //- Panel
    cdl_ui_widget_new(&ui->gameoverPanel, &ui->canvas, 0, UIWidgetType_Panel, panelAttrib);
    
    //- 
    cdl_ui_widget_new(&ui->gameoverLabel, &ui->gameoverPanel, "GAME OVER", UIWidgetType_Label, panelElementAttrib);
    ui->gameoverLabel.fontSize = 50;
    
    cdl_ui_widget_new(&ui->gameoverRestartButton, &ui->gameoverPanel, "Restart", UIWidgetType_Button, panelElementAttrib);
    ui->gameoverRestartButton.fontSize = 50;
    
    panelElementAttrib.theme = UITheme_Danger;
    cdl_ui_widget_new(&ui->gameoverQuitButton, &ui->gameoverPanel, "Quit", UIWidgetType_Button, panelElementAttrib);
    ui->gameoverQuitButton.fontSize = 50;
}

internal void application_update(application_memory* app, thread_work_queue* wq)
{
    cdl_ui_update(&app->sUI.master);
    
    switch (app->state) {
        case AppState_None:
        {
            init_ui(app);
            app->startSFX = cdl_assets_audio_create_and_load(AudioType_SFX, "assets/demo_snake/start.wav");
            app->pickupSFX = cdl_assets_audio_create_and_load(AudioType_SFX, "assets/demo_snake/pickup.wav");
            app->gameoverSFX = cdl_assets_audio_create_and_load(AudioType_SFX, "assets/demo_snake/gameover.wav");
            app->state = AppState_RunTransition;
        } break;
        
        case AppState_RunTransition:
        {
            app->s.tailLength = 0;
            app->s.tail[app->s.tailLength++] = v2i_new(random_i32_in_range(&core->rng, 0, GRID_W), random_i32_in_range(&core->rng, 0, GRID_H));
            app->s.direction = v2i_new(0, 0);
            
            app->applePos = v2i_new(random_i32_in_range(&core->rng, 0, GRID_W), random_i32_in_range(&core->rng, 0, GRID_H));
            app->gameTickFrequency = 10;
            
            cdl_audio_track_queue(app->startSFX);
            
            app->sUI.gameoverPanel.hide = 1;
            app->state = AppState_Run;
        } break;
        
        case AppState_Run:
        {
            snake* s = &app->s;
            
            //- Inputs 
            {
                if ( keyboard_key_on_down(KEY_DOWN) ) {
                    s->direction = v2i_new(0, 1);
                }
                
                if ( keyboard_key_on_down(KEY_UP) ) {
                    s->direction = v2i_new(0, -1);
                }
                
                if ( keyboard_key_on_down(KEY_LEFT) ) {
                    s->direction = v2i_new(-1, 0);
                }
                
                if ( keyboard_key_on_down(KEY_RIGHT) ) {
                    s->direction = v2i_new(1, 0);
                }
            }
            
            
            //- Game Tick 
            if ( app->gameTick++ % app->gameTickFrequency == 0 ) {
                // Move Snake 
                v2i futureHead = v2i_add(s->tail[0], s->direction);
                u32 nextLength = s->tailLength;
                
                // Resolve Apple 
                if ( v2i_equals(futureHead, app->applePos) ) {
                    nextLength++;
                    if ( nextLength % 10 == 0 ) {
                        app->gameTickFrequency = i32_max(app->gameTickFrequency - 1, 2);
                    }
                    
                    app->applePos = v2i_new(
                                            random_i32_in_range(&core->rng, 0, GRID_W), 
                                            random_i32_in_range(&core->rng, 0, GRID_H)
                                            );
                    
                    cdl_audio_track_queue(app->pickupSFX);
                }
                
                // Resolve Map Limit Collisions
                if ( futureHead.x > GRID_W || futureHead.x < 0
                    || futureHead.y > GRID_H || futureHead.y < 0 ) {
                    app->state = AppState_GameOverTransition;
                }
                
                // Resolve Head/Tail Collisions
                for (u32 i = 1; i < s->tailLength; i++) {
                    if ( v2i_equals(futureHead, s->tail[i]) ) {
                        app->state = AppState_GameOverTransition;
                    }
                }
                
                // Resolve Tail 
                for (u32 i = nextLength - 1; i > 0; i--) {
                    s->tail[i] = s->tail[i - 1];
                }
                
                s->tail[0] = futureHead;
                s->tailLength = nextLength;
            }
            
            
            //- Draw Game 
            {
                f32 minDim = f32_min(core->frameWidth, core->frameHeight);
                v2f scale = v2f_new(minDim / 50, minDim / 50);
                v2f gridTopLeft = v2f_new(core->frameWidth / 2 - scale.x * GRID_W / 2, core->frameHeight / 2 - scale.y * GRID_H / 2);
                
                // Snake
                for (u32 i = 0; i < s->tailLength; i++) {
                    v2f pos = v2f_add(gridTopLeft, v2f_new(s->tail[i].x * scale.x, s->tail[i].y * scale.y));
                    ui_quad(pos, scale, color_white);
                }
                
                // Apple
                v2f apos = v2f_add(gridTopLeft, v2f_new(app->applePos.x * scale.x, app->applePos.y * scale.y));
                ui_quad(apos, scale, color_red);
                
                // Grid Border
                v2f gridOutlineDim = v2f_new(scale.x * (GRID_W + 1), scale.y * (GRID_H + 1));
                v2f gridOutlineTopLeft = v2f_sub(gridTopLeft, v2f_times(scale, 0.5));
                rect2 grid = rect2_new(gridOutlineTopLeft, v2f_add(gridOutlineTopLeft, gridOutlineDim));
                ui_rect_outline(grid, color_white, 2);
                
                // Score
                char toPrint[32];
                sprintf(toPrint, "Score: %d", (s->tailLength - 1) * 10);
                
                cdl_text_atb attributes = cdl_text_atb_new();
                attributes.size = 32;
                attributes.anchor = TextAnchor_TopLeft;
                ui_text(toPrint, v2f_new(10, 10), attributes);
            }
            
        } break;
        
        case AppState_GameOverTransition:
        {
            app->sUI.gameoverPanel.hide = 0;
            app->state = AppState_GameOver;
            
            cdl_audio_track_queue(app->gameoverSFX);
        } break;
        
        case AppState_GameOver:
        {
            if ( w_pressed(app->sUI.gameoverRestartButton) ) {
                app->state = AppState_RunTransition;
            }
            
            if ( w_pressed(app->sUI.gameoverQuitButton) ) {
                app->state = AppState_Quit;
            }
        } break;
        
        
        case AppState_Quit:
        {
            core->run = 0;
        } break;
        
        default:
        {
        } break;
    }
}