struct application_memory {
    bool8 toggleRadio,
    toggleWindow;
    
    i32 selectedIndex;
    
    u32 sliderUInt;
    v2i sliderVectorInt;
    v4f sliderVectorFloat;
    
    v2f cosineRange;
    
    f32 processTimes[200];
    f32 deltaTimes[200];
    
    i32 toggledTab;
};

enum DEVTab {
    DEVTab_InteractDebug,
    DEVTab_Charts,
    DEVTab_LoadedTextures,
    DEVTab_ThemeEditor,
    
    DEVTab_Count
};

global char* DEVTabNames[] = {
    "Interact Debug",
    "Charts",
    "Loaded Textures",
    "Theme Editor",
};

s_assert(DEVTab_Count == ARRAY_COUNT(DEVTabNames), "Tab names missing");

internal f32 sin_to_plot(f32 x)
{
    return (f32)sin(x);
}

internal f32 cos_to_plot(f32 x)
{
    return (f32)cos(x);
}

internal void application_imgui_tab_interact_debug(application_memory* app)
{
    // Interactions
    im_header("Interactions");
    im_text("Active: %u\0", g_imctx->active.primary);
    im_text("Hot: %u\0", g_imctx->hot.primary);
    im_text("Scroll: (%.3f, %.3f)", g_imctx->mScroll.x, g_imctx->mScroll.y);
    im_text("Window Y Offset: %.3f", g_imctx->windows->offset.y);
    
    // Windows
    im_header("Windows");
    
    im_window* hovered = im_context_find_hovered_window();
    im_window* focused = g_imctx->focusedWindow;
    
    im_layout_begin(IMLayout_Horizontal);
    {
        im_layout_begin(IMLayout_Vertical);
        {
            im_text("Hovered");
            
            im_current_layout()->margins.y = -5;
            u32 count = 0;
            for (im_window* w = g_imctx->lastWindow; w; w = w->top, count++) {
                if ( w == hovered ) {
                    cdl_text_atb attr = cdl_text_atb_new();
                    attr.color = color_yellow;
                    im_text_with_attributes(attr, "> %d : %d", count, w->id.primary);
                } else {
                    im_text("- %d : %d", count, w->id.primary);
                }
            }
        }
        im_layout_end();
        
        im_layout_begin(IMLayout_Vertical);
        im_current_layout()->margins.x = 100;
        {
            im_text("Focused");
            
            im_current_layout()->margins.y = -5;
            u32 count = 0;
            for (im_window* w = g_imctx->lastWindow; w; w = w->top, count++) {
                if ( w == focused ) {
                    cdl_text_atb attr = cdl_text_atb_new();
                    attr.color = color_red;
                    im_text_with_attributes(attr, "> %d : %d", count, w->id.primary);
                } else {
                    im_text("- %d : %d", count, w->id.primary);
                }
            }
        }
        im_layout_end();
    }
    im_layout_end();
    
    // Dockspaces
    im_header("Dockspaces");
    
    im_dockspace* hoveredDockspace = im_internal_dockspace_get_hovered();
    im_text("Mouse is in dockspace: %d", hoveredDockspace - g_imctx->dockspaces);
    
    v2f dsDrawAreaDim = v2f_new(200, 200 * core->frameHeight / core->frameWidth);
    rect2 dsDrawArea = ZeroStruct;
    dsDrawArea.min = im_current_layout()->cursor;
    dsDrawArea.max = v2f_add(dsDrawArea.min, dsDrawAreaDim);
    
    for (im_dockspace* dock = g_imctx->dockspaces; dock < g_imctx->dockspaces + ARRAY_COUNT(g_imctx->dockspaces); dock++) {
        if ( dock->child ) {
            rect2 dockDrawRect = ZeroStruct;
            dockDrawRect.min = v2f_add( v2f_mult(dock->rect.min, dsDrawAreaDim), dsDrawArea.min );
            dockDrawRect.max = v2f_add( v2f_mult(dock->rect.max, dsDrawAreaDim), dsDrawArea.min );
            color8 color = color_white;
            
            if ( dock->child == focused ) {
                color = color_red;
            } else if ( dock->child == hovered ) {
                color = color_yellow;
            }
            
            im_draw_rect_filled(dockDrawRect, color);
            im_draw_rect(dockDrawRect, 1, color_black);
        }
    }
}

internal void application_imgui_tab_charts(application_memory* app)
{
    im_layout_begin(IMLayout_Horizontal);
    {
        im_chart_atb atb = im_chart_atb_new();
        atb.min = 0;
        atb.max = 0.02;
        atb.dim.height = 75;
        atb.dim.width = 400;
        
        if ( im_chart_begin("Frame Times", atb) ) {
            im_chart_plot_lines("Frame Processing", ARRAY_COUNT(app->processTimes), (f32*)app->processTimes, atb);
            
            atb.color = color_red;
            im_chart_plot_lines("Delta Time", ARRAY_COUNT(app->deltaTimes), (f32*)app->deltaTimes, atb);
            im_chart_end();
        }
        
        if ( im_last_is_hovered() ) {
            im_tooltip_begin("@ChartTooltip@");
            im_text("Here is some interesting information about this chart");
            im_tooltip_end();
        }
        
        im_layout_begin(IMLayout_Vertical);
        {
            im_text("Processing Time: %.3f ms", core->processingTime * 1000.f);
            im_text("Delta Time: %.3f ms", core->deltaTime * 1000.f);
        }
        im_layout_end();
        
    }
    im_layout_end();
    
    {
        f32 values[] = {
            7, 10, 9,
            12, 10, 11, 10.5, 14, 8,
            20, 21, 20.5, 17,
            15, 5, 15, 10, 15, 5, 15,
        };
        u32 valueCounts[] = { 3, 6, 4, 7 };
        
        im_chart_atb atb = im_chart_atb_new();
        atb.min = 0;
        atb.max = 30;
        atb.dim = v2f_new(400, 75);
        atb.lineWeight = 2;
        
        if ( im_chart_begin("Multi Chart", atb) ) {
            u32 offset = 0;
            for (u32 i = 0; i < ARRAY_COUNT(valueCounts); i++) {
                atb.color = g_imctx->theme.chart_colors[i];
                
                if ( i == ARRAY_COUNT(valueCounts) - 1 ) {
                    atb.marker = IMChartMarker_Square;
                } else {
                    atb.marker = IMChartMarker_None;
                }
                
                im_chart_plot_lines("v", valueCounts[i], values + offset, atb);
                offset += valueCounts[i];
            }
            im_chart_end();
        }
    }
}

internal void application_imgui_tab_loaded_textures(application_memory* app)
{
    im_layout_begin(IMLayout_Horizontal);
    {
        for (cdl_renderer_texture* t = renderer->storage.textures;
             t < renderer->storage.nextTexture;
             t++) {
            
            u32 textureUnit = cdl_renderer_texture_push(renderer->ui, t);
            f32 aspectRatio = t->asset->width / (f32)t->asset->height;
            v2f dim = v2f_new(100 * aspectRatio, 100);
            im_image_inspect("Texture", textureUnit, dim);
        }
    }
    im_layout_end();
}

internal void application_imgui_tab_theme_editor(application_memory* app)
{
    // Fonts
    if ( im_group_begin("Fonts") ) {
        im_rgba("Headers", &g_imctx->theme.font_header);
        im_rgba("Default", &g_imctx->theme.font_default);
        im_rgba("Hints", &g_imctx->theme.font_hint);
        im_group_end();
    }
    
    // Windows
    if ( im_group_begin("Windows") ) {
        im_rgba("Background", &g_imctx->theme.window.background);
        im_rgba("Border", &g_imctx->theme.window.border);
        im_group_end();
    }
    
    // Headers
    if ( im_group_begin("Window Headers") ) {
        im_rgba("Background", &g_imctx->theme.window_header.background);
        im_rgba("Border", &g_imctx->theme.window_header.border);
        im_rgba("Hovered", &g_imctx->theme.window_header.hovered);
        im_rgba("Active", &g_imctx->theme.window_header.active);
        im_group_end();
    }
    
    // Generic Widgets
    if ( im_group_begin("Widgets") ) {
        im_rgba("Background", &g_imctx->theme.widget.background);
        im_rgba("Border", &g_imctx->theme.widget.border);
        im_rgba("Hovered", &g_imctx->theme.widget.hovered);
        im_rgba("Active", &g_imctx->theme.widget.active);
        im_rgba("Handle", &g_imctx->theme.widget.handle);
        im_group_end();
    }
    
    // Danger Widgets
    if ( im_group_begin("Danger") ) {
        im_rgba("Background", &g_imctx->theme.widget_danger.background);
        im_rgba("Border", &g_imctx->theme.widget_danger.border);
        im_rgba("Icon", &g_imctx->theme.widget_danger.icon);
        im_rgba("Hovered", &g_imctx->theme.widget_danger.hovered);
        im_rgba("Active", &g_imctx->theme.widget_danger.active);
        im_group_end();
    }
    
    // Checkboxes
    if ( im_group_begin("Checkbox Widget") ) {
        im_rgba("Background", &g_imctx->theme.checkbox_icon.background);
        im_rgba("Border", &g_imctx->theme.checkbox_icon.border);
        im_rgba("Hovered", &g_imctx->theme.checkbox_icon.hovered);
        im_rgba("Toggled", &g_imctx->theme.checkbox_icon.toggled);
        im_group_end();
    }
    
    // Radio
    if ( im_group_begin("Radio Widget") ) {
        im_rgba("Icon Background", &g_imctx->theme.radio_icon.background);
        im_rgba("Icon Border", &g_imctx->theme.radio_icon.border);
        im_rgba("Icon Hovered", &g_imctx->theme.radio_icon.hovered);
        im_rgba("Icon Selected", &g_imctx->theme.radio_icon.selected);
        im_group_end();
    }
}

internal void application_imgui_first_window(application_memory* app)
{
    if ( im_menu_bar_begin() ) {
        if ( im_menu_begin("File") ) {
            im_button("Menu 1");
            im_button("Menu 2");
            
            if ( im_button("Quit", IMWidgetFlag_Danger) ) {
                core->run = 0;
            }

            im_menu_end();
        }
        
        im_menu_bar_end();
    }
    
    im_header("Nice Header");
    
    im_layout_set_widget_width(IMDimensionRule_Relative, 1);
    if ( im_button("Button") ) {}
    
    im_layout_begin(IMLayout_Horizontal);
    {
        im_layout_set_widget_width(IMDimensionRule_Relative, 0.2);
        for (u32 i = 0; i < 5; i++) {
            char tempBuffer[64] = { };
            sprintf(tempBuffer, "Button %d", i);
            if ( im_button(tempBuffer) ) {
                
            }
        }
    }
    im_layout_end();
    
    im_separator();
    
    im_layout_begin(IMLayout_Horizontal);
    {
        im_checkbox("Toggle Me!", &app->toggleRadio);
        im_hint("(This will toggle a boolean in the backend)");
    }
    im_layout_end();
    
    if ( app->toggleRadio ) {
        for (u32 i = 0; i < 5; i++) {
            char tempBuffer[64] = { };
            sprintf(tempBuffer, "Radio %d", i);
            if ( im_radio(tempBuffer, app->selectedIndex == (i32)i) ) {
                app->selectedIndex = i;
            }
        }
    }
    im_checkbox("Open second window", &app->toggleWindow);
    
    im_separator();
    im_layout_set_widget_width(IMDimensionRule_Fixed, DEF_WIDTH);
    im_slider1u("UInt", 0, 20, &app->sliderUInt);
    im_slider2i("Int 2", -5, 5, &app->sliderVectorInt);
    im_slider4f("Float 4", -5, 5, &app->sliderVectorFloat);
    
    im_slider2f("Cos Range:", 0, 10, &app->cosineRange);
    im_layout_begin(IMLayout_Horizontal);
    {
        if ( im_chart_begin("Sine") ) {
            im_chart_plot_func("f", core->totalTime, core->totalTime + 10, 100, &sin_to_plot);
            im_chart_end();
        }
        
        im_chart_atb atb = im_chart_atb_new();
        atb.color = color_red;
        atb.lineWeight = 3;
        
        if ( im_chart_begin("Cosine") ) {
            im_chart_plot_func("f", app->cosineRange.x, app->cosineRange.y, 100, &cos_to_plot, atb);
            im_chart_end();
        }
    }
    im_layout_end();
}

internal void application_update(application_memory* app, thread_work_queue* wq)
{
    PROFILED_FUNC;
    
    MemoryCopy(app->processTimes, (app->processTimes + 1), ( ARRAY_COUNT(app->processTimes) - 1 ) * sizeof(f32));
    app->processTimes[ARRAY_COUNT(app->processTimes) - 1] = core->processingTime;
    
    MemoryCopy(app->deltaTimes, (app->deltaTimes + 1), ( ARRAY_COUNT(app->deltaTimes) - 1 ) * sizeof(f32));
    app->deltaTimes[ARRAY_COUNT(app->deltaTimes) - 1] = core->deltaTime;
    
    im_frame_begin();
    
    //- First Window
    if ( im_window_begin("First Window") ) {
        application_imgui_first_window(app);
        im_window_end();
    }
    
    //- Secondary Window
    if ( app->toggleWindow ) {
        if ( im_window_begin("Second Window") ) {
            
            im_layout_begin(IMLayout_Horizontal);
            im_layout_set_widget_width(IMDimensionRule_Fixed, 150);
            for (i32 i = 0; i < DEVTab_Count; i++) {
                bool8 selected = ( app->toggledTab == i );
                if ( im_radio(DEVTabNames[i], selected, selected ? IMWidgetFlag_UseActive : IMWidgetFlag_None) ) {
                    app->toggledTab = i;
                }
            }
            im_layout_end();
            
            im_separator();
            
            switch (app->toggledTab) {
                case DEVTab_InteractDebug:
                {
                    application_imgui_tab_interact_debug(app);
                } break;
                
                case DEVTab_Charts:
                {
                    application_imgui_tab_charts(app);
                } break;
                
                case DEVTab_LoadedTextures:
                {
                    application_imgui_tab_loaded_textures(app);
                } break;
                
                case DEVTab_ThemeEditor:
                {
                    application_imgui_tab_theme_editor(app);
                } break;
            }
            
            im_window_end();
        }
    }
    
    if ( im_window_begin("Third Window") ) {
        
        // Group tests
        if ( im_group_begin("Top level group") ) {
            if ( im_group_begin("2nd level group") ) {
                if ( im_group_begin("3rd level group") ) {
                    im_button("3rd level");
                    im_group_end();
                }
                
                im_button("2nd level");
                im_group_end();
            }
            
            im_button("Top level");
            im_group_end();
        }
        
        im_window_end();
    }
    
    im_frame_end();
}