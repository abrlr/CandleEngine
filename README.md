# Candle Engine

<center><img style="width:100%;" src="docs/banner_large.png" alt="Candle Banner"></center>

Candle is my personnal side projet. It is primarily a platform for me to learn how to implement low-level systems as well as more high-level features for the engine.

README under construction.

## Documents

- [Changelog](docs/CHANGELOG.md)

## Code Style

This project is also a way for me to learn how to structure and organise code to ensure that I am able to expand and maintain the existing code if necessary. 

The first version of Candle was fully written in C++, with most of the core code coming from [this youtube series](https://www.youtube.com/watch?v=JxIZbV_XjAs&list=PLlrATfBNZ98dC-V-N3m0Go4deliWHPFwT) by TheCherno. It is a great video series, and I learned a lot following it. However, at one point I felt stuck when I tried to implement my own features.

Thanks to [Casey Muratori and Handmade Hero](https://handmadehero.org/), I realised that, for me, this was mostly an issue with the code structure and the OOP paradigm itself. After I realised that, I changed my code style and that allowed me to focus more on the actual problem that I am trying to solve, rather than on the architecture of the solution.

Thus the code for this project is written in a "C-flavoured C++" where only some features of C++ are used, mostly:
- function overloading,
- constructor and destructor for code that needs to be executed when a scope is exitted.