@echo off
pushd %~dp0\..\bin

::---------------------------------------------
::
:: Set APP_FILE to the application file path in /application
set APP_FILE=demos/application_demo.cpp
::
::---------------------------------------------

:: set vcvars
::call "C:\Program Files (x86)\Microsoft Visual Studio 14.0\VC\bin\amd64\vcvars64.bat"
call "C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\VC\Auxiliary\Build\vcvars64.bat" > nul

del *.exe > NUL 2> NUL
del *.pdb > NUL 2> NUL
del *.cod > NUL 2> NUL
del *.dll > NUL 2> NUL
del *.lib > NUL 2> NUL
del *.obj > NUL 2> NUL
del *.exp > NUL 2> NUL

:: Common Flags
set baseOpts=-MT -nologo -DEBUG -Od -WX -W4
set ignoredWarn=-wd4311 -wd4302 -wd4201 -wd4505 -wd4100 -wd4189 -wd4273 -wd4005 -wd4244 -wd4305 -wd4996
set compileTimeFilesOpts=-FAsc -Zi

:: Compiler Flags
set includesPath=-I "../application" -I "../src/" -I "../trdp/GLEW/include" -I "../trdp/GLFW/include"
set commonCompilerFlags=-DCDL_DEBUG -DAPPLICATION_INCLUDE=%APP_FILE% %baseOpts% %ignoredWarn% %compileTimeFilesOpts% %includesPath%

:: Linker Flags
set libsPath=-LIBPATH:"../trdp/GLEW/lib" -LIBPATH:"../trdp/GLFW/lib"
set linkedLibs=glew32s.lib glfw3.lib gdi32.lib opengl32.lib user32.lib kernel32.lib msvcrt.lib comdlg32.lib shell32.lib Winmm.lib Xinput.lib /NODEFAULTLIB:LIBCMT
set commonLinkerFlags=-incremental:no -opt:ref %libsPath% %linkedLibs%

echo [COMPILATION] Preprocessor
cl %commonCompilerFlags% -I "../src/" ..\src\meta\preprocessor.cpp /link %commonLinkerFlags%

pushd ..\src
	echo [PREPROCESSOR] Generating Code MetaData
	..\bin\preprocessor.exe
popd

echo [COMPILATION] App DLL
set filenameFlags=/Fe"application.dll" /Fo"application.obj" /Fa"application.cod"
cl %commonCompilerFlags% -DGLEW_STATIC /LD %filenameFlags% ..\src\engine\cdl_engine.cpp /link -LIBPATH:"../trdp/GLEW/lib" glew32s.lib opengl32.lib user32.lib -EXPORT:EngineInit -EXPORT:EngineHotReload -EXPORT:EngineUpdateAndRender -EXPORT:EngineSetPlatform -EXPORT:EngineClean -PDB:engine_%random%.pdb -incremental:no -opt:ref -ignore:4099


echo [COMPILATION] Platform Executable
set filenameFlags=/Fe"candle.exe" /Fo"candle.obj" /Fa"candle.cod"
cl %commonCompilerFlags% %filenameFlags% -DGLEW_STATIC ..\src\main.cpp /link %commonLinkerFlags%

popd