mkdir -p -- "../bin"
cp -r "../assets" "../bin/assets"

pushd ..

#app="-DAPPLICATION_INCLUDE=demos/application_demo.cpp"
#app="-DAPPLICATION_INCLUDE=application.cpp"
app="-DAPPLICATION_INCLUDE=demos/imgui_dev.cpp"
includes="-DCDL_DEBUG_GL -DCDL_DEBUG -DGLEW_STATIC -Iapplication -Isrc -Isrc/engine -Itrdp/GLEW/include -Itrdp/GLFW/include"
opts="-g3 -std=c++17"

echo "[Preprocessor]"
g++ $opts -Wno-writable-strings -Isrc src/meta/preprocessor.cpp -o bin/preprocessor -pthread
pushd ./src/
../bin/preprocessor > cdl_generated.h 2>&1
popd

echo "[Application .so]"
libs="-lGL -lGLEW -pthread -ldl -lX11"
g++ $app $opts $includes -save-temps=obj src/engine/cdl_engine.cpp -o bin/application.so -shared -fPIC $libs

echo "[Platform Executable]"
libs="-Ltrdp/GLFW/lib -lGL -lGLEW -lglfw3 -lX11 -lXxf86vm -lXrandr -pthread -lXi -lGLEW -ldl"
g++ $opts $includes src/main.cpp -o bin/candle $libs

popd