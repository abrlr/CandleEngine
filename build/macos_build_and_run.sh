#!/bin/bash

cd ..

app="-DAPPLICATION_INCLUDE=demos/application_demo.cpp"
includes="-DCDL_DEBUG -Iapplication -Isrc -Isrc/engine -Itrdp/GLEW/include -Itrdp/GLFW/include"
libs="-framework Cocoa -framework OpenGL -Ltrdp/GLEW/lib -lGLEW -Ltrdp/GLFW/lib -lglfw -lpthread"
opts="-g3 -std=c++17"

echo "[Preprocessor]"
g++ $opts -Wno-writable-strings -Isrc src/meta/preprocessor.cpp -o bin/preprocessor
pushd src/
../bin/preprocessor > cdl_generated.h 2>&1
popd

echo "[Application .dylib]"
g++ $app $opts $includes -dynamiclib src/engine/cdl_engine.cpp -o bin/application.dylib -framework OpenGL -Ltrdp/GLEW/lib -lGLEW

echo "[Platform Executable]"
g++ $opts $includes $libs src/main.cpp -o bin/candle

#./bin/app