# Changelog

## Introduction

This is a list of all major changes done to the engine and project, so every changes, especially minor ones, are not necessarily listed here. I try to add images when I can show new tools or capabilities.

The number in front of each section represent the commit's date using the *yymmdd* format: two digits for the year, the month and the day.

## Change List

### 220626: Working Linux Build
- Added working shell script to compile on linux
- Still some bugs that have to be addressed, mostly due to OpenGL.
<center><img style="width:500px;" src="progress/220726_LinuxBuild.png" alt="Linux Build Working"></center>

### 220611: IM Groups
- Added IM Groups to the IMGUI code,
- Similar behaviour as windows (group_begin & group_end should be called),
- Refactored code of the imgui_dev demo.
<center><img style="width:500px;" src="progress/220611_IMGroups.png" alt="IMGUI Groups"></center>

### 220602: Vertical Dockspaces
<center><img style="width:500px;" src="progress/220602_Dockspaces.png" alt="Dockspaces in IMGUI"></center>

### 220601: Pixel Editor Demo Application
- Added basic pixel editor demo,
- Implement custom imgui button.

<center><img style="width:500px;" src="progress/220601_CrudePixelArtApplication.png" alt="Crude Pixel Art Demo Application"></center>

### 220426: IMGUI Rendering
- The IM framework now builds per window draw command buffer then send all draw commands at the end of the IM Frame,
- Introduced to allow the focused window to be rendered always on top, no matter the order of the code,
- Decouples the framework from the main Candle rendering API.

### 220412: Progress on IMGUI
<center><img style="width:500px;" src="progress/220412_ImmediateUIProgress.png" alt="IMGUI Progress"></center>

### 220311: Merged branch imgui into master
- WIP UI framework inspired by [Casey Muratori's](https://www.youtube.com/watch?v=Z1qyvQsjK5Y) talk and the [Dear Imgui](https://github.com/ocornut/imgui) library,
- Will be used for debug systems & editor tools instead of retained UI for quicker setup,
- Example of imgui_dev.cpp:

<center><img style="width:500px;" src="progress/220311_ImmediateUI.png" alt="Immediate UI"></center>

### 220225: Render States + Renderer clean up
- Render states fixes (line thickness & clip rect were not accounted for properly)
- Renderer clean up:
    - UI Lines are now drawn as thin (depending on the thickness) quads, instead of using GL_LINES
    - Removed point batch, points are now draw as quads
- Number of draw calls reduced (e.g. now the engine management window can be rendered using a single draw call).
- Removing the need to call an editor_update function.

### 220217: Scene Views
- [WIP] Added scene views to render a scene from multiple angles,
- Added a model viewer in the demo application to showcase this:

<center><img style="width:500px;" src="progress/220217_CustomSceneViews.png" alt="Custom Scene Views"></center>
<center>Example model viewer, user defined scene rendered on an offsceen framebuffer</center>

- Renderer API clarity, tweaks.

### 220212: Profiler Tab
- The debug ui is now only using the UIWidget API,
- A profiler tab is available in the engine management window,
- Frames can be inspected like in the browser tool (no tooltip yet):

<center><img style="width:500px;" src="progress/220212_ProfilerTab.png" alt="Profiler Tab"></center>

- UI code cleanup,
- GUI Dev Application (demos/gui_dev.cpp):

<center><img style="width:500px;" src="progress/220213_GUIDevApplication.png" alt="GUI Test Application"></center>

### 220125: Asset Inspector & UUIDs
- Reworked cdl_random to use xorshifts ([Comparison of different RNG](https://www.pcg-random.org/))
- Added UUID Generator based for assets, not used yet,
- [WIP] Added an asset inspector in the debug ui (F2):

<center><img style="width:500px;" src="progress/220125_AssetInspector.png" alt="Asset Inspector"></center>

- Texture Target Widget to display renderer & framebuffers textures in the UI,
- Transient Memory Layout is now regularly optimised to free space progressively,
- [WIP] UI Widgets API clarity.

### 220118: Lighting work
- Lights can be dynamicaly changed (color / position ),
- HDR FBOs,
- Gamma correction.

<center><img style="width:500px;" src="progress/220118_Lighting.png" alt="Lights"></center>

### 220115: Snake Demo with 8bit SFX
- Added a snake demo, it uses the audio system for Sound Effects

<center><img style="width:500px;" src="progress/220115_SnakeDemo.gif" alt="Snake Demo"></center>

### 220112: WAV file loading & playing
- WAV file can be loaded (hard restrictions on the format: 48kHz, 16 bits per sample),
- Audio Tracks can be played in engine, one track can have multiple instances of it playing at the same time,
- Track settings (volume, effetcs in the future) can be changed in real time.

### 220107: Profiler Overhaul
- The profiler work now like a graph,
- In Engine: Profiled block list shows the blocks sorted by time,

<center><img style="width:500px;" src="progress/220108_ProfilerList.png" alt="Profiler Inspector List"></center>

- Using F10 with the debug menu open will save the next frame profiler data to disk as a JSON object,
- Created a tool in HTML/JS to visualise the callstack:

<center><img style="width:500px;" src="progress/220107_ProfilerInspector.png" alt="Profiler Inspector Timeline"></center>
<center><img style="width:500px;" src="progress/220107_ProfilerInspector_Close.png" alt="Profiler Inspector Timeline zoomed"></center>

### 220104: Framebuffers, Multisampling & Post processing
- First commit of 2022, Happy New Year !
- Render scenes can now be rendered to offscreen framebuffers (multisampled or not),
- Added a first draft of a post-processing pipeline > little "tree" of effects,
- See the example below, with bloom enabled (bright values of the scene > gaussian blur > added on top of the original scene),

<center><img style="width:500px;" src="progress/220104_Framebuffers.png" alt="Framebuffers & Bloom"></center>

### 211230: Win32 work
- Win32 cleaned up a bit & split in different files,
- File System utilities for iterating over files & folders in a specific folder,
- Doesn't support recursive yet,
- Utility functions to change the window title, toggle fullscreen, request attention,
- Fixed FPS in windowed & fullscreen mode.

### 211228: UI work
- Fixed wrapped text display in Text Area Widgets,
- Added a blinking cursor to text area widgets,
- The draw text function returns the cursor position.

### 211222: Engine work
- Color struct: replaced v4f with 4 uint8 > Struct size divided by 4 = smaller memory footprint:

| Struct         | Previous Size    | Current Size     | Compression |
| -------------- | ---------------- | ---------------- | ----------- |
| Color          | 16 bytes         | 4 bytes          | 75%         |
| Batch Vertex   | 68 bytes         | 56 bytes         | 17.6%       |
| Triangle Batch | 2 588 720 bytes  | 2 195 504 bytes  | 15.2%       |
| Render Scene   | 10 623 248 bytes | 9 001 232 bytes  | 15.3%       |
| Renderer       | 42 494 856 bytes | 36 006 792 bytes | 15.3%       |

- OSX compatibility tweaks, world geometry still doesn't render (see [#1](https://gitlab.com/abrlr/CandleEngine/-/issues/1)),
- Support for nested widget groups:

<center><img style="width:500px;" src="progress/211222_NestedGroups.gif" alt="Nested Groups"></center>

### 211220: UI Circles & Disks
- Added smooth circle & disks rendering in the UI shader,

<center><img style="width:500px;" src="progress/211220_UICircles.png" alt="UI Circles"></center>

- Radio Buttons now use round icons to differentiate from toggles.

### 211219: Multiple Materials
- Support for use of multiple materials/shaders for world geometry,

<center><img style="width:500px;" src="progress/211219_MutlipleMaterials.png" alt="Multiple World Materials"></center>

- Renderer API consistency work.

### 211215: Specular Lighting
- Added specular lighting rendering to models,
- View Matrix computations are now done by the renderer instead of the application,

<center><img style="width:500px;" src="progress/211215_SpecularLighting.png" alt="Basic Specular Lighting"></center> 

### 211214: Bézier Curves & Renderer Adjustements
- Added computations & rendering support for linear, quadratic & cubic Bézier curves, both in 2D & 3D,

<center><img style="width:500px;" src="progress/211214-BezierCurves.gif" alt="Bézier Curves"></center>

- UI rendering now use float vectors instead of int vectors as it was the cause of bad quality/jagged curve rendering,
- Added basic wireframe support using RenderStateVars (instanced models & static geometry don't use these),
- Added two demo files: application_demo.cpp & editor_demo.cpp, showcasing some of the engine features & how to,
- Instanced model render commands with an unavailable model now uses a default cube instead of nothing.

<center><img style="width:500px;" src="progress/211214_Demos.png" alt="Demo application with default cube"></center>

### 211209: UI Icons
- Added UI Icons support using SDF (Signed Distance Field) for all-resolution support (same technique as text rendering),

<center><img style="width:200px;" src="progress/211209_UIIcons.png" alt="UI Icons at multiple resolutions"></center>

- UI Booleans & Radio Buttons now have icons indicating their state for comprehensive visual feedback,

<center><img style="width:200px;" src="progress/211209_UIToggles.png" alt="UI Icons with Boolean Widgets"></center>

- Additionnal work is required as the picture above is a best case scenario, extreme cases will easily break everything,
- Minor fixes & improvements in assets workflows & texture loading.

### 211208: Rounded Quads are BACK !
- Finally fixed issue [#2](https://gitlab.com/abrlr/dod-engine/-/issues/2),
- The fix uses bit shifting & masking techniques to pack more information in the vertex data.

<center><img style="width:200px;" src="progress/211208_RoundedQuads.png" alt="Rounded Quads"></center>

### 211207: Resizable UI Panels
- Basic support for resizing UI Panels using the appropriate flag,
- Changes aren't kept during a window resize event yet,
- UIWidgets with a relative size are resized properly with their parent panel.

<center><img style="width:500px;" src="progress/211207_UIPanelTransforms.gif" alt="Text Style Bold"></center>

### 211206: Bold Text
- Rewrote some text rendering code for more flexibility,
- Added support for font weight (values set for a weight from 1 to 10, see below).

<center><img style="width:500px;" src="progress/211206_TextStyleBold.gif" alt="Text Style Bold"></center>

### 211205: Win32 Direct Sound
- Basic audio support for Windows using Direct Sound,
- Only outputs a sine wave at the moment.

### 211202: Scenes Rendering
- Huge rework of Rendering API,
- The renderer now works with a "scene" concept:
    - Scenes contains render commands data,
    - Scenes are rendered at the end of the frame using their viewport & cameras,
    - Only 4 hard coded right now, hopefully this will enable a more flexible system later,
    - Scenes are drawn on top of each other in this order (temporary):
        1. Application World,
        2. Debug World (debug geometry, lines, etc..),
        3. Application UI,
        4. Debug UI (engine_debug.cpp for example).

<center><img style="width:500px;" src="progress/211202_DebugGeometry.png" alt="Multiple Viewports"></center>
<center>Example of debug geometry only shown in the Editor viewport (the red lines are in world space, compare with previous screenshot below)</center>

### 211113: Split Screen / Multiple Viewports
- WIP: Added the possibility to render more than one viewport using different cameras, API is buggy and not finished,
- Enabled only for 3D rendering, UI rendering is still fullscreen atm.

<center><img style="width:500px;" src="progress/211113_EditorViewport.png" alt="Multiple Viewports"></center>

### 211111: Points, Color Picker, Enhanced Profiler & Images
- Added world & UI point rendering (mainly for debug usage),

<center><img style="width:200px;" src="progress/211111_Points.png" alt="Point Rendering"></center>

- Added a basic color picker widget, only output shades of red right now,
- UI Shader supports color picker display (uniform quad color interpolation instead of OpenGL triangle interpolation),

<center><img style="width:200px" src="progress/211111_ColorPicker.png" alt="Color Picker"></center>

- Debug Frame Profiler can now display different levels of functions (0 to 3, change using arrow keys),

<div style="display:flex; justify-content:space-between">
<img src="progress/211111_FrameProfiler_Depth2.png" alt="Frame Profiler Depth 2"><img src="progress/211111_FrameProfiler_Depth3.png" alt="Frame Profiler Depth 2">
</div>

- Added maths & color utility functions,
- Widgets draw rects are now only computed when needed (rect changed, window resize) instead of every frame,
- Started adding images to the README, will do it also for previous commits.

### 211110: UI & Render work
- Changes in UI & Widgets update mechanism > allow multiple UI (app, editor, debug),
- Example use of UI widgets in engine_debug,
- RenderState bug corrections (push/pop didn't behave correctly),
- WIP: working on multithreading support for the renderer (see cdl_renderer_push_world_quad), works almost reliably.

### 211107: Generalised UI State to Render State
- UI States are now used in the world too (multiple line width, clip rects, etc..),
- Updated renderer code to support Render States,
- Fixed empty draw calls that were dispatched to the GPU,
- Basic support for multiple lights in the world shader, WIP still.

### 211027: Text Area Widget & Text Rendering
- Added a TextArea Widget that supports keyboard text input,
- Merged text rendering with the UI rendering, less drawcalls & context changes, less memory footprint.

### 211025: Shader Assets & Model Instances
- Shader are now separated in two parts, like textures, fonts, etc..
- First a shader asset has to be created, then the renderer use this asset to load the shader to the gpu,
- Models can now be displayed and instanced in the world,
- Model instance lists can be static (flushed by the user) or dynamic (flushed on each frame),
- Memory Pools are renamed Memory Blocks since the behaviour was not identical to the usual pool definition,
- Engine shader are in their own .shader file, this allows a hot reloading capability in the future.

### 211018: UI State
- Removed the render queries & render pass concept for now,
- Added a render_ui_state to use clipping rectangle, custom viewports, etc.. 
- UIWidgets are compatible with this

### 211017: Metaprogramming
- preprocessor.cpp: Compiled & executed before the engine & application compile time, generate type introspection data,
- Will be usefull when working on serialisation systems to serialise complex structs,
- Work in the asset system: ressources are loaded when necessary, difference between asset creation, requested & loaded ( not thread safe )

### 211016: UIWidgets basic framework working
- Existing constraints & layouts are now working really well, they are respected when resizing the app window,
- At the moment, UI interfaces are built one, then the system only updates what needs to be updated depending on the user inputs,
- Added callback functions for start & end of widget interactions, will add more in the future for buttons, sliders, etc..

### 211015: More UIWidgets
- Different widget types,
- Added constraints & layouts for the widgets,
- Optimisation: Increased size for triangles batches, removed MemoryZeros at each begin_frame that were taking quite some time

### 211014: Point Light & UIWidgets
- Basic point lighting, defined in shader (if no normal defined, lighting is not used),
- Started work on in game/editor UI, will have a similar structure as the debug variables,
- Started work on a more refined renderer design using render passes & render queries.

### 211012: OBJ Loader & String utilities
- Finished function to load OBJs into raw_model assets,
- Added string utility functions for string splits, reading numbers, etc

### 210930: Debug views & variables
- Reworked the debug variables rendering & interactions
- Removed completely the cdl_imgui_* functions since they are now obsolete
- Moved debug variables from editor.cpp to cdl_engine_debug.h
- Moved all the debug views from cdl_engine.cpp to cdl_engine_debug.cpp
- cdl_engine.cpp is now much cleaner

### 210929: Random, Assets & Memory Management
- cdl_random.h: provide utilities for random number generation
- cdl_assets.h & .cpp: Asset system first draft, textures are uploaded to the gpu when needed
- cdl_base.h: Introduced "allocators" for memory pools, pools allow tracking of the allocated memory chunk
- Various clean-ups & renaming
- Will likely do a dedicated commit for code formatting & harmonization

### 210926: Maths & Batch Rendering
- Matrix Maths: Inverse & Mult by a f32 factor
- Some vector functions have been implemented
- Maths functions are now taking input vectors by value rather than by reference
- The batch rendering system has been reworked a bit to allow tracking of the models
- World geometry is now separated in two batches: static and dynamic

### 210922: Threads & Profiler II
- Apple version of cdl_threads.h & cdl_profiler.h, should work on linux (POSIX compatible)
- Updated macos_main.cpp to initialise the thread pool

### 210730: Threads, Profiler & Stuff
- cdl_threads.h: thread lib, allow to create a work queue and add work entries to it
- Example of MT in demos/geometry.cpp (currently doesn't render properly because render commands aren't thread safe)
- Profiler rewritten to minimise its impact on the CPU usage & cache when used
- Started decoupling interactions & rendering in UI code
- Last commit before vacations

### 210726: Triangles & Pools
- Added support for triangles & models in the world renderer
- Added a memory pool struct for dynamic allocations
- Started working on IMGUI dropdowns
- IMGUI will soon be refactored to separate interactions from rendering
- Some code renaming

### 210719: Few tweaks & Block Profiler
- Added cdl_profiler.h > block profiler for the engine to have a clear understanding of its performances
- Files in application/ are now two boiler plates to use the engine
- Fixed font rendering code that was miscomputing the text dimensions

### 210718: IMGUI Enhancement & Bug Fixes
- Fixes: [#5](https://gitlab.com/abrlr/dod-engine/-/issues/5), [#4](https://gitlab.com/abrlr/dod-engine/-/issues/4) & [#3](https://gitlab.com/abrlr/dod-engine/-/issues/3)
- Refactoring of IMGUI internal behaviour
- Added IMGUI Widgets
- Added support for smooth pixel art filtering in the world shader

### 210716: Batch Rendering Complete
- Batch rendering divided into two categories: World/UI
- Supports Textured & Gradient Quads, Lines, Glyphs
- Triangle support will be added next
- Added support for perspective cameras
- Removed unnecessary shaders
- Implemented some matrix maths & fixed translate method
- Merged cdl_core.h & cdl_types.h into cdl_base.h
- Added 4Coder project file

### 210710: Basic Batch Rendering & Debug Console
- Batch rendering for untextured quads, glyphs & line rendering
- Simplified shader accessing and creation
- If CDL_CONSOLE_ENABLE is defined, a cdl_console captures all log messages
- Rendering the console in frame allows for easier live debugging
- Added text to some widgets
- Clean up

### 210708: Text Rendering API
- Moved API into Renderer code
- Added anchor flags to position text
- Movd changelog.txt into readme.md

### 210704: Basic text rendering
- Text format: .fnt file + .png texture
- Text rendering: using signed distance field to allow for scalabe glyphs
- .fnt file reading
- Basic text rendering added
- Added a GUI handle
- Added /data folder for ressources files used

### 210627: Work on UI elements, rendering & filesystem
- Color themes support
- Clean up
- Clean up
- Rounded corners shader
- Texture loading
- Added file explorer capabilities for windows platform (open & save file)

### 210626-02: Started code clean up & some work on renderer
- Merged files in maths/ into cdl_maths.h [header only]
- Merged log.h & log.cpp into cdl_log.h [header only]
- Removed gRenderer as GUI functions argument
- Added GLOBAL_UNIFORMS to shaders (time, resolution, etc...)
- Added a rainbow shader

### 210626-01:
- Maths functions renaming
- Variations for UIPanel and UIButtons

### 210625-02:
- Basic UI elements: UIPanel, IMGUIWindow, IMGUIHead & IMGUIPanel
- moved functions from core to maths
- Alpha blending for UI panels
