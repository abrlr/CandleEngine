let range = 0;
let originalStart = Infinity;
let originalEnd = -1;

let start = Infinity;
let end = -1;
let palette = new Map();

let elementHeight = 15;
let leftRightMargin = 50;

function getRandomColor() {
    let letters = '0123456789ABCDEF';
    let color = '#';
    for (let i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}

function getWidth() {
    return Math.max(
      document.body.scrollWidth,
      document.documentElement.scrollWidth,
      document.body.offsetWidth,
      document.documentElement.offsetWidth,
      document.documentElement.clientWidth
    );
}

function FirstTimeline() {
    originalStart = frame.blocks[0].start;
    for (let i = 0; i < frame.blocks.length; i++) {
        const block = frame.blocks[i];
        if ( block.end > originalEnd ) originalEnd = block.end;

        if ( !palette.has(block.name)) {
            palette.set(block.name, getRandomColor());
        }
    }

    start = originalStart;
    end = originalEnd;

    range = end - start;
    CreateTimeline();
}

function CreateTimeline() {
    document.getElementById('timeline').innerHTML = '';
    let pixelPerCounter = ( end - start ) / ( document.documentElement.clientWidth - leftRightMargin * 2 );
    
    // Build the scale
    let yOffsetForScale = 100;
    {
        let hmtlBlock = document.createElement('div');
        hmtlBlock.innerHTML = (end - start) / 10000 + 'ms';
        hmtlBlock.id = 'scale';
        hmtlBlock.style.backgroundColor = 'rgb(199, 255, 238)';
        hmtlBlock.style.top = yOffsetForScale + 'px';
        hmtlBlock.style.left = leftRightMargin + 'px';
        hmtlBlock.style.width = ( end - start ) / pixelPerCounter + 'px';
        hmtlBlock.style.height = elementHeight + 'px';
        document.getElementById('timeline').appendChild(hmtlBlock);
    }

    // Loop on the block depth ( display the blocks in a callstack fashion )
    let yOffsetForTimeline = yOffsetForScale + elementHeight / 2;
    for (let depth = -1; depth < 50; depth++) {
        let yoffset = yOffsetForTimeline + ( depth + 1 ) * (elementHeight + 5);

        // Loop over all blocks
        for (let i = 0; i < frame.blocks.length; i++) {
            const block = frame.blocks[i];

            // If the current block is at the drawn depth
            if ( block.depth == depth ) {
                let hmtlBlock = document.createElement('div');
                hmtlBlock.setAttribute('data-index', i);
                hmtlBlock.className = 'block';
                hmtlBlock.id = 'block-' + i;
                if ( block.name.substring(0, 2) == 'gl' ) {
                    hmtlBlock.style.backgroundColor = "red";
                } else {
                    hmtlBlock.style.backgroundColor = palette.get(block.name);
                }
                hmtlBlock.style.top = yoffset + 'px';
                hmtlBlock.style.left = leftRightMargin + ( block.start - start ) / pixelPerCounter + 'px';
                hmtlBlock.style.width = ( block.end - block.start ) / pixelPerCounter + 'px';
                hmtlBlock.style.height = elementHeight + 'px';
                hmtlBlock.addEventListener('mouseover', function(event) {
                    const index = event.target.getAttribute('data-index');
                    document.getElementById('infos').innerHTML = 
                    `File: ${frame.blocks[index].file}<br>
                    Name: ${frame.blocks[index].name}<br>
                    Time: ${frame.blocks[index].time / 1000000.0} ms`;
                });
                document.getElementById('timeline').appendChild(hmtlBlock);
            }
        }
    }
}

function UpdateTimeline(str) {
    if ( str == 'frameBegin' ) {
        start = document.getElementById('frameBegin').value * range / 100 + originalStart;
    }

    else if ( str == 'frameEnd' ) {
        end = document.getElementById('frameEnd').value * range / 100  + originalStart;
    }
    
    let pixelPerCounter = ( end - start ) / ( document.documentElement.clientWidth - leftRightMargin * 2 );
    for (let i = 0; i < frame.blocks.length; i++) {
        const block = frame.blocks[i];
        let hmtlBlock = document.getElementById('block-' + i);
        hmtlBlock.style.left = leftRightMargin + ( block.start - start ) / pixelPerCounter + 'px';
        hmtlBlock.style.width = ( block.end - block.start ) / pixelPerCounter + 'px';
    }

    let timeInMS = ( end - start ) / 10000;
    document.getElementById('scale').innerHTML = timeInMS.toPrecision(3) + 'ms';
}

FirstTimeline();